Vue.component('glosario', {
	props : ['type'],
	template : `
	<div>
		<p v-if="type == 'Abstracción de datos'"><strong>Abstracción de datos</strong>: Consiste en tratar los datos en términos de su descripción lógica. Fue creada para separar la visión que tiene la computadora sobre los datos, de la percibida por el ser humano.</p>
		<p v-if="type == 'Apilar'"><strong>Apilar</strong>: Consiste en incluir un nuevo elemento en una pila.</p>
		<p v-if="type == 'Apuntador'"><strong>Apuntador</strong>: Campo de un nodo que contiene  la dirección de memoria del siguiente nodo si éste existe.</p>
		<p v-if="type == 'Arreglo unidimensional'"><strong>Arreglo unidimensional</strong>: Se define como una colección finita, homogénea y ordenada de elementos.</p>
		<p v-if="type == 'Arreglo anidado'"><strong>Arreglo anidado</strong>: Es aquel donde cada elemento del arreglo tiene otro arreglo.</p>
		<p v-if="type == 'Arreglo bidimensional o matriz'"><strong>Arreglo bidimensional o matriz</strong>: Es un conjunto de datos homogéneo, finito y ordenado, donde se hace referencia a cada elemento por medio de dos índices.</p>
		<p v-if="type == 'Arreglos de listas'"><strong>Arreglos de listas</strong>: Son  estructuras de datos estáticas que poseen en su campo de información uno o más campos  apuntadores a  estructuras de datos dinámicas (Listas).</p>
		<p v-if="type == 'Arreglos de registros'"><strong>Arreglos de registros</strong>: Son arreglos donde cada elemento será un registro (todos los componentes del arreglo serán del mismo tipo de registro).</p>
		<p v-if="type == 'Arreglo multidimensional'"><strong>Arreglo multidimensional</strong>: Es una colección de K1 x K2 x ... x KN elementos. Para hacer referencia a cada componente de un arreglo de N dimensiones, se usarán N índices (uno para cada dimensión).</p>
		<p v-if="type == 'Arreglo unidimensional'"><strong>Arreglo unidimensional</strong>: Es un tipo de datos estructurado que está formado de una colección finita y ordenada de datos del mismo tipo.Es la estructura natural para modelar listas de elementos iguales.</p>
		<p v-if="type == 'Arreglos paralelos'"><strong>Arreglos paralelos</strong>: Son dos o más arreglos que utilizan el mismo subíndice para referirse a términos homólogos.</p>
		<p v-if="type == 'Campo de enlace'"><strong>Campo de enlace</strong>: Ver apuntador.</p>
		<p v-if="type == 'Campo de información'"><strong>Campo de información</strong>: Campo de un nodo que contiene información almacenada con un fin.</p>
		<p v-if="type == 'Cola'"><strong>Cola</strong>: Es una lista ordenada de elementos, en la cual las eliminaciones se realizan en un solo extremo, llamado frente o principio de la cola, y los nuevos elementos son añadidos por el otro extremo, llamado fondo o final de la cola.</p>
		<p v-if="type == 'Cola circular'"><strong>Cola circular</strong>: Es aquella en la cual existe un índice desde el último elemento al primero de la cola.</p>
		<p v-if="type == 'Cola de prioridades'"><strong>Cola de prioridades</strong>: Es aquella donde no se atienden a los elementos únicamente aplicando el concepto de cola,  sino  que cada  uno tiene asociada  una  prioridad basada en un criterio objetivo.</p>
		<p v-if="type == 'Colisión'"><strong>Colisión</strong>: Es cuando la misma dirección es utilizada para dos claves diferentes.</p>
		<p v-if="type == 'Columna'"><strong>Columna</strong>: Representa la primera dimensión de una matriz.</p>
		<p v-if="type == 'Componentes'"><strong>Componentes</strong>: Son aquellos que hacen referencia a los elementos que forman el arreglo.</p>
		<p v-if="type == 'Datos'"><strong>Datos</strong>: Son los nombres del mundo de la programación: los objetos que son manipulados.</p>
		<p v-if="type == 'Datos estructurados'"><strong>Datos estructurados</strong>: Son aquellos que se caracterizan por el hecho de que con un nombre se hace referencia a un grupo de casillas de memoria.</p>
		<p v-if="type == 'Datos simples'"><strong>Datos simples</strong>: Son aquellos que ocupan sólo una casilla de memoria, por lo tanto una variable simple hace referencia a un único valor a la vez.</p>
		<p v-if="type == 'Desapilar'"><strong>Desapilar</strong>: Consiste en sacar un elemento de una pila.</p>
		<p v-if="type == 'Dirección'"><strong>Dirección</strong>:  Es un número que identifica cada una de las celdas de la memoria de una computadora.</p>
		<p v-if="type == 'Doble cola o bicola'"><strong>Doble cola o bicola</strong>: Es una generalización de una estructura de cola simple, los elementos pueden ser insertados o eliminados por cualquiera de los extremos, es en realidad, una cola bidireccional.</p>
		<p v-if="type == 'Doble cola con entrada restringida'"><strong>Doble cola con entrada restringida</strong>: Es aquella que permite que las eliminaciones puedan hacerse por cualquiera de los dos extremos, mientras que las inserciones sólo por el final de la cola.</p>
		<p v-if="type == 'Doble cola con salida restringida'"><strong>Doble cola con salida restringida</strong>: Es aquella que permite que las inserciones puedan hacerse por cualquiera de los dos extremos, mientras que las eliminaciones sólo por el frente de la cola.</p>
		<p v-if="type == 'Doble dirección hash'"><strong>Doble dirección hash</strong>: Consiste en que una vez detectada la colisión se debe generar otra dirección aplicando la función hash a la dirección previamente obtenida.</p>
		<p v-if="type == 'Encadenamiento'"><strong>Encadenamiento</strong>: Consiste en que cada elemento del arreglo tenga un apuntador a una lista enlazada, la cual se irá generando e irá almacenando los valores colisionados a medida que se requiera.</p>
		<p v-if="type == 'Encapsulamiento de datos'"><strong>Encapsulamiento de datos</strong>: Puede definirse como la separación de la representación de los datos de las aplicaciones que utilizan los datos a nivel lógico.</p>
		<p v-if="type == 'Estructura de datos'"><strong>Estructura de datos</strong>: Es la forma práctica de almacenar datos del mismo tipo en la memoria interna o central de las computadoras.</p>
		<p v-if="type == 'Estructuras de datos dinámicas (avanzadas)'"><strong>Estructuras de datos dinámicas (avanzadas)</strong>: Son aquellas que tienen capacidad de variar en tamaño y ocupar tanta memoria como utilicen realmente, es decir, se amplían  o reducen a medida que se requiera durante la ejecución,  cambiando sus posiciones de memoria asociada.</p>
		<p v-if="type == 'Estructuras de datos estáticas (fundamentales o básicas)'"><strong>Estructuras de datos estáticas (fundamentales o básicas)</strong>: Son  aquellas a la que se asigna una cantidad fija de memoria antes de la ejecución del programa (durante la compilación).</p>
		<p v-if="type == 'Estructuras de datos lineales'"><strong>Estructuras de datos lineales</strong>: Son aquellas en que cada elemento tiene máximo dos elementos adyacentes (posterior y/o anterior).</p>
		<p v-if="type == 'Estructuras de datos no lineales'"><strong>Estructuras de datos no lineales</strong>: Son aquellas cuyos elementos pueden tener más de 2 adyacentes, a los que pueden acceder directamente (no tiene sentido el concepto de anterior/siguiente), entre las cuales se tienen Árboles y Grafos.</p>
		<p v-if="type == 'Fila'"><strong>Fila</strong>: Representa la segunda dimensión de una matriz.</p>
		<p v-if="type == 'FIFO'"><strong>FIFO</strong>:  First-In, First-Out -  primero en entrar, primero en salir.</p>
		<p v-if="type == 'Función cuadrado'"><strong>Función cuadrado</strong>: Consiste en elevar al cuadrado la clave y tomar los dígitos centrales como dirección.</p>
		<p v-if="type == 'Función módulo (por división)'"><strong>Función módulo (por división)</strong>: Consiste en tomar el residuo de la división de la clave entre el número de componentes del arreglo.</p>
		<p v-if="type == 'Función plegamiento'"><strong>Función plegamiento</strong>: Consiste en dividir la clave en partes de igual número de dígitos (la última puede  tener menos dígitos) y operar con ellas, tomando como dirección los dígitos menos significativos.</p>
		<p v-if="type == 'Función truncamiento'"><strong>Función truncamiento</strong>: Consiste en tomar algunos dígitos de la clave y formar con ellos una dirección.</p>
		<p v-if="type == 'Gestión de memoria'"><strong>Gestión de memoria</strong>:  Supone la administración del recurso limitado de la memoria por medio del empleo de distintas estrategias.</p>
		<p v-if="type == 'Identificador de campo'"><strong>Identificador de campo</strong>: Nombre único que Identifica cada uno de los campos de un registro.</p>
		<p v-if="type == 'Indices'"><strong>Indices</strong>: Son aquellos que especifican cuántos elementos tendrá el arreglo y además de qué modo podrán accesarse esos componentes.</p>
		<p v-if="type == 'LIFO'"><strong>LIFO</strong>:  Last In, First Out – ultimo en entrar, primero en salir.</p>
		<p v-if="type == 'Listas auto-organizadas'"><strong>Listas auto-organizadas</strong>: Consisten en reordenar los elementos de las listas conforme a reglas heurísticas que pretenden colocar los elementos que son más frecuentemente solicitados hacia el inicio de la lista.</p>
		<p v-if="type == 'Lista circular'"><strong>Lista circular</strong>: Esta estructura conocida también como anillo, es una subclase de una lista simple consistente en enlazar el último elemento de la lista con el primero.</p>
		<p v-if="type == 'Lista con nodo de cabecera'"><strong>Lista con nodo de cabecera</strong>: Es aquella en la que el primer nodo de la lista contendrá en su campo dato algún valor que lo diferencie de los demás nodos.</p>
		<p v-if="type == 'Listas con saltos'"><strong>Listas con saltos</strong>: Son aquellas en las que se mejoran el tiempo de búsqueda añadiendo punteros adicionales a ciertos nodos de la misma.</p>
		<p v-if="type == 'Listas de arreglos'"><strong>Listas de arreglos</strong>: Son listas enlazadas en la que uno o más de sus campos está conformado por un arreglo.</p>
		<p v-if="type == 'Listas de registros'"><strong>Listas de registros</strong>: Son listas enlazadas en la que uno o más de sus campos está conformado por un registro.</p>
		<p v-if="type == 'Lista doble'"><strong>Lista doble</strong>: Una lista doble o doblemente enlazada es una colección de nodos en la cual cada nodo tiene dos punteros, uno de ellos apuntando a su predecesor (li) y otro a su sucesor (ld).</p>
		<p v-if="type == 'Lista doblemente enlazada circular'"><strong>Lista doblemente enlazada circular</strong>: Es un tipo de lista doble, el  puntero izquierdo del primer nodo apunta al último nodo de la lista, y el puntero derecho del último nodo apunta al primer nodo de la lista.</p>
		<p v-if="type == 'Lista enlazada'"><strong>Lista enlazada</strong>: Es una secuencia de nodos en el que cada uno de ellos está enlazado o conectado con el siguiente.</p>
		<p v-if="type == 'Lista enlazada como un arreglo sencillo'"><strong>Lista enlazada como un arreglo sencillo</strong>: Es aquella en la que cada nodo de la lista constando de un de registro (al menos) dos campos, Info y Siguiente.</p>
		<p v-if="type == 'Lista heterogénea'"><strong>Lista heterogénea</strong>: Es aquella  que almacena distintos tipos de elementos.</p>
		<p v-if="type == 'Lista homogénea'"><strong>Lista homogénea</strong>: Es  aquella en la que todos los elementos almacenados son del mismo tipo.</p>
		<p v-if="type == 'Listas simples'"><strong>Listas simples</strong>: Son aquellas que tienen un primer y un último elemento, existiendo una relación estructural entre sus diferentes elementos; normalmente cada elemento está relacionado con el anterior y el siguiente, excepto el primero (sólo con el segundo) y el último (sólo con el anterior).</p>
		<p v-if="type == 'Listas ortogonales'"><strong>Listas ortogonales</strong>: Se utilizan para representar matrices, ya que los nodos contienen cuatro apuntadores. Uno para apuntar al nodo izquierdo (li),otro para apuntar al derecho(ld), otro al nodo inferior(lb) y por último un apuntador al nodo superior(la).</p>
		<p v-if="type == 'Lista vacía'"><strong>Lista vacía</strong>: Es aquella que no tiene nodos.</p>
		<p v-if="type == 'Matriz'"><strong>Matriz</strong>: Es un término matemático, utilizado para definir un conjunto de elementos organizados por medio de renglones y columnas.</p>
		<p v-if="type == 'Matriz antisimétrica'"><strong>Matriz antisimétrica</strong>: Es aquella matriz de n x n elementos donde A[i, j] es igual a -A[j, i], y esto se cumple para todo i y para todo j; considerando a i ¹ j.</p>
		<p v-if="type == 'Matriz cuadrada'"><strong>Matriz cuadrada</strong>: Es aquella que tiene igual número de renglones y de columnas.</p>
		<p v-if="type == 'Matriz poco densa'"><strong>Matriz poco densa</strong>: Es aquella matriz que contiene una proporción muy alta de ceros.</p>
		<p v-if="type == 'Matriz simétrica'"><strong>Matriz simétrica</strong>: Es aquella matriz de n x n elementos donde A[i,j] es igual a A[j,i], y esto se cumple para todo i y para todo j.</p>
		<p v-if="type == 'Matriz triangular'"><strong>Matriz triangular</strong>: Es aquella matriz cuadrada en la que los elementos que se encuentran arriba o debajo de la diagonal principal son iguales a cero.</p>
		<p v-if="type == 'Matriz triangular inferior'"><strong>Matriz triangular inferior</strong>: Es aquella donde los elementos iguales a cero se encuentran sobre la diagonal principal.</p>
		<p v-if="type == 'Matriz triangular superior'"><strong>Matriz triangular superior</strong>: Es aquella donde los elementos iguales a cero se encuentran debajo de la diagonal principal.</p>
		<p v-if="type == 'Matriz tridiagonal'"><strong>Matriz tridiagonal</strong>: Es aquella donde los elementos distintos de cero se encuentran en la diagonal principal o en las diagonales por encima o debajo de ésta.</p>
		<p v-if="type == 'Memoria'"><strong>Memoria</strong>: Se puede representar como un conjunto de “celdas” numeradas que contienen un valor o dato.</p>
		<p v-if="type == 'Memoria principal'"><strong>Memoria principal</strong>: Se puede definir como un arreglo de celdas de memoria, cada una de los cuales puede llevar a cabo un solo byte de información.</p>
		<p v-if="type == 'Memoria secundaria'"><strong>Memoria secundaria</strong>: Es una memoria de acceso mucho más lento que la Memoria Principal, es de costo inferior y el almacenamiento de información es permanente e ilimitado.</p>
		<p v-if="type == 'Método de búsqueda'"><strong>Método de búsqueda</strong>: Es aquel que permite recuperar datos  previamente  almacenados.</p>
		<p v-if="type == 'Método de búsqueda secuencial o lineal'"><strong>Método de búsqueda secuencial o lineal</strong>: Consiste en revisar elemento por elemento hasta encontrar el dato buscado, o hasta llegar al final de la lista de datos disponibles.</p>
		<p v-if="type == 'Método de búsqueda binaria'"><strong>Método de búsqueda binaria</strong>: Consiste en dividir el intervalo de búsqueda en dos partes, comparando el elemento buscado con el central.</p>
		<p v-if="type == 'Método de búsqueda por transformación de claves (hash)'"><strong>Método de búsqueda por transformación de claves (hash)</strong>: Es aquel que permite aumentar la velocidad de búsqueda sin necesidad de tener los elementos ordenados.</p>
		<p v-if="type == 'Método de ordenación'"><strong>Método de ordenación</strong>: Es aquel que permite ordenar los elementos de un arreglo.</p>
		<p v-if="type == 'Método de ordenación directo'"><strong>Método de ordenación directo</strong>: Es aquel que tiene la característica de que sus programas son cortos y de fácil elaboración y comprensión, aunque es ineficiente cuando el número de elementos del arreglo es medio o grande.</p>
		<p v-if="type == 'Método de ordenación logarítmico'"><strong>Método de ordenación logarítmico</strong>: Es aquel que requiere de menos comparaciones y movimientos para ordenar sus elementos que el método de ordenación directo, pero su elaboración y comprensión resulta más sofisticada y abstracta.</p>
		<p v-if="type == 'Método de ordenación por intercambio directo (Burbuja)'"><strong>Método de ordenación por intercambio directo (Burbuja)</strong>: Consiste en comparar pares de elementos adyacentes e intercambiarlos entre sí hasta que todos se encuentren ordenados.</p>
		<p v-if="type == 'Método de ordenación por intercambio directo con señal'"><strong>Método de ordenación por intercambio directo con señal</strong>: Este método es una modificación del método de intercambio directo.La idea central consiste en utilizar una marca o señal para indicar que no se ha producido ningún intercambio en una pasada. Es decir, se comprueba si el arreglo está totalmente ordenado después de cada pasada, terminando su ejecución en caso afirmativo.</p>
		<p v-if="type == 'Método de ordenación por shaker sort (sacudida)'"><strong>Método de ordenación por shaker sort (sacudida)</strong>: Es una optimización del método de intercambio directo, consiste en mezclar las dos formas en que se puede realizar el método de la burbuja.</p>
		<p v-if="type == 'Método de ordenación por inserción binaria'"><strong>Método de ordenación por inserción binaria</strong>: Es aquel que utiliza una búsqueda binaria para insertar un elemento en la parte izquierda del arreglo, que ya se encuentra ordenado.</p>
		<p v-if="type == 'Método de ordenación por inserción directa (Baraja)'"><strong>Método de ordenación por inserción directa (Baraja)</strong>: Consiste en insertar un elemento del arreglo en la parte izquierda del mismo, que ya se encuentra ordenada. Este proceso se repite desde el segundo hasta el  n-ésimo elemento.</p>
		<p v-if="type == 'Método de ordenación por shell'"><strong>Método de ordenación por shell</strong>: Es una versión mejorada del método de inserción directa. Consiste en  comparar cada elemento para su ubicación correcta en el arreglo, con los elementos que se encuentran en la parte izquierda del mismo. Si el elemento a insertar es más pequeño que el grupo de elementos que se encuentran a su izquierda, es necesario efectuar entonces varias comparaciones antes de su ubicación.</p>
		<p v-if="type == 'Método de ordenación por selección directa'"><strong>Método de ordenación por selección directa</strong>: Consiste en buscar el menor elemento del arreglo y colocarlo en la primera posición.</p>
		<p v-if="type == 'Método de ordenación quicksort'"><strong>Método de ordenación quicksort</strong>: Es una mejora sustancial del método de intercambio directo y recibe este nombre por la velocidad con que ordena los elementos del arreglo.</p>
		<p v-if="type == 'Nodo'"><strong>Nodo</strong>: Es una secuencia de caracteres en memoria dividida en campos (de cualquier tipo).</p>
		<p v-if="type == 'Nodo cabecera'"><strong>Nodo cabecera</strong>: Es un nodo que siempre estará al comienzo de una lista usado para simplificar el procesamiento de la lista y/o para contener información sobre la lista.</p>
		<p v-if="type == 'Nodo final'"><strong>Nodo final</strong>: Es un nodo que siempre estará al final de una lista usado para simplificar el procesamiento de la misma.</p>
		<p v-if="type == 'Nulo'"><strong>Nulo</strong>: Valor que indica que no se esta apuntado ningún nodo.</p>
		<p v-if="type == 'Ordenar'"><strong>Ordenar</strong>: Significa permutar estos elementos de tal forma que los mismos queden de acuerdo con un orden preestablecido.</p>
		<p v-if="type == 'Overflow'"><strong>Overflow</strong>: Es un error que se produce si la pila estuviera llena y se intentara insertar un nuevo valor.</p>
		<p v-if="type == 'Pila'"><strong>Pila</strong>: Es un grupo ordenado de elementos homogéneos (todos del mismo tipo).</p>
		<p v-if="type == 'Prueba cuadrática'"><strong>Prueba cuadrática</strong>: Este método es similar al de la prueba lineal. La diferencia consiste en que en el cuadrático las direcciones alternativas se generarán como D + 1, D + 4, D + 9, . . . , D + i2  en vez de D + 1, D + 2, . . ., D + i.</p>
		<p v-if="type == 'Prueba lineal'"><strong>Prueba lineal</strong>: Consiste en que una vez detectada la colisión se debe recorrer el arreglo secuencialmente a partir del punto de colisión, buscando al elemento.</p>
		<p v-if="type == 'Puntero'"><strong>Puntero</strong>: Ver Apuntador.</p>
		<p v-if="type == 'Puntero externo'"><strong>Puntero externo</strong>: Apuntador que contiene la dirección (referencia) al primer nodo de la lista.</p>
		<p v-if="type == 'Puntero nulo'"><strong>Puntero nulo</strong>: Campo de la lista que no apunta a ningún elemento, es decir no contiene ninguna dirección.</p>
		<p v-if="type == 'RAM (Random Access Memory)'"><strong>RAM (Random Access Memory)</strong>: Cuando es utilizada por sí misma, el término RAM se refiere a memoria de lectura y escritura; es decir, los datos pueden ser escritos y leídos en la RAM.</p>
		<p v-if="type == 'Registro'"><strong>Registro</strong>: Es un dato estructurado, donde cada uno de sus componentes se denomina campo. Los campos de un registro pueden ser todos de diferentes tipos. Cada campo se identifica por un nombre único a través del identificador de campo.</p>
		<p v-if="type == 'Registro de arreglos'"><strong>Registro de arreglos</strong>: Es aquel registro que tiene por lo menos un campo que es un arreglo.</p>
		<p v-if="type == 'Registro heterogéneo'"><strong>Registro heterogéneo</strong>: Es aquel que almacena datos de tipos diferentes.</p>
		<p v-if="type == 'Registro homogéneo'"><strong>Registro homogéneo</strong>: Es aquel que almacena datos del mismo tipo.</p>
		<p v-if="type == 'Registros anidados'"><strong>Registros anidados</strong>: Registros que posee  al menos un campo del tipo registro.</p>
		<p v-if="type == 'Registros con variantes'"><strong>Registros con variantes</strong>: Son aquellos  que  pueden tener campos que sean mutuamente exclusivos. Es decir, campos que nunca se utilizan al mismo tiempo.</p>
		<p v-if="type == 'Registros de listas'"><strong>Registros de listas</strong>: Son datos estructurados, donde cada uno de sus componentes se denomina campo.</p>
		<p v-if="type == 'Renglón'"><strong>Renglón</strong>: Ver Fila.</p>
		<p v-if="type == 'ROM (Read Only Memory)'"><strong>ROM (Read Only Memory)</strong>: Es la memoria fija realizada por el fabricante que viene incorporada a la computadora, es decir, son los programas o instrucciones necesarias para el arranque del computador.</p>
		<p v-if="type == 'TAD'"><strong>TAD</strong>: Ver Tipo Abstracto de Datos.</p>
		<p v-if="type == 'Tipo abstracto de datos'"><strong>Tipo abstracto de datos</strong>: Es un conjunto de operaciones.</p>
		<p v-if="type == 'Underflow'"><strong>Underflow</strong>:  Es un error que se produce si se deseara quitar un elemento de una pila vacía.</p>
	</div>
	`,
})