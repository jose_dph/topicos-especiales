﻿Vue.component('evaluacion', {
    props: ['contenido'],
    methods: {
        select: function (contenido) {
            evaluaciones.contenido = contenido;
        },
        selectAnswer: function () {
            $(".form-check")
                .removeClass("checked");
            $("input[type=radio]:checked")
                .parent("label")
                .parent("div")
                .addClass("checked");
        },
        evaluate: function(contenido) {
            var answer = [];
            switch(contenido) {
                case 1:
                    answer = ["A","B","A","B","B"];
                    break;
                case 2:
                    answer = ["A","B","D","B"];
                    break;
                case 3:
                    answer = ["C","A","A","B","B"];
                    break;
                case 4:
                    answer = ["A","A","B","A"];
                    break;
                case 5:
                    answer = ["A","C","C","B"];
                    break;
                case 6:
                    answer = ["A","A","C","B"];
                    break;
                case 7:
                    answer = ["A","B","B","B"];
                    break;
                default:
                    return 0;
            }
            $.each($(".card"),function(key, item) {
                $(item).addClass("border");
                $(item).addClass("border-danger");
                var goodAnswer = $(item).children(".card-body").children(".checked").children("label").children("input:checked").val();
                if (goodAnswer == answer[key] ) {
                    $(item).removeClass("border-danger");
                    $(item).addClass("border-success");
                }
            });
        }
    },
    template: `
        <div>
            <div v-if="contenido == 0">
                <div>
                    <h3 class="list-cont">Seleccione el contenido</h3>
                    <ul class="list-cont">
                        <li><a v-on:click="select(1)">Contenido 1: conceptos basícos</a></li>
                        <li><a v-on:click="select(2)">Contenido 2: Arreglos</a></li>
                        <li><a v-on:click="select(3)">Contenido 3: Registros</a></li>
                        <li><a v-on:click="select(4)">Contenido 4: Listas enlazadas</a></li>
                        <li><a v-on:click="select(5)">Contenido 5: Pilas</a></li>
                        <li><a v-on:click="select(6)">Contenido 6: Colas</a></li>
                        <li><a v-on:click="select(7)">Contenido 7: Combinación de estructuras de datos lineales</a></li>
                    </ul>
                </div>
            </div>
            <div v-if="contenido == 1">
                <div class ="card">
                    <div class ="card-header">
                        1. Los datos a procesar por una computadora pueden ser:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) Simples y Estructurados
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) Estructurados y Compuestos
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) Simples y Estándares
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Las Estructuras de datos pueden ser:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) Estáticas y lineales
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) Dinámicas y no lineales
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Lineales y no lineales
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. A los Estructuras de datos estáticas se le asigna una cantidad fija de memorias en:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Tiempo de ejecución
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Tiempo de compilación
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Ambos
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. La memoria de una computadora se puede representar coo un conjunto de "celdas" numeradas que contienen un valor o dato, y al número de la celda se le llama:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Byte
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Dirección
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Ubicación
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        5. Para el manejo de la memoria en una computadora se utiliza la siguiente estructura de datos:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A5" name="P5" value="A">A) Cola
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B5" name="P5" value="B">B) Pila
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C5" name="P5" value="C">C) Lista enlazada
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(1)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 2">
                <div class ="card">
                    <div class ="card-header">
                        1. Un arreglo es una colección de elementos finita, ordenada y
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) Homogénea
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) Heterogénea
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) Ambas
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Los arreglos pueden ser:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) Unidimensionales
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) Bidimensionales
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Multidimensionales
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="D3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="D3" name="P3" value="D">D) Todas las anteriores
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. El tipo de acceso a un arreglo es:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Secuencial
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Directo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Lineal
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. El tamaño de un arreglo se define en:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Tiempos de ejecución
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Tiempo de compilación
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Ambos
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(2)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 3">
                <div class ="card">
                    <div class ="card-header">
                        1. Es un dato estructurado, donde cada uno de sus componentes se denomina campo. Los campos son de diferentes tipos y se identifican con un nombre único (el identificador de campo).
                    
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) Arreglo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) Registro
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) Registro heterogéneo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="D1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="D1" name="P1" value="D">D) Ninguna de los anteriores
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Si un registro tiene una parte fija y una parte variante, se debe definir primero:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) La parte fija
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) La parte variante
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) El campo indicativo
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. Suponga que se desean accesar los campos de un registro que a su vez son registros. ¿Cuál es la forma correcta de acceso?
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) variables_registro.id_campo.id_campo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) variables_registro.id_campo.
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) id_campo.id_campo
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. El registro que tiene algunos campos mutuamente exclusivos, se le conoce como:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Registro heterogéneo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Registro con variante
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Registro homogéneo
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. El elemento que se utiliza para discriminar entre las diferentes variantes de un registro, se denomina:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A5" name="P5" value="A">A) CASO
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B5" name="P5" value="B">B) Campo indicativo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C5" name="P5" value="C">C) Parte fija
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="D5">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="D5" name="P5" value="D">D) Items
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(3)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 4">
                <div class ="card">
                    <div class ="card-header">
                        1. La secuencia de nodos en la que cada nodo está enlazado o conectado con el siguiente, se le conoce como:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) Lista enlazada
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) Lista doble
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) Lista simple
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="D3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="D3" name="P1" value="D">D) Nodo
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Es una secuencia de caracteres en memoria dividida en campos (de cualquier tipo):
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) Registros
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) Nodo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Listas
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. Las listas enlazadas pueden ser:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Simples, dobles, circulares, dobles circulares y miltiligadas
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Simples, dobles y circulares
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Dobles circulares y multiligadas
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. El nodo que siempre estará al comienzo de una lista y puede ser usado para simplificar el procesamiento de la lista y/o para contener información sobre la lista. Se conoce como:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Nodo cabecera
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Nodo dinal
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Nodo
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(4)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 5">
                <div class ="card">
                    <div class ="card-header">
                        1. Una pila es una estructura de tipo:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) LIFO
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) FIFO
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) Ambas
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. En cuál de las siguientes áreas son laplicadas las pilas:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) Sistemas de tiempo compartido
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) Colas de impresión
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Recursión
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. Una pila puede ser representada por medio de:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Arreglos
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Listas enlazadas
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Ambos
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. Cuando se pretende incluir un nuevo valor de pila  llena, se produce un error llamado:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Underflow o subdesbordamiento
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Overflow o desbordamiento
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Espacios compartidos
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(5)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 6">
                <div class ="card">
                    <div class ="card-header">
                        1. Una cola es una estructura donde sus elementos:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) Se introducen por un extremo y se elimina por otro
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) Se introducen y eliminnan por el mismo estremo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) No importa el orden de inserción y eliminación
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Para el manejo de una cola se debe mantener almacenada la posición de:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) El primer elemento
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) El último elemento
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Ambos
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. Las colas pueden ser implementadas mediante el uso de:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Arreglos y listas enlazadas
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Arreglos y pilas
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Todas las anteriores
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. Las colas en las que se pueden insertar los elementos por cualquiera de los extremos mientras que las eliminaciones se realizan sólo por el frente, se denomina:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Doble Cola con entrada restringida
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Doble cola con salida restringida
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Cola de prioridades
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(6)">Evaluar</button>
                </div>
            </div>
            <div v-if="contenido == 7">
                <div class ="card">
                    <div class ="card-header">
                        1. La manera de acceder a un elemento de una lista enlazada de arreglos es la siguiente:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A1" name="P1" value="A">A) L^.Campo[i]
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B1" name="P1" value="B">B) L.Campo
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C1">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C1" name="P1" value="C">C) L^.Campo
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        2. Un arreglo cuyos camponentes son de tipo registro se denomina:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A3" name="P3" value="A">A) Registro de arreglos
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B3" name="P3" value="B">B) Arreglo de registros
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C3">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C3" name="P3" value="C">C) Listas de arreglos
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        3. Una lista enlazada en la que uno o más de sus campos está conformado por un registro se denomina:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A2" name="P2" value="A">A) Listas de arreglos
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B2" name="P2" value="B">B) Listas de resgistros
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C2">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C2" name="P2" value="C">C) Regisros de listas
                            </label>
                        </div>
                    </div>
                </div>
                <!--Preguntas-->
                <div class ="card">
                    <div class ="card-header">
                        4. Un arreglo de listas es una estructura de datso que posee en su campo de información uno o más campos apuntadores a:
                    </div>
                    <div class ="card-body">
                        <div class ="form-check">
                            <label class ="form-check-label" for="A4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="A4" name="P4" value="A">A) Registros
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="B4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="B4" name="P4" value="B">B) Listas enlazadas
                            </label>
                        </div>
                        <div class ="form-check">
                            <label class ="form-check-label" for="C4">
                                <input type="radio" v-on:click="selectAnswer" class ="form-check-input" id="C4" name="P4" value="C">C) Arreglos 
                            </label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px" class ="d-flex justify-content-center">
                    <button class ="btn btn-success" v-on:click="evaluate(7)">Evaluar</button>
                </div>
            </div>
        </div>
   `
})