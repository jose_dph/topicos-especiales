Vue.component('nomenclatura', {
	props: ['type'],
	template: `
	<div>
		<div v-show="type == 'Asignación'">
			<h4>1) Asignación</h4>
			<p>Nombre_Variable = Operación Aritmética</p>
			<p>Nombre_Variable = Nombre_Variable</p>
		</div>
		<div v-show="type == 'Lectura'">
			<h4>2) Lectura</h4>
			<p><strong>Leer</strong> (Nombre_Variable)</p>
		</div><div v-show="type == 'Escritura'">
			<h4>3) Escritura</h4>
			<p><strong>Escribir</strong> “Texto que se quiere mostrar por pantalla”</p>
			<p><strong>Escribir</strong> “Texto”, Nombre_Variable</p>
			<p><strong>Escribir</strong> Nombre_Variable</p>
		</div>
		<div v-show="type == 'Estructuras de Control'">
			<h4>4) Estructuras de Control</h4>
			<h5>a) Selección Simple</h5>
			<p><strong>Si</strong> Expresión Condición <strong>Entonces</strong></p>
			<p style="margin-left:30px">Acción(es)</p>
			<p><strong>Fin si</strong></p>
			<h5>b) Selección Doble</h5>
			<p><strong>Si</strong> Expresión Condición <strong>Entonces</strong></p>
			<p style="margin-left:30px">Acción(es)</p>
			<p><strong>Sino</strong></p>
			<p style="margin-left:30px">Acción(es)</p>
			<p><strong>Fin si</strong></p>
			<h5>c) Selección Múltiple</h5>
			<p><strong>Caso</strong> Nombre_Variable</p>
			<p style="margin-left:30px">Valor1: Acción(es)</p>
			<p style="margin-left:30px">Valor2: Acción(es)</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">Valorn: Acción(es)</p>
			<p><strong>Fin Caso</strong></p>
		</div>
		<div v-show="type == 'Estructuras Repetitivas'">
			<h4>5) Estructuras Repetitivas</h4>
			<h5>a) Repetir/Hasta</h5>
			<p><strong>Repetir</strong></p>
			<p style="margin-left:30px">Acción(es)</p>
			<p><strong>Hasta</strong> Expresión Condicional</p>
			<h5>b) Mientras/Fin Mientras</p></h5>
			<p><strong>Mientras</strong> Expresión Condicional Hacer</p>
			<p style="margin-left:30px">Acción(es)</p>
			<p>Fin Mientras</p>
			<h5>c) Para/Fin Para</h5>
			<p><strong>Para</strong> Nombre_Variable = Vi <strong>Hasta</strong> Vf <strong>Inc o Dec</strong> [Valor]</p>
			<p style="margin-left:30px">Acción(es)</p>
			<p><strong>Fin Para</strong></p>
			<p><strong>Donde:</strong></p>
			<p style="margin-left:30px"><strong>Nombre_Variable</strong> : Es la variable a utilizar</p>
			<p style="margin-left:30px">Vi</strong>: Valor Inicial</p>
			<p style="margin-left:30px">Vf</strong>: Valor Final</p>
			<p style="margin-left:30px"><strong>Inc</strong>: Incremento</p>
			<p style="margin-left:30px"><strong>Dec</strong>: Decremento</p>
			<p style="margin-left:30px"><strong>Valor</strong>: Valor del Incremento o Decremento</p>
			<p style="margin-left:30px"><strong>Nota</strong>: Si se omiten el Inc o Dec, se asume un incremento de 1</p>
		</div>
		<div v-show="type == 'Arreglos'">
			<h4>6) Arreglos</h4>
			<p><strong>Arreglos Unidimensionales</strong></p>
			<p style="margin-left:30px">Nombre_Variable: Arreglo [Tamaño del Arreglo] de Tipo de Dato</p>
			<p><strong>Arreglos Bidimensionales</strong></p>
			<p style="margin-left:30px">Nombre_Variables: Arreglo [Fila, Columna] de Tipo de Dato</p>
			<p><strong>Arreglos Multidimensionales</strong></p>
			<p style="margin-left:30px">Nombre_Variables: Arreglo [Fila, Columna, Página] de Tipo de Dato</p>
		</div>
		<div v-show="type == 'Registros'">
			<h4>7) Registros</h4>
			<p><strong>Registro</strong></p>
			<p style="margin-left:30px">Nombre_Campo1: Tipo de Dato</p>
			<p style="margin-left:30px">Nombre_Campo2: Tipo de Dato</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">.</p>
			<p style="margin-left:30px">Nombre_CampoN: Tipo de Dato</p>
			<p><strong>Fin Registro</strong></p>
		</div>
		<div v-show="type == 'Listas Enlazadas'">
			<h4>8) Listas Enlazadas</h4>
			<h5>a) Declaración</h5>
			<p>Nombre_Puntero = ^Tipo de Dato</p>
			<p><strong>Donde:<strong></p>
			<p>Tipo puede ser entero, registro, arreglo, etc.</p>
			<h5>b) Acceso a un Campo</h5>
			<p>Nombre_Nodo ^.Campo</p>
			<h5>c) Creación de nuevo elemento</h5>
			<p>Crear(Nombre_Nodo)</p>
			<h5>d) Eliminación de un elemento</h5>
			<p>Liberar(Nombre_Nodo)</p>
		</div>
	</div>
  	`,
})