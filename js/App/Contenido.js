Vue.component('contenido', {
	props : ['type'],
	data: function () {
		return { 
			abst : "Consiste en tratar los datos en términos de su descripción lógica. Fue creada para separar la visión que tiene la computadora sobre los datos, de la percibida por el ser humano.",
			Apilar:"Consiste en incluir un nuevo elemento en una pila.",
			Apuntador:"Campo de un nodo que contiene  la dirección de memoria del siguiente nodo si éste existe.",
			ArregloUnidimensional:"Se define como una colección finita, homogénea y ordenada de elementos.",
			ArregloAnidado:"Es aquel donde cada elemento del arreglo tiene otro arreglo.",
			ArregloBidimensionalOMatriz:"Es un conjunto de datos homogéneo, finito y ordenado, donde se hace referencia a cada elemento por medio de dos índices.",
			ArreglosDeListas:"Son  estructuras de datos estáticas que poseen en su campo de información uno o más campos  apuntadores a  estructuras de datos dinámicas (Listas).",
			ArreglosDeRegistros:"Son arreglos donde cada elemento será un registro (todos los componentes del arreglo serán del mismo tipo de registro).",
			ArregloMultidimensional:"Es una colección de K1 x K2 x ... x KN elementos. Para hacer referencia a cada componente de un arreglo de N dimensiones, se usarán N índices (uno para cada dimensión).",
			ArregloUnidimensional:"Es un tipo de datos estructurado que está formado de una colección finita y ordenada de datos del mismo tipo.Es la estructura natural para modelar listas de elementos iguales.",
			ArreglosParalelos:"Son dos o más arreglos que utilizan el mismo subíndice para referirse a términos homólogos.",
			CampoDeEnlace:"Ver apuntador.",
			CampoDeInformación:"Campo de un nodo que contiene información almacenada con un fin.",
			Cola:"Es una lista ordenada de elementos, en la cual las eliminaciones se realizan en un solo extremo, llamado frente o principio de la cola, y los nuevos elementos son añadidos por el otro extremo, llamado fondo o final de la cola.",
			ColaCircular:"Es aquella en la cual existe un índice desde el último elemento al primero de la cola.",
			ColaDePrioridades:"Es aquella donde no se atienden a los elementos únicamente aplicando el concepto de cola,  sino  que cada  uno tiene asociada  una  prioridad basada en un criterio objetivo.",
			Colisión:"Es cuando la misma dirección es utilizada para dos claves diferentes.",
			Columna:"Representa la primera dimensión de una matriz.",
			Componentes:"Son aquellos que hacen referencia a los elementos que forman el arreglo.",
			Datos:"Son los nombres del mundo de la programación: los objetos que son manipulados.",
			DatosEstructurados:"Son aquellos que se caracterizan por el hecho de que con un nombre se hace referencia a un grupo de casillas de memoria.",
			DatosSimples:"Son aquellos que ocupan sólo una casilla de memoria, por lo tanto una variable simple hace referencia a un único valor a la vez.",
			Desapilar:"Consiste en sacar un elemento de una pila.",
			Dirección:" Es un número que identifica cada una de las celdas de la memoria de una computadora.",
			DobleColaOBicola :"Es una generalización de una estructura de cola simple, los elementos pueden ser insertados o eliminados por cualquiera de los extremos, es en realidad, una cola bidireccional.",
			DobleColaConEntradaRestringida:"Es aquella que permite que las eliminaciones puedan hacerse por cualquiera de los dos extremos, mientras que las inserciones sólo por el final de la cola.",
			DobleColaConSalidaRestringida:"Es aquella que permite que las inserciones puedan hacerse por cualquiera de los dos extremos, mientras que las eliminaciones sólo por el frente de la cola.",
			DobleDirecciónHash:"Consiste en que una vez detectada la colisión se debe generar otra dirección aplicando la función hash a la dirección previamente obtenida.",
			Encadenamiento:"Consiste en que cada elemento del arreglo tenga un apuntador a una lista enlazada, la cual se irá generando e irá almacenando los valores colisionados a medida que se requiera.",
			EncapsulamientoDeDatos:"Puede definirse como la separación de la representación de los datos de las aplicaciones que utilizan los datos a nivel lógico.",
			EstructuraDeDatos:"Es la forma práctica de almacenar datos del mismo tipo en la memoria interna o central de las computadoras.",
			EstructurasDeDatosDinámicas:"Son aquellas que tienen capacidad de variar en tamaño y ocupar tanta memoria como utilicen realmente, es decir, se amplían  o reducen a medida que se requiera durante la ejecución,  cambiando sus posiciones de memoria asociada.",
			EstructurasDeDatosEstáticas:"Son  aquellas a la que se asigna una cantidad fija de memoria antes de la ejecución del programa (durante la compilación).",
			EstructurasDeDatosLineales:"Son aquellas en que cada elemento tiene máximo dos elementos adyacentes (posterior y/o anterior).",
			EstructurasDeDatosNoLineales:"Son aquellas cuyos elementos pueden tener más de 2 adyacentes, a los que pueden acceder directamente (no tiene sentido el concepto de anterior/siguiente), entre las cuales se tienen Árboles y Grafos.",
			Fila:"Representa la segunda dimensión de una matriz.",
			FIFO:" First-In, First-Out -  primero en entrar, primero en salir.",
			FunciónCuadrado:"Consiste en elevar al cuadrado la clave y tomar los dígitos centrales como dirección.",
			FunciónMódulo:"Consiste en tomar el residuo de la división de la clave entre el número de componentes del arreglo.",
			FunciónPlegamiento:"Consiste en dividir la clave en partes de igual número de dígitos (la última puede  tener menos dígitos) y operar con ellas, tomando como dirección los dígitos menos significativos.",
			FunciónTruncamiento:"Consiste en tomar algunos dígitos de la clave y formar con ellos una dirección.",
			GestiónDeMemoria:" Supone la administración del recurso limitado de la memoria por medio del empleo de distintas estrategias.",
			IdentificadorDeCampo:"Nombre único que Identifica cada uno de los campos de un registro.",
			Indices:"Son aquellos que especifican cuántos elementos tendrá el arreglo y además de qué modo podrán accesarse esos componentes.",
			LIFO:" Last In, First Out – ultimo en entrar, primero en salir.",
			ListasAutoOrganizadas:"Consisten en reordenar los elementos de las listas conforme a reglas heurísticas que pretenden colocar los elementos que son más frecuentemente solicitados hacia el inicio de la lista.",
			ListaCircular:"Esta estructura conocida también como anillo, es una subclase de una lista simple consistente en enlazar el último elemento de la lista con el primero.",
			ListaConNodoDeCabecera:"Es aquella en la que el primer nodo de la lista contendrá en su campo dato algún valor que lo diferencie de los demás nodos.",
			ListasConSaltos:"Son aquellas en las que se mejoran el tiempo de búsqueda añadiendo punteros adicionales a ciertos nodos de la misma.",
			ListasDeArreglos:"Son listas enlazadas en la que uno o más de sus campos está conformado por un arreglo.",
			ListasDeRegistros:"Son listas enlazadas en la que uno o más de sus campos está conformado por un registro.",
			ListaDoble:"Una lista doble o doblemente enlazada es una colección de nodos en la cual cada nodo tiene dos punteros, uno de ellos apuntando a su predecesor (li) y otro a su sucesor (ld).",
			ListaDoblementeEnlazadaCircular:"Es un tipo de lista doble, el  puntero izquierdo del primer nodo apunta al último nodo de la lista, y el puntero derecho del último nodo apunta al primer nodo de la lista.",
			ListaEnlazada:"Es una secuencia de nodos en el que cada uno de ellos está enlazado o conectado con el siguiente.",
			ListaEnlazadaComoUnArregloSencillo:"Es aquella en la que cada nodo de la lista constando de un de registro (al menos) dos campos, Info y Siguiente.",
			ListaHeterogénea:"Es aquella  que almacena distintos tipos de elementos.",
			ListaHomogénea:"Es  aquella en la que todos los elementos almacenados son del mismo tipo.",
			ListasSimples:"Son aquellas que tienen un primer y un último elemento, existiendo una relación estructural entre sus diferentes elementos; normalmente cada elemento está relacionado con el anterior y el siguiente, excepto el primero (sólo con el segundo) y el último (sólo con el anterior).",
			ListasOrtogonales:"Se utilizan para representar matrices, ya que los nodos contienen cuatro apuntadores. Uno para apuntar al nodo izquierdo (li),otro para apuntar al derecho(ld), otro al nodo inferior(lb) y por último un apuntador al nodo superior(la).",
			ListaVacía:"Es aquella que no tiene nodos.",
			Matriz:"Es un término matemático, utilizado para definir un conjunto de elementos organizados por medio de renglones y columnas.",
			MatrizAntisimétrica:"Es aquella matriz de n x n elementos donde A[i, j] es igual a -A[j, i], y esto se cumple para todo i y para todo j; considerando a i ¹ j.",
			MatrizCuadrada:"Es aquella que tiene igual número de renglones y de columnas.",
			MatrizPocoDensa:"Es aquella matriz que contiene una proporción muy alta de ceros.",
			MatrizSimétrica:"Es aquella matriz de n x n elementos donde A[i,j] es igual a A[j,i], y esto se cumple para todo i y para todo j.",
			MatrizTriangular:"Es aquella matriz cuadrada en la que los elementos que se encuentran arriba o debajo de la diagonal principal son iguales a cero.",
			MatrizTriangularInferior:"Es aquella donde los elementos iguales a cero se encuentran sobre la diagonal principal.",
			MatrizTriangularSuperior:"Es aquella donde los elementos iguales a cero se encuentran debajo de la diagonal principal.",
			MatrizTridiagonal:"Es aquella donde los elementos distintos de cero se encuentran en la diagonal principal o en las diagonales por encima o debajo de ésta.",
			Memoria:"Se puede representar como un conjunto de “celdas” numeradas que contienen un valor o dato.",
			MemoriaPrincipal:"Se puede definir como un arreglo de celdas de memoria, cada una de los cuales puede llevar a cabo un solo byte de información.",
			MemoriaSecundaria:"Es una memoria de acceso mucho más lento que la Memoria Principal, es de costo inferior y el almacenamiento de información es permanente e ilimitado.",
			MétodoDeBúsqueda:"Es aquel que permite recuperar datos  previamente  almacenados.",
			MétodoDeBúsquedaSecuencialOLineal:"Consiste en revisar elemento por elemento hasta encontrar el dato buscado, o hasta llegar al final de la lista de datos disponibles.",
			MétodoDeBúsquedaBinaria:"Consiste en dividir el intervalo de búsqueda en dos partes, comparando el elemento buscado con el central.",
			MétodoDeBúsquedaPorTransformaciónDeClaves:"Es aquel que permite aumentar la velocidad de búsqueda sin necesidad de tener los elementos ordenados.",
			MétodoDeBrdenación:"Es aquel que permite ordenar los elementos de un arreglo.",
			MétodoDeBrdenaciónDirecto:"Es aquel que tiene la característica de que sus programas son cortos y de fácil elaboración y comprensión, aunque es ineficiente cuando el número de elementos del arreglo es medio o grande.",
			MétodoDeBrdenaciónLogarítmico:"Es aquel que requiere de menos comparaciones y movimientos para ordenar sus elementos que el método de ordenación directo, pero su elaboración y comprensión resulta más sofisticada y abstracta.",
			MétodoDeBrdenaciónPorIntercambioDirecto:"Consiste en comparar pares de elementos adyacentes e intercambiarlos entre sí hasta que todos se encuentren ordenados.",
			MétodoDeBrdenaciónPorIntercambioDirectoConSeñal:"Este método es una modificación del método de intercambio directo.La idea central consiste en utilizar una marca o señal para indicar que no se ha producido ningún intercambio en una pasada. Es decir, se comprueba si el arreglo está totalmente ordenado después de cada pasada, terminando su ejecución en caso afirmativo.",
			MétodoDeBrdenaciónPorShakerSort:"Es una optimización del método de intercambio directo, consiste en mezclar las dos formas en que se puede realizar el método de la burbuja.",
			MétodoDeBrdenaciónPorInserciónBinaria:"Es aquel que utiliza una búsqueda binaria para insertar un elemento en la parte izquierda del arreglo, que ya se encuentra ordenado.",
			MétodoDeBrdenaciónPorInserciónDirecta:"Consiste en insertar un elemento del arreglo en la parte izquierda del mismo, que ya se encuentra ordenada. Este proceso se repite desde el segundo hasta el  n-ésimo elemento.",
			MétodoDeBrdenaciónPorShell:"Es una versión mejorada del método de inserción directa. Consiste en  comparar cada elemento para su ubicación correcta en el arreglo, con los elementos que se encuentran en la parte izquierda del mismo. Si el elemento a insertar es más pequeño que el grupo de elementos que se encuentran a su izquierda, es necesario efectuar entonces varias comparaciones antes de su ubicación.",
			MétodoDeOrdenaciónPorSelecciónDirecta:"Consiste en buscar el menor elemento del arreglo y colocarlo en la primera posición.",
			MétodoDeOrdenaciónQuicksort:"Es una mejora sustancial del método de intercambio directo y recibe este nombre por la velocidad con que ordena los elementos del arreglo.",
			Nodo:"Es una secuencia de caracteres en memoria dividida en campos (de cualquier tipo).",
			NodoCabecera:"Es un nodo que siempre estará al comienzo de una lista usado para simplificar el procesamiento de la lista y/o para contener información sobre la lista.",
			NodoFinal:"Es un nodo que siempre estará al final de una lista usado para simplificar el procesamiento de la misma.",
			Nulo:"Valor que indica que no se esta apuntado ningún nodo.",
			Ordenar:"Significa permutar estos elementos de tal forma que los mismos queden de acuerdo con un orden preestablecido.",
			Overflow:"Es un error que se produce si la pila estuviera llena y se intentara insertar un nuevo valor.",
			Pila:"Es un grupo ordenado de elementos homogéneos (todos del mismo tipo).",
			PruebaCuadrática:"Este método es similar al de la prueba lineal. La diferencia consiste en que en el cuadrático las direcciones alternativas se generarán como D + 1, D + 4, D + 9, . . . , D + i2  en vez de D + 1, D + 2, . . ., D + i.",
			PruebaLineal:"Consiste en que una vez detectada la colisión se debe recorrer el arreglo secuencialmente a partir del punto de colisión, buscando al elemento.",
			Puntero:"Ver Apuntador.",
			PunteroExterno:"Apuntador que contiene la dirección (referencia) al primer nodo de la lista.",
			PunteroNulo:"Campo de la lista que no apunta a ningún elemento, es decir no contiene ninguna dirección.",
			RAM:"Cuando es utilizada por sí misma, el término RAM se refiere a memoria de lectura y escritura; es decir, los datos pueden ser escritos y leídos en la RAM.",
			Registro:"Es un dato estructurado, donde cada uno de sus componentes se denomina campo. Los campos de un registro pueden ser todos de diferentes tipos. Cada campo se identifica por un nombre único a través del identificador de campo.",
			RegistroDeArreglos:"Es aquel registro que tiene por lo menos un campo que es un arreglo.",
			RegistroHeterogéneo:"Es aquel que almacena datos de tipos diferentes.",
			RegistroHomogéneo:"Es aquel que almacena datos del mismo tipo.",
			RegistrosAnidados:"Registros que posee  al menos un campo del tipo registro.",
			RegistrosConVariantes:"Son aquellos  que  pueden tener campos que sean mutuamente exclusivos. Es decir, campos que nunca se utilizan al mismo tiempo.",
			RegistrosDeListas:"Son datos estructurados, donde cada uno de sus componentes se denomina campo.",
			Renglón:"Ver Fila.",
			ROM:"Es la memoria fija realizada por el fabricante que viene incorporada a la computadora, es decir, son los programas o instrucciones necesarias para el arranque del computador.",
			TAD:"Ver Tipo Abstracto de Datos.",
			TipoAbstractoDeDatos:"Es un conjunto de operaciones.",
			Underflow:" Es un error que se produce si se deseara quitar un elemento de una pila vacía.",
		};
	},
	methods : {
		navNext : function() {
			$( '#bb-bookblock' ).bookblock( 'next' );
		},
		navPrev : function() {
			$( '#bb-bookblock' ).bookblock( 'prev' );
		},
		navFirst : function() {
			$( '#bb-bookblock' ).bookblock( 'first' );
		},
		navLast : function() {
			$( '#bb-bookblock' ).bookblock( 'last' );
		},
		showEvaluation: function (contenido) {
			evaluaciones.contenido = contenido;
		}
	},
	template : `
	<div id="bb-bookblock" class="bb-bookblock" style="width: 1000px;height: 620px">
		<!--Contenido 1-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>1.1
					Introducción.</b>
				</p>
				<p>Las computadoras fueron creadas como una herramienta mediante la cual se pueden
					realizar operaciones de cálculo complicadas en un lapso mínimo de tiempo, pero
					la mayoría de las aplicaciones de este gran invento del hombre requieren
					almacenar y accesar a grandes cantidades de información.
				</p>
					<p>Sin embargo, uno de los problemas más serios con los que se enfrentará el
					programador es, precisamente, la estructuración y almacenamiento de la
					información, ya que estos factores son de vital importancia para que la
					solución dada a un problema determinado sea eficiente, tanto en espacio de
					memoria como en tiempo de trabajo de máquina.</p>
					
					<p>La
					información que se procesa en la computadora está constituida por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a>, bien
					sean simples o estructurados. Debido a que por lo general se tiene que tratar
					conjuntos de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> y no <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">datos simples</a>, es necesario manipular con estructuras de
					datos adecuadas a cada necesidad.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 20" >
					Las estructuras de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> son una colección de datos cuya organización se
					caracteriza por las funciones de acceso que se usan para almacenar y acceder a
					elementos individuales de
					datos.
				</p>

				<p style="line-height: 150%; text-indent: 20" >
				Este capítulo se dedicará al estudio de las estructuras de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> en forma general, involucrando conceptos como la 
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Abstracción de datos" v-bind:data-content="abst">abstracción de datos</a> y gestión de memoria.
				</p>

				<p style="line-height: 150%; text-indent: 0" ><b>1.2
				Definición de Dato</b></p>
				<p style="line-height: 150%; text-indent: 20" >Los
				datos son los nombres del mundo de la programación: los objetos que son
				manipulados. La información que es procesada por un programa de computadora. En
				cierto sentido, esta información es simplemente un conjunto de bits que pueden
				ponerse en “on” u “off”. La propia computadora necesita tener los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a>
				de esta forma. Sin embargo, los humanos tienden a pensar en la información en
				términos de unidades algo mayores como los números y las listas; por tanto,
				los programas deben referirse a los datos en porciones más legibles por los
				hombres de forma que tengan sentido.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 0" >
				<b>1.3 Tipos de Datos.</b></p>
				<p style="line-height: 150%; text-indent: 20" >Según
				Cairó y Guardatí (1993), los datos a procesar por una computadora pueden ser:</p>
				<p style="line-height: 150%; text-indent: 20" >-Simples
				(Escalares o Estándares).</p>
				<p style="line-height: 150%; text-indent: 20" >-Estructurados
				(Compuestos).</p>

				<p>La
				principal característica de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> simples es que ocupan sólo una casilla
				de memoria, por lo tanto una variable simple hace referencia a un único valor a
				la vez. Dentro de este grupo de datos se encuentran: enteros, reales,
				caracteres, booleanos, etc.</p>

				<p>Los
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">datos estructurados</a> se caracterizan por el hecho de que con un nombre
				(identificador de variable estructurada) se hace referencia a un grupo de
				casillas de memoria, es decir, un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a> tiene varios <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>
				(Ver Figura 1.1).
				Cada uno de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> puede ser a su vez un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">dato simple</a> o estructurado.
				Sin embargo, los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> básicos (los del nivel más bajo) de cualquier
				tipo estructurado son <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">datos simples</a>.</p></div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				1.1. </b><a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">datos simples</a> y Estructurados.</p>
				<p style="text-indent: 20; line-height: 100%" align="center">a)
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">Dato Simple</a> b) <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">Dato Estructurado</a></p>
				<p style="text-indent: 20; line-height: 100%" align="center"><img border="0" src="contenido/capI/1.1.gif" width="225" height="40"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 0; line-height: 100%"><b>1.4
				Abstracción de Datos.</b></p>
				<p>Fue
				creada para separar la visión que tiene la computadora sobre los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a>, de la
				percibida por el ser humano. Mucha gente se siente más tranquila y segura con
				las cosas que percibe como reales que con las cosas que piensa como abstractas.
				Por tanto, “la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Abstracción de datos" v-bind:data-content="abst">abstracción de datos</a>” parece más inaccesible que una
				entidad más concreta como “entero”. Sin embargo, analizando “entero”
				desde el punto de vista abstracto, se tiene que: ¿Qué es
				exactamente un entero? Los enteros se representan físicamente de formas
				diferentes sobre diferentes computadoras.</p>

				<p> En la memoria de una máquina, un
				entero puede ser un decimal codificado en binario. En otras puede ser un binario
				con signo y magnitud.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p>En
				una tercera puede representarse en notación de complemento
				Al ó complemento A2. Aunque los cambios son tales y habrá quienes ni siquiera
				saben lo que significan estos términos. La Figura 1.2 muestra algunas
				representaciones posibles para un número entero.</p>

				<p>¿Marcan
				algunas diferencias el que los enteros se representen de forma distinta? ¡Por
				supuesto! La implementación de las operaciones sobre los enteros depende
				directamente de su representación en la máquina.</p>

				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				1.2. </b>Equivalentes decimales de binarios de 8 bits.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capI/12.gif" width="335" height="114"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p>Sin embargo, los
				programadores normalmente no deben referirse a este nivel: simplemente utilizan
				los enteros.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p> Se sabe cómo declarar una variable entera y qué operaciones
				están permitidas sobre los enteros: asignación, suma, resta, multiplicación,
				división y módulo.</p>

				<p>Los
				lenguajes de programación han rodeado el tipo de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> Integer con un paquete
				que da justo la información que se necesita para crear y manipular los datos de
				este tipo.</p>

				<p>Otra
				palabra para “rodear” es “encapsular”. Por ejemplo, si piensa en las
				cápsulas que rodean a las medicinas que se obtienen de la farmacia, no es
				necesario saber nada sobre su composición química para reconocer la cápsula
				azul como el antibiótico o la cápsula roja como el digestivo. Por lo tanto el
				Encapsulamiento de datos significa que la representación física de los datos
				del programa está rodeada.</p>

				<p>El usuario no tiene que ver la implementación;
				solamente que trata con los datos en términos de su descripción lógica, su
				abstracción. El encapsulamiento tiene como objetivo principal proteger las
				abstracciones de datos.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">				<p>El
				Encapsulamiento de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> puede definirse como la separación de la
				representación de los datos de las aplicaciones que utilizan los datos a nivel
				lógico. </p>

				<p style="text-indent: 20; line-height: 150%" > Pero
				si el dato está encapsulado, ¿cómo puede utilizarlo el usuario? Las
				operaciones deben estar de forma que permitan al usuario crear, acceder y
				cambiar el dato. La Figura 1.3 muestra como el Pascal, uno de los lenguajes de
				programación, ha encapsulado el tipo integer en un buen paquete.</p>

				<p  style="line-height: 150%; text-indent: 20">El
				aspecto clave de este análisis es que se está tratando con una abstracción de
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> lógicos de “entero” desde el principio. </p>

				<p  style="line-height: 150%; text-indent: 20">La ventaja de hacer esto así
				es clara: se puede pensar en los datos y en las operaciones en un sentido
				lógico y se puede considerar su uso sin tener que preocuparse de los detalles de implementación. Los niveles inferiores están allí, sólo que
				están ocultos. </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				1.3. </b>Una caja negra que representa un número entero.</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capI/1.3.gif" width="273" height="160"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p align="left" style="line-height: 100%"><b>1.5
				Definición de Estructura de Dato</b></p>
				<p>Una
				estructura de datos es la forma práctica de almacenar <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos" v-bind:data-content="Datos">Datos</a> del mismo tipo en
				la memoria interna o central de las computadoras. El medio en el que se
				relacionan unos elementos con otros determina el tipo de estructura de datos.</p>



				<p>Según
				Joyanes (1992), el valor de la estructura se determina por:</p>
				<p>a)
				Los valores de los elementos.</p>
				<p>b)
				La disposición de los elementos.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p>
					Normalmente
					la selección de una estructura de datos se hará en función de su cometido;
					tras la selección, se elegirá la mejor disposición de los datos y a
					continuación se colocan los valores en los datos. En consecuencia, los valores
					de los datos pueden ser cambiados con relativa frecuencia, la disposición de
					ellos se cambia con menos frecuencia y estructura de los datos se cambia
					raramente.
				</p>
				<p>
					Este es el caso, por ejemplo, en una guía de teléfono donde las entradas cambian anualmente (cambios de direcciones de los abonados, altas, bajas, etc.), sin embargo, la disposición alfabética no se suele variar. No obstante, el ejemplo anterior no siempre es significativo pues, en ocasiones, un cambio en los valores de los datos produce un cambio en la disposición de los mismos. Si se considera la tabla de clasificación de Liga Venezolana de Béisbol Profesional, los resultados de los partidos semanales influirán en la clasificación modificando no sólo la puntuación, sino el orden de los equipos.
				</p>
				<p>
					Otro caso significativo, aunque no frecuente, se produce cuando se modifica el tipo de estructura de datos y los valores
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p>
					de los datos permanecen constantes; este es el caso de la guía telefónica por direcciones.
				</p>
				<h3>1.6
					Características de las Estructuras de Datos
				</h3>
				<p>
					Según Dale y Lilly (1989), las estructuras de datos tienen algunas características propias:
				</p>

				<ul style="line-height: 150%">
				  <li>
				    <p>
				    	Pueden ser “ descompuestas ” en sus elementos <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>.
				    </p>
				  </li>
				</ul>
				<ul style="line-height: 150%">
				  <li>
				    <p>La
				    	colocación de los elementos es una característica de la estructura que afectará a cómo se accede a cada elemento.
					</p>
				  </li>
				</ul>
				<ul>
				  <li>
				    <p>La
				    	colocación de los elementos y la forma en la que se accede a ellos puede ser encapsulada.
				    </p>
				  </li>
				</ul>
				<p>
					Para	comprender estas características, se toma como ejemplo una biblioteca.
				</p>
				<ul style="line-height: 150%">
				  <li>
				    <p>Una biblioteca puede descomponerse en sus elementos, libros.
					</p>
				  </li>
				</ul>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul style="line-height: 150%">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Los
				    Libros pueden ser colocados de varias formas (materia, alfabéticamente,
				    autor, etc.) y la forma en que se colocan los libros determina cómo se
				    puede buscar un determinado volumen.</li>
				</ul>
				<ul style="line-height: 150%">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >En
				    la biblioteca, el usuario le solicita el libro deseado al bibliotecario,
				    quien se lo entrega. La estructura de datos “biblioteca” está compuesta
				    de elementos (libros) en una colocación particular, por lo que el acceso a
				    un libro específico requiere el conocimiento de la colocación de los
				    libros.</li>
				</ul>

				<p>El
				usuario de la biblioteca no tiene por qué conocer este método de acceso,
				porque la estructura ha sido encapsulada: el acceso de los usuarios a los libros
				se hace sólo a través del bibliotecario.</p>

				<p style="text-indent: 0; line-height: 150%" >
					<b>1.7 Clasificación de las Estructuras de Datos.</b>
				</p>
				<p>
					Según Heileman (1998), las Estructuras de Datos pueden ser clasificadas de la siguiente	manera:
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p>
					<b>Estructuras de Datos Estáticas (fundamentales o básicas)</b>
				</p>
				<p>
					Una estructura de datos se dice que es estática si se asigna una cantidad fija de memoria para esa estructura antes de la ejecución del programa (durante la compilación). Esto es, la cantidad de memoria asignada a una estructura de datos estática permanece inalterable a lo largo de la ejecución del programa, es decir, las variables no pueden crearse o destruirse durante la ejecución del programa.
				</p>

				<p>
					Según el web site <i>http://members.tripod.com/fcc98/ tutores/ed1/ed1.html</i>, las	Estructuras de Datos Estáticas pueden ser:
				</p>

				<p>
					1.- Simples o primitivas:
				</p>
				<blockquote>
				  <p>
				  	a) Booleano.
				  </p>
				  <p>
				  	b) Caracter.</p>
				  <p>
				  	c) Entero.
				  </p>
				  <p>
				  	d) Real.
				  </p>
				</blockquote>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p>
					2.-Compuestas:
				</p>
				<blockquote>
				  <p>a)Arreglos.</p>
				  <p>b)Conjuntos.</p>
				  <p>c)Cadenas.</p>
				  <p>d)Registros.</p>
				  <p>e)
				  Archivos.</p>
				</blockquote>

				<p>
					<b>Estructuras de Datos Dinámicas (avanzadas)</b>
				</p>
				<p>
					Las estructuras de datos dinámicas son aquellas que tienen capacidad de variar en tamaño y ocupar tanta memoria como utilicen realmente, es decir, se amplían (expanden) o reducen (contraen) a medida que se requiera durante la ejecución, cambiando sus posiciones de memoria asociada.
				</p>

				<p>
					Las variables que se crean y se destruyen durante la ejecución se llaman variables dinámicas (también anónimas). Así, durante la ejecución de un programa, puede haber una posición de memoria especifica asociada con una variable
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" >dinámica
				  y posteriormente puede no existir ninguna posición de memoria asociada con
				  ella.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >Las
				  estructuras de datos dinámicas son útiles especialmente para almacenar y
				  procesar conjuntos de datos cuyos tamaños cambian durante la ejecución del
				  programa, por ejemplo, el conjunto de trabajos que se han introducido en una
				  computadora y están esperando su ejecución o el conjunto de nombres de
				  pasajeros y asignación respectiva de asientos de un vuelo de avión
				  determinado.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >Si
				  no se sabe por anticipado cuánta memoria será requerida por una estructura
				  de datos durante la ejecución del programa, entonces puede usarse una
				  estructura de datos dinámica para asignar memoria a medida que sea necesaria
				durante la ejecución del programa, esto
				  se conoce como asignación dinámica de memoria.
				</p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >
				De este modo, con una
				  estructura de datos dinámica, la cantidad de memoria que puede usar la
				  estructura de datos no queda fijada durante la compilación.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >Según
				  el Web Site <i>http://www.unife.edu.pe/Algoritmos%20
				  Garcia/Listas%20Enlazadas.htm</i>, las Estructuras de Datos Dinámicas pueden
				  ser:
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >1.-
				  Lineales: Son aquellas estructuras de datos en que cada elemento tiene máximo
				  dos elementos adyacentes (posterior y/o anterior). Desde un elemento
				  cualquiera sólo se puede acceder a uno de estos dos, lo que implica que la
				  relación entre cada elemento es anterior-posterior o posterior- anterior;
				  entre ellas se encuentran:
				</p>

				<blockquote>
				    <p>a) Pilas.</p>
				    <p>b) <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">Colas</a>.</p>
				    <p>c) Listas Enlazadas.</p>
				  
				</blockquote>
				  <p>2.-
				  No lineales: Son aquellas cuyos elementos pueden tener más de 2 adyacentes, a
				  los que pueden acceder directamente (no tiene sentido el concepto de
				  anterior/siguiente), entre las cuales se tienen:</p>

				<blockquote>
				  <p>a)
				  Árboles.</p>
				  <p>b)
				  Grafos.</p>
				</blockquote>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" >
					<b>1.8 Tipos Abstractos de Datos (TAD)</b>
				</p>
				<p>Una
				de las reglas básicas concernientes a la programación es que ninguna rutina
				debe exceder una página. Esto se logra dividiendo el programa en módulos. Cada
				módulo es una unidad lógica y hace un trabajo específico. Su tamaño se
				mantiene pequeño llamando a otros módulos. La modularidad tiene varias
				ventajas. Primera, es más fácil depurar rutinas pequeñas que grandes.
				Segunda, es más fácil que varias personas trabajen simultáneamente en un
				programa modular. Tercera, un programa modular bien escrito pone ciertas
				dependencias en sólo una rutina, haciendo más fáciles los cambios. Por
				ejemplo, si se necesita escribir la salida en algún formato, ciertamente es
				importante tener una rutina que lo haga. Si los enunciados de impresión están
				dispersos por todo el programa, se tardará un tiempo considerable en hacer
				modificaciones. La idea de que las variables globales y sus efectos laterales
				son malos se atribuye directamente a la idea de que la modularidad es buena.
				</p>

				<p>
					Un tipo abstracto de datos (TAD) es un conjunto de operaciones. Los TAD son abstracciones matemáticas; en ninguna parte de su definición se hace mención alguna a cómo se implanta el conjunto de operaciones.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >
					Esto se puede ver como una extensión del diseño modular. Los	objetos tales como listas, conjuntos y grafos, así como sus operaciones, se	pueden considerar como TAD, al igual que los enteros, los reales y los booleanos son tipos de datos. Los enteros, los reales y los booleanos tienen operaciones asociadas, igual que los tipos de datos abstractos.
				</p>
				<p>Para
				el TAD conjunto, se pueden tener operaciones como la unión, la intersección,
				el tamaño y el complemento. Alternativamente,
				quizá sólo se deseen las dos operaciones de unión y buscar, las cuales podrían
				definir un TAD diferente sobre conjuntos.</p>

				<p>La
				idea básica es que la implantación de esas operaciones se escribe sólo una
				vez en el programa, y cualquier otra parte del programa que necesite realizar
				una operación sobre el TAD puede hacerlo llamando a la función apropiada. Si
				por alguna razón hay que cambiar detalles de la implantación, debe ser fácil
				hacerlo cambiando sólo las rutinas que realizan las operaciones del TAD. Este
				cambio, en un mundo perfecto, podría ser completamente transparente para el
				resto del programa. </p>
			</div>
			<div class="pagina" @click="navNext">
				<p>No
					hay ninguna regla que indique qué operaciones debe manejar cada TAD; ésta es
					una decisión de diseño. El
					manejo de errores y de rupturas (cuando se requiera) son aspectos que conciernen
					también al diseñador del programa.
				</p>
				<p style="text-indent: 0; line-height: 200%" >
					<b>1.9 Memoria de la Computadora.</b>
				</p>
				<p>La
					memoria de una computadora se puede representar como un conjunto de “celdas”
					numeradas que contienen un valor o dato, y al número de la celda se le llama
					<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a>.
				</p>
				<p>
					Se puede pensar en la memoria principal como arreglo de celdas de memoria, cada una
					de los cuales puede llevar a cabo un solo byte de información. La memoria
					funciona de manera similar a un juego de cubículos divididos, usados para
					clasificar la correspondencia en la oficina postal.
				</p>

				<p>A
				cada bit de datos se asigna una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a>. Cada dirección corresponde a un cubículo
				(ubicación) en la memoria.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p>Con
				el fin de entender muchos aspectos de la programación, es esencial adquirir una
				comprensión de cómo está organizada la memoria de las computadoras. Según el
				Web Site <i>http://www.sscc.co.cl/informatica /conceptos.html</i>, la memoria
				puede clasificarse en:</p>
				<p style="text-indent: 0; line-height: 150%" >
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" ><b>Memoria
				    Principal</b></li>
				</ul>
				<blockquote>
				  <p>Existen
				  dos tipos de memoria principal:</p>
				  
				  <p><i><b>RAM
				  (Random Access Memory).</b></i></p>
				  <p><b><i>ROM
				  ( Read Only Memory).</i></b></p>
				</blockquote>

				<p><b><i>RAM
				(Random Access Memory)</i></b></p>
				<p>Cuando
				es utilizada por sí misma, el término RAM se refiere a memoria de lectura y
				escritura; es decir, los datos pueden ser escritos y leídos en ella. La mayoría
				de la RAM es volátil, es decir, requiere un flujo constante de la electricidad
				para mantener su contenido.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p>Tan
				pronto como el suministro de poder sea interrumpido, todos los datos que estaban
				en RAM se pierden.</p>

				<p>La
				memoria RAM se asume que tiene acceso directo. Esto es, un acceso a cualquier
				posición de la memoria necesita una cantidad de tiempo fija, independiente de
				los accesos anteriores.</p>



				<p>Además, el tiempo de acceso para la memoria principal
				habitualmente es varios órdenes de magnitud más rápido que el tiempo de
				acceso al almacenamiento secundario.</p>



				<p>
					<b><i>ROM( Read Only Memory)</i></b>
				</p>
				<p>
					Es la memoria fija realizada por el fabricante que viene incorporada a la
					computadora, es decir, son los programas o instrucciones necesarias para el
					arranque del computador.
				</p>



				<p>
					En la memoria ROM no se puede escribir, se decir, es
					memoria sólo de lectura.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" ><b>Memoria
				    Secundaria</b></li>
				</ul>
				<p>Es
				una memoria de acceso mucho más lento que la Memoria Principal, es de costo
				inferior y el almacenamiento de información es permanente e ilimitado. Existe
				una variedad de dispositivos
				que cumplen la función de Memoria Secundaria, entre los cuales se pueden
				mencionar: disquetes, discos duros, discos ópticos, CD, DVD, etc.</p>

				<p style="text-indent: 0; line-height: 150%" ><b>1.10
				Gestión de la Memoria</b></p>
				<p>La
				gestión de la memoria supone la administración del recurso limitado de la
				memoria por medio del empleo de distintas estrategias. A continuación se
				considera un modelo simple de la memoria principal que es útil para explicar
				cómo la memoria es asignada cuando una estructura de datos es creada, y cómo
				es devuelta cuando una estructura de datos no se necesita más.</p>

				<p>Resulta
				útil considerar este modelo como un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> de posiciones de
				almacenamiento (o bytes) que está dividido en tres partes.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p>A
				las variables que se mantendrán en la memoria a lo largo de la ejecución del
				programase les
				asigna memoria estática. La cantidad de espacio asignado para la memoria
				estática se determina durante la compilación, y esa cantidad no cambia durante
				la ejecución del programa.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p  style="line-height: 150%; text-indent: 20"><span style="font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Century Gothic&quot;">
				</span>El
				sistema de la computadora mantiene una pila de ejecución en la zona baja de la
				memoria (memoria baja).</p>

				<p align="center" style="text-align: center; line-height: 100%; text-indent: 0"></p>

				<p align="center" style="text-align: center; line-height: 100%; text-indent: 0"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><b>Figura
				1.4.</b> Un Modelo Lógico de la memoria principal.</span></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capI/1.4.gif" width="192" height="204"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capI/F1.4.gif" align="right" width="162" height="16"></p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">La cantidad de espacio que la pila de ejecución utiliza
				variará durante la ejecución del programa. Las flechas que salen de la pila de
				ejecución en la Figura 1.4 indican que la pila de ejecución “crece” hacia
				la memoria alta y “mengua” hacia la memoria baja. Cada vez que un
				procedimiento es invocado en un programa, un registro de activación se crea y
				se almacena en la memoria de la computadora en la pila de ejecución.</p>


				<p  style="line-height: 150%; text-indent: 20">Este
				registro de activación contiene espacio de almacenamiento para todas las
				variables declaradas en el procedimiento, así como una copia de, o una
				referencia a los parámetros que están siendo pasados al procedimiento.
				Adicionalmente un registro de activación debe contener alguna información que
				especifique dónde debe continuar la ejecución del programa cuando el
				procedimiento se complete. A la terminación del procedimiento, el registro de
				activación asociado será eliminado de la pila de ejecución, y el control del
				programa volverá al punto especificado en el registro de activación.</p>

				<p  style="line-height: 150%; text-indent: 20">Finalmente,
				el modelo lógico de la memoria principal de la</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >
					Figura 1.4 muestra una zona de memoria libre que “crece” hacia la memoria baja y
					“mengua” hacia la memoria alta. La memoria que se asigna durante la ejecución
					(memoria asignada dinámicamente) se almacena en la memoria libre.
				</p>



				<p>La
				pila de ejecución y la memoria libre “crecen” la una contra la otra en este
				modelo. De este modo, una situación de error obvia se produce si, o bien,
				demasiada memoria es asignada dinámicamente sin ser devuelta, o bien, se crean
				demasiados registros de activación.</p>



				<p>El
				usuario, sin embargo, es completamente ignorante de cómo la zona memoria libre
				está manejando realmente su memoria cuando realiza estas tareas.</p>


				<p>Mientras
				la asignación y desasignación de memoria en la pila de ejecución son
				controladas por el sistema de la computadora, ese puede no ser el caso de la
				memoria libre. En muchos lenguajes de programación, la responsabilidad de
				devolver la memoria asignada dinámicamente es del programador.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p> Si la memoria no
				es devuelta por el programador, entonces permanece asignada en la memoria libre.
				Este enfoque de devolución de la memoria asignada dinámicamente se conoce como
				el enfoque explícito.</p>



				<p>Por el contrario, en un enfoque implícito de la
				desasignación de la memoria dinámica, son las funciones de gestión de la
				memoria aportadas por el sistema las responsables de devolver la memoria que no
				va a ser necesitada más. El enfoque implícito es habitualmente denominado
				recolección de basura.</p>



				<p>El
				programador puede habitualmente mejorar las rutinas suministradas por el
				compilador. Esto resulta particularmente cierto
				en los casos en los que se tiene algún conocimiento a priori de las necesidades
				de memoria de un programa, por ejemplo, cuando hay una gran cantidad de objetos
				pequeños que son continuamente asignados y desasignados, o cuando deben ser
				gestionados numerosos trozos de memoria del mismo tamaño.</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>Resumen</b></p>
				<p>Los
				datos son los nombres del mundo de la programación: los objetos que son
				manipulados, la información que es procesada por un programa de computadora.</p>

				<p>La
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Abstracción de datos" v-bind:data-content="abst">Abstracción de datos</a> fue creada para separar la visión que tiene la
				computadora sobre los datos de la percibida por el ser humano, para proteger las
				abstracciones de datos está el Encapsulamiento de Datos.</p>

				<p>Encapsulamiento
				de datos significa que la representación física de los datos del programa
				está rodeada. El usuario no tiene que ver la implementación; solamente que
				trata con los datos en términos de su descripción lógica, su abstracción.</p>



				<p>La
				forma de almacenar datos del mismo tipo en la memoria de las computadoras se denomina Estructura de Datos. </p>


				<p>La
				memoria es el conjunto de “celdas” numeradas que</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" >contienen un valor o dato,
				puede ser Memoria Principal o Memoria Secundaria.</p>

				<p>Las
				Estructuras de datos pueden ser: Estáticas y Dinámicas.</p>

				<p>Una
				estructura de datos se dice que es Estática si se asigna una cantidad fija de
				memoria para esa estructura antes de la ejecución del programa, las estructuras
				de datos estáticas pueden ser: Simples o Compuestas.</p>


				<p>Las
				estructuras de datos Dinámicas son aquellas que tienen capacidad de variar, es
				decir, se amplían (expanden) o reducen (contraen) a medida que se requiera
				durante la ejecución de un programa, cambiando sus posiciones de memoria
				asociada, éstas pueden ser: Lineales si cada elemento tiene como mucho dos
				elementos adyacentes y No lineales si cada elemento pueden tener más de 2
				adyacentes, a los que pueden acceder directamente.</p>


				<p><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 1)">Autoevaluación</a></p>
			</div>
			<div class="pagina" @click="navNext">			</div>
		</div>
		<!--Contenido 2-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.1
				Introducción.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Hay
				ocasiones en las que el programador se enfrenta a problemas en los que resulta
				totalmente ineficiente el uso de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">datos simples</a>; por ejemplo si se desea resolver
				el siguiente problema: Una empresa que cuenta con 100 empleados, desea
				establecer una estadística sobre los sueldos de sus empleados, para saber cual
				es el salario promedio y cuántos de sus empleados gana entre Bs. 850.000 y Bs.
				1.500.000.</p>				<p style="text-indent: 20; line-height: 150%" >Si
				se toma la decisión de tratar este tipo de problemas con <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos simples" v-bind:data-content="DatosSimples">datos simples</a>, se
				incurrirá en un enorme desperdicio de tiempo, almacenamiento y velocidad; por
				lo tanto se hace necesario un nuevo tipo de datos que permita tratar estos
				problemas de una manera más adecuada. Los tipos de datos que ayudan a resolver
				problemas como éste son los denominados arreglos.</p>
				<p style="text-indent: 20; line-height: 150%" >En
				el capítulo que se presenta a continuación se realizará un estudio detallado
				de esta estructura de datos abarcando, entre otras cosas su definición,
				clasificación, métodos de ordenamiento y búsqueda.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>2.2
				Definición de Arreglo</b></p>
				<p style="text-indent: 20; line-height: 150%" >Según
				Cairó y Guardatí (1993), un Arreglo se define como una colección finita,
				homogénea y ordenada de elementos.</p>

				<p style="text-indent: 20; line-height: 150%" >Finita:
				todo arreglo tiene un límite, es decir, debe determinarse cuál será el
				número máximo de elementos que podrán formar parte del arreglo.</p>				<p style="text-indent: 20; line-height: 150%" >Homogénea:
				todos los elementos de un arreglo son del mismo tipo (todos enteros, booleanos,
				etc., pero nunca una combinación de distintos tipos).</p>				<p style="text-indent: 20; line-height: 150%" >Ordenada:
				se puede determinar cuál es el primer elemento, el segundo, el tercero, y el
				enésimo elemento.</p>				<p style="text-indent: 20; line-height: 150%" >Un
				arreglo puede representarse gráficamente como sigue:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				2.1.</b> Representación de Arreglos</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="2.1.gif" width="221" height="103"></p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Si
				un arreglo tiene la característica de que puede almacenar a N elementos del
				mismo tipo, deberá tener la facilidad de permitir el acceso a cada uno de
				ellos.</p>

				    <p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >

				    <p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >Así,
				    se distinguen dos partes en los arreglos:

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" >Los
				    <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>.</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Los
				    índices.</li>
				</ul>				<p style="text-indent: 20; line-height: 150%" >Los
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> hacen referencia a los elementos que forman el arreglo, es decir, a
				los valores que se almacenan en cada una de las casillas del mismo.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Los
				índices especifican cuántos elementos tendrá el arreglo y además de qué
				modo podrán accesarse esos <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>.</p>

				<p style="text-indent: 20; line-height: 150%" >Los índices permiten hacer referencia
				a los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo en forma individual, es decir, distinguir entre
				los elementos del mismo.</p>

				<p style="text-indent: 20; line-height: 150%" >El número de índices necesario para especificar un
				elemento de un arreglo se denomina dimensión.</p>
				<p style="text-indent: 20; line-height: 150%" >Por
				lo tanto, para hacer referencia a un elemento de un arreglo se utiliza:</p>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >El
				    nombre del arreglo.</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >El
				    índice del elemento.</li>
				</ul>

				    

				    <p style="text-indent: 20; line-height: 150%" >En
				la figura 2.2 se representa un arreglo indicando sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> y sus
				índices:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.2.</b> Índices y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> de un arreglo</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.2.gif" width="295" height="113"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 100%" >Por
				lo tanto, un arreglo se define de la siguiente manera:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="C:\Edl-Book\contenido\capII\Arr1.gif" width="349" height="17"></p>

				<p style="text-indent: 20; line-height: 150%" >En
				el Tamaño del Arreglo se debe especificar el número de elementos que tendrá
				el arreglo, a través de un límite superior y un límite inferior, de la
				siguiente manera:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>[líminf
				... límsup]</b></p>

				<p style="text-indent: 20; line-height: 150%" >El
				número total de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> (NTC) que tendrá el arreglo puede calcularse con
				la fórmula:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>NTC
				= límsup – líminf +1;;;;;;;;;;
				(2.1)</b></p>

				<p style="text-indent: 20; line-height: 150%" >Con
				Tipo se declara el tipo de datos para todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo. El tipo
				de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> no tiene que ser necesariamente el mismo que el tipo de los
				índices.</p>				<p style="text-indent: 20; line-height: 150%" ><b>Observaciones:</b></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" type="a">
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >El
				    tipo del índice puede ser cualquier tipo ordinal (carácter, entero,
				    enumerado).</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >El
				    tipo de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> puede ser cualquier tipo (entero, real, cadena de
				    caracteres, registro, arreglo, etc.).</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Se
				    utilizan los corchetes “[ ]” para indicar el índice de un arreglo.
				    Entre los [ ] se debe escribir un valor ordinal (puede ser una variable,
				    constante o una expresión, pero que de como resultado un valor ordinal).</li>
				</ol>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.3
				Aplicaciones de los arreglos</b></p>
				<p style="text-indent: 20; line-height: 150%" >Un
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> es la estructura natural para modelar listas de elementos
				de datos iguales. Algunos ejemplos son una lista de compras, una lista de
				precios, una lista de números de teléfono y una lista de registro de
				estudiantes. Un uso frecuente es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> de caracteres, el
				cual señala frecuentemente una cadena (string).</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4
				Clasificación de los arreglos</b></p>
				<p style="text-indent: 20; line-height: 150%" >Los
				arreglos pueden ser clasificados de la siguiente manera:</p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 150%" ><b>2.4.1
				Unidimensionales o Vectores</b></p>
				<p style="text-indent: 20; line-height: 150%" >Un
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> es un tipo de datos estructurado que está formado de una
				colección finita y ordenada de datos del mismo tipo. Es la estructura natural
				para modelar listas de elementos iguales.</p>
				<p style="text-indent: 20; line-height: 150%" >El;
				tipo; de; acceso a; los; arreglos; unidimensionales es; el; acceso; directo,;
				es; decir,; se; puede;; acceder;; a;;
				cualquier</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" > elemento del arreglo, sin tener que consultar a
				elementos anteriores o posteriores, esto mediante el uso de un índice para cada
				elemento del arreglo que da su posición relativa.</p>

				<p style="text-indent: 20; line-height: 150%" >Para
				implementar arreglos unidimensionales se debe reservar espacio en memoria, y se
				debe proporcionar la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> base del arreglo, la cota superior y la
				inferior.</p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 100%" ><b>2.4.1.1
				Declaración de arreglos unidimensionales</b></p>
				<p style="text-indent: 20; line-height: 100%" >El
				formato para definir un tipo arreglo es:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="C:\Edl-Book\contenido\capII\Arr2.gif" width="346" height="17"></p>

				<p style="text-indent: 20; line-height: 150%" >Donde:</p>
				<p style="text-indent: 20; line-height: 150%" ><b>Nombre_Variable</b>:
				identificador válido</p>
				<p style="text-indent: 20; line-height: 150%" ><b>Tamaño
				de Arreglo</b>: Puede ser de tipo ordinal (booleano o caracter, un tipo) enumerado o
				un tipo subrango.</p>

				<p style="text-indent: 20; line-height: 150%" ><b>Tipo
				de dato</b>: Describe el tipo de cada elemento del vector; todos los elementos de un
				vector son del mismo tipo.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Las
				variables de tipo arreglo se declaran en la sección Variables.</p>

				<p style="line-height: 100%; text-indent: 0" ></p>

				<p style="line-height: 150%; text-indent: 0" ><b>Tipos</b></p>

				<p style="line-height: 100%; text-indent: 20" >nombres
				: arreglo[1..30] de cadena</p>

				<p style="line-height: 100%; text-indent: 20" >calif
				: arreglo[1..30] de real</p>

				<p style="line-height: 100%; text-indent: 20" >numero
				: arreglo[0..100] de entero</p>

				<p style="line-height: 100%; text-indent: 0" ></p>

				<p style="line-height: 150%; text-indent: 0" ><b>Variables</b></p>
				<p style="line-height: 100%; text-indent: 20" >nom
				: nombres</p>
				<p style="line-height: 100%; text-indent: 20" >califica
				: calif</p>
				<p style="line-height: 100%; text-indent: 20" >num
				: numero</p>

				<p style="line-height: 100%; text-indent: 0" ></p>

				<p style="line-height: 100%; text-indent: 0" ><b>2.4.1.2
				Representación en memoria de los arreglos;</b></p>

				<p style="line-height: 100%; text-indent: 50" ><b> unidimensionales</b></p>
				<p style="line-height: 150%; text-indent: 20" >Los
				arreglos unidimensionales se representan en memoria de la forma siguiente:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 20" align="center">x
				: arreglo[1..5] de entero</p>

				<p style="line-height: 150%; text-indent: 20" align="center"></p>
				<p style="line-height: 150%; text-indent: 20" align="center"><b>Figura
				2.3.</b> Representación de la memoria</p>
				<p style="line-height: 150%; text-indent: 0" align="center"><img border="0" src="contenido/capII/2.3.gif" width="157" height="213"></p>
				<p style="line-height: 150%; text-indent: 0" align="right"><img border="0" src="contenido/capII/F2.3.gif" align="right" width="359" height="10"></p>
				<p style="line-height: 150%; text-indent: 20" >Para
				establecer el rango del arreglo (número total de elementos) que componen el
				arreglo se utiliza la siguiente fórmula:</p>

				<p style="text-indent: 20; line-height: 150%" align="center"></p>

				<p style="text-indent: 20; line-height: 150%" align="center"><b>RANGO
				= (Ls – Li) + 1;;;;;;; (2.2)</b></p>
							</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Donde:</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 150%" ><b>Ls</b>
				  : Límite superior del arreglo</p>
				  <p style="text-indent: 20; line-height: 150%" ><b>Li</b>
				  : Límite inferior del arreglo</p>
				</blockquote>				<p style="text-indent: 20; line-height: 150%" >Para
				calcular la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> de memoria de un elemento dentro de un arreglo se usa la
				siguiente fórmula:</p>
				<p style="text-indent: 20; line-height: 150%" align="center"></p>
				<p style="text-indent: 20; line-height: 150%" align="center"><b>A[i]
				= base (A) + [(i - Li) * w];;;;;;;;;;;
				(2.3)</b></p>				<p style="text-indent: 20; line-height: 150%" >Donde
				:</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 150%" ><b>A</b>
				  : Identificador único del arreglo</p>
				  <p style="text-indent: 20; line-height: 150%" ><b>i</b>
				  : Índice del elemento</p>
				  <p style="text-indent: 20; line-height: 150%" ><b>Li</b>
				  : Límite inferior</p>
				  <p style="text-indent: 20; line-height: 150%" ><b>w</b>
				  : Número de bytes tipo componente</p>
				</blockquote>	
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 200%" >Si
				el arreglo en el cual se está trabajando tiene un índice numerativo se
				utilizarán las siguientes fórmulas:</p>

				<p style="text-indent: 0; line-height: 150%; margin-top: 5" align="center"><b>RANGO
				= ord (Ls) - (ord (Li)+1);;;;;;;;;;;;;;;;;;;;;;;
				(2.4)</b></p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>A[i]
				= base (A) + [ord (i) - ord (Li) * w];;;;;;;;;;;;;;;;
				(2.5)</b></p>
				<p style="line-height: 150%; text-indent: 20" >Donde
				:</p>
				<p style="line-height: 150%; text-indent: 20" ><b>Ord</b>
				= valor entero que indica el máximo valor del tamaño del arreglo cuando se
				trata de Ls y el mínimo valor del tamaño del arreglo cuando se trata de Li.</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-top: 0; margin-bottom: 0" ></p>
				<p class="MsoNormal" style="text-align:justify;line-height:150%"><b>2.4.1.3
				Operaciones con Arreglos unidimensionales</b></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				los arreglos son <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">datos estructurados</a>, muchas de sus operaciones no pueden
				llevarse a cabo de manera global, sino que se debe trabajar sobre cada
				componente. A continuación se analizará cada una de las operaciones:</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" ></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" ><b>2.4.1.3.1
				Lectura /Escritura</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" ></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" ><b>Lectura</b></p>
				<p style="text-indent: 20; line-height: 150%" >El;
				proceso; de; lectura; de; un; arreglo; consiste;
				en; leer; y </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" > asignar
				un
				valor a cada uno
				de sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>. Si se desea leer todos los elementos del arreglo V en forma
				consecutiva, podría hacerse de la siguiente manera:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				V[1]</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				V[2]</p>
				<p style="text-indent: 20; line-height: 100%" >...</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				V[50]</p>
				<p style="text-indent: 20; line-height: 150%" >De
				esta forma no resulta práctico, por lo tanto se usará un ciclo para leer todos
				los elementos de un arreglo:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 50 <b>Inc</b> [1]</p>
				<blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				V[I]</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>

				<p style="text-indent: 20; line-height: 150%" >Al
				variar el valor de I, cada; elemento leído; se; asigna; al</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >correspondiente
				componente del arreglo según la posición indicada por I.</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> N <b>Inc</b> [1]</p>
				<blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				V[I]</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>

				<p style="text-indent: 20; line-height: 150%" >Al
				finalizar el ciclo de lectura se tendrá asignado un valor a cada uno de los
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo V. El arreglo queda como se muestra en la figura
				siguiente:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.4.</b> Lectura de arreglos</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.4.gif" width="218" height="68"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Puede
				suceder que no se necesite leer todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>, sino solamente algunos de
				ellos.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" > Si por ejemplo, deben leerse los elementos con índices comprendidos
				entre el 1 y el 30, el ciclo necesario es el siguiente:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 30 <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				  V[I]</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>				<p style="text-indent: 20; line-height: 100%" >El
				arreglo queda como se muestra en la siguiente figura:</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.5.</b> ;Lectura de Arreglos</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.5.gif" width="258" height="67"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p class="MsoNormal"  style="line-height: 100%; tab-stops: 426.5pt 449.0pt"><b>Escritura</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				caso de la escritura es similar al de la lectura. Se debe escribir el valor de
				cada uno de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Si se desean escribir los primeros N <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>
				del arreglo V en forma consecutiva, los pasos a seguir son los siguientes:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> N <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Escribir</b>
				  V[I]</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>				<p style="text-indent: 20; line-height: 150%" >Al
				variar el valor de I se escribe el elemento de V correspondiente a la posición
				indicada por I.</p>				<p style="text-indent: 20; line-height: 100%" >Para
				I = 1 , se escribe el valor de V[1]</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 2 , se escribe el valor de V[2]</p>
				<p style="text-indent: 20; line-height: 100%" >...</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= N , se escribe el valor de V[N]</p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.1.3.2
				Asignación</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				general no es posible asignar directamente un valor a </p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" > todo el arreglo; sino que
				se debe asignar el valor deseado a cada componente.  A continuación se analizan
				algunos ejemplos de asignación<span style="font-size:12.0pt;font-family:&quot;Century Gothic&quot;;
				mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
				mso-ansi-language:ES;mso-fareast-language:ES;mso-bidi-language:AR-SA"><span style="mso-spacerun: yes"></span>realizados
				en la estructura mostrada en la figura 2.6.</span></p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 20; line-height: 150%" >En
				los dos primeros casos se asigna un valor a una determinada casilla del arreglo
				(en el primero a la señalada por el índice ene, y en el segundo a la indicada
				por el índice mar).</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Caso
				Nº 1 : CICLO[ene]
				= 123.89</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Caso
				Nº 2 : CICLO[mar]
				= CICLO[ene]/2</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 20; line-height: 150%" >En
				el tercer caso se asigna el cero a todas las casillas del arreglo.</p>
				<p style="text-indent: 20; line-height: 100%" >Caso
				Nº 3 :;</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				MES = ene <b>Hasta</b> dic <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" >CICLO[MES]
				  = 0</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 200%" align="center"><b>Figura
				2.6.</b> Asignación en un arreglo</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.6.gif" width="291" height="73"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >En
				algunos lenguajes es posible asignar una variable tipo arreglo a otra
				exactamente del mismo tipo:</p>
				<p style="text-indent: 20; line-height: 150%" align="center">V1
				= V</p>
				<p style="text-indent: 20; line-height: 150%" >La
				expresión anterior es equivalente a:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 50 <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" >V<sub>1</sub>[I]
				  = V[I]</p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.4.1.3.3
				Actualización.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Resulta
				interesante que dado un arreglo, puedan insertarse nuevos elementos, eliminar
				y/o modificar algunos de los ya existentes. Para llevar a cabo estas operaciones
				eficientemente se debe tener en cuenta si el arreglo está o no ordenado, es
				decir, si sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> respetan algún orden (creciente o decreciente). Las
				operaciones de inserción, eliminación y modificación serán tratadas
				separadamente para arreglos ordenados y desordenados:</p>
				<p style="text-indent: 20; line-height: 150%" ><b>ARREGLOS
				DESORDENADOS:</b></p>
				<p style="text-indent: 20; line-height: 150%" >Considérese
				un arreglo V de 100 elementos como el presentado en la figura 2.7:</p>

				<p style="text-indent: 20; line-height: 150%" align="center"></p>

				<p style="text-indent: 20; line-height: 150%" align="center"><b>Figura
				2.7.</b> Actualización de arreglos desordenados</p>

				<p style="text-indent: 20; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.7.gif" width="269" height="57"></p>

				<p style="text-indent: 20; line-height: 150%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Es
				fácil observar que los primeros N <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> tienen asignado un valor.</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Inserción</b></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				insertar un elemento “Y” en un arreglo “V” desordenado debe verificarse
				que exista espacio, si se cumple esta condición, entonces se asignará a la
				posición N+1 el nuevo elemento. A continuación se presenta el algoritmo de
				inserción de arreglos desordenados</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><a href="a2.1">Algoritmo
				2.1. ;Inserta desordenado</a></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 20; line-height: 150%" >Luego
				de la inserción el arreglo V queda como se muestra en la figura 2.8:</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				2.8.</b> Inserción en arreglos desordenados</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.8.gif" width="311" height="64"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" ><b>Eliminación</b></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				eliminar un elemento X de un arreglo V desordenado debe verificarse que el
				arreglo no esté vacío y que X se encuentre en el arreglo. Si se cumplen estas
				condiciones entonces se procederá a recorrer todos los elementos que están a
				su derecha una posición a la izquierda, decrementando finalmente el número de
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo. A continuación se presenta el algoritmo de
				eliminación en arreglos desordenados.</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				2.9.</b> Eliminación en arreglos desordenados</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.9.gif" width="377" height="71"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p align="center" style="line-height: 200%; text-indent: 20"><span style="mso-bidi-font-size: 12.0pt"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:283.5pt;
				 height:54pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/02/clip_image001.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				</span><a href="a2.2">Algoritmo
				2.2. ;Elimina desordenado</a></p>

				<p align="center" style="line-height: 100%; text-indent: 20"></p>
				<p style="text-indent: 20; line-height: 150%" >Luego
				de la eliminación, el arreglo V queda como se muestra en la figura 2.10.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><b>Figura
				2.10.</b> Eliminación en arreglos desordenados</p>

				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"></p>

				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.10.gif" width="375" height="67"></p>

				<p align="right" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="line-height: 150%; text-indent: 20"><b>Modificación</b></p>
				<p  style="line-height: 150%; text-indent: 20">Para
				modificar un elemento X de un arreglo V desordenado, debe verificarse que el
				arreglo no esté vacío y que X se encuentre en el arreglo. Si se cumplen estas
				condiciones, entonces se procederá a su actualización. Puede observarse que
				existen tareas comunes con la operación de eliminación:</p>
				<ul>
				  <li>
				    <p  style="line-height: 150%; text-indent: 0">Determinar
				    que el arreglo no esté vacío.</li>
				</ul>
				<ul>
				  <li>
				    <p  style="line-height: 150%; text-indent: 0">Encontrar
				    el elemento a modificar (eliminar).</li>
				</ul>

				<p  style="line-height: 150%; text-indent: 20">A
				continuación se presenta el algoritmo de modificación en arreglos
				desordenados.</p>

				<p align="center" style="line-height: 150%; text-indent: 0"><a href="a2.3">Algoritmo
				2.3. ;Modifica desordenado</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Luego
				de la modificación, el arreglo V queda como se muestra en la figura 2.11.</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				2.11.</b> Modificación en arreglos desordenados</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.11.gif" width="313" height="65"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="line-height: 150%; tab-stops: 426.5pt 449.0pt; text-indent: 20"><span style="mso-bidi-font-size: 12.0pt"><!--[if gte vml 1]><v:shapetype id="_x0000_t75"
				 coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
				 filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:231pt;
				 height:42.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/02/clip_image001.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				 </span><b>ARREGLOS ORDENADOS</b></p>
				<p style="text-indent: 20; line-height: 150%" >Considérese
				un arreglo V de 100 elementos. Los primeros N <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del mismo tienen
				asignado un valor. En este caso se trabajará con un arreglo ordenado de manera
				creciente, es decir: V[1]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				V[2] <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				V[3] <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				... <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				V[N]</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				2.12.</b>Actualización de arreglos ordenados</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.12.gif" width="293" height="63"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Cuando
				se trabaja con arreglos ordenados debe evitarse alterar el orden al insertar
				nuevos elementos o al modificar los existentes.</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Inserción</b></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				insertar un elemento X en un arreglo V ordenado debe verificarse que exista
				espacio. Luego tendrá que encontrarse la posición en la que debería estar el
				nuevo valor para no alterar el orden del arreglo. Una vez detectada la posición,
				se procederá a recorrer todos los elementos desde la misma hasta la N-ésima
				posición, un lugar a la derecha. Finalmente se asignará el valor de X en la
				posición encontrada.</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 20; line-height: 150%" >Generalmente,
				cuando se quiere hacer una inserción debe verificarse que el elemento no se
				encuentre en el arreglo. En la mayoría de los casos prácticos no interesa
				tener información duplicada, por lo tanto si el valor a insertar ya estuviera
				en el arreglo, la operación no se llevaría a cabo.</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 20; line-height: 100%" >Antes
				de presentar el algoritmo de inserción, se; definirá; una; función;
				auxiliar que; se; utilizará; tanto en el proceso de</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" >inserción como en el de eliminación. Esta
				función busca un elemento X en un arreglo ordenado V.</p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 20; line-height: 150%" >Si
				el elemento X fue encontrado, la función dará como resultado la posición de
				dicho elemento en el arreglo, de lo contrario
				da como resultado el negativo de la posición en la que debería estar el
				elemento X para mantener el orden del arreglo V.</p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a2.4">Algoritmo
				2.4. Busca elemento</a></p>				<p style="text-indent: 20; line-height: 150%" >A
				continuación se trata el algoritmo de inserción en un arreglo ordenado:</p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a2.5">Algoritmo
				2.5. ;Inserta ordenado</a></p>				<p style="text-indent: 20; line-height: 150%" >Luego
				de la inserción, el arreglo queda como se muestra en la figura 2.13.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><b>Figura
				2.13.</b> Inserción en arreglos ordenados</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.13.gif" width="363" height="75"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 20; line-height: 150%" ><b>Eliminación</b></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				eliminar un elemento X de un arreglo ordenado V debe verificarse que el arreglo
				no esté vacío. Si se cumple esta condición entonces tendrá que buscarse la
				posición del elemento a eliminar.</p>				<p style="text-indent: 20; line-height: 150%" >Si
				el resultado de la función es un valor positivo, quiere decir que el elemento
				se encuentra en el arreglo y por lo tanto puede ser eliminado; en otro caso no
				se puede ejecutar la eliminación. A continuación se presenta el algoritmo de
				eliminación en arreglos ordenados.</p>				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.6">Algoritmo
				2.6. ;Elimina ordenado</a></p>

			</div>
		</div>
				<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Luego
				de la eliminación, el arreglo queda como se muestra en la figura 2.14.</p>				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.14.</b> Eliminación en arreglos ordenados</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.14.gif" width="334" height="66"></p>
				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>				<p style="text-indent: 20; line-height: 150%" ><span style="mso-bidi-font-size: 12.0pt">
				</span><b>Modificación</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				procede de manera similar a la modificación de un elemento en un arreglo
				desordenado. La variante se presenta en que al modificar el valor X por un valor
				Y, debe verificarse que el orden del arreglo no se altere. Si esto llegara a
				suceder, entonces podría rechazarse la operación o reordenar el arreglo.</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a2.7">Algoritmo
				2.7. ;Modifica ordenado</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>2.4.1.4
				Arreglos paralelos</b></p>
				<p style="text-indent: 20; line-height: 150%" >Por
				arreglos paralelos se entiende dos o más arreglos que utilizan el mismo
				subíndice para referirse a términos homólogos. Para ilustrar esta idea, a
				continuación se presentará un caso práctico y su solución, usando arreglos
				paralelos.</p>
				<p style="text-indent: 20; line-height: 150%" >Si
				se conoce el nombre y la calificación obtenida en un examen de un grupo de 30
				alumnos, y se quisieran usar estos datos para generar información, por ejemplo
				promedio del grupo, calificación más alta, nombre de los alumnos con
				calificación inferior al promedio, etc., se tendrían dos alternativas:</p>

				<p align="center" style="line-height: 150%; text-indent: 0"><b>Figura
				2.15.</b> Arreglos paralelos</p>
				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.15.gif" width="253" height="131"></p>

				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"></p>

				<p align="right" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">En
				este caso, a cada elemento del arreglo NOMBRES le corresponde uno del arreglo
				CALIFICACIÓN. Así, si se quiere hacer referencia a la calificación de
				NOMBRES[I], se utilizará CALIFICACIÓN [I].</p>				<p  style="line-height: 150%; text-indent: 20">Por
				ejemplo, si se hace referencia a NOMBRES[1] y CALIFICACIÓN [1], da como
				resultado que Alfonzo obtuvo una calificación de 9.5, y si recorre el arreglo
				hasta llegar a la última posición se obtendrán los siguientes resultados:</p>				<p  style="line-height: 150%; text-indent: 20">España
				obtuvo una calificación de 5.8</p>
				<p  style="line-height: 150%; text-indent: 20">Fernández
				obtuvo una calificación de 7.4</p>
				<p  style="line-height: 150%; text-indent: 20">.
				. .</p>
				<p  style="line-height: 150%; text-indent: 20">Zabala
				obtuvo una calificación de 10.0</p>				<p style="text-indent: 20; line-height: 150%" >A
				continuación se incluye un algoritmo para calcular el promedio del grupo e
				imprimir el nombre de los alumnos que tengan una calificación menor al
				promedio.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.8">Algoritmo
				2.8. <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos paralelos" v-bind:data-content="ArreglosParalelos">Arreglos paralelos</a></a></p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2
				Bidimensionales o Matrices</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-right: 0; margin-bottom: 0" >Son
				un tipo de datos estructurado formado por una colección finita, de tamaño fijo
				de elementos homogéneos ordenados. Su mecanismo de acceso es directo.</p>				<p style="text-indent: 20; line-height: 150%" >Un
				par de índices especifica el elemento deseado dando su posición relativa en la
				colección. Un arreglo bidimensional es la estructura de datos ideal para
				modelar datos que están estructurados de forma lógica como una tabla con
				renglones (filas) y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>.</p>				<p style="text-indent: 20; line-height: 150%" > La primera dimensión representa las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>.
				Cada elemento del arreglo contiene un valor y cada dimensión representa una
				relación.</p>				<p style="text-indent: 20; line-height: 150%" >Para
				entender mejor la estructura de los arreglos bidimensionales, se presenta el
				siguiente ejemplo.</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.1</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				tabla 2.1 contiene los costos de producción correspondientes a los primeros 6
				meses del año anterior, de cada departamento de una fábrica.</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>Tabla
				2.1.</b> Costos mensuales por departamentos (En
				Bolívares)</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/T2.1.gif" width="296" height="136"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"></p>
				<p style="text-indent: 20; line-height: 150%" >Es
				posible interpretar esta tabla de la siguiente manera: dado un mes, se conocen
				los costos de producción de cada uno de los departamentos de la fábrica; y
				dado un departamento, se conocen los costos de producción mensuales. Si
				se quisiera almacenar esta información con los tipos de datos que se conocen,
				se tendrían dos alternativas:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >1.-
				Definir 6 arreglos de 2 elementos cada uno. Cada arreglo almacenará la
				información relativa a un mes.</p>

				<p align="center" style="text-indent: 0; line-height: 150%"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><b>Figura
				2.16.</b> Almacenamiento de la información por mes</span></p>
				<p align="center" style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.16.gif" width="271" height="63"></p>
				<p align="right" style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p  style="text-indent: 20; line-height: 150%">Definir 2 arreglos de 6 elementos cada uno. Cada arreglo almacenará la
				información relativa a un departamento en los primeros 6 meses.</p>

				<p align="center" style="text-indent: 0; line-height: 200%"><b>Figura
				2.17.</b> Almacenamiento de la información por departamento</p>
				<p align="center" style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.17.gif" width="198" height="118"></p>

				<p align="right" style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 20; line-height: 150%">Sin
				embargo, adoptar alguna de las dos alternativas no resulta muy práctico. Se
				necesita una estructura que permita manejar los datos considerando los meses
				(renglones de la tabla), y los departamentos (<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> de la tabla), es decir,
				una estructura que trate a la información como un todo.</p>
				<p style="text-indent: 20; line-height: 200%" ></p>
				<p style="text-indent: 20; line-height: 150%" >La
				estructura que tiene esta característica se denomina arreglo bidimensional. Un
				arreglo bidimensional es un conjunto de datos homogéneo, finito y ordenado,
				donde se hace referencia a cada elemento por medio de dos índices. El primero
				de los índices se utiliza generalmente para indicar renglón (fila), y el
				segundo para indicar columna. Un arreglo bidimensional también puede definirse
				como un arreglo de arreglos.</p>
				<p style="text-indent: 20; line-height: 200%" ></p>
				<p style="text-indent: 20; line-height: 150%" >En
				la figura 2.18 se presenta gráficamente un arreglo bidimensional. El arreglo
				A(M x N) tiene M renglones y N <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>. Un elemento A[I, J] estará en el renglón
				I, y en la columna J. Internamente en memoria se reservan M x N posiciones
				consecutivas para almacenar todos los elementos del arreglo.</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.18.</b> Arreglo Bidimensional</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.18.gif" width="169" height="163"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 0; line-height: 100%" ><b>2.4.2.1
				Declaración de arreglos bidimensionales</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				declararán los arreglos bidimensionales especificando el número de renglones y
				el número de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>, junto con el tipo de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>.</p>				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/Arr3.gif" width="348" height="18"></p>				<p style="text-indent: 20; line-height: 150%" >Con
				Renglón y Columna se declara cuántos renglones y cuántas <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> tendrá el
				arreglo. Con tipo se declara el tipo de datos de todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del
				arreglo.</p>				
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >A
				su vez Renglón y Columna deben ser especificados con un límite superior e
				inferior, de la siguiente manera:</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>[líminfr..límsupr,
				líminfc..límsupc]</b></p>				<p style="text-indent: 20; line-height: 100%" >El
				número total de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> (NTC) está determinado por la expresión:</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>NTC
				= (límsupr - líminfr + 1) * (límsupc -líminfc + 1);;;;;;;;;
				(2.6)</b></p>				<p style="text-indent: 20; line-height: 150%" >Lo
				mismo que en el caso de los arreglos unidimensionales, los índices pueden ser
				cualquier tipo de dato ordinal (escalar, entero, carácter), mientras que los
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> pueden ser de cualquier tipo (reales, enteros, cadenas de
				caracteres, etc.).</p>				<p style="text-indent: 20; line-height: 150%" >A
				continuación se muestran algunos ejemplos de arreglos bidimensionales.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.2</b></p>
				<p style="text-indent: 20; line-height: 150%" >Sea
				MATRIZ un arreglo bidimensional de números enteros. Su
				representación queda como se muestra en la figura 2.19.</p>

				<p style="text-indent: 0; line-height: 200%" align="center">MATRIZ
				= Arreglo [1..6,1..5] de entero</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.19</b><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt">
				;
				</span></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.19.gif" width="169" height="136"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"></p>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >NTC
				    = (6 - 1 + 1) * (5 - 1 + 1) = 6 * 5 = 30</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Cada
				    componente de MATRIZ será un número entero.</li>
				</ul>

				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 20; line-height: 100%" >Para
				hacer referencia a cada uno de ellos se usarán; dos</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" >índices y el nombre de la
				variable tipo arreglo: MATRIZ[i, j]</p>
				<p style="text-indent: 20; line-height: 100%" >Donde:
				1 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				i <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				6;; 1 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£
				</span>j <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">
				£ 5</span></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" ><b>EJEMPLO
				2.3</b></p>
				<p style="text-indent: 20; line-height: 150%" >Sea
				GASTOS un arreglo bidimensional de números reales. Su representación queda
				como se muestra en la figura 2.20 </p>

				<p style="text-indent: 20; line-height: 150%" align="center">días
				= (lun, mar, mie, jue, vie, sab, dom)</p>
				<p style="text-indent: 20; line-height: 100%" align="center">coordinaciones
				= (Finanzas, Logística, Publicidad)</p>
				<p style="text-indent: 20; line-height: 100%" align="center">GASTOS
				= Arreglo[días, coordinaciones] de real</p>

				<p style="text-indent: 20; line-height: 100%" align="center"></p>

				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				2.20</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.20.gif" width="219" height="142"></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Cada
				    componente de GASTOS será un real.</li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Para
				hacer referencia a cada uno de ellos se usaran dos índices y el nombre de la
				variable tipo arreglo GASTOS [i ,j], donde:;
				lun <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				i <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				dom;; Finanzas <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				j <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				Publicidad</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" ><b>EJEMPLO
				2.4</b></p>
				<p style="text-indent: 20; line-height: 150%" >Sea
				CARACTER un arreglo bidimensional de cadenas de caracteres con índices para los
				renglones de tipo carácter y para las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> de tipo entero. Su
				representación queda como se muestra en la figura 2.21</p>
				<p style="text-indent: 0; line-height: 100%" align="center">CARACTER
				= Arreglo [‘a’ .. ‘z’,1 ... 10] de cadena</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.21</b></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.21.gif" width="166" height="154"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >NTC
				    = ( ord (‘z’) - ord(‘a’) + 1 ) * (10 - (1) + 1) = (122-97 + 1)*(9 +
				    1)= 26 * 10 =260</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Cada componente de CARÁCTER será un valor tipo cadena
				    de caracteres.</li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Para
				hacer referencia a cada uno de ellos se usarán dos índices y el nombre de la
				variable tipo arreglo: CARACTER[i , j],;</p>

				<p style="text-indent: 20; line-height: 100%" >Donde:
				‘a’ <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				i <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				‘z’; 1 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				j <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				10</p>

				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.5</b></p>
				<p style="text-indent: 20; line-height: 150%" >Sea
				NOMBRES un arreglo bidimensional de caracteres con índices enteros. Su
				representación queda como se muestra en la figura 2.22</p>				<p style="text-indent: 0; line-height: 150%" align="center">NOMBRES
				= Arreglo [ -6..-1, -3..3] de caracteres</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.22</b></p>
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.22.gif" width="164" height="128"></p>

				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >NTC
				    = (-1 - (-6) + 1) * (3 - (-3) + 1) = 6 * 7 = 42</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Cada
				    componente de NOMBRES será un valor tipo cadena de caracteres.</li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Para
				hacer referencia a cada uno de ellos se usarán dos índices y el nombre de la
				variable tipo arreglo: NOMBRES[i , j]</p>

				<p style="text-indent: 20; line-height: 150%" >Donde:
				-6 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				i <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				-1; -3 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				j <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				3</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" ><b>2.4.2.2
				Representación en memoria de arreglos;</b></p>

				<p style="text-indent: 45; line-height: 100%" ><b> bidimensionales</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				representación en memoria se realiza de dos formas: almacenamiento por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>
				o por renglones.</p>				<p style="text-indent: 20; line-height: 150%" >Para
				determinar el número total de elementos en un arreglo bidimensional se usarán
				las siguientes fórmulas:</p>
				<p style="text-indent: 0; line-height: 200%" align="center"></p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>RANGO
				DE RENGLONES (R1) = Ls1 - (Li1+1);;;;;;;;;
				(2.7)</b></p>				
				<p style="text-indent: 0; line-height: 100%" align="center"><b>RANGO
				DE COLUMNAS (R2) = Ls2 - (Li2+1);;;;;;;;;;
				(2.8)</b></p>				
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Nº
				TOTAL DE COMPONENTES = R1 * R2;;;;;;;;;;;;;;;;;;;
				(2.9)</b></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 200%" align="center"><b>REPRESENTACION
				EN MEMORIA POR COLUMNAS</b></p>
				<p style="text-indent: 0; line-height: 200%" align="center"></p>
				<p style="text-indent: 0; line-height: 200%" align="center"><b>Figura
				2.23.</b> Arreglo [1..5,1..7] de entero</p>
				<p style="text-indent: 0; line-height: 200%" align="center"><img border="0" src="contenido/capII/2.23.gif" width="181" height="135"></p>
				<p style="text-indent: 0; line-height: 200%" align="right"><img border="0" src="contenido/capII/F2.23.gif" align="right" width="359" height="10"></p>
				<p style="text-indent: 20; line-height: 200%" >Para
				calcular la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> de memoria de un elemento se utiliza la siguiente
				fórmula:</p>				<p style="text-indent: 0; line-height: 150%" align="center"><b>A[i,j]
				= base (A) + [((j - li2) R1 + (i + li1))*w];;;;;;;;;;;
				(2.10)</b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>REPRESENTACION
				EN MEMORIA POR RENGLONES</b></p>				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.24.</b> Arreglo [1..5,1..7] de entero</p>				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.24.gif" width="177" height="135"></p>

				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/F2.24.gif" align="right" width="359" height="10"></p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.3
				Operaciones con arreglos bidimensionales</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				general los arreglos bidimensionales son una generalización de los
				unidimensionales, por lo que se presentará una rápida revisión de las
				operaciones de los arreglos unidimensionales. Para ilustrarlas se utilizarán
				los ejemplos anteriores.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>2.4.2.3.1
				Lectura/Escritura</b></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Lectura</b></p>
				<p style="text-indent: 20; line-height: 150%" >Cuando
				se introdujo la lectura en arreglos unidimensionales se dijo que se iban
				asignando valores a cada uno de los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>. </p>

				<p style="text-indent: 20; line-height: 150%" >Lo
				mismo sucede con los arreglos bidimensionales. Sin embargo, como sus elementos
				deben referenciarse por medio de dos índices, normalmente se usan dos ciclos
				para lograr la lectura de elementos consecutivos. Supóngase
				que se desea leer todos los elementos del arreglo bidimensional MATRIZ.</p>

				<p style="text-indent: 20; line-height: 100%" >Los
				pasos a seguir son los siguientes:</p>
				<p style="text-indent: 20; line-height: 200%" ><b>Para</b>
				I = 1 <b>Hasta</b> 6 <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				  J = 1 <b>Hasta</b> 5 <b>Inc</b> [1]</p>
				  <blockquote>
				    <p style="text-indent: 20; line-height: 100%" ><b>Leer</b>
				    (MATRIZ[I, J])</p>
				  </blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Fin
				  Para</b></p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Al
				variar los índices de I y J cada elemento leído se asigna al correspondiente
				componente del arreglo, según la posición indicada por los índices I y J.</p>
				<p style="text-indent: 20; line-height: 150%" >Para:</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 1 y J = 1, se lee el elemento del renglón 1 y columna 1.</p>
				<p style="text-indent: 20; line-height: 100%" >...</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 6 y J = 5, se lee el elemento del renglón 6 y columna 5.</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Escritura</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				escritura de un arreglo bidimensional también se lleva a cabo elemento por
				elemento. Supóngase que se quiere escribir todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo
				MATRIZ. Los pasos a seguir son los que se muestran a continuación:</p>
				<p style="text-indent: 20; line-height: 150%" ><b>Para
				</b>I = 1 <b>Hasta</b> 6 <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				  J = 1 <b>Hasta</b> 5 <b>Inc</b> [1]</p>
				  <p style="text-indent: 20; line-height: 100%" >;;
				  <b>Escribir</b> (MATRIZ[I,J])</p>
				  <p style="text-indent: 20; line-height: 100%" ><b>Fin
				  Para</b></p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Al
				variar los valores de I y J se escribe el elemento de MATRIZ correspondiente a
				la posición indicada por los índices I y J.</p>

				<p style="text-indent: 0; line-height: 150%" >Para:</p>
				<p style="text-indent: 0; line-height: 150%" >I
				= 1 y J = 1, se escribe el elemento del renglón 1 y columna 1</p>
				<p style="text-indent: 0; line-height: 150%" >I
				= 2 y J = 2, se escribe el elemento del renglón 2 y columna 2</p>
				<p style="text-indent: 0; line-height: 200%" >...</p>
				<p style="text-indent: 0; line-height: 150%" >I
				= 6 y J = 5, se escribe el elemento del renglón 6 y columna 5</p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.3.2
				Asignación</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				asignación de valores a un arreglo bidimensional puede realizarse de dos
				maneras diferentes, según el número de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> involucrados:</p>				<p style="text-indent: 20; line-height: 150%" >A
				todos los elementos del arreglo: En este caso se necesitarán dos ciclos para
				recorrer todo el arreglo.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 6 <b>Inc</b> [1]</p>
				<blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				  J = 1<b>Hasta</b> 5 <b>Inc</b> [1]</p>
				  <blockquote>
				    <p style="text-indent: 20; line-height: 100%" >MATRIZ[I,J]
				    = 0</p>
				  </blockquote>
				  <p style="text-indent: 20; line-height: 100%" ><b>Fin
				  Para</b></p>
				</blockquote>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>
				<p style="text-indent: 20; line-height: 150%" >Al
				variar los valores de I y J se asigna el 0 al elemento de MATRIZ correspondiente
				a la posición indicada por los índices I y J.</p>

				<p style="text-indent: 20; line-height: 100%" >Para:</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 1 y J = 1, se asigna el valor 0 al elemento del renglón 1 y columna 1.</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 1 y J = 2, se asigna el valor 0 al elemento del renglón 1 y columna 2.</p>

				<p style="text-indent: 20; line-height: 100%" >...</p>
				<p style="text-indent: 20; line-height: 100%" >I
				= 6 y J = 5, se asigna el valor 0 al elemento del renglón 6 y columna 5.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >En
				la figura 2.25 se presenta cómo queda el arreglo bidimensional una vez asignado
				el valor 0 a cada una de las casillas:</p>
				<p style="text-indent: 20; line-height: 200%" align="center"><b>Figura
				2.25.</b> Asignación de arreglos</p>
				<p style="text-indent: 20; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.25.gif" width="155" height="130"></p>
				<p style="text-indent: 20; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="text-indent: 20; line-height: 150%"><span style="mso-bidi-font-size: 12.0pt"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:123pt;
				 height:103.5pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				</span>A
				un elemento en particular del arreglo: en este caso la asignación es directa.
				Por ejemplo, para asignar el valor 8 al elemento del renglón 2 y columna 5.</p>
				<p style="text-indent: 0; line-height: 100%" align="center">MATRIZ[2,5]
				= 8</p>
				<p style="text-indent: 20; line-height: 100%" >El
				arreglo queda como se muestra en la figura 2.26.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.26. </b>Asignación de arreglos</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.26.gif" width="155" height="130"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
				</span></p>
				<p  style="text-indent: 20; line-height: 150%"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:113.25pt;
				 height:98.25pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				 Es necesario aclarar que tanto las operaciones de lectura, escritura y asignación
				a todos los elementos de un arreglo bidimensional pueden hacerse tanto por
				renglones como por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>.</p>

				<p style="text-indent: 20; line-height: 150%" >Los
				lenguajes de programación pueden representar un arreglo bidimensional A, de m x
				n elementos, mediante un bloque de m x n posiciones sucesivas. Esta
				representación puede a su vez realizarse de dos formas diferentes: renglón a
				renglón, llamada también ordenación por renglones, y es la que utilizan la
				mayoría de los lenguajes de programación (BASIC, COBOL, PASCAL, etc.) o bien
				columna a columna, llamada también ordenación por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> y es la que utiliza</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >el
				lenguaje FORTRAN.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.4
				Matriz Poco Densa</b></p>
				<p style="text-indent: 20; line-height: 150%" >Matriz
				es un término matemático, utilizado para definir un conjunto de elementos
				organizados por medio de renglones y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>; equivalente al término arreglo
				bidimensional utilizado en computación. Se emplea este término aquí porque es
				el que más se utiliza con relación a arreglos bidimensionales poco densos.
				Poco denso indica una proporción muy alta de ceros entre los elementos de la
				matriz.</p>

				<p style="text-indent: 20; line-height: 100%" >Obsérvese la matriz A de 5 x 7 elementos, de la figura 2.27</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				2.27.</b> Matriz de 5 x 7</p>

				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.27.gif" width="129" height="87"></p>

				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"></p>

				<p align="right" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">Es
				fácil darse cuenta que esta matriz tiene una gran cantidad de ceros.
				El 71% de
				los elementos de la misma son ceros. ¿Qué ocurriría si en lugar de tener una
				matriz de 5 X 7, se tuviera una matriz de 500 X 800 y la mayor parte de sus
				elementos fueran iguales a cero? (con el porcentaje anterior y para este caso en
				particular, se tendrían 284.000 elementos iguales
				a cero). </p>

				<p style="text-indent: 20; line-height: 150%" > Indudablemente existiría una gran cantidad de espacio desperdiciado.
				Cuando suceden estos casos, lo más conveniente es almacenar en un arreglo
				unidimensional solamente los elementos distintos a cero.</p>

				<p style="text-indent: 20; line-height: 150%" > Cada elemento del
				arreglo puede ser un registro, donde se especifique el renglón, la columna y el
				valor del elemento distinto a cero de la matriz.</p>

				<p style="text-indent: 20; line-height: 150%" > En la tabla 2.2 se muestra la
				forma de almacenar los elementos de la matriz poco densa, presentada en la
				figura 2.27</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Tabla
				2.2.</b> Forma de almacenar los elementos de una matriz poco densa</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><span style="mso-bidi-font-size: 14.0pt"><b><img border="0" src="contenido/capII/T2.2.gif" width="165" height="179">
				</b></span></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p  style="text-indent: 20; line-height: 150%"></p>

				<p  style="text-indent: 20; line-height: 150%"><span style="mso-bidi-font-size: 12.0pt"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:128.25pt;
				 height:137.25pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="T2"/>
				</v:shape><![endif]--></span>Cabe
				aclarar que puede resultar muy conveniente también, almacenar el número de
				renglones y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> originales
				de la matriz. A continuación se presenta un algoritmo muy simple que carga en
				un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> los elementos distintos de cero de una matriz.</p>

				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a2.9">Algoritmo
				2.9. Carga</a></p>
				<p style="text-indent: 20; line-height: 150%" >Este
				tipo de matrices son matrices cuadradas que se dividen en los siguientes tipos:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.4.1
				Matriz Tridiagonal</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				dice que una matriz es tridiagonal si los elementos distintos de cero se
				encuentran en la diagonal principal o en las diagonales por encima o debajo de
				ésta. Por lo tanto, el valor absoluto del índice i menos el índice j será
				menor o igual que 1. En la figura 2.28 se puede observar una matriz tridiagonal.</p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.28.</b> Matriz tridiagonal</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.28.gif" width="120" height="82"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/F2.28.gif" align="right" width="364" height="15"></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea almacenar en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a>
				U (figura 2.29) la matriz tridiagonal presentada en la figura 2.28.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.29.</b> Almacenamiento de una matriz tridiagonal en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
				;
				</span></p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.29.gif" width="346" height="38"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="elabpropia.gif" align="right" width="150" height="11"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
				</span></p>
				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
				</span>Es
				fácil observar que el arreglo A tiene n elementos en la diagonal principal y (n
				- 1) elementos en las diagonales por encima y debajo de ésta. Por tanto, el
				número de elementos de una matriz tridiagonal se calcula como:</p>
				<p  style="line-height: 100%; text-indent: 20"></p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>n
				+ 2*(n – l);;;;;;;;;; (2.11)</b></p>				<p style="text-indent: 20; line-height: 100%" >Al
				hacer las operaciones queda:</p>				
				<p style="text-indent: 0; line-height: 100%" align="center"><b>3n-2;;;;;;;;;;;;;;;;;;;;
				(2.12)</b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.4.2
				Matriz Triangular</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				matrices cuadradas son aquellas que tienen igual número de renglones y de
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>. Ahora bien, las matrices cuadradas en las que los elementos que se
				encuentran arriba o debajo de la diagonal principal son iguales a cero, reciben
				el nombre de matrices triangulares, que a su vez puede ser Inferior o Superior.</p>
				<p style="text-indent: 0; line-height: 150%" ></p>
				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.4.2.1
				Matriz Triangular Inferior</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				llama matriz triangular inferior si los elementos iguales a cero se encuentran
				sobre la diagonal principal (figura 2.30).</p>				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.30.</b> Matriz triangular Inferior</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.30.gif" width="118" height="84"></p>
				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/F2.28.gif" align="right" width="364" height="15"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea almacenar en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B (figura 2.31) la matriz
				triangular inferior de la figura 2.30. </p>

				<p style="text-indent: 20; line-height: 100%" >Es
				fácil observar que el arreglo B tendrá:</p>
				<p style="text-indent: 0; line-height: 200%" align="center">1
				+ 2 + 3 + 4 + . . . + n</p>

				<p style="text-indent: 20; line-height: 100%" >Elementos,
				que es igual a:</p>				<p style="text-indent: 0; line-height: 200%" align="center"><img border="0" src="contenido/capII/f2.13.gif" width="146" height="33">;;;;</p>				<p style="text-indent: 20; line-height: 150%" >Por
				el principio de inducción matemática. Lo que resta de este capítulo y los
				siguientes, estarán apoyados en esta afirmación.</p>

			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.31.</b> Almacenamiento de una matriz triangular inferior en un arreglo
				unidimensional<span lang="ES-TRAD" style="font-size:10.0pt;mso-bidi-font-size:14.0pt;font-family:&quot;Century Gothic&quot;;
				mso-ansi-language:ES-TRAD">
				;
				</span></p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.31.gif" width="373" height="59"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.4.2.2
				Matriz Triangular Superior</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				considera matriz triangular superior si los elementos iguales a cero se
				encuentran debajo de la diagonal principal (figura 2.32).</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				2.32.</b> Matriz Triangular Superior</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.32.gif" width="127" height="79"></p>

				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/F2.28.gif" align="right" width="364" height="15"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Ahora
					bien, si se desea almacenar en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B (figura 2.33) la
					matriz triangular superior de la figura 2.32 , el arreglo B tendrá, como en el
					caso anterior:</p>
					
					<p style="text-indent: 0; line-height: 100%" align="center">n
					+ . . . + 4 + 3 + 2 + 1</p>

					<p style="text-indent: 20; line-height: 150%" >Elementos,
					que es igual a la fórmula 2.13, vista anteriormente.</p>

					<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
					2.33.</b> Almacenamiento de una matriz triangular superior en un arreglo
					unidimensional<span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
					</span></p>
					<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.33.gif" width="348" height="62"></p>

					<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

					<p style="text-indent: 0; line-height: 150%" ><b>2.4.2.5
					Matrices Simétricas y Antisimétricas</b></p>
					<p style="text-indent: 20; line-height: 150%" >Una
					matriz A de n x n elementos es simétrica si A[i,j] es igual a A[j,i], y esto se
					cumple para todo i y para todo j. </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" > A continuación se presentan dos ejemplos de
				matrices simétricas:</p>
				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				2.34. </b>Matriz simétrica de 4 x 4.</p>
				<p style="text-indent: 20; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.34.gif" width="94" height="81"></p>
				<p style="text-indent: 20; line-height: 100%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.35.</b> Matriz simétrica de 5 x 5</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="contenido/capII/2.35.gif" width="118" height="104"></p>

				<p style="text-indent: 0; line-height: 150%" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Por
				otra parte, una matriz A de n x n elementos es antisimétrica si A[i, j] es
				igual a -A[j, i], y esto se cumple para todo i y para todo j; considerando a i <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 10.0pt; mso-bidi-font-size: 14.0pt; font-family: Symbol; mso-ascii-font-family: Century Gothic; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Century Gothic; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES-TRAD; mso-fareast-language: ES; mso-bidi-language: AR-SA" lang="ES-TRAD">¹</span>
				j.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >A continuación el lector puede observar dos ejemplos de matrices
				antisimétricas.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.36. </b>Matriz antisimétrica de 4 x 4.</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.36.gif" width="96" height="83"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				2.37.</b> Matriz antisimétrica de 5 x 5</p>
				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/2.37.gif" width="120" height="101"></p>
				<p align="center" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"></p>
				<p align="right" style="line-height: 100%; text-indent: 0; word-spacing: 0; margin: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype id="_x0000_t75"
				 coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
				 filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:60.75pt;
				 height:80.25pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				 </span>Supóngase que se desea almacenar en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B la
				matriz simétrica de la figura, Esto puede hacerse almacenando solamente los
				elementos de la matriz triangular inferior o superior.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 2.38 se presenta un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B que almacena la matriz
				triangular inferior de la matriz simétrica mostrada en la figura 2.34.</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				2.38.</b> Almacenamiento de una matriz simétrica en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capII/2.38.gif" width="394" height="65"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="line-height: 100%; text-indent: 20"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shape id="_x0000_i1026"
				 type="#_x0000_t75" style='width:271.5pt;height:54pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image003.png"
				  o:title="2"/>
				</v:shape><![endif]-->
				</span></p>
				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">
				</span>Para
				localizar cualquier elemento de la matriz simétrica, debe aplicarse la fórmula
				presentada anteriormente, para matriz triangular inferior. Cabe aclarar, que si
				en determinado momento se necesitara localizar un elemento de la matriz
				simétrica tal que el índice j sea mayor que el índice i, necesitarían
				invertirse los mismos y aplicar la misma fórmula.</p>

				<p  style="line-height: 150%; text-indent: 20">Por ejemplo si se desea
				localizar el elemento A[l, 2], se tendrá que buscar el elemento A[2, 1].</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 20">Si
				ahora se desea almacenar en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B los elementos de la
				matriz antisimétrica de la figura 2.36, se procederá de la misma forma que en
				el caso anterior. Puede almacenarse en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a>, solamente los
				elementos de la matriz triangular inferior o superior.</p>

				<p style="text-indent: 20" > En la figura 2.39 se
				presenta un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> B que almacena la matriz triangular superior
				de la matriz antisimétrica de la figura 2.36.</p>				<p align="center" style="text-indent: 0; line-height: 100%"><b>Figura
				2.39.</b> Almacenamiento de una matriz antisimétrica en un arreglo
				unidimensional</p>
				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capII/2.39.gif" width="390" height="58"></p>

				<p align="right" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p  style="text-indent: 20; line-height: 150%">Para
				localizar en este caso un elemento de la matriz antisimétrica, debe aplicarse
				la fórmula presentada anteriormente para matriz triangular superior.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 20; line-height: 150%">Cabe
				aclarar que si se tuviera que localizar un elemento de la matriz antisimétrica,
				tal que el índice i sea mayor que el índice j, se necesitaría invertir los
				mismos y aplicar la misma fórmula. Posteriormente al contenido de la celda
				direccionada debe multiplicársele por -1. Por ejemplo si interesa localizar el
				elemento A[3,1], se buscará la posición del elemento A[l, 3] y el contenido de
				dicha posición se multiplicará por –1.</p>
				<p  style="text-indent: 0; line-height: 150%"><b>2.4.3
				Multidimensionales</b></p>
				<p  style="text-indent: 20; line-height: 150%">Un
				arreglo de N dimensiones es una colección de K1 x K2 x ... x KN elementos. Para
				hacer referencia a cada componente de
				un arreglo de N dimensiones,
				se usarán N índices (uno para cada dimensión). El número de dimensiones
				(índices) permitido depende del lenguaje elegido.</p>

				<p style="text-indent: 20; line-height: 150%" >Se
				definirá el arreglo A de N dimensiones de la siguiente manera:</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><b>A
				= Arreglo [LI1..LSt, LI2..LS2,. . ., LIN..LSN] de tipo</b></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >El
				total de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> de A será:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>NTC
				= (LS1 –LI1 + 1) * (LS2 -LI2 + 1) * . . . * (LSN - LIN + 1)
				(2.14)</b></p>				<p style="text-indent: 20; line-height: 150%" >Por
				ejemplo, el arreglo tridimensional A[1..3, 1..2, 1...3] tendrá:</p>
				<p style="text-indent: 0; line-height: 200%" align="center">(3
				- 1+1) * (2-1+1)* (3 -1+1) =3 * 2 * 3 = 18 elementos</p>

				<p style="text-indent: 20; line-height: 150%" >Gráficamente
				puede representarse el arreglo A como se muestra en las figuras 2.40 y 2.41:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.40.</b> Representación de arreglos de más de dos dimensiones</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capII/2.40.gif" width="199" height="84"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capII/F2.40.gif" align="right" width="200" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p align="center" style="text-indent: 0; line-height: 100%"><b>Figura
				2.41.</b> Representación de arreglos de más de dos dimensiones</p>
				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capII/2.41.gif" width="226" height="150"></p>
				<p align="right" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capII/F2.41.gif" align="right" width="200" height="11"></p>
				<p  style="text-indent: 20; line-height: 100%"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shape id="_x0000_i1026"
				 type="#_x0000_t75" style='width:180.75pt;height:119.25pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image003.png"
				  o:title="fig8" croptop="2798f"/>
				</v:shape><![endif]-->
				</span></p>

				<p  style="text-indent: 20; line-height: 150%">Un
				ejemplo del uso de arreglos tridimensionales para almacenar información es
				pertinente en esta parte.</p>

				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				2.6</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				empresa lleva un registro del total producido mensualmente por el departamento
				de compras. Dicho departamento consta de 5 oficinas y la información se ha
				registrado a lo largo de los últimos 4 años. Para almacenar los datos de la
				producción se definirá un arreglo de 3 dimensiones (5x12x4 = 240
				elementos), como el presentado en la figura 2.42</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" align="center">A
				= Arreglo [1..5, 1..12, 1..4] de real</p>

				<p style="text-indent: 20; line-height: 100%" align="center"></p>

				<p style="text-indent: 20; line-height: 100%" align="center"><b>Figura
				2.42</b></p>
				<p style="text-indent: 20; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.42.gif" width="219" height="106"></p>
				<p style="text-indent: 20; line-height: 100%" align="right"><img border="0" src="contenido/capII/F2.41.gif" align="right" width="200" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que la empresa necesita obtener la siguiente información:</p>

				<p style="text-indent: 20; line-height: 150%" >a)
				El total mensual de cada oficina durante el segundo año.</p>

				<p style="text-indent: 20; line-height: 150%" >Para obtener la
				información solicitada, deberán realizarse los siguientes pasos:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 5 <b>Inc</b> [1]</p>
				<p style="text-indent: 20; line-height: 100%" >;;
				<b>Para</b> J = 1 <b>Hasta</b> 12 <b>Inc</b> [1]</p>
				<p style="text-indent: 20; line-height: 100%" >;
				;;;; <b>Escribir</b> (A[I,J,2])</p>
				<p style="text-indent: 20; line-height: 100%" >;;
				<b>Fin Para</b></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>				<p style="text-indent: 20; line-height: 150%" >En
				este caso se asigna la constante 2 al tercer índice (correspondiente a los
				años) y se hace variar a los otros dos índices.</p>				<p style="text-indent: 20; line-height: 150%" >De esta manera se escribirán
				las producciones mensuales.</p>				<p style="text-indent: 20; line-height: 150%" >b)
				El total de la producción durante el primer año.</p>

				<p style="text-indent: 20; line-height: 150%" >Para obtener la información
				solicitada deberán realizarse los siguientes pasos:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" >SUMA
				= 0</p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" ><b>Para</b>
				I = 1 <b>Hasta</b> 5 <b>Inc</b> [1]</p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" ><b>;;
				Para</b> J = 1 <b>Hasta</b> 12 <b>Inc</b> [1]</p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" >;
				;;;;;;;; SUMA = SUMA + A[I,J,1]</p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" >;;
				<b>Fin Para</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" ><b>Fin
				Para</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-right: 0; margin-bottom: 0" ><b>Escribir</b>
				(SUMA)</p>

				<p style="text-indent: 20; line-height: 150%" >Este
				caso es similar al anterior. La diferencia radica en que las cantidades
				mensuales no se escribirán, sino que se acumularán obteniendo un total anual.</p>
				<p style="text-indent: 20; line-height: 150%" >c)
				El total de la producción de la oficina 3 a lo largo del último año.</p>

				<p style="text-indent: 20; line-height: 150%" >Para
				obtener la información solicitada será necesario ejecutar los siguientes
				pasos:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >SUMA
				= 0</p>
				<p style="text-indent: 20; line-height: 100%" >;;
				<b>Para</b> J =1 <b>Hasta</b> 12 <b>Inc</b> [1]</p>
				<p style="text-indent: 20; line-height: 100%" >;
				;;;; SUMA = SUMA + A[3,J,4]</p>
				<p style="text-indent: 20; line-height: 100%" >;;
				<b>Fin Para</b></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Escribir</b>
				(SUMA)</p>				<p style="text-indent: 20; line-height: 150%" >En
				este caso se tienen dos índices constantes, el de oficinas y el de años, y se
				hace variar solamente el índice de meses. Concluido el ciclo se escribirá el
				total producido por el departamento tres durante el cuarto año.</p>

				<p style="text-indent: 20; line-height: 150%" >Los
				lenguajes de programación de alto nivel pueden representar un arreglo A, de
				más de dos dimensiones de m1 x m2 x ... x mn elementos, mediante un bloque de
				m1 x m2 x ... x mn posiciones sucesivas.</p>

				<p style="text-indent: 20; line-height: 150%" >Esta
				representación, al igual que en el caso de arreglos bidimensionales, puede
				realizarse de dos formas diferentes: renglón a renglón, llamada también
				ordenación por renglones</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" > y columna a columna, llamada también ordenación por
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.43.</b> Arreglo Tridimensional</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.43.gif" width="366" height="224"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/F2.43.gif" align="right" width="212" height="17"></p>

				<p style="text-indent: 0; line-height: 100%" ><!--[if gte vml 1]><v:rect
				 id="_x0000_s1026" style='position:absolute;left:0;text-align:left;
				 margin-left:295pt;margin-top:189.8pt;width:15pt;height:13.6pt;z-index:1'
				 stroked="f">
				 <w:wrap anchorx="page"/>
				</v:rect><![endif]--><b>2.5
				Métodos de Ordenación</b></p>
				<p style="text-indent: 20; line-height: 150%" >Formalmente
				se define ordenación de la siguiente manera.</p>				<p style="text-indent: 20; line-height: 100%" >Sea
				A una lista de N elementos:</p>
				<p style="text-indent: 20; line-height: 100%" align="center">A1
				, A2 , A3 , . . . , An</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Ordenar
				significa permutar estos elementos de tal forma que los mismos queden de acuerdo
				con un orden preestablecido.</p>
				<p style="text-indent: 0; line-height: 150%" align="center">Ascendente:
				A<sub>1</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A<sub>2</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A<sub>3</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				. . . <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A<sub>n</sub></p>
				<p style="text-indent: 0; line-height: 150%" align="center">Descendente:
				A<sub>1</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A<sub>2</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A<sub>3</sub> <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				. . . <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A<sub>n</sub></p>				<p style="text-indent: 20; line-height: 150%" >La
				ordenación en arreglos recibe también el nombre de ordenación interna, ya que
				los elementos o <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo se encuentran en la memoria principal de
				la computadora. Ejemplificando, se puede mencionar que para la máquina, la
				ordenación interna, representa lo que para un humano significa ordenar un
				conjunto de tarjetas que se encuentran visibles y extendidas todas sobre una
				mesa.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				2.44.</b> Ordenación Interna</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/2.44.gif" width="122" height="106"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capII/F2.44.gif" align="right" width="219" height="16"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Los
				métodos de ordenación interna se explicarán con arreglos
				unidimensionales, pero su uso puede extenderse a otros tipos de arreglos. Por
				ejemplo: bidimensionales y tridimensionales, considerando el proceso de ordenación
				respecto a renglones y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> en el caso de arreglos bidimensionales y
				renglones, <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a> y páginas en el caso de arreglos tridimensionales. También
				debe señalarse que se trabajará con métodos de ordenación &quot;in
				situ&quot;, es decir, métodos que no requieren de arreglos auxiliares para su
				ordenación.</p>

				<p style="text-indent: 20; line-height: 150%" >Según
				Cairó y Guardatí (1993), los métodos de ordenación interna a su vez pueden
				ser clasificados en dos tipos:</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Métodos
				    directos (n<sup>2</sup>)</li>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Métodos
				    logarítmicos (n * log n)</li>
				</ul>

				<p style="text-indent: 20; line-height: 150%" >Los
				métodos directos tienen la característica de que sus programas son cortos y de
				fácil elaboración y comprensión, aunque son ineficientes cuando N (número
				de elementos del arreglo) es medio o grande.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" > Los métodos logarítmicos son más
				complejos que los métodos directos. </p>

				<p style="text-indent: 20; line-height: 150%" > Cierto es que requieren de menos
				comparaciones y movimientos para ordenar sus elementos, pero su elaboración y
				comprensión resulta más sofisticada y abstracta. </p>

				<p style="text-indent: 20; line-height: 150%" >Debe
				aclararse que una buena medida de eficiencia entre los distintos métodos lo
				presenta el tiempo de ejecución del algoritmo y éste depende fundamentalmente
				del número de comparaciones y movimientos que se realicen entre elementos.</p>
				<p style="text-indent: 20; line-height: 150%" >Como
				conclusión puede decirse que cuando N es pequeño deben utilizarse métodos
				directos y cuando N es medio o grande deben emplearse métodos logarítmicos.</p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.5.1
				Métodos de ordenación directos (n<sup>2</sup>)</b></p>
				<p style="text-indent: 20; line-height: 150%" >Los
				métodos directos más conocidos son:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>2.5.1.1
				Ordenación por intercambio directo (Burbuja)</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				método de intercambio directo, conocido coloquialmente con el nombre de la
				burbuja, es el más utilizado entre los estudiantes principiantes de
				computación, por su fácil comprensión y programación, pero es preciso
				señalar que es probablemente el método más ineficiente.</p>

				<p style="text-indent: 20; line-height: 150%" >El
				método de intercambio directo puede trabajar de dos maneras diferentes.
				Llevando los elementos más pequeños hacia la parte izquierda del arreglo o
				bien llevando los elementos más grandes hacia la parte derecha del mismo.</p>

				<p style="text-indent: 20; line-height: 150%" >La
				idea básica de este algoritmo consiste en comparar pares de elementos
				adyacentes e intercambiarlos entre sí hasta que todos se encuentren ordenados.
				Se realizan (n-1) pasadas, transportando en cada una de las mismas el menor o
				mayor elemento (según sea el caso) a su posición ideal.</p>

				<p style="text-indent: 20; line-height: 150%" >Al
				final de las (n-1) pasadas los elementos del arreglo estarán ordenados.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				2.7</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que desean ordenarse las siguientes claves del arreglo A, transportando en cada
				pasada el menor elemento hacia la parte izquierda del arreglo.</p>
				<p style="text-indent: 0; line-height: 200%" align="center"><b>A:
				10 62 02 08 43</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				comparaciones que se realizan son las siguientes:</p>				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (08 &gt; 43) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (02 &gt; 08) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (62&gt; 02) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (10 &gt; 02) sí hay intercambio</p>

				<p style="text-indent: 20; line-height: 150%" >Luego
				de la primera pasada el arreglo queda de la siguiente forma:</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				;;;02 10 62 08 43</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >El
				elemento más pequeño, en este caso 02, fue situado en la parte izquierda del
				arreglo.</p>				<p style="text-indent: 20; line-height: 100%" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (08 &gt; 43) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (62 &gt; 08) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (10 &gt; 08) ;;;sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (02 &gt; 08) no hay intercambio</p>				<p style="text-indent: 20; line-height: 150%" >Luego
				de la segunda pasada el arreglo queda de la siguiente forma:</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 62 43</p>

				<p style="text-indent: 20; line-height: 100%" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (62 &gt; 43) sí hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (10 &gt; 43) ;;no hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (08&gt; 10) no hay intercambio</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (02 &gt; 08) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>

				<p style="text-indent: 20; line-height: 100%" ><b>CUARTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (43 &gt; 62) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (10 &gt; 43) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (08 &gt; 10) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (02 &gt; 08) no hay intercambio</p>				<p style="text-indent: 20; line-height: 100%" >Aquí
				termina el proceso, y el arreglo queda de la siguiente manera:</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>

				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo de ordenación por el método de intercambio directo que transporta en
				cada pasada el menor elemento hacia la parte izquierda del arreglo es el
				siguiente:</p>

				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a2.10">Algoritmo
				2.10. Método burbuja1</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" ><b>EJEMPLO
				2.8</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea ordenar las siguientes claves del arreglo A transportando en cada
				pasada el mayor elemento hacia
				la parte derecha del arreglo.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>A:
				10 62 02 08 43</b></p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >Las
				comparaciones que se realizan son las siguientes:				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ><b>PRIMERA
				PASADA				</b></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[1]
				&gt; A[2]; (10 &gt; 62); no hay intercambio				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[2]
				&gt; A[3]; (62 &gt; 02); sí hay intercambio				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[3]
				&gt; A[4]; (62 &gt; 08); sí hay intercambio				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[4]
				&gt; A[5]; (62 &gt; 43); sí hay intercambio				</p>

				<p align="center" style="line-height: 150%; text-indent: 20; margin-left: 0; margin-right: 0">A:;;
				10;; 02; 08;; 43;;; 62</p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ><b>SEGUNDA
				PASADA
				 
				</b></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[1]
				&gt; A[2] (10 &gt; 02); sí hay intercambio
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[2]
				&gt; A[3] (10 &gt; 08); sí hay intercambio</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[3]
				&gt; A[4] (10 &gt; 43); no hay intercambio				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[4]
				&gt; A[5] (43 &gt; 62); no hay intercambio				</p>
				<p align="center" style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0">A:;;
				02;; 08;; 10;;; 43; ;;62				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" ><b>TERCERA
				PASADA				</b></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0; margin-right: 0" >A[1]
				&gt; A[2] (02; &gt; 08);no hay
				intercambio				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[2]
				&gt; A[3] (08; &gt; 10);no hay
				intercambio
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[3]
				&gt; A[4] (10 &gt; 43) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[4]
				&gt; A[5] (43 &gt; 62) no hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 150%" >Aquí
				termina el proceso, y el arreglo queda de la siguiente manera:</p>

				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>

				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo de ordenación por el método de intercambio directo que transporta en
				cada pasada el elemento mayor hacia la parte derecha del arreglo es el
				siguiente:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a2.11">Algoritmo
				2.11. Método Burbuja2</a></p>

				<p style="text-indent: 20; line-height: 150%" >A
				continuación se presentarán dos variantes del método de intercambio directo:
				método de intercambio directo con señal y método de shaker sort.</p>
				<p style="text-indent: 0; line-height: 150%" ><b>2.5.1.1.1
				Ordenación por el Método de Intercambio Directo con Señal</b></p>
				<p style="text-indent: 20; line-height: 150%" >Este
				método es una modificación del método de intercambio directo. La idea;
				central de este algoritmo consiste en utilizar una marca o señal para indicar que no se				ha producido ningún intercambio en una pasada.</p>

				<p style="text-indent: 20; line-height: 150%" >Es
				decir, se comprueba si el arreglo está totalmente ordenado después de cada
				pasada, terminando su ejecución en caso afirmativo. El algoritmo de ordenación
				por el método de la burbuja con señal es el siguiente:</p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a2.12">Algoritmo
				2.12. Método de Burbuja con Señal</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" ><b>2.5.1.1.2
				Ordenación por el Método de la Sacudida</b></p>
				<p style="text-indent: 60; line-height: 100%" ><b>(Shaker Sort)</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				método de Shaker Sort, más conocido en el mundo del habla hispana como el
				método de la sacudida, es una optimización del método de intercambio directo.
				La idea básica de este algoritmo consiste en mezclar las dos formas en que se
				puede realizar el método de la burbuja.</p>				<p style="text-indent: 20; line-height: 150%" >En
				este algoritmo cada pasada tiene dos etapas. En la primera etapa “de derecha a
				izquierda” se trasladan los elementos más pequeños hacia la parte izquierda
				del arreglo, almacenando en una variable la posición del último elemento
				intercambiado. En la segunda etapa “de izquierda a derecha” se trasladan los
				elementos más grandes hacia la parte derecha del arreglo, almacenando en otra
				variable, la posición del último elemento intercambiado.</p>

				<p style="text-indent: 20; line-height: 150%" >Las
				sucesivas pasadas trabajan con los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo comprendidos
				entre las posiciones almacenadas en las variables. El algoritmo
				termina cuando en una etapa no se producen intercambios o; bien,; cuando el; contenido;
				de; la</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >variable
				que almacena el extremo izquierdo del arreglo es mayor que el contenido de la
				variable que almacena el extremo derecho.</p>

				<p style="text-indent: 20; line-height: 150%" ><b>EJEMPLO
				2.9</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que desean ordenarse las siguientes claves del arreglo A utilizando el método
				de la sacudida.</p>
				<p style="text-indent: 0; line-height: 150%" align="center">A:
				10; 62; 02; 08; 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" ><i>Primera
				etapa (de derecha a izquierda)</i></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (08 &gt; 43) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (02 &gt; 08) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] ;(62 &gt; 02) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (10 &gt; 02) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 150%" align="center">A:
				02 10 62 08 43</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><i>Segunda
				etapa (de izquierda a derecha)</i></p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (10 &gt; 62) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (62 &gt; 08) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (62 &gt; 43) sí hay intercambio</p>

				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 10 08 62 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" ><i>Primera
				etapa (de derecha a izquierda)</i></p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (08 &gt; 62) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (10 &gt; 08) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (02 &gt; 08) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 62 43</p>				<p style="text-indent: 20; line-height: 100%" ><i>Segunda
				etapa (de izquierda a derecha)</i></p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&gt; A[4] (10 &gt; 62) no hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" >A[4]
				&gt; A[5] (62 &gt; 43) sí hay intercambio</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>

				<p style="text-indent: 20; line-height: 100%" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" ><i>Primera
				etapa (de derecha a izquierda)</i></p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&gt; A[3] (08 &gt; 10) no hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[1]
				&gt; A[2] (02 &gt; 08) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>				<p style="text-indent: 20; line-height: 150%" >Al
				realizar la primera etapa de la tercera pasada se observa que no se realizaron
				intercambios, por lo tanto la ejecución del algoritmo se termina. El algoritmo
				de ordenación por el método de la sacudida es el siguiente:</p>				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.13">Algoritmo
				2.13. Método de la sacudida</a></p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 100%" ><b>2.5.1.2
				Ordenación por inserción directa</b></p>
				<p style="text-indent: 20; line-height: 150%" >El;
				método; de; ordenación; por; inserción; directa es; el; que; generalmente; utilizan;
				los; jugadores; de; cartas; cuando</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" >ordenan éstas, de ahí que también se conozca
				con el nombre de método de la baraja.</p>

				<p style="text-indent: 20; line-height: 150%" >La
				idea central de este algoritmo consiste en insertar un elemento del arreglo en
				la parte izquierda del mismo, que ya se encuentra ordenada. Este proceso se
				repite desde el segundo hasta el n-ésimo elemento. Véase
				un ejemplo.</p>
				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				2.10</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea ordenar las siguientes claves del arreglo A utilizando el método
				de inserción directa:</p>
				<p style="text-indent: 0; line-height: 150%" align="center">A:
				10 62 02 08 43</p>				<p style="text-indent: 20; line-height: 100%" >Las
				comparaciones que se realizan son las siguientes:</p>				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&lt; A[1] (62 &lt; 10) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				10 62 02 08 43</p>				
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&lt; A[2] (02 &lt; 62) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&lt; A[1] (02 &lt; 10) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 10 62 08 43</p>

				<p style="text-indent: 20; line-height: 100%" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&lt; A[3] (08 &lt; 62) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&lt; A[2] (08 &lt; 10) sí hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" >A[2]
				&lt; A[1] (08 &lt; 02) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 62 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>CUARTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[5]
				&lt; A[4] (43 &lt; 62) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>

				<p style="text-indent: 20; line-height: 150%" >Obsérvese
				que una vez que se determina la posición correcta del elemento se interrumpen
				las comparaciones.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo de ordenación por el método de inserción directa es el siguiente:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.14">Algoritmo
				2.14. Inserción por el Método de Inserción Directa</a></p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 100%" ><b>2.5.1.3
				Ordenación por Inserción Binaria</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				método de ordenación por inserción directa puede mejorarse fácilmente. Para
				ello se recurre a una búsqueda binaria en lugar de una búsqueda secuencial
				para insertar un elemento en la parte izquierda del arreglo, que ya se encuentra
				ordenado. El proceso, al igual que en el método de inserción directa, se
				repite desde el segundo hasta el n-ésimo elemento. Véase un ejemplo.</p>

				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				2.11</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea ordenar las siguientes claves del arreglo A utilizando el método
				de inserción binaria.</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				10 62 02 08 43</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >Las
				comparaciones que se realizan son las siguientes:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[2]
				&lt; A[l] ;(62 &lt; 10) no hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[3]
				&lt; A[1] (02 &lt; 10) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 10 62 08 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&lt; A[2] (08 &lt; 10) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%" >A[4]
				&lt; A[1] (08 &lt; 02) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 62 43</p>

				<p style="text-indent: 20; line-height: 100%" ><b>CUARTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >A[5]
				&lt; A[2] (43 &lt; 08) no hay intercambio</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >A[5]
				&lt; A[3] (43 &lt; 10) no hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%" >A[5]
				&lt; A[4] (43 &lt; 62) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 43 62</p>				<p style="text-indent: 20; line-height: 100%" >El
				algoritmo de ordenación por el método de inserción binaria es el siguiente:</p>

				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a2.15">Algoritmo
				2.15. Ordenación por el Método de Inserción Binaria</a></p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 100%" ><b>2.5.1.4
				Ordenación por Selección Directa</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				método de ordenación por selección directa es más eficiente que los métodos
				analizados anteriormente. Pero, aunque su comportamiento es mejor que el de
				aquellos y su programación es fácil y comprensible, no es recomendable
				utilizarlo cuando el número de elementos del arreglo es medio o grande.</p>				<p style="text-indent: 20; line-height: 150%" >La idea
				básica de este algoritmo consiste en buscar el menor elemento del arreglo y
				colocarlo en la primera posición.</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Luego
				se busca el segundo elemento más pequeño del arreglo y se coloca en la segunda
				posición.</p>

				    <p style="text-indent: 20; line-height: 100%" >

				    <p style="text-indent: 20; line-height: 150%" >El
				    proceso continúa hasta que todos los elementos del arreglo hayan sido
				    ordenados. El método se basa en los siguientes principios:

				    <p style="text-indent: 20; line-height: 150%" >

				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Seleccionar
				    el menor elemento del arreglo.</li>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Intercambiar
				    dicho elemento con el primero.</li>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Repetir
				    los pasos anteriores con los (n-1), (n -2) elementos y así sucesivamente
				    hasta que sólo quede el elemento mayor.</li>
				</ol>

				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.12</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea ordenar las siguientes claves del arreglo A utilizando el método
				de selección directa.</p>
				<p style="text-indent: 0; line-height: 150%" align="center">A:
				10 62 02 08 43</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >Las
				comparaciones que se realizan son las siguientes:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[l] (10)</p>
				<p style="text-indent: 20; line-height: 100%" >(A[2]
				&lt; MENOR) (62 &lt; 10) no se cumple</p>
				<p style="text-indent: 20; line-height: 100%" >(A[3]
				&lt; MENOR) (02 &lt; 10) sí se cumple</p>

				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[3] (02)</p>

				<p style="text-indent: 20; line-height: 100%" >(A[4]
				&lt; MENOR) (08 &lt; 02) no se cumple</p>
				<p style="text-indent: 20; line-height: 100%" >(A[5]
				&lt; MENOR) (43 &lt; 02) no se cumple</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 62 10 08 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[2] (62)</p>
				<p style="text-indent: 20; line-height: 100%" >(A[3]
				&lt; MENOR) (10 &lt; 62) sí se cumple</p>

				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[3] (10)</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >(A[4]
				&lt; MENOR) (08 &lt; 10) sí se cumple</p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[4] (08)</p>
				<p style="text-indent: 20; line-height: 100%" >(A[5]
				&lt; MENOR) (43 &lt; 08) no se cumple</p>
				<p style="text-indent: 0; line-height: 100%" align="center">A:
				02 08 10 62 43</p>

				<p style="text-indent: 20; line-height: 100%" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[3] (10)</p>
				<p style="text-indent: 20; line-height: 100%" >(A[4]
				&lt; MENOR) (62 &lt; 10) no se cumple</p>

				<p style="text-indent: 20; line-height: 100%" >(A[5]
				&lt; MENOR) (43 &lt; 10) no se cumple</p>

				<p style="text-indent: 20; line-height: 100%" align="center">A:
				02 ;08 10 62 43</p>				<p style="text-indent: 20; line-height: 100%" ><b>CUARTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[4] (62)</p>
				<p style="text-indent: 20; line-height: 100%" >(A[5]
				&lt; MENOR) (43 &lt; 62) sí se cumple</p>
				<p style="text-indent: 20; line-height: 100%" >MENOR
				= A[5] (43)</p>

				<p style="text-indent: 20; line-height: 100%" align="center">A:
				02 08 10 43 62</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo de ordenación por el método de selección directa es el siguiente:</p>				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.16">Algoritmo
				2.16. Ordenación por el Método de Selección Directa</a></p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 150%" ><b>2.5.2
				Métodos Logarítmicos (n * log n)</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				los siguientes incisos se analizarán los métodos logarítmicos más
				importantes:</p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>2.5.2.1
				Ordenación por el método de Shell</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				método de Shell es una versión mejorada del método de inserción directa.</p>

				<p style="text-indent: 20; line-height: 150%" >Recibe
				ese nombre en honor a su autor Donald L. Shell quien lo propuso en 1959. Este método también se conoce con el nombre de inserción con incrementos
				decrecientes.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >En
				el método de ordenación por inserción directa cada elemento se compara para
				su ubicación correcta en el arreglo, con los elementos que se encuentran en la
				parte izquierda del mismo. Si el elemento a insertar es más pequeño que el
				grupo de elementos que se encuentran a su izquierda,
				es necesario efectuar entonces varias comparaciones antes de su ubicación. </p>

				<p style="text-indent: 20; line-height: 150%" >Shell
				propone que las comparaciones entre elementos se efectúen con saltos de mayor
				tamaño pero con incrementos decrecientes, así, los elementos quedarán
				ordenados en el arreglo más rápidamente. Para comprender mejor este algoritmo
				analícese el siguiente caso.</p>
				<p style="text-indent: 20; line-height: 150%" > </p>

				<p style="text-indent: 20; line-height: 150%" >Considérese
				un arreglo que contenga 16 elementos. En primer lugar se dividirán los
				elementos del arreglo en ocho grupos teniendo en cuenta los elementos que se
				encuentran a ocho posiciones de distancia entre sí y se ordenarán por
				separado. Quedarán en el primer grupo los elementos (A[l], A[9]); en el
				segundo, (A[2], A[10]); en el tercero, (A[3], A[11]), y así sucesivamente.   Después de este primer paso se dividirán los elementos del arreglo en cuatro;
				grupos,; teniendo; en; cuenta </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" > ahora los elementos que se encuentren a cuatro
				posiciones de distancia entre sí y se los ordenará por separado. Quedarán en
				el primer grupo los elementos (A[l], A[5], A[9], A[13]); en el segundo, (A[2],
				A[6], A[10], A[14]), y así sucesivamente. </p>

				<p style="text-indent: 20; line-height: 100%" > ;</p>

				<p style="text-indent: 20; line-height: 150%" > En el tercer paso se dividirán los
				elementos del arreglo en grupos, tomando en 
				cuenta
				los elementos que se encuentran ahora a dos posiciones de distancia entre sí y
				nuevamente se les ordenará por separado. En el primer grupo quedarán (A[l],
				A[3], A[5], A[7], A[9], A[11], A[13], A[15]) y en el segundo grupo (A[2], A[4],
				A[6], A[8], A[10], A[12], A[14], A[16]).</p>

				<p style="text-indent: 20; line-height: 150%" >Finalmente
				se agruparán y ordenarán los elementos de manera normal, es decir, de uno en
				uno. Véase el siguiente ejemplo.</p>				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.13.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se desea ordenar los elementos que se encuentran en el arreglo A utilizando
				el método de Shell:</p>
				<p style="text-indent: 0; line-height: 150%" align="center">A:
				10 62 02 08 43</p>				
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%" >(A[1]
				&gt; A[4]) (10 &gt; 08) sí se cumple la condición</p>
				<p style="text-indent: 20; line-height: 100%" >(A[2]
				&gt; A[5]) (62 &gt; 43) sí se cumple la condición</p>

				<p style="text-indent: 0; line-height: 100%" align="center">A:
				08 43 02 10 62</p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><b>SEGUNDA PASADA</b></span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[1] &gt;
				A[4]) (08 &gt; 10) no se cumple la condición</span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[2] &gt;
				A[5]) (43 &gt; 62) no se cumple la condición</span></p>

				<p style="line-height: 100%; text-indent: 0; margin-left: 0" align="center"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">A:<span style="mso-spacerun:
				yes">; </span>08<span style="mso-spacerun: yes">;; </span>43<span style="mso-spacerun: yes">;;
				</span>02<span style="mso-spacerun: yes">;; </span>10<span style="mso-spacerun: yes">;;
				</span>62</span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><b>TERCERA PASADA</b></span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[1] &gt;
				A[2]) (08 &gt; 43) no se cumple la condición</span></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[2] &gt;
				A[3]) (43 &gt; 02) sí se cumple la condición</span></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[3] &gt;
				A[4]) (43 &gt; 10) sí se cumple la condición</span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[4]
				&gt; A[5]) (43 &gt; 62) no se cumple la condición</span></p>
				<p style="line-height: 100%; text-indent: 0; margin-left: 0" align="center"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">A:<span style="mso-spacerun:
				yes">; </span>08<span style="mso-spacerun: yes">;; </span>02<span style="mso-spacerun: yes">;;
				</span>10<span style="mso-spacerun: yes">;; </span>43<span style="mso-spacerun: yes">;;
				</span>62</span></p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><b>CUARTA
				PASADA</b></span></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[1] &gt;
				A[2]) (08 &gt; 02) sí se cumple la condición
				</span></p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[2] &gt;
				A[3]) (08 &gt; 10) no se cumple la condición</span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[3]
				&gt; A[4]) (10 &gt; 43) no se cumple la condición</span></p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">(A[4]
				&gt; A[5]) (43 &gt; 62) no se cumple la condición</span></p>
				<p style="line-height: 100%; text-indent: 0; margin-left: 0" align="center"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">A:<span style="mso-spacerun:
				yes">; </span>02<span style="mso-spacerun: yes">;; </span>08<span style="mso-spacerun: yes">;;
				</span>10<span style="mso-spacerun: yes"> ;;</span>43<span style="mso-spacerun:
				yes">;; </span>62</span></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ><b>QUINTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >(A[1]
				&gt; A[2]) (02 &gt; 08) no se cumple la condición</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >(A[2]
				&gt; A[3]) (08 &gt; 10) no se cumple la condición</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >(A[3]
				&gt; A[4]) (10 &gt; 43) no se cumple la condición</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >(A[4]
				&gt; A[5]) (43 &gt; 62) no se cumple la condición</p>

				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:;;;
				08; ; 02;;; 10;;;; 43;;;;
				62</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >A
				continuación se presenta el algoritmo por el método de Shell.</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center"><a href="a2.17">Algoritmo
				2.17. Método de Shell</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" ><b>2.5.2.2
				Ordenación por el Método Quicksort</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >El
				método de ordenación quicksort es actualmente el más eficiente y veloz de los
				métodos de ordenación interna. Es también conocido con el nombre de método rápido
				y de ordenación por partición, en el mundo de habla hispana. Este método es
				una mejora sustancial del método de intercambio directo y recibe el nombre de
				Quicksort por la velocidad con que ordena los elementos del
				arreglo. Su autor C. A. Hoare lo bautizó así.</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >La
				idea central de este algoritmo consiste en lo siguiente:</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" >Se
				    toma un elemento X de una posición cualquiera del arreglo.</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" >Se
				    trata de ubicar a X en la posición correcta del arreglo, de tal forma que
				    todos los elementos que se encuentren a su izquierda sean menores o iguales
				    a X y todos los elementos que se encuentren a su derecha sean mayores o
				    iguales a X.</li>
				</ol>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ol start="3" style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" >Se
				    repiten los pasos anteriores pero ahora para los conjuntos de datos que se
				    encuentran a la izquierda y a la derecha de la posición correcta de X en el
				    arreglo.</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; margin-left: 0; margin-right: 0" >El
				    proceso termina cuando todos los elementos se encuentran en su posición
				    correcta en el arreglo.</li>
				</ol>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >Véase
				el siguiente ejemplo.</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" ><b>EJEMPLO
				2.14.</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0; margin-right: 0" >Supóngase
				que se desea ordenar los elementos que se encuentran en el arreglo A utilizando
				el método quicksort.</p>

				<p align="center" style="line-height: 100%">A: 10 62 02 08 43</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ><b>PRIMERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[1]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[5] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				43) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[1]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[4] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				08) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:
				08 62 02 10 43</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[4]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A[1] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				08) sí hay intercambio</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[4]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A[2] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				62) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:
				08 10 02 62 43</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ><b>SEGUNDA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[2]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[4] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				62) sí hay intercambio</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[2]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[3] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				02) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:
				08 02 10 62 43</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[3]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A[2] (10 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				02) sí hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:
				08 02 10 62 43</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ></p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" ><b>TERCERA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[4]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[5] (62 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				43) no hay intercambio</p>

				<p style="text-indent: 0; line-height: 100%; margin-left: 0; margin-right: 0" align="center">A:
				08 02 10 43 62</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0; margin-right: 0" >A[5]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				A[4] (62 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">³</span>
				43) sí hay intercambio</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ><b>CUARTA
				PASADA</b></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >A[1]
				<span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				A[2] (08 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">£</span>
				02) no hay intercambio</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0" align="center">A:
				02 08 10 43 62</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Este
				proceso de particionamiento aplicado para localizar la posición correcta de un
				elemento X en el arreglo se repite cada vez que queden conjuntos formados por
				dos o más elementos. Se puede realizar de manera iterativa o recursiva.</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Se
				utilizará el algoritmo de ordenación de quicksort aplicando iteratividad, por
				medio del uso de pilas. Deben almacenarse en las pilas los índices de los dos
				conjuntos de datos que falta tratar. Se utilizarán dos pilas, PILAMENOR y
				PILAMAYOR. En la primera, se almacenará el extremo izquierdo y en la otra se
				almacenará el extremo derecho de los conjuntos de datos que falta tratar.</p>
				<p style="text-indent: 20; line-height: 150%" >Véase
				ahora el algoritmo de ordenación del quicksort utilizando iteratividad.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.18">Algoritmo
				2.18. Método de Ordenación del Quicksort utilizando Iteratividad</a></p>

				<p style="text-indent: 20; line-height: 150%" >Nótese
				que el algoritmo Método de ordenación del Quicksort utilizando iteratividad
				necesita para su funcionamiento de otro algoritmo, que se presenta a continuación:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a2.19">Algoritmo
				2.19. Reduceiterativo</a></p>				<p style="text-indent: 20; line-height: 150%" >Una
				leve mejora en el funcionamiento del método rápido puede producirse si el
				primer elemento posicionado en el arreglo, se encuentra en la mitad o muy próximo
				a la mitad del mismo.</p>

				<p style="text-indent: 20; line-height: 150%" >Es decir, se permutan los elementos del arreglo de tal
				forma que para el elemento X, todos los elementos que se encuentren a su
				izquierda desde A[l] hasta A[i], donde i es igual a ((n/2) - 1), sean menores o
				iguales a él y todos los elementos que se encuentren a su derecha desde A[i +
				1] hasta A[N] sean mayores o iguales a él.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>2.6
				Métodos de Búsqueda</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				operación de búsqueda permite recuperar datos; previamente;
				almacenados. El; resultado; que; puede; arrojar
				esta operación es éxito, si se encuentra
				el elemento buscado, o fracaso, en otras circunstancias.</p>

				<p style="text-indent: 20; line-height: 150%" >La
				operación de búsqueda puede llevarse a cabo sobre elementos ordenados y sobre
				elementos desordenados. En el primer caso, la búsqueda se facilita, y por lo
				tanto se ocupará menos tiempo que si se trabaja con elementos desordenados.</p>				<p style="text-indent: 20; line-height: 150%" >Se
				denomina búsqueda interna cuando todos los elementos se encuentran en memoria
				principal, por ejemplo, almacenados en arreglos. Los
				métodos de búsqueda interna más importantes son:</p>
				<p style="text-indent: 0; line-height: 150%" ><b>2.6.1
				Secuencial o Lineal</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				búsqueda secuencial consiste en revisar elemento por elemento hasta encontrar
				el dato buscado, o hasta llegar al final de la lista de datos disponibles.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Cuando
				se habla de búsqueda en arreglos debe distinguirse entre arreglos desordenados
				y arreglos ordenados. La búsqueda secuencial en arreglos desordenados consiste,
				básicamente, en recorrer el arreglo de izquierda a derecha hasta que se
				encuentre el elemento buscado o se termine el arreglo, lo que ocurra primero.
				Normalmente cuando una función de búsqueda concluye con éxito,; interesa;
				conocer en qué posición fue
				hallado el elemento buscado. Esta idea puede generalizarse para todos los métodos
				de búsqueda.</p>				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo de búsqueda secuencial en arreglos desordenados es el que se explica
				a continuación.</p>
				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a2.20">Algoritmo
				2.20. Búsqueda secuencial en arreglos desordenados</a></p>				<p style="text-indent: 20; line-height: 150%" >Son
				dos los posibles resultados a obtener por este algoritmo: la posición en la que
				encontró al elemento, o un mensaje de fracaso si el elemento no se halla en el
				arreglo. Si hubiera dos o más ocurrencias del mismo valor, se encuentra la
				primera de ellas. Sin embargo, es posible modificar el algoritmo para obtener
				todas las ocurrencias del dato buscado.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >El
				algoritmo 2.20 también recibe el nombre de secuencial con bandera por utilizar
				la variable auxiliar booleana en la condición de parada del ciclo. El empleo de
				esta variable evita seguir buscando una vez que el dato ha sido encontrado, con
				la ventaja de que disminuye el número de comparaciones a realizar y por lo
				tanto aumenta la eficiencia del algoritmo.</p>
				<p style="text-indent: 20; line-height: 150%" >La
				búsqueda secuencial en arreglos ordenados es similar al caso anterior. Sin
				embargo, el orden que existe entre los elementos del arreglo permite incluir una
				nueva condición que hace más
				eficiente al proceso. Estúdiese ahora el algoritmo de búsqueda secuencial en
				arreglos ordenados.</p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a2.21">Algoritmo
				2.21. Búsqueda secuencial en arreglos ordenados</a></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				el arreglo está ordenado, se impone una nueva condición para controlar la búsqueda:
				el elemento a encontrar debe ser mayor o igual que el elemento en curso para
				proseguir con la búsqueda. </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Cuando el valor buscado no esté en el arreglo y sea
				un valor intermedio, V[1] &lt; X &lt; V[N], se detendrá
				la búsqueda al dejar de cumplirse la condición X ³ V[I]. Si X no se encontró
				hasta el momento, tampoco se encontrará entre los restantes elementos.
				El
				número de comparaciones determina la complejidad de los métodos de búsqueda.</p>

				<p style="text-indent: 0; line-height: 150%; word-spacing: 0; margin-left: 0; margin-top: 0; margin-bottom: 0" ><b>2.6.2
				Binaria</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				búsqueda binaria consiste en dividir el intervalo de búsqueda en dos partes,
				comparando el elemento buscado con el central. En caso de no ser iguales se
				redefinen los extremos del intervalo (según el elemento central sea mayor o
				menor que el buscado) disminuyendo el espacio de búsqueda. El proceso concluye
				cuando el elemento es encontrado, o bien cuando el intervalo de búsqueda se
				anula. Este método funciona exclusivamente para arreglos ordenados.</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-top: 0; margin-bottom: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Con
				cada iteración del método el espacio de búsqueda se reduce a la mitad, por lo
				tanto el número de comparaciones a realizar disminuye notablemente. Esta
				disminución resulta significativa cuanto más grande sea el tamaño del
				arreglo. A continuación se presenta el algoritmo de búsqueda binaria.
				</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p align="center" style="line-height: 150%; text-indent: 0; margin-left: 0"><a href="a2.22">Algoritmo
				2.22. Algoritmo de búsqueda binaria</a></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Véase
				ahora un ejemplo para ilustrar el funcionamiento del algoritmo 2.22.
				</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" >				</p>
				<p style="text-indent: 0; line-height: 150%; margin-left: 0" ><b>EJEMPLO
				2.15.
				</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Sea
				V un arreglo de números enteros, ordenado de manera creciente como se muestra
				en la figura 2.45
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >				</p>
				<p align="center" style="line-height: 150%; text-indent: 20; margin-left: 0"><b>Figura
				2.45.</b> Arreglo de Números Enteros</p>
				<p align="center" style="line-height: 150%; text-indent: 0; margin-left: 0"><img border="0" src="contenido/capII/2.45.gif" width="198" height="48"></p>
				<p align="right" style="line-height: 150%; text-indent: 0; margin-left: 0"><img border="0" src="contenido/capII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="line-height: 100%; text-indent: 20; margin-left: 0"></p>

				<p  style="line-height: 150%; text-indent: 20; margin-left: 0">En
				la tabla 2.3 se presenta el seguimiento del algoritmo 2.22 para X = 43.
				</p>				
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" align="center"><b>Tabla
				2.3.</b> Búsqueda binaria
				</p>
				<p align="center" style="text-indent: 0; line-height: 100%; margin-left: 0"><!--[if gte vml 1]><v:shape id="_x0000_i1025"
				 type="#_x0000_t75" style='width:247.5pt;height:74.25pt'>
				 <v:imagedata src="C:/WINDOWS/TEMP/msoclip1/01/clip_image003.png"
				  o:title="T2.3"/>
				</v:shape><![endif]--><img border="0" src="contenido/capII/T2.3.gif" width="399" height="70">
				</p>

				<p align="right" style="text-indent: 0; line-height: 100%; margin-left: 0"><img border="0" src="elabpropia.gif" align="right" width="150" height="11">
				</p>

				<p  style="text-indent: 0; line-height: 150%; margin-left: 0">
				<b> 2.6.3
				Por transformación de claves				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Los
				dos métodos analizados anteriormente permiten encontrar un elemento en un
				arreglo, pero en ambos el tiempo de búsqueda es proporcional al número de
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del mismo. Es decir, que a un mayor número de elementos se debe
				realizar un mayor número de comparaciones. Además se mencionó que si bien el
				método de búsqueda binaria es más eficiente que el secuencial, tiene la
				restricción de que el arreglo debe estar ordenado.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Esta
				sección se dedica a un nuevo método de búsqueda. Este método, llamado por
				transformación de claves (hash), permite aumentar la velocidad de búsqueda sin
				necesidad de tener los elementos ordenados.
				</p>		
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Cuenta también con la ventaja de
				que el tiempo de búsqueda es prácticamente independiente del número de
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo.
				</p>

				<p style="text-indent: 20; line-height: 150%" >Supóngase
				que se tiene una colección de datos, cada uno de ellos identificado por una
				clave. Es claro que resulta atractivo poder tener acceso a ellos de manera
				directa (sin tener que recorrer algunos datos antes de llegar al buscado). El método
				por transformación de claves permite esto.
				</p>

				<p style="text-indent: 20; line-height: 150%" >Trabaja basándose en una función
				de transformación o función hash (H) que convierte una clave dada en una
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> (índice) dentro del arreglo.
				</p>				<p align="center" style="line-height: 150%; text-indent: 20">dirección
				&lt;-- H(clave)
				</p>
				<p style="line-height: 150%; text-indent: 20" >				</p>
				<p style="text-indent: 20; line-height: 150%" >La
				función hash aplicada a la clave da un índice del arreglo, lo que permite
				accesar directamente sus elementos. El caso más trivial se presenta cuando las
				claves son números enteros consecutivos.
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Supóngase que se desea almacenar la
				información relacionada a 100 alumnos cuyas matrículas son números del 1 al
				100.En este caso debe definirse un arreglo de 100 elementos con índices numéricos
				comprendidos entre los valores 1 y 100. Los datos de cada alumno ocuparán la
				posición del arreglo que se corresponda con el número de la matrícula, de
				esta manera se podrá accesar directamente la información de cada alumno. Supóngase
				ahora que se desea almacenar la información de 100 empleados.</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La clave de cada
				empleado será su número del seguro social. Si la clave está formada por 11 dígitos,
				resulta por completo ineficiente definir un arreglo con 99999999999 elementos
				para almacenar solamente los datos de 100 empleados. Utilizar un arreglo tan
				grande asegura la posibilidad de accesar directamente sus elementos, sin
				embargo, el costo en memoria es excesivo. Siempre debe equilibrarse el costo por
				espacio de memoria con el costo por tiempo de búsqueda.
				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Cuando
				se tienen claves que no se corresponden con índices (por ejemplo, por ser
				alfanuméricas), o bien cuando las
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" >claves
				son valores numéricos muy grandes, debe utilizarse una función hash que permita transformar la clave para obtener
				la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> apropiada.Esta función hash debe ser simple de calcular y debe
				asignar direcciones de la manera más uniforme posible, es decir, dadas dos
				claves diferentes debe generar posiciones diferentes. Si esto no ocurre (H(K1) =
				d, H(K2) = d y K1 ¹; K2), hay una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a>.</p>				<p style="text-indent: 20; line-height: 150%" >Se
				define, entonces, una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a> como la asignación de una misma <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> a
				dos o más claves distintas.
				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p style="text-indent: 20; line-height: 150%" >Por
				todo lo mencionado, para trabajar con este método de búsqueda debe elegirse
				previamente:
				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; mso-list: l60 level2 lfo89; tab-stops: list 26.5pt; margin-left: 0" >Una función hash que sea fácil de calcular y que distribuya uniformemente las
				claves.
				</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; mso-list: l60 level2 lfo89; tab-stops: list 26.5pt; margin-left: 0" >Un método para resolver colisiones. Si éstas se presentan se debe contar con
				algún método que genere posiciones alternativas.
				</li>
				</ul>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 200%; margin-left: 0" >Se
				tratará sobre estos dos aspectos separadamente.</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ><b>Funciones
				Hash</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Como
				ya se ha mencionado, seleccionar una buena función hash es importante, pero
				también es difícil. No hay reglas que permitan determinar cuál será la función
				más apropiada para un conjunto de claves, de tal manera que asegure la máxima
				uniformidad en la distribución de las mismas. Hacer un análisis de las
				principales características de las claves, puede ayudar en la elección de la
				función hash. Algunas de las funciones hash más utilizadas se detallan abajo.
				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ><b>Función
				Módulo (por división)
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Consiste
				en tomar el residuo de la división de la clave entre el número de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>
				del arreglo. Supóngase que se tiene un arreglo de N elementos, y sea K la clave
				del dato a buscar. La función hash queda definida por la siguiente fórmula:
				</p>
				<p align="center" style="line-height: 100%; text-indent: 20; margin-left: 0"><b>
				</b>
				</p>
				<p align="center" style="line-height: 100%; text-indent: 20; margin-left: 0"><b>H(k)
				= ( K mod N ) + 1;;;;; (2.15)
				 </b>
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >En
				la fórmula anterior puede observarse que al residuo de la división se le suma
				1, esto es para obtener un valor entre 1 y N. Para lograr una mayor uniformidad
				en la distribución, N debe ser un número primo o divisible por muy pocos números.
				Por lo tanto, dado N, si éste no es un número primo se tomará el valor primo
				más cercano. El ejemplo 2.16 presenta un caso de función hash módulo.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>
				<p style="text-indent: 0; line-height: 150%; margin-left: 0" ><b>EJEMPLO
				2.16.
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Sean
				N = 20 el tamaño del arreglo, y sean sus direcciones los números entre 1 y 20.
				Sean K1 = 6126 y K2 = 8142 dos
				claves a las que deban asignarse posiciones en el arreglo. Se aplica la fórmula
				con N = 20, para calcular las direcciones correspondientes a K1 y K2.
				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >H(K1)
				= ( 6126 mod 20 ) + 1 = 7
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >H(K2)
				= ( 8142 mod 20 ) + 1 = 3
				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Con
				N = 20 no hay <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a>.
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ><b>Función
				Cuadrado
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Consiste
				en elevar al cuadrado la clave y tomar los dígitos centrales como <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a>.
				El número de dígitos a tomar queda determinado por el rango del índice. Sea K
				la clave del dato a buscar. La función hash queda definida por la siguiente fórmula:
				</p>
				<p align="center" style="line-height: 150%; text-indent: 20; margin-left: 0"><b>
				</b>
				</p>
				<p align="center" style="line-height: 150%; text-indent: 20; margin-left: 0"><b>H(k)
				= dígitos_centrales (K2) + 1;;;; (2.16)
				</b>
				</p>
				<p  style="line-height: 100%; text-indent: 20; margin-left: 0">				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				suma de una unidad a los dígitos centrales es para obtener un valor entre 1 y
				N. El ejemplo; 2.17 presenta un caso de función hash cuadrado.
				</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" >				</p>
				<p style="text-indent: 0; line-height: 150%; margin-left: 0" ><b>EJEMPLO
				2.17.</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Sean
				N = 20 el tamaño del arreglo, y sean sus direcciones los números entre 1 y 20.
				Sean;;;;;;; K1 = 6156 y K2 = 8218 dos claves
				a las que deban asignarse posiciones en el arreglo. Se aplica la fórmula para
				calcular las direcciones correspondientes a K1 y K2.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >K12
				=; 37896336</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >K22
				=; 67535524</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >H(K1)
				= dígitos_centrales (37 896 336) + 1 =; 97
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >H(K2)
				= dígitos_centrales (67 535 524) + 1 =; 36</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Como
				el rango de índices en el ejemplo, varía de 1 a 20 se toman solamente los dos
				dígitos centrales del cuadrado de las claves.
				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ><b>Función
				Plegamiento</b></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Consiste
				en dividir la clave en partes de igual número de dígitos (la última puede;
				tener menos dígitos) y operar con ellas, tomando como <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> los dígitos
				menos significativos. La operación entre las partes puede hacerse por medio de
				sumas o multiplicaciones. Sea K la clave del dato a buscar. K está formada por
				los dígitos d1, d2, ..., dn. La función hash queda definida por la siguiente fórmula:
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="line-height: 100%; text-indent: 0"><b>
				</b>
				</p>
				<p align="center" style="line-height: 100%; text-indent: 0"><b>H(k)
				= dígmensig ((d1 ... di) + (di+1 ... dj) + ... + (d1 ... dn)) + 1;;;;
				(2.17)
				</b>
				</p>
				<p  style="line-height: 100%; text-indent: 20">				</p>
				<p style="text-indent: 20; line-height: 150%" >El
				operador que aparece en la fórmula operando las partes de la clave es el de
				suma. Pero como se aclaró antes, puede ser el de la multiplicación. La suma de
				una unidad a los dígitos menos significativos (dígmensig) es para obtener un
				valor entre 1 y N. El ejemplo 2.18 presenta un caso de función hash por
				plegamiento.</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				2.18.
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%" >Sean
				N = 20 el tamaño del arreglo, y sean sus direcciones los números entre 1 y 20.
				Sean;K1 = 6086 y K2 = 8142 dos claves
				a las que deban asignarse posiciones en el arreglo. Se aplica la fórmula para
				calcular las direcciones correspondientes a K1 y K2.
				</p>
				<p  style="text-indent: 20; line-height: 100%">				</p>
				<p  style="line-height: 100%; text-indent: 20">H(K1)
				= dígmensig (60 + 86) + 1 = dígmensig (147) + 1 = 48</p>
				<p  style="line-height: 100%; text-indent: 20">H(K2)
				= dígmensig (81 + 42) + 1 = dígmensig (124) + 1 = 25
				</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >De
				la suma de las partes se toman solamente dos dígitos porque los índices del
				arreglo varían de 1 a 20.
				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Función
				Truncamiento
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%" >Consiste
				en tomar algunos dígitos de la clave y formar con ellos una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a>. Este método
				es de los más sencillos, pero es también de los que ofrecen menos uniformidad
				en la distribución de las claves.
				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p style="text-indent: 20; line-height: 150%" >Sea
				K la clave del dato a buscar. K está formada por los dígitos d1, d2,..., dn.
				La función hash queda definida por la siguiente fórmula:
				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p align="center" style="line-height: 100%; text-indent: 20"><b>
				</b>
				</p>
				<p align="center" style="line-height: 100%; text-indent: 20"><b>H(k)
				= elegirdígitos (d1, d2... dn) + 1;;;;; (2.18)
				 </b>
				</p>

				<p align="center" style="line-height: 100%; text-indent: 20"></p>
				<p  style="line-height: 150%; text-indent: 20">La
				elección de los dígitos es arbitraria. Podrían tomarse los dígitos de las
				posiciones impares o de las pares. Luego podría unírseles de izquierda a
				derecha o de derecha a izquierda.
				</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La suma de una unidad a los dígitos
				seleccionados es para obtener un valor entre 1 y 100.
				 El
				ejemplo 2.19 presenta un caso de función hash por truncamiento.
				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" >				</p>
				<p style="text-indent: 0; line-height: 100%; margin-left: 0" ><b>EJEMPLO
				2.19.
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Sean
				N = 20 el tamaño del arreglo, y sean sus direcciones los números entre 1 y 20.
				Sean;K1 = 6156 y K2 = 8218 dos claves
				a las que deban asignarse posiciones en el arreglo. Se aplica la fórmula
				anterior para calcular las direcciones correspondientes a K1 y K2.				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" >H(K1)
				= elegirdígitos (6 1 5 6) + 1 = 66
				</p>
				<p style="line-height: 100%; text-indent: 20; margin-left: 0" >H(K2)
				= elegirdígitos (8 2 1 8) + 1 = 82				</p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >En
				este ejemplo se toma el primer y tercer número de la clave y se une éste de
				izquierda a derecha.
				</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 100%; margin-left: 0" >En
				todos los casos anteriores se presentan ejemplos de claves numéricas.
				</p>				
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Sin
				embargo, en la realidad las claves pueden ser alfabéticas o alfanuméricas.
				</p>

				<p style="text-indent: 20; line-height: 150%" >En
				general, cuando aparecen letras en las claves se suele asociar a cada una un
				entero a efectos de convertirlas en numéricas.				</p>
				<p style="text-indent: 20; line-height: 100%" >				</p>
				<p align="center" style="line-height: 100%; text-indent: 20">A;;;;;
				B;;;;; C;;;;;; D; .
				. .; Z
				</p>
				<p align="center" style="line-height: 100%; text-indent: 20">01;;;
				02;;;; 03;;;; 04;;;;;;;;
				27				</p>
				<p  style="line-height: 100%; text-indent: 20">				</p>
				<p style="text-indent: 20; line-height: 150%" >Si
				por ejemplo la clave fuera ADA, su equivalente numérica sería 010401. Si
				hubiera combinación de letras y números, se procedería de la misma manera.
				Por ejemplo, dada una clave Z4F21, su equivalente numérica sería 2740621.
				</p>				<p style="text-indent: 20; line-height: 150%" >Otra
				alternativa sería, para cada carácter, tomar el valor decimal asociado según
				el código ASCII. Una vez obtenida la clave en su forma numérica, se puede
				utilizar normalmente cualquiera de las funciones antes mencionadas.
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ><b>Solución
				de colisiones</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				elección de un método adecuado para resolver colisiones es tan importante como
				la elección de una buena función hash. Cuando la función hash obtiene una
				misma <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Dirección" v-bind:data-content="Dirección">Dirección</a> para dos claves diferentes, se está ante una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a>.
				Normalmente, cualquiera que sea el método elegido, resulta costoso tratar las
				colisiones. Es por ello que debe hacerse un esfuerzo por encontrar la función
				que ofrezca mayor uniformidad en la distribución de las claves.
				</p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				manera más natural de resolver el problema de las colisiones es reservar una
				casilla por clave. Es decir, que aquellas se correspondan una a una con las
				posiciones del arreglo. Pero como ya se mencionó, esta solución puede tener un
				alto costo en memoria. Por lo tanto, deben analizarse otras alternativas que
				permitan equilibrar el uso de memoria con el tiempo de búsqueda.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >A
				continuación se presentan algunos de los métodos más utilizados para resolver
				colisiones, los cuales pueden clasificarse en:
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; mso-list: l71 level1 lfo58; tab-stops: list 35.0pt; margin-left: 0" >Reasignación</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; mso-list: l71 level1 lfo58; tab-stops: list 35.0pt; margin-left: 0" >Arreglos anidados</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%; mso-list: l71 level1 lfo58; tab-stops: list 35.0pt; margin-left: 0" >Encadenamiento</li>
				</ul>
				<p style="text-indent: 20; line-height: 150%; mso-list: l71 level1 lfo58; tab-stops: list 35.0pt; margin-left: 34.0pt" ></p>
				<p style="text-indent: 20; line-height: 150%; mso-list: l71 level1 lfo58; tab-stops: list 35.0pt; margin-left: 0" ><b>Reasignación</b>
				</p>
				<p style="text-indent: 20; line-height: 150%" >Existen
				varios métodos que trabajan bajo el principio de comparación y reasignación
				de elementos. Se analizarán tres de ellos:
				</p>
				<p style="text-indent: 20; line-height: 150%; mso-list: l34 level1 lfo59; tab-stops: list 53.4pt; margin-left: 53.4pt" >a);;;;;
				Prueba Lineal.
				</p>
				<p style="text-indent: 20; line-height: 150%; mso-list: l34 level1 lfo59; tab-stops: list 53.4pt; margin-left: 53.4pt" >b);;;;;
				Prueba Cuadrática.
				</p>
				<p style="text-indent: 20; line-height: 150%; mso-list: l34 level1 lfo59; tab-stops: list 53.4pt; margin-left: 53.4pt" >c);;;;;
				Doble Dirección Hash.</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 35.4pt" >				</p>
				<p style="line-height: 150%; text-indent: 20" ><b>a)
				Prueba Lineal
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%" >Consiste
				en que una vez detectada la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a> se debe recorrer el arreglo
				secuencialmente a partir del punto de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a>, buscando al elemento.
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >El
				proceso de búsqueda concluye cuando el elemento es hallado, o bien cuando se
				encuentra una posición vacía. Se trata al arreglo como a una estructura
				circular: el siguiente elemento después del último es el primero.</p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >A
				continuación se expone el algoritmo de solución de colisiones por medio de la
				prueba lineal.
				</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" >
				 
				</p>
				<p align="center" style="text-indent: 0; line-height: 150%; margin-left: 0"><a href="a2.23">Algoritmo
				2.23. Solución de colisiones por medio de la prueba lineal</a>
				</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				cuarta condición del ciclo mientras, (DX &lt;&gt; X), es para evitar caer en un
				ciclo infinito, si el arreglo estuviera lleno y el elemento a buscar no se
				encontrara en él.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				principal desventaja de este método es que puede haber un fuerte agrupamiento
				alrededor de ciertas claves, mientras que otras zonas del arreglo permanecerían
				vacías.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" >Si
				las concentraciones de claves son muy frecuentes, la búsqueda será
				principalmente secuencial perdiendo así las ventajas del método hash.</p>

				<p style="line-height: 100%; text-indent: 20; margin-left: 0" ></p>

				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ><b>b)
				Prueba Cuadrática
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Este
				método es similar al de la prueba lineal. La diferencia consiste en que en el
				cuadrático las direcciones alternativas se generarán como D + 1, D + 4, D + 9,
				. . . , D + i2; en vez de D + 1, D + 2, . . ., D + i. Esta variación
				permite una mejor distribución de las claves colisionadas.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >El
				algoritmo de solución de colisiones por medio de la prueba cuadrática se
				estudia en seguida.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >
				 
				</p>
				<p align="center" style="text-indent: 0; line-height: 150%; margin-left: 0"><a href="a2.24">Algoritmo
				2.24. Solución de colisiones por medio de la prueba cuadrática</a>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				principal desventaja de este método es que pueden quedar casillas del arreglo
				sin visitar.
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" > Además, como los valores de las direcciones varían en I2
				unidades, resulta difícil determinar una condición general para detener el
				ciclo mientras. Este problema podría solucionarse empleando una variable
				auxiliar, cuyos valores dirijan el recorrido del arreglo de tal manera que
				garantice que serán visitadas todas las casillas.
				</p>

				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ></p>

				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ><b>c)
				Doble Dirección Hash
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Consiste
				en que una vez detectada la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Colisión" v-bind:data-content="Colisión">Colisión</a> se debe generar otra dirección aplicando
				la función hash a la dirección previamente obtenida. El proceso se detiene
				cuando el elemento es hallado, o bien cuando se encuentra una posición vacía.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				función hash que se aplique a las direcciones puede o no ser la misma que
				originalmente se aplicó a la clave. No existe una regla que permita decidir cuál
				será la mejor función a emplear en el cálculo de las sucesivas direcciones.
				Analícese ahora el algoritmo de solución de colisiones por medio del método
				de la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble dirección hash" v-bind:data-content="DobleDirecciónHash">doble dirección hash/a>.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">				<p align="center" style="line-height: 200%; text-indent: 0; margin-left: 0"><a href="a2.25">Algoritmo
				2.25. Solución de colisiones por medio del método de la <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble dirección hash" v-bind:data-content="DobleDirecciónHash">doble dirección hash/a>
				</a>
				</p>

				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ></p>

				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ><b>Arreglos
				Anidados
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Este
				método consiste en que cada elemento del arreglo tenga otro arreglo en el cual
				se almacenen los elementos colisionados. Si bien la solución parece ser
				sencilla, es claro también que resulta ineficiente. Al trabajar con arreglos se
				depende del espacio que se haya asignado a éste. Lo cual conduce a un nuevo
				problema difícil de solucionar: elegir un tamaño adecuado de arreglo que
				permita un equilibrio entre el costo de memoria y el número de valores
				colisionados que pudiera almacenar.
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >
				 
				</p>
				<p style="line-height: 150%; text-indent: 20; margin-left: 0" ><b>Encadenamiento
				</b>
				</p>
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Consiste
				en que cada elemento del arreglo tenga un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> a una lista ligada, la cual
				se irá generando e irá almacenando los valores colisionados a medida que se
				requiera. Es el método más eficiente debido al dinamismo propio de las listas.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Cualquiera que sea el número de colisiones registradas en una posición,
				siempre será posible tratar una más.</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Como
				desventajas del método de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento/a> se cita el hecho de que ocupa espacio
				adicional al de la tabla, y que exige el manejo de listas ligadas. Además, si
				las listas crecen demasiado se perderá la facilidad de acceso directo del método
				hash.</p>

				<p style="text-indent: 20; line-height: 100%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >La
				figura 2.46. muestra la estructura de datos necesaria para resolver colisiones
				por medio de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento/a>.
				</p>
				<p  style="line-height: 100%; text-indent: 20; margin-left: 0">
				 
				</p>
				<p align="center" style="line-height: 100%; text-indent: 0; margin-left: 0"><b>Figura2.46.</b> Solución de colisiones por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento/a></p>
				<p align="right" style="line-height: 100%; text-indent: 0; margin-left: 0"><img border="0" src="c:\edl-book\contenido\capII\i2.46.gif" align="right" width="209" height="11"></p>
				<p align="right" style="line-height: 100%; text-indent: 0; margin-left: 0"><img border="0" src="c:\edl-book\contenido\capII\F2.46.gif" align="right" width="209" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 20; line-height: 150%"><!--[if gte vml 1]><v:shape id="_x0000_i1027" 
				 type="#_x0000_t75" style='width:175.5pt;height:89.25pt'> 
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/01/clip_image005.png" 
				  o:title="fig79"/> 
				</v:shape><![endif]-->
				 A continuación se presenta el algoritmo de solución de colisiones por
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento/a>.
				</p>
				<p style="text-indent: 20; line-height: 100%" >
				 
				</p>
				<p align="center" style="text-indent: 0; line-height: 100%"><a href="a2.26">Algoritmo
				2.26. Solución de colisiones por <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento/a></a>
				</p>

				<p style="line-height: 100%; tab-stops: 35.4pt; text-indent: 0" ></p>

				<p style="line-height: 150%; tab-stops: 35.4pt; text-indent: 0" ><b>Resumen</b></p>
				<p style="line-height: 150%; tab-stops: 35.4pt; text-indent: 20" >Un
				arreglo puede definirse como un grupo o una colección finita, homogénea y
				ordenada de elementos. Se distinguen dos partes en los arreglos: los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>;
				que hacen referencia a los elementos que forman el arreglo y los índices que
				especifican cuántos elementos tendrá el arreglo y además de que modo podrán
				accesarse esos <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>. El número de índices necesario para especificar un
				elemento de un arreglo se denomina dimensión. ;De acuerdo a el número de
				índices los arreglos pueden ser: de una dimensión (unidimensional), de dos
				dimensiones (bidimensionales) y de tres o más dimensiones (multidimensional).
				</p>
				<p style="text-indent: 20; line-height: 150%" >
				 
				</p>
				<p style="text-indent: 20; line-height: 150%" >Un
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">arreglo unidimensional</a> es la estructura natural para modelar listas de elementos
				de datos iguales.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Si; dos o más arreglos unidimensionales ;utilizan
				el mismo subíndice para referirse a términos homólogos se dice que son;
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos paralelos" v-bind:data-content="ArreglosParalelos">arreglos paralelos</a>.
				</p>

				<p style="text-indent: 20; line-height: 200%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Un
				arreglo bidimensional es la estructura de datos ideal para modelar datos que están
				estructurados de forma lógica como una tabla con filas y <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Columna" v-bind:data-content="Columna">Columnas</a>. Su mecanismo
				de acceso es directo. Un par de índices especifica el elemento deseado dando su
				posición relativa en la colección.
				</p>

				<p style="text-indent: 20; line-height: 200%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Entre los arreglos bidimensionales se
				encuentran: la ;Matriz Poco densa indica que hay una proporción muy alta
				de ceros entre sus elementos, la ;Matriz Tridiagonal ;en donde los
				elementos distintos de cero se encuentran en la diagonal principal o en las
				diagonales por encima o debajo de ésta, las Matrices Cuadradas en las que los
				elementos que se encuentran arriba o debajo de la diagonal principal son iguales
				a cero, y se pueden recibir el nombre de Matrices Triangulares, Matriz Simétrica
				si A[i, j] es igual a A[j, i], y esto se cumple para todo i y para todo j.
				</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Un
				arreglo de N dimensiones es una colección de K1 x K2 x ... x KN elementos. Para
				hacer referencia a cada componente de un arreglo de N dimensiones, se usarán N
				índices (uno para cada dimensión). El número de dimensiones (índices)
				permitido depende del lenguaje elegido, algunos admiten hasta siete dimensiones.
				</p>				<p style="text-indent: 20; line-height: 150%" >Existe
				métodos de ordenación en arreglos los cuales pueden aplicarse a arreglos de
				cualquier dimensión, estos pueden ser:</p>

				<p style="text-indent: 20; line-height: 150%" >Métodos
				directos (n<sup>2</sup>): Los métodos directos tienen la característica de que
				sus programas son cortos y de fácil elaboración y comprensión, aunque son
				ineficientes cuando N (el número de elementos del arreglo) es medio o grande.
				Entre ellos se encuentran: Ordenación por intercambio directo (burbuja),
				Ordenación por inserción directa, Ordenación por inserción binaria y
				Ordenación por selección directa.</p>				<p style="text-indent: 20; line-height: 150%" >Métodos
				Logarítmicos (n * log n): Los métodos logarítmicos son más complejos que los
				métodos directos.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >Cierto es que requieren de menos comparaciones y movimientos
				para ordenar sus elementos, pero su elaboración y comprensión resulta más
				sofisticada y abstracta. Entre los cuales están: Métodos logarítmicos (n *
				log n), Ordenación por el método de Shell, Ordenación por el Método
				Quicksort.</p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >También
				se pueden recuperar datos previamente almacenados en los arreglos a través de
				las operaciones de búsqueda. El resultado que puede arrojar esta operación es
				éxito, si se encuentra el elemento buscado, o fracaso, en otras circunstancias.
				Las
				operaciones de búsqueda pueden llevarse a cabo sobre elementos ordenados y
				sobre elementos desordenados.</p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" ></p>

				<p style="text-indent: 20; line-height: 150%; margin-left: 0" >En
				el primer caso, la búsqueda se facilita, y por lo tanto se ocupará menos
				tiempo que si se trabaja con elementos desordenados. Se denomina búsqueda
				interna cuando todos los elementos se encuentran en memoria principal, por
				ejemplo, almacenados en arreglos. Los métodos de búsqueda interna más
				importantes son: secuencial o lineal, Binaria, y por transformación de claves.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" ><b>Ejercicios
				Propuestos</b></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >En
				    un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">Arreglo unidimensional</a> se almacenan las calificaciones finales de 20
				    alumnos. Escriba un pseudocódigo que calcule e imprima :</li>
				</ol>
				<p style="text-indent: 50; line-height: 150%" >a)
				El promedio general del grupo</p>
				<p style="text-indent: 50; line-height: 150%" >b)
				El número de alumnos aprobados</p>
				<p style="text-indent: 50; line-height: 150%" >c)
				El número de alumnos reprobados</p>
				<p style="text-indent: 50; line-height: 150%" >d)
				El número de alumnos cuya calificación fue menor</p>
				<p style="text-indent: 68; line-height: 150%" >o
				igual a 5</p>
				<p style="text-indent: 0; line-height: 150%" ></p>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="2">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Se
				    tienen 4 arreglos Unidimensionales llamados Norte, Sur, Este y Oeste donde
				    se almacenan por orden alfabético los nombres de los estados venezolanos
				    del Norte, Sur, Este y Oeste respectivamente. Realice un pseudocódigo que
				    mezcle los arreglos anteriores y forme un quinto arreglo llamado Venezuela
				    en el cual aparezcan ordenados alfabéticamente todos los estados del país.</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="3">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Realice
				    pseudocódigos propios para insertar, eliminar y modificar un elemento de un
				    arreglo de números enteros ordenado ascendentemente. ;</li>
				</ol>
				<p style="text-indent: 0; line-height: 150%" ></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="4">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Realice
				    pseudocódigos propios para insertar, eliminar y modificar un elemento de un
				    arreglo desordenado de números reales. ;</li>
				</ol>
				<p style="text-indent: 0; line-height: 150%" >
				;</p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="5">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Se
				tiene un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo bidimensional o matriz" v-bind:data-content="ArregloBidimensionalOMatriz">Arreglo bidimensional o matriz</a> que almacena las calificaciones
				obtenidas por 35 alumnos en 4 exámenes diferentes. Escriba un pseudocódigo que
				calcule:</li>
				</ol>
				<p style="text-indent: 50; line-height: 150%" >a)
				El promedio general de calificaciones del grupo</p>
				<p style="text-indent: 68; line-height: 150%" >de alumnos considerando todos
				los exámenes.</p>
				<p style="text-indent: 50; line-height: 150%" >b)
				La mayor calificación obtenida en el 3er examen.</p>
				<p style="text-indent: 50; line-height: 150%" >c)
				¿En cuál examen fue más alto el promedio del</p>

				<p style="text-indent: 68; line-height: 150%" >grupo?</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">				<ol style="font-family: Century Gothic; font-size: 12 pt" start="6">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >
				Escriba un pseudocódigo que llene de ceros una matriz A (NxN) excepto en la
				diagonal principal donde deben asignar 1.</li>
				</ol>
				<p style="text-indent: 0; line-height: 100%" ></p>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="7">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Escriba un pseudocódigo que intercambie las filas de una matriz NxN de la
				siguiente manera: los elementos de la fila 1 deben intercambiarse con los de la
				fila N, los de la fila 2 con los de la fila N-1 y así sucesivamente. Ej.:</li>
				</ol>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capII/tablaejer.gif" width="237" height="98"></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="8">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >En un Arreglo Bidimensional se ha almacenado el número total de toneladas de
				granos cosechadas durante cada mes del año 2001. Escriba un pseudocódigo que
				calcule:</li>
				</ol>
				<p style="text-indent: 50; line-height: 100%" >a)
				El promedio anual de toneladas cosechadas</p>

				<p style="text-indent: 50; line-height: 100%" >b)
				¿Cuántos meses tuvieron una cosecha superior al;</p>

				<p style="text-indent: 68; line-height: 100%" >promedio anual?</p>
			</div>
			<div class="pagina" @click="navNext">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="9">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Dado un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo unidimensional" v-bind:data-content="ArregloUnidimensional">Arreglo unidimensional</a> donde se encuentra almacenada una cadena de
				caracteres, se desea saber el número de veces que aparece cada letra en dicha
				cadena. Realice un pseudocódigo para obtener la información deseada.</li>
				</ol>
				<p style="text-indent: 0; line-height: 100%" ></p>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="10">
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Una empresa lleva un registro del total producido mensualmente por sus 4
				departamentos. La información se ha registrado a lo largo de 5 años, por lo
				tanto se tiene un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglo multidimensional" v-bind:data-content="ArregloMultidimensional">arreglo Multidimensional</a> de 4 x 12 x 5 elementos. Escriba un
				pseudocódigo que calcule:</li>
				</ol>
				<p style="text-indent: 50; line-height: 100%" >a)
				El total mensual de cada departamento durante el</p>
				<p style="text-indent: 68; line-height: 100%" >tercer año.</p>
				<p style="text-indent: 50; line-height: 100%" >b)
				El total de producción durante el segundo año.</p>
				<p style="text-indent: 50; line-height: 100%" >c)
				El total de producción del departamento 2 en el</p>

				<p style="text-indent: 68; line-height: 100%" >último año.</p>

				<p style="text-indent: 68; line-height: 100%" ></p>
				<p style="text-indent: 68; line-height: 100%" ><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 2)">Autoevaluación</a></p>
			</div>
		</div>
		<!--Contenido 3-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 0" ><b><span style="mso-bidi-font-size: 12.0pt">3.1
				Introducción</span></b></p>

				<p style="line-height: 150%; text-indent: 20" ><span style="mso-bidi-font-size: 12.0pt">Cuando
				se habló de<span style="mso-spacerun: yes"> </span>arreglo,<span style="mso-spacerun:
				yes"> </span>se hizo mención de que se trataba de una colección de datos
				del mismo tipo, que era un tipo de datos estructurado y que usándolo se podían
				solucionar un gran número de problemas. Sin embargo, en la práctica a veces se
				necesitan estructuras que permitan almacenar distintos tipos de datos, característica
				con la cual no cuentan los arreglos, ya que son estructuras homogéneas. Para
				ilustrar este problema se incluye el siguiente ejemplo.</span></p>

				<p style="line-height: 150%; text-indent: 0" ></p>
				<p style="line-height: 150%; text-indent: 0" ><b><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">EJEMPLO
				3.1;</span></b></p>
				<p style="line-height: 150%; text-indent: 20" ><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt">Una
				compañía tiene por cada empleado los siguientes datos:</span></p>
				<ul style="float: left">
				  <li>
				    <p style="line-height: 150%; mso-list: l0 level1 lfo1; tab-stops: list 13.5pt left 22.5pt; margin-left: 13.5pt" align="left"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">Nombres<span style="mso-spacerun: yes">
				    </span>(cadena de caracteres).
				    
				    </span></li>
				  <li>
				    <p style="line-height: 150%; mso-list: l0 level1 lfo1; tab-stops: list 13.5pt left 22.5pt; margin-left: 13.5pt" align="left"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">Cédula
				    de Identidad<span style="mso-spacerun:yes">; </span>(
				    entero).
				    
				    </span></li>
				  <li>
				    <p style="line-height: 150%; mso-list: l0 level1 lfo1; tab-stops: list 13.5pt left 22.5pt; margin-left: 13.5pt" align="left"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><span style="font-style: normal; font-variant: normal; font-weight: normal"></span>Edad<span style="mso-spacerun: yes">
				    </span>(entero).
				    </span></li>
				  <li>
				    <p style="line-height: 150%; mso-list: l0 level1 lfo1; tab-stops: list 13.5pt left 22.5pt; margin-left: 13.5pt" align="left"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD">Dirección<span style="mso-spacerun: yes">;
				    </span>(cadena de caracteres).
				    
				    </span></li>
				  <li>
				    <p style="line-height: 150%; mso-list: l0 level1 lfo1; tab-stops: list 13.5pt left 22.5pt; margin-left: 13.5pt" align="left"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><span style="font-style: normal; font-variant: normal; font-weight: normal"></span>Sexo<span style="mso-spacerun: yes">
				    </span>(caracter).</span></li>
				</ul>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 20; line-height: 150%">Si
				se quisieran almacenar estos datos en un arreglo sería imposible, ya que sus
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> deben ser todos del mismo tipo y en este caso se tienen datos
				heterogéneos. La estructura que puede guardar esta información es la que se
				conoce como Registro.</p>

				<p  style="text-indent: 20; line-height: 150%"></p>

				<p  style="text-indent: 20; line-height: 150%">Este
				capítulo estará dedicado al estudio de los registros, estableciendo entre
				otras cosas las principales diferencias que existen entre ellos y los arreglos.</p>

				<p  style="text-indent: 0; line-height: 150%"><b>3.2
				Definición de Registro</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Según
				Cairó y Guardatí (1993<i>)</i>, un<b> </b>registro es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a>,
				donde cada uno de sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> se denomina campo. Los campos de un registro
				pueden ser todos de diferentes tipos. Por lo tanto, también podrán ser
				registros o arreglos. Cada campo se identifica por un nombre único (el
				identificador de campo).</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 20; line-height: 150%">Un
				registro puede representarse gráficamente en función de sus campos, como se
				muestra en las siguientes figuras:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p ALIGN="center" style="text-indent: 0; line-height: 150%"><b>Figura
				3.1.</b> Registro con la
				identificación de cada campo y su respectivo tipo.</p>

				<p ALIGN="center" style="text-indent: 0; line-height: 150%"></p>
				<b>
				<p ALIGN="left" style="text-indent: 20; line-height: 150%">Registro_Empleado</p>
				<p ALIGN="center" style="text-indent: 20; line-height: 150%"><img border="0" src="contenido/capIII/Fig%203.1.gif" width="333" height="47"></p>
				<p ALIGN="right" style="text-indent: 20; line-height: 150%"><img border="0" src="contenido/capIII/Fuente1.gif" width="463" height="16"></p>
				<p ALIGN="center" style="text-indent: 0; line-height: 150%">Figura
				3.2 
				</b>Registro con la
				identificación de sus campos.</p>
				<i><p ALIGN="center" style="text-indent: 20; line-height: 100%"></p>
				</i>

				<p ALIGN="center" style="text-indent: 20; line-height: 100%"><img border="0" src="contenido/capIII/fig%203.2.gif" width="234" height="118"></p>
				<p ALIGN="right" style="text-indent: 20; line-height: 100%"><img border="0" src="C:\Edl-book\contenido\capIII\Fuente1.gif" width="463" height="16"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">Un
				registro se definirá de la siguiente manera:</p>
				<p  style="line-height: 100%; text-indent: 20">ident_registro
				= <b>REGISTRO</p>
				</b>
				<p  style="line-height: 100%; text-indent: 150">id_campol:
				tipol</p>
				<p  style="line-height: 100%; text-indent: 150">id_campo2:
				tipo2</p>
				<p  style="line-height: 100%; text-indent: 150">.</p>
				<p  style="line-height: 100%; text-indent: 150">.</p>
				<p  style="line-height: 100%; text-indent: 150">.</p>
				<p  style="line-height: 100%; text-indent: 150">id_campon:
				tipon</p>
				<b>
				<p  style="line-height: 100%; text-indent: 20">Fin_ident_registro</p>				<p  style="line-height: 150%; text-indent: 20"></b>Donde:</p>
				<b>
				<p  style="line-height: 150%; text-indent: 40">ident_registro</b>
				es el nombre del dato tipo registro</p>
				<p  style="line-height: 150%; text-indent: 40"><b>id_campo</b>
				es el nombre del campo i</p>
				<b>
				<p  style="line-height: 150%; text-indent: 40">id_cam</b><b>po</b><sub><b>i</b>
				</sub><sub>¹ <b>id_campoj</b>
				&quot;I,j = 1, …, n ei ¹ j</p>
				<b>
				<p  style="line-height: 150%; text-indent: 40">tipo</b>
				es el tipo del campo i</p>
				</sub>

			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%; text-indent: 0">3.3
				Clasificación de los Registros</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Entre
				los tipos de registros se encuentran los siguientes:</p>
				</sub>
				<p  style="line-height: 150%; text-indent: 0"></p>
				<p  style="line-height: 150%; text-indent: 0"><b>3.3.1
				Registro Homogéneo</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Es
				aquel que almacena datos del mismo tipo. Véase el siguiente ejemplo:</p>
				<p  style="line-height: 150%; text-indent: 0"></p>
				<p  style="line-height: 150%; text-indent: 0"><b>EJEMPLO
				3.2</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Sea
				DATOS_PERSONALES un registro formado por tres campos de tipo CADENA. Su
				representación queda como se muestra en la Tabla 3.1.</p>				<p  style="line-height: 150%; text-indent: 20">DATOS_
				PERSONALES = REGISTRO</p>
				<p style="line-height: 100%; text-indent: 185">Nombres:
				cadena_de_ caracteres</p>
				<p style="line-height: 100%; text-indent: 185">Apellidos:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 185">CI
				: cadena_de_caracteres</p>
				<p ALIGN="left" style="line-height: 100%; text-indent: 20">Fin_DATOS_
				PERSONALES</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p align="center" style="line-height: 150%; text-indent: 0">Tabla
				3.1. </b> DATOS_PERSONALES</p>

				<p align="center" style="line-height: 150%; text-indent: 0"><b><img border="0" src="Tab3.1.gif" width="273" height="71"></p>

				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIII/fuente2.gif" width="156" height="20"></p>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">3.3.2
				Registro Heterogéneo</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Es
				aquel que almacena datos de tipos diferentes. Véanse los siguientes ejemplos:</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 0; line-height: 150%"><b>EJEMPLO
				3.3</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Sea
				DIRECCION_HAB un registro formado por cuatro campos, uno de los cuales es
				numérico y los tres restantes son del tipo cadena de caracteres. Su
				representación queda como se muestra en la Tabla
				3.2.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 100%; text-indent: 0" >DIRECCIÓN_
				HAB = REGISTRO</p>
				<p style="line-height: 100%; text-indent: 150" >Calle:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 150" >Numero_casa:
				entero</p>
				<p style="line-height: 100%; text-indent: 150" >Ciudad:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 150" >Pais:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 0" >Fin_DIRECCIÓN_
				HAB</p>
				<p align="center" style="line-height: 150%">
				<b>
				Tabla 3.2. </b> DIRECCION_HAB</p>
				<p align="center" style="line-height: 150%"><img border="0" src="contenido/capIII/tab3.2.gif" width="396" height="69"></p>
				<p align="right" style="line-height: 150%"><img border="0" src="contenido/capIII/fuente2.gif" width="156" height="20"></p>

				<p style="line-height: 150%" ><b>EJEMPLO
				3.4</b></p>
				<p style="line-height: 150%; text-indent: 20" >Sea
				EMPLEADO un registro formado por cuatro campos, tres del tipo cadena de
				caracteres, uno del tipo real. Su representación queda como se muestra en la
				Tabla 3.3.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%" >EMPLEADO
				=REGISTRO</p>
				<p style="line-height: 100%; text-indent: 100" >Nombres:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 100" >Telefono:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 100" >CI:
				cadena_de _caracteres</p>
				<p style="line-height: 100%; text-indent: 100" >Sueldo:
				real</p>
				<p style="line-height: 100%" >Fin_EMPLEADO</p>
				<p style="line-height: 100%" align="center"><b>Tabla
				3.3. </b> EMPLEADO</p>
				<p style="line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capIII\tab33.gif" width="394" height="72"></p>
				<p style="line-height: 150%" align="right"><img border="0" src="contenido/capIII/fuente2.gif" width="156" height="20"></p>				<p style="line-height: 150%" ><b>3.3.3
				Registros Anidados</b></p>
				<p style="line-height: 150%; text-indent: 20" >Según
				Cairó y Guardatí (1993), en los registros anidados, al menos un campo del
				registro es del tipo registro. Véase un ejemplo.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%"><b>EJEMPLO 3.5</b></p>
				<p style="line-height: 150%; text-indent: 20" >Una
				empresa registra para cada uno de sus Empleados los siguientes datos:</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="text-indent: 20; line-height: 150%">·
				Nombre (cadena de caracteres).</p>
				<p style="text-indent: 20; line-height: 150%">·
				Direccion:</p>
				<p style="text-indent: 50; line-height: 150%">o
				Calle (cadena de caracteres).</p>
				<p style="text-indent: 50; line-height: 150%">o
				Numero_casa (entero).</p>
				<p style="text-indent: 50; line-height: 150%">o
				Ciudad (cadena de caracteres).</p>
				<p style="text-indent: 50; line-height: 150%">o
				Pais (cadena de caracteres).</p>
				<p style="text-indent: 20; line-height: 150%">·
				Sueldo (real).</p>
				<p style="line-height: 150%; text-indent: 20"></p>
				<p style="line-height: 150%; text-indent: 20" >Para
				definir el tipo del campo Dirección es necesario definir previamente un
				registro formado por los cuatro <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> (Calle, Numero_casa, Ciudad y Pais)
				que se especifican. Se usará el registro definido en el ejemplo 3.3 para
				resolver este caso.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%" >EMPLEADO
				= REGISTRO</p>
				<p style="line-height: 100%; text-indent: 100" >Nombre:
				cadena_de_caracteres</p>
				<p style="line-height: 100%; text-indent: 100" >Direccion:
				DIRECCION_HAB</p>
				<p style="line-height: 100%; text-indent: 100" >Sueldo:
				real</p>
				<p style="line-height: 100%" >Fin_EMPLEADO</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 20" >La
				Tabla 3.4 muestra la estructura de datos requerida para el caso del registro
				anidado EMPLEADO .</p>
				<p style="line-height: 150%" align="center"></p>

				<p style="line-height: 150%" align="center"><b>Tabla
				3.4. </b> Registro EMPLEADO</p>

				<p style="line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capIII\Tab3.4.gif" width="375" height="115"></p>
				<p style="line-height: 150%" align="right"><img border="0" src="contenido/capIII/fuente2.gif" width="156" height="20"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 20" >En
				este caso, el registro tiene un campo (dirección) que es del tipo DOMICILIO, el
				cual es un registro de cuatro campos.</p>

				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 20" >Para
				tener acceso a los campos que a su vez sean registros, en la mayoría de los
				lenguajes se sigue la siguiente sintaxis:</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 0" align="center"><b>variable_registro.id_campo1.id_campon</b></p>
				<p style="line-height: 150%; text-indent: 0" align="center"></p>
				<p style="line-height: 150%; text-indent: 0" >Donde:</p>
				<p style="line-height: 150%; text-indent: 20" ><b>variable_registro
				</b>es una variable de tipo registro.</p>
				<p style="line-height: 150%; text-indent: 20" ><b>id_campo1</b>
				es el identificador de un campo del registro (el campo es de tipo registro).</p>
				<p style="line-height: 150%; text-indent: 20" ><b>id_campon</b>
				es el identificador de un campo.</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 20" >Para
				accesar los campos del registro del ejemplo anterior, la secuencia a seguir es
				la siguiente:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Nombre</p>
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Direccion.Calle</p>
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Direccion.Numero_casa</p>
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Dirección.Ciudad</p>
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Dirección.PaIs</p>
				<p style="line-height: 150%; text-indent: 20" >EMPLEADO.Saldo</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 0" ><b>3.3.4
				Registros con Variantes</b></p>
				<p style="line-height: 150%; text-indent: 20" >Dentro
				de un tipo registro puede haber algunos campos que sean mutuamente exclusivos.
				Esto es, los campos A y B puede que nunca se utilicen al mismo tiempo. Según
				Dale y Lilly (1989), los registros con variantes tienen dos partes: la parte
				fija, en la cual los campos son siempre los mismos y la parte variante, en la
				cual los campos pueden variar. Debido a que cada vez sólo se utiliza una
				porción de los campos variantes, pueden compartir espacio de memoria. El
				compilador necesita asignar sólo el espacio suficiente para las variables
				registro de forma que incluya a la variante más grande.</p>
			</div>
			<div class="pagina" @click="navNext">				<p style="line-height: 150%; text-indent: 20" >Si
				un registro tiene una parte Fija y una parte variante, la parte fija debe
				definirse primero. El siguiente es un ejemplo de una definición de registros
				que contiene partes fijas y variantes:</p>

				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-top: 0; margin-bottom: 0" ></p>
				<p style="line-height: 100%; word-spacing: 0; margin-top: 0; margin-bottom: 0" ><b>TIPO</b></p>
				<p style="line-height: 100%; text-indent: 20" >TipoItem
				= (Montaje, Tuerca, Cerrojo, Arandela, Cerradura)</p>
				<p style="line-height: 100%; text-indent: 20" >CadenaIO
				= ARREGLO[1 . .10] De Caracter</p>
				<p style="line-height: 100%; text-indent: 20" >TipoPieza
				= REGISTRO</p>
				<p style="line-height: 100%; text-indent: 100" >ID
				: CadenaIO</p>
				<p style="line-height: 100%; text-indent: 100" >Cant
				: Integer</p>
				<p style="line-height: 100%; text-indent: 100" >CASE
				item : TipoItem DE</p>
				<p style="line-height: 100%; text-indent: 150" >Montaje
				: (IDDibujo : CadenaIO</p>
				<p style="line-height: 100%; text-indent: 150" >Código
				: 1..12</p>
				<p style="line-height: 100%; text-indent: 150" >Clas
				: (A, B, C, D) )</p>
				<p style="line-height: 100%; text-indent: 150" >Tuerca,
				Cerrojo, Arandela : ( )</p>
				<p style="line-height: 100%; text-indent: 150" >Cerradura
				: (NoClave : Entero)</p>
				<p style="line-height: 100%; text-indent: 20" >Fin_TipoPieza</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>Variable</b></p>
				<p style="text-indent: 20; line-height: 150%" >Pieza
				: TipoPieza</p>				<p style="text-indent: 20; line-height: 150%" >El
				campo indicador después de la palabra clave CASO se llama el “campo
				indicativo”. Se utiliza para discriminar entre las diferentes variantes del
				registro. En el objetivo anterior, ítem es el campo indicativo de TipoPieza.</p>				<p style="text-indent: 20; line-height: 150%" >Cuando
				se utiliza una variable registro con variante, el programador es responsable de
				acceder a los campos de forma consistente con el campo indicativo. Por ejemplo,
				si el valor del campo indicativo es Tuerca, Cerrojo o Arandela, sólo pueden ser
				accedidos a los campos fijados. Si el campo indicativo es Cerradura,
				Pieza.NoClave es una referencia legal de campo. Si el campo indicativo es
				Montaje, Pieza.IDDibujo, Pieza.Código y Pieza.Clase son todas referencias
				válidas. El registro Pieza con variante se ilustra en la figura 3.2.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 100%; text-indent: 20" align="center"><b>Figura
				3.3.</b> Registro con Variante.</p>

				<p style="line-height: 100%; text-indent: 20" align="center"></p>
				<p style="line-height: 100%; text-indent: 20" align="center"><img border="0" src="C:\Edl-Book\contenido\capIII\fig3.3.gif" width="301" height="176"></p>
				<p style="line-height: 100%; text-indent: 20" align="right"><img border="0" src="C:\Edl-Book\contenido\capIII\Dale2.gif" width="196" height="18"></p>
				<p style="line-height: 150%; text-indent: 20" >Se
				deben apuntar varios aspectos respecto a la definición y uso de los registros
				con variantes. Pueden haber varios campos (incluso ninguno) en la parte Fija de
				un registro con variante. La definición de registro puede contener sólo una
				parte variante, aunque la lista de campos en la parte variante puede a su vez
				contener una parte variante (variantes anidadas). Todos los identificadores de
				campo dentro de la definición de registro deben ser únicos. El campo
				indicativo se utiliza para indicar la variante usada en una variable registro:
				su tipo puede ser cualquier tipo escalar.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 20" >Según
				Dale y Lilly (1989), la cláusula CASO de la parte variante no es la misma que
				una sentencia CASO.Hay varias diferencias:</p>
				<ol>
				  <li>
				    <p style="line-height: 150%; text-indent: 0" >No
				    hay un FIN de emparejamiento para CASO; se utiliza el FIN de la definición
				    del registro.</li>
				  <li>
				    <p style="line-height: 200%; text-indent: 0" >El
				    selector del caso es la declaración del campo indicativo (nombre de campo :
				    tipo) o sencillamente el tipo;;; del campo indicativo.</li>
				  <li>
				    <p style="line-height: 200%; text-indent: 0" >Cada
				    variante es una lista de campo etiquetada por una lista de etiquetas del
				    caso. Cada etiqueta es un valor del tipo indicativo (ítem).</li>
				  <li>
				    <p style="line-height: 200%">La lista
				    de campo está entre paréntesis.</li>
				  <li>
				    <p style="line-height: 200%">La lista
				    de campo define los campos y tipos de campos de esa variante.</li>
				  <li>
				    <p style="line-height: 200%">Una lista de campo puede estar
				    vacía, lo cual se denota por “( )”.</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >En
				tiempo de ejecución se asigna la variante a usar. La variante puede cambiarse
				mediante asignaciones a otros campos variantes y al campo indicativo. Cuando se
				utiliza una variante, los datos (si los hay) de la variante anterior se pierden.
				Es un error acceder a un campo que no sea parte de la variante actual.</p>				<p style="text-indent: 20; line-height: 150%" >La
				cláusula caso de la parte variante de la definición de registro se empareja
				frecuentemente con una sentencia CASO en el cuerpo del programa. Por ejemplo, el
				siguiente fragmento de programa se utilizaría para imprimir los datos de un
				registro.</p>				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a3.1">Algoritmo
				3.1. Imprimir registro con variantes</a></p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 100%" ><b>3.4
				Acceso a los Campos de un Registro</b></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				un registro es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a> no puede accesarse directamente como un
				todo, sino que debe especificarse qué elemento (campo) del registro interesa.
				Para ello, en la mayoría de los lenguajes se sigue la siguiente sintaxis:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>variable_registro.id_campo</b></p>
				<p style="text-indent: 0; line-height: 150%" >Donde:</p>
				<p style="text-indent: 20; line-height: 150%" ><b>variable_registro</b>
				es una variable de tipo registro</p>
				<p style="text-indent: 20; line-height: 150%" ><b>id_campo</b>
				es el identificador del campo deseado</p>				<p style="text-indent: 20; line-height: 150%" >Es
				decir, se usarán dos nombres para hacer referencia a un elemento: el nombre de
				la variable tipo registro y el nombre del componente, separados entre sí por un
				punto.</p>				<p style="text-indent: 20; line-height: 150%" >Con
				base en los ejemplos 3.2, 3.3 y 3.4, los siguientes casos ilustran el acceso a
				campos de registros.</p>				<p style="text-indent: 20; line-height: 150%" >a)
				Para leer los tres campos de una variable D tipo DATOS_PERSONALES:</p>

				<p style="text-indent: 40; line-height: 100%">Leer
				(D.Nombres)</p>
				<p style="text-indent: 40; line-height: 100%">Leer(D.Apellidos)</p>
				<p style="text-indent: 40; line-height: 100%">Leer
				(D.CI)</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >b)
				Para escribir los cuatro campos de una variable D tipo DIRECCIÓN_HAB:</p>
				<p style="text-indent: 40; line-height: 100%" >Escribir
				(D.Calle)</p>
				<p style="text-indent: 40; line-height: 100%" >Escribir
				( D.Número_casa)</p>
				<p style="text-indent: 40; line-height: 100%" >Escribir
				(D.Ciudad)</p>
				<p style="text-indent: 40; line-height: 200%" >Escribir
				( D.Pais)</p>
				<p style="text-indent: 20; line-height: 150%" >c)
				Para asignar valores a algunos de los campos de una variable E tipo EMPLEADO:</p>
				<p style="text-indent: 40; line-height: 100%" >E.Sueldo
				= 650000,00</p>
				<p style="text-indent: 40; line-height: 100%" >E.Telefono
				= “2633470”</p>
				<p style="text-indent: 40; line-height: 200%" >En
				general el orden en el cual se manejan los campos no es importante. Es decir, se
				podrían haber leído los campos de la variable D de la siguiente manera:</p>
				<p style="text-indent: 40; line-height: 100%" >Leer
				(D.CI)</p>
				<p style="text-indent: 40; line-height: 100%" >Leer(D.Apellidos)</p>
				<p style="text-indent: 40; line-height: 100%" >Leer
				(D.Nombres)</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Sólo
				debe tenerse en cuenta que los datos se correspondan en tipo con los campos.</p>				<p style="text-indent: 0; line-height: 150%" ><b>3.5
				Diferencias entre Arreglos y Registros</b></p>
				<p style="text-indent: 20; line-height: 150%" >Según
				Cairó y Guardatí (1993), las dos diferencias sustanciales existentes con la
				estructura de datos tipo arreglo son:</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Un
				    arreglo puede almacenar N elementos del mismo tipo, mientras que un registro
				    puede almacenar N elementos de diferentes tipos.</li>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Los
				    <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> de un arreglo se accesan por medio de índices (que indican la
				    posición del componente en el arreglo), mientras que los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> de un
				    registro (los campos) se accesan por medio de su nombre, el cual es único.</li>
				</ul>
				    <p style="text-indent: 0; line-height: 100%" >
				<p style="line-height: 150%"><b>3.6
				Representación en Memoria de los Registros.</b></p>
				<p style="line-height: 150%; text-indent: 20" >Según
				Dale y Lilly (1989), un registro, al igual que un arreglo, ocupa un bloque de
				celdas consecutivo de memoria. La</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >función
				de acceso al arreglo calcula la posición de una celda particular a partir de un
				Índice; el registro calcula la posición de un selector de campo con nombre.
				Sin embargo, la cuestión básica es la misma: ¿Qué celda (o celdas) en este
				bloque consecutivo se quiere tener?. El nombre de la variable registro se asocia
				con una posición de memoria. Para acceder a cada campo, se necesita simplemente
				calcular cuánto hay que saltar del registro para obtener el campo deseado. Esto
				se hace calculando cuántas celdas necesita cada campo.</p>

				<p style="text-indent: 20; line-height: 150%" >Cuando
				se declara un registro, se crea una tabla que relaciona a un nombre de campo con
				su posición dentro del registro. Cuando se utiliza un selector de campo del
				programa, se examina esta tabla para ver dónde está el campo deseado dentro de
				la variable registro.</p>
				<p class="MsoNormal" style="line-height: 150%" ><b><span style="mso-bidi-font-size: 12.0pt">Resumen				</span></b></p>
				<p style="line-height: 150%; text-indent: 20" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES-TRAD; mso-fareast-language: ES; mso-bidi-language: AR-SA">Un<b>
				</b>registro es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a>, donde cada uno de sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> se
				denomina campo. Los campos de un registro pueden ser todos de diferentes tipos.
				Por lo tanto, también podrán ser registros o arreglos. Cada campo</span>
				<span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES-TRAD; mso-fareast-language: ES; mso-bidi-language: AR-SA">se
				identifica por</span>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 0" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES-TRAD; mso-fareast-language: ES; mso-bidi-language: AR-SA">un nombre único (el identificador de campo). Los registros
				pueden ser: </span><i><span style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">Homogéneos
				</span></i><span style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">si
				almacena información<span style="mso-spacerun: yes"> </span>del mismo
				tipo y <i>Heterogéneos </i>si<i> </i><span style="mso-spacerun: yes"></span>almacena
				datos de tipos diferentes.</span>

				<p style="line-height: 150%; text-indent: 20" >

				<p style="line-height: 150%; text-indent: 20" >Como
				un registro es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a> no puede accesarse directamente como un
				todo, sino que debe especificarse qué elemento (campo) del registro interesa,
				es decir, se usarán dos nombres para hacer referencia a un elemento del
				registro: el nombre de la variable tipo registro y el nombre del componente,
				separados entre sí por un punto, el orden en el cual se manejan los campos de
				un registro no es importante sólo debe tenerse en cuenta que los datos que se
				manipulen correspondan en tipo con los campos del registro, Sin embargo existen
				lenguajes de programación que si permiten el acceso directo a los campo de un
				registro.</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 20" >Los
				Registros Anidados son aquellos que contienen al menos un campo de tipo
				registro.</p>
				<p style="line-height: 150%; text-indent: 20" ></p>				<p style="line-height: 150%; text-indent: 20" >Los
				Registros con Variantes contienen algunos campos que son mutuamente excluyentes.
				Esto es, los campos A y B puede </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%" >que
				nunca se utilicen al mismo tiempo. Los registros con variantes tienen dos
				partes: la parte fija, en la cual los campos son siempre los mismos y la parte
				variante, en la cual los campos pueden variar. Debido a que cada vez sólo se
				utiliza una porción de los campos variantes, pueden compartir espacio de
				memoria. Si un registro tiene una parte Fija y una parte variante, la parte fija
				debe definirse primero.</p>

				<p style="line-height: 150%" ></p>

				<p style="line-height: 150%; text-indent: 20" ><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 3)">Autoevaluación</a></p>
			</div>
		</div>
		<!--Contenido 4-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>4.1
				Introducción.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Todas
				las estructuras de datos consideradas hasta este momento son estructuras de
				datos estáticas y por ende la cantidad de memoria que ocupan debe ser declarado
				por anticipado y no puede ser modificado durante la ejecución del programa. En
				muchas ocasiones se necesitan estructuras que puedan cambiar de tamaño durante
				la ejecución del programa. Por supuesto, es posible hacer arreglos dinámicos,
				pero una vez creados, su tamaño también será fijo, y para hacer que crezcan o
				diminuyan de tamaño, deben ser reconstruidos desde el principio.</p>				<p style="text-indent: 20; line-height: 150%" >Ahora
				se introduce un nuevo concepto, el de estructuras dinámicas de datos. Las
				estructuras dinámicas permiten crear estructuras de datos que se adapten a las
				necesidades reales a las que suelen enfrentarse los programadores, pero no sólo
				eso, también permiten crear estructuras de datos muy flexibles, ya
				sea en cuanto al orden, la estructura interna o las relaciones entre los
				elementos que las componen. Este tipo de estructura es generada a partir de un
				tipo de dato conocido con el nombre de puntero.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Las
				listas son una de las estructuras de datos dinámicas más fundamentales entre;las empleadas para almacenar una colección de elementos.</p>

				<p style="text-indent: 20; line-height: 150%" >En
				el capítulo que se presenta a continuación se tratarán detalladamente estas
				estructuras de datos tan importantes, abarcando su definición, clasificación.</p>

				<p style="text-indent: 0; line-height: 150%" ><b>4.2
				Definición de Lista Enlazada</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				lista enlazada es una secuencia de nodos en el que cada uno de ellos está
				enlazado o conectado con el siguiente. Un nodo es una secuencia de caracteres en
				memoria
				dividida en campos (de cualquier tipo). La lista enlazada es una estructura de
				datos dinámica cuyos nodos suelen ser normalmente registros y que no tienen un
				tamaño fijo.</p>

				<p style="text-indent: 20; line-height: 150%" >Una
				lista es una estructura que se utiliza para almacenar información del mismo
				tipo, este ordenamiento explicito se </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" > manifiesta en que cada elemento contiene en sí mismo
				la dirección del siguiente elemento.</p>

				<p style="text-indent: 20; line-height: 150%" >En
				un nodo se puede considerar que hay dos campos, campo de información del nodo
				apuntado (info) y campo de enlace (Sig) conocido también como <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="apuntador" v-bind:data-content="Apuntador">apuntador</a> o
				puntero, que es la dirección de memoria del siguiente nodo si éste existe.
				Las figuras 4.1 y 4.2, muestran gráficamente las partes de un nodo.</p>

				<p style="line-height: 100%" align="center"><b>Figura
				4.1. </b> Estructura de un nodo</p>
				<p style="line-height: 100%" align="center"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.1.gif" width="111" height="28"></p>
				<p style="line-height: 100%" align="right"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>

				<p style="line-height: 100%" align="center"><b>Figura
				4.2. </b> Partes de un nodo.</p>
				<p style="line-height: 100%" align="center"><img border="0" src="contenido/capIV/Fig%204.2.gif" width="311" height="54"></p>
				<p style="line-height: 100%" align="right"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%; text-indent: 20" >A
				una lista enlazada se accede desde un puntero externo que contiene la dirección
				(referencia) al primer nodo de la lista. El campo siguiente del último elemento
				de la lista no debe de apuntar a ningún elemento, no debe de tener ninguna
				dirección, por lo que contiene un valor especial denominado puntero nulo.
				Según Cairó y Guardatí (1993), la lista vacía, es aquella que no tiene
				nodos, tiene el puntero externo de acceso a la lista apuntando a nulo, la figura
				4.3 representa gráficamente lo dicho anteriormente.</p>

				<p style="line-height: 150%; text-indent: 20" ></p>

				<p style="line-height: 150%" align="center"><b>Figura
				4.3. </b> Lista enlazada sin información o inicialización de lista (lista vacía).</p>

				<p style="line-height: 150%" align="center"><img border="0" src="contenido/capIV/fig4.3.gif" width="311" height="58"></p>
				<p style="line-height: 150%" align="right"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>

				<p style="line-height: 150%; text-indent: 20" >El
				número de nodos puede variar rápidamente en un proceso, aumentando los nodos
				por inserciones, o bien disminuyendo por eliminación de nodos.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 20" >La
				Figura 4.4 representa un ejemplo de una lista que almacena apellidos.</p>

				<p style="line-height: 100%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 0" align="center"><b>Figura
				4.4. </b> Lista de apellidos.</p>
				<p style="line-height: 100%; text-indent: 0" align="center"><img border="0" src="contenido/capIV/fig%204.4.gif" width="328" height="58"></p>
				<p style="line-height: 100%; text-indent: 0" align="right"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p style="line-height: 150%; text-indent: 20" >El
				primer nodo de la lista (figura 4.4) está apuntado por una variable Lista, de
				tipo puntero (Lista almacena la dirección del primer nodo). El campo siguiente
				del último nodo de la lista, tiene un valor NULO que indica que dicho nodo no
				apunta a ningún otro.</p>
				<p style="line-height: 150%; text-indent: 20" ></p>

				<p style="line-height: 150%; text-indent: 20" >Si
				todos los elementos almacenados en una lista son del mismo tipo, entonces se
				dice que la lista es <b><i>homogénea</i></b>. Sin embargo, si distintos tipos de elementos
				están almacenados en la lista (un elemento es entero y otro es un número
				real), entonces la lista es <b><i> heterogénea.</i></b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 20; line-height: 150%">La
				primera cosa que hay que observar acerca de las listas enlazadas es que ya no se
				tiene acceso directo a un elemento arbitrario de la lista. No se puede acceder a
				los nodos de una lista enlazada directamente, esto es, no se puede obtener
				directamente el elemento veinticinco de la lista, como se haría en una
				representación secuencial, en vez de ello, se accede al primer nodo mediante un
				puntero externo, al segundo nodo a través del puntero “siguiente” del
				primer nodo, al tercer nodo a través del puntero “siguiente” del segundo
				nodo, y así sucesivamente hasta el final. Así, cualquier operación que
				conlleve una posición en la lista recorrerá en el caso peor toda la lista para
				encontrar la posición deseada en la lista enlazada.</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 20; line-height: 150%">La
				mayor ventaja de las listas enlazadas es la utilización eficaz de la memoria,
				ya que se puede utilizar cualquier espacio vacío por no exigir posiciones
				secuenciales de memoria. Por el contrario, su mayor inconveniente es la lentitud
				del proceso, ya que es necesario ir recorriendo elemento a elemento a través de
				los punteros para poder acceder a uno específico.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="text-indent: 0; line-height: 150%"><b>4.3
				Aplicaciones de las Listas Enlazadas</b></p>
				<p  style="text-indent: 20; line-height: 150%">Según
				Cairó y Guardatí (1993), dos de las aplicaciones más conocidas de listas
				enlazadas son:</p>
				<blockquote>
				  <blockquote>
				    <ul>
				      <li>
				        <p  style="text-indent: 0; line-height: 200%">Representación
				        de polinomios.</li>
				      <li>
				        <p  style="text-indent: 0; line-height: 200%">Resolución
				        de colisiones (hash).</li>
				    </ul>
				        <p  style="text-indent: 0; line-height: 100%">
				  </blockquote>
				</blockquote>
				<p  style="text-indent: 20; line-height: 150%">En
				general puede decirse que las listas son muy útiles para aquellas aplicaciones
				en las cuales se necesite dinamismo en el crecimiento y reducción de las
				estructuras de datos.</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 20; line-height: 150%"><b><i>Representación
				de polinomios</i></b></p>
				<p  style="text-indent: 20; line-height: 150%">Las
				listas se utilizan para almacenar los coeficientes diferentes de cero del
				polinomio, junto al exponente. Así, por ejemplo, dado el polinomio:</p>
				<p ALIGN="center" style="text-indent: 20; line-height: 100%">P(x)
				= 3X4 + 0.5X3 + 6X – 4</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="text-indent: 0; line-height: 150%">una
				representación mediante listas, sería la que se muestra en la figura 4.5.</p>
				<b>				<p ALIGN="center" style="text-indent: 0; line-height: 150%">Figura
				4.5.</b> Representación de
				Polinomios usando listas</p>
				<p ALIGN="center" style="text-indent: 0; line-height: 150%"></p>
				<p ALIGN="center" style="text-indent: 0; line-height: 150%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.5.gif" width="282" height="48"></p>
				<p ALIGN="right" style="text-indent: 0; line-height: 150%"><img border="0" src="contenido/capIV/fuente%20cairo.gif" width="245" height="19"></p>
				<p ALIGN="right" style="text-indent: 0; line-height: 150%"></p>
				<p  style="text-indent: 20; line-height: 150%">El
				campo información de cada nodo de la lista contendrá dos campos: el campo
				COEFICIENTE y el campo EXPONENTE.</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 20; line-height: 150%">En
				el ejemplo, se utilizó una lista enlazada simple, pero se pudo haber utilizado
				una circular o también una lista doblemente enlazada (simple o circular).</p>
			</div>
			<div class="pagina" @click="navNext">
				<b><i>
				<p  style="text-indent: 20; line-height: 150%">Solución
				de colisiones (hash)</p>
				</i></b>
				<p  style="text-indent: 20; line-height: 150%">Al
				tratar el método de búsqueda por transformación de claves, se utilizan listas
				para resolver colisiones (método de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Encadenamiento" v-bind:data-content="Encadenamiento">encadenamiento</a>). A fin de evitar la
				reiteración, se sugiere al lector remitirse al Capítulo II (Arreglos) en al
				apartado<a href="c2112"> 2.6.3</a> donde se estudia el método por transformación de claves.</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<b>
				<p  style="text-indent: 0; line-height: 150%">4.4
				Clasificación de las Listas Enlazadas</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">De
				acuerdo a su estructura las listas enlazadas se pueden clasificar en:</p>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<b>
				<p  style="text-indent: 0; line-height: 150%">4.4.1
				Listas Simples</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Según
				Joyanes (1992), una lista simple es aquella que tiene un primero y un último
				elemento, existiendo una relación estructural entre sus diferentes elementos;
				normalmente cada elemento está relacionado con el anterior y el siguiente,
				excepto el primero (sólo con el segundo) y el último (sólo con el anterior).</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >En
				general se suelen expresar también las listas como un conjunto lógico de
				átomos; un átomo es un conjunto de elementos relacionados llamados campos,
				siendo el campo la unidad básica de información. Un campo se puede dividir en
				subcampos.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				4.6. </b> Representación de una lista simple.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capIV/fig%204.6.gif" width="332" height="59"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Declaración
				de una Lista Simple:</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				formato para definir una Lista Simple es:</p>				<p style="text-indent: 20; line-height: 100%" >Id_Lista
				= ^ Id_Nodo</p>
				<p style="text-indent: 20; line-height: 100%" >Id_Nodo
				= REGISTRO</p>
				<p style="text-indent: 100; line-height: 100%" >Nombre
				del campo de informacion: Tipo</p>
				<p style="text-indent: 100; line-height: 100%" >Nombre
				del campo de enlace:Id_Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin_Id_Nodo</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%">Donde:</p>
				<b>
				<p  style="line-height: 150%; text-indent: 20">Id_Lista
				</b>:<i> </i>Identificador
				válido de la lista.</p>
				<b>
				<p  style="line-height: 150%; text-indent: 20">Id_Nodo
				</b>:<i> </i>Identificador
				válido de la<i> </i>estructura de cada nodo de la lista.</p>
				<p  style="line-height: 150%; text-indent: 20"><b>Nombre
				del <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Campo de información" v-bind:data-content="CampoDeInformación">campo de información</a> : </b>Campo
				de información del nodo apuntado.</p>
				<p  style="line-height: 150%; text-indent: 20"><b>Nombre
				del campo de enlace: </b>Es la
				dirección de memoria del siguiente nodo.</p>
				<b>
				<p  style="line-height: 150%; text-indent: 20">Tipo:</b>
				Describe el tipo del <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Campo de información" v-bind:data-content="CampoDeInformación">campo de información</a> del nodo apuntado. Puede ser de
				cualquier tipo.</p>
				<p  style="line-height: 150%; text-indent: 20">Las
				variables de tipo Lista se declaran en la sección Variables.</p>

				<b>
				<p  style="line-height: 100%; text-indent: 0">Tipos</p>
				</b>
				<p  style="line-height: 100%; text-indent: 20">Lista
				= ^Nodo</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 100%; text-indent: 20">Nodo
				= Registro</p>
				<p  style="line-height: 100%; text-indent: 85">Info:
				Entero</p>
				<p  style="line-height: 100%; text-indent: 85">Sig:
				Lista</p>
				<p  style="line-height: 100%; text-indent: 20">Fin_Nodo</p>

				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 100%">Variables</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">L
				: Lista;</p>
				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%">4.4.1.1
				Operaciones con Listas Simples</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones que se pueden realizar con la lista simple son:</p>
				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.1.1.1
				Recorrido de una Lista Simple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de recorrido consiste en visitar cada uno de los nodos que forman la
				lista. La visita de un nodo puede definirse por medio de una operación muy
				simple (por ejemplo, la impresión de la información del mismo), o por medio de
				operaciones tan complejas como se desee.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 20">Para
				recorrer todos los nodos de una lista se comienza con el primero. Tomando el
				valor del campo SIGUIENTE de éste se avanza al segundo; a su vez, el campo
				SIGUIENTE del segundo dará acceso al tercero, y así sucesivamente. En general
				la dirección de un nodo, excepto el primero, está dada por el campo SIGUIENTE
				de su predecesor.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p  style="line-height: 100%; text-indent: 20">El
				algoritmo 4.1 presenta los pasos necesarios para recorrer iterativamente una
				lista.</p>

				
				<p ALIGN="center" style="line-height: 200%; text-indent: 0"><a href="a4.1"><b>Algoritmo
				4.1. Imprimir</b></a></p>

				<b>
				<p  style="line-height: 100%; text-indent: 0">4.4.1.1.2
				Inserción de un Elemento en una Lista Simple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de inserción consiste en agregar un nuevo nodo a la lista, siempre
				supone crear un nuevo nodo y hacer un movimiento de punteros. Se pueden
				presentar cuatro casos en la operación de inserción:</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<b>
				<p  style="line-height: 100%; text-indent: 0">4.4.1.1.2.1
				Inserción al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca al principio de la lista,</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 0"> convirtiéndose en el primero de
				la misma. El
				proceso es relativamente simple, aparece descrito en el algoritmo 4.2.</p>
				<p  style="line-height: 150%; text-indent: 0"></p>

				
				<p ALIGN="center" style="line-height: 150%; text-indent: 20"><a href="a4.2">Algoritmo
				4.2. </a><a href="a4.2">Inserción
				al Inicio</a></p>
				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%; text-indent: 0">EJEMPLO
				4.1.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.7 se presenta un ejemplo de inserción al inicio de la lista.</p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				<p ALIGN="center" style="line-height: 150%"><b>Figura
				4.7. </b> Inserción al inicio de la lista</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/fig%204.7.gif" width="284" height="78"></p>
				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/nota1.gif" width="388" height="25"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%"><b>4.4.1.1.2.2
				Inserción al Final</b></p>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca al final de la lista, convirtiéndose en el último de la
				misma. El algoritmo 4.3. describe este proceso.<b></p>
				</b>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.3">Algoritmo
				4.3. Inserción al Final</a></p>				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%">EJEMPLO
				4.2.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.8. se presenta un ejemplo de inserción al final de la lista.</p>				<p ALIGN="center" style="line-height: 100%"><b>Figura
				4.8. </b> Inserción al final de la lista</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig%204.8.gif" width="284" height="73"></p>
				<p ALIGN="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota2.gif" width="411" height="23"></p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%" ><b>4.4.1.1.2.3
				Inserción antes de un Elemento Dado</b></p>

				<p style="line-height: 150%; text-indent: 20" >El
				nuevo nodo se coloca antes de otro nodo dado como referencia.</p>

				<p style="line-height: 100%; text-indent: 20" ></p>

				<b>
				<p  style="line-height: 150%">EJEMPLO
				4.3.</b></p>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.9 se presenta un ejemplo de inserción antes de un elemento dado,
				aplicando el algoritmo 4.4.</p>
				<p align="center" style="line-height: 100%"></p>
				<p align="center" style="line-height: 100%"><b>Figura
				4.9. </b> Inserción de un nodo antes de un elemento dado</p>
				<p align="center" style="line-height: 100%"></p>
				<b>
				<p align="center" style="line-height: 100%"><img border="0" src="contenido/capIV/fig4.9.gif" width="351" height="106"></p>
				</b>
				<p align="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p align="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota7.gif" width="411" height="25"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="line-height: 150%"><a href="a4.4">Algoritmo
				4.4. Inserción antes de un Elemento Dado</a></p>
				<p align="center" style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.1.1.2.4
				Inserción después de un Elemento Dado</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">A
				continuación se presenta el algoritmo correspondiente a la inserción de un
				nodo sucediendo a otro dado como referencia.</p>

				
				<p ALIGN="center" style="line-height: 150%"></p>
				<p ALIGN="center" style="line-height: 150%"><a href="a4.5">Algoritmo
				4.5. Inserción después de un Elemento Dado</a></p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%; text-indent: 0">EJEMPLO
				4.4.</p>
				<p  style="line-height: 150%; text-indent: 20"></b>En
				la figura 4.10 se da un ejemplo de inserción de un nodo, aplicando el algoritmo
				4.5.</p>
				<p  style="line-height: 150%; text-indent: 20">En
				todas las operaciones de listas tratadas hasta el momento, no se ha considerado
				algún orden entre los elementos. Si se supone que la lista está ordenada, en
				el momento de insertarle un nuevo valor, habrá que mantener el orden
				previamente establecido.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 0">Sin
				embargo, como los elementos están relacionados por medio de punteros, esta
				operación (inserción manteniendo el orden) no tiene mayor costo ya que no se
				deben mover elementos.</p>



				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><b>Figura
				4.10. </b> Inserción de nodo<b>s</b></p>

				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.10.gif" width="326" height="117"></p>

				<p  style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>

				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\nota3.gif" width="410" height="25"></p>

				<b>
				<p  style="line-height: 100%">4.4.1.1.3
				Borrado de un Elemento en una Lista Simple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">A
				continuación se tratará sobre la operación de borrado de nodos de una lista
				simple. Como en el caso de la inserción, sólo se considerarán listas
				desordenadas (los algoritmos para listas ordenadas no varían mucho de los
				correspondientes para listas desordenadas).</p>

			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 20">La
				operación de borrado supone enlazar el nodo anterior con el nodo siguiente al
				que va a ser borrado y liberar la memoria ocupada por el nodo a borrar. Se
				pueden presentar tres casos en esta operación:</p>				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.1.1.3.1
				Borrado al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				quita el primer elemento de la lista, redefiniendo el valor del puntero al nuevo
				inicio de la misma. El proceso es muy sencillo; aparece descrito en el algoritmo
				4.6.</p>				
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.6">Algoritmo
				4.6. Borrado
				al Inicio</a> </p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">EJEMPLO
				4.5.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.11. se presenta un ejemplo de borrado del primer nodo de una lista,
				aplicando el algoritmo anterior.</p>				
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p align="center" style="line-height: 100%"><b>Figura 4.11.
				</b>
				Eliminación del primer nodo de una lista.</p>
				<p align="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.11.gif" width="280" height="80"></p>
				<p align="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p align="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota4.gif" width="411" height="24"></p>
				<p align="center" style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.1.1.3.2
				Borrado al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				quita el último nodo de la lista, redefiniendo a NULO el campo SIGUIENTE de su
				predecesor. Para alcanzar el último nodo se deberá recorrer previamente toda
				la lista, excepto si se usara un puntero indicando el final de la misma. A
				continuación se presenta un algoritmo de solución, considerando que solamente
				se tiene un puntero al inicio de la lista.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.7">Algoritmo
				4.7. Borrado al Final</a></p>			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%">EJEMPLO
				4.6.</p>
				<p  style="line-height: 150%; text-indent: 20"></b>En
				la figura 4.12. se presenta un ejemplo de borrado del último nodo de una lista.</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 20"><b>Figura
				4.12. </b> Eliminación del último
				nodo de una lista </p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.12.gif" width="313" height="101"></p>
				<p ALIGN="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota5.gif" width="411" height="23"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 100%">4.4.1.1.3.3
				Borrado de un Nodo con Información Dada.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				debe buscar al nodo que contenga la información dada como referencia y se
				elimina, estableciendo el correspondiente enlace entre su predecesor y su
				sucesor. El algoritmo 4.8. describe este proceso.</p>
				
				<p ALIGN="center" style="line-height: 200%"><a href="a4.8">Algoritmo
				4.8. Borrado de un Nodo con Información Dada</a></p>
				<p ALIGN="center" style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 100%">EJEMPLO
				4.7.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.13. presentamos un ejemplo de borrado de un nodo con información
				Dada.</p>

				<p ALIGN="center" style="line-height: 200%"><b>Figura
				4.13. Eliminación de un nodo con información Dada</b></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.13.gif" width="271" height="98"></p>
				<p ALIGN="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota6.gif" width="399" height="13"></p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.1.1.4
				Búsqueda en una Lista Simple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista se realiza de modo
				secuencial. Se deben recorrer los nodos, tomando el campo SIGUIENTE como puntero
				al siguiente nodo a visitar. Debido a esto puede decirse que esta operación está
				implícita en algunos de los casos de inserción y borrado.</p>
								<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.9">Algoritmo
				4.9. Búsqueda en una lista simple</a></p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.2
				Lista Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Según
				Joyanes (1992), esta estructura conocida también como anillo es una subclase de
				una lista simple consistente en enlazar el último elemento de la lista con el
				primero (Ver figura 4.14).</p>				<p  style="line-height: 150%; text-indent: 20">Este
				tipo de lista es cerrada. Puede recorrerse circularmente, en el sentido indicado
				por los punteros. Las listas circulares presentan el inconveniente de los bucles
				o lazos infinitos que se presentan cuando no se tiene especial </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 0"> cuidado en
				detectar el final de la lista.  Un
				convenio popular consiste en tener la última celda con un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> de regreso
				a la primera.</p>
				<b>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0">Figura
				4.14.</b> Lista circular</p>
				<b>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\Fig4.14.gif" width="280" height="45"></p>
				<p ALIGN="right" style="line-height: 150%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p  style="line-height: 150%; text-indent: 20">Declaración
				de una Lista Circular:</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				declaración de una lista circular es similar a la declaración de una lista
				simple, en caso de duda se sugiere al lector remitirse al apartado <a href="c416"> 4.4.1</a> de este
				capítulo.</p>				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.2.1
				Operaciones con Listas Circulares</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones en listas circulares son similares a las operaciones en listas
				simples.</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%">4.4.2.1.1
				Recorrido de una Lista Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				el caso de la operación de recorrido de listas circulares, es necesario aclarar
				que se debe considerar algún criterio para detectar cuándo se han visitado
				todos los nodos para evitar caer en ciclos infinitos.</p>
				<b>
				<p ALIGN="CENTER" style="line-height: 100%"></p>
				</b>
				<p ALIGN="CENTER" style="line-height: 150%"><a href="a4.10">Algoritmo
				4.10. Recorrido de una Lista Circular</a></p>
				<p ALIGN="CENTER" style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.2.1.2
				Inserción de un Elemento en una Lista Circular</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Para
				explicar la inserción de un elemento en una lista circular,; se considerarán los
				siguientes casos:</p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.2.1.2.1
				Inserción antes de un Elemento Dado</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%">Se
				debe recorrer la lista (considerando algún criterio para no caer en ciclos
				infinitos) hasta encontrar el nodo dado como referencia para insertar el nuevo
				nodo antes del mismo.</p>
				<p  style="line-height: 100%"></p>				<p ALIGN="CENTER" style="line-height: 150%"><a href="a4.11">Algoritmo
				4.11. Inserción antes de un Nodo Dado</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.2.1.2.2
				Inserción después de un Elemento Dado</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				debe recorrer la lista (considerando algún criterio para no caer en ciclos
				infinitos) hasta encontrar el nodo dado como referencia para insertar el nuevo
				nodo después del mismo.</p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.12">Algoritmo
				4.12. Inserción después de un Elemento Dado</a></p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.2.1.3
				Borrado de un Elemento dado en una lista circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				debe recorrer la lista (considerando algún criterio para no caer en ciclos
				infinitos) hasta encontrar el nodo a eliminar, estableciendo el correspondiente
				enlace entre su predecesor y su sucesor.</p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.13">Algoritmo
				4.13. Borrado de un Elemento Dado</a></p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.2.1.4
				Búsqueda de un Elemento en una Lista Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 0">circular
				se realiza de modo secuencial, considerando algún criterio para no caer en
				ciclos infinitos.Se
				deben recorrer los nodos, tomando el campo SIGUIENTE como puntero al siguiente
				nodo a visitar. Debido a esto puede decirse que esta operación está implícita
				en algunos de los casos de inserción y borrado.</p>
				<p  style="line-height: 150%; text-indent: 0"></p>

				
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.14">Algoritmo
				4.14.
				Búsqueda de un Elemento</a></p>
				<p  style="line-height: 150%; text-indent: 0"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.3
				Lista Doblemente Enlazada</p>
				<p  style="line-height: 150%; text-indent: 20"></b>Según
				el Web Site; <b>http://www.itlp.edu.mx/publica/; tutoriales/estru1/46.htm</b>, una
				lista doble o doblemente enlazada es una colección de nodos en la cual cada
				nodo tiene dos punteros, uno de ellos apuntando a su predecesor (li) y otro a su
				sucesor (ld). Por medio de estos punteros se podrá avanzar o retroceder a
				través de la lista, según se tomen las direcciones de uno u otro puntero.</p>
				<p  style="line-height: 150%; text-indent: 0"></p>
				<p  style="line-height: 150%; text-indent: 20">La
				figura 4.15 muestra la estructura de un nodo en una lista doble.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p ALIGN="center" style="line-height: 200%; text-indent: 0">Figura
				4.15. </b>Nodo de una lista doble</p>
				<p ALIGN="center" style="line-height: 200%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.15.gif" width="103" height="31"></p>
				<p  style="line-height: 200%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>

				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.16 se muestra un ejemplo de una lista doblemente enlazada que
				almacena números enteros:</p>				<p ALIGN="center" style="line-height: 200%; text-indent: 0"><b>Figura
				4.16. </b>Lista doblemente enlazada</p>
				<p ALIGN="center" style="line-height: 200%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.16.gif" width="303" height="28"></p>
				<p ALIGN="right" style="line-height: 200%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p class="MsoNormal" style="text-align: justify; text-indent: 20; line-height: 150%"><b><span style="font-family:&quot;Century Gothic&quot;">Declaración
				de una Lista Doblemente Enlazada:				</span></b></p>
				<p class="MsoNormal" style="text-align: justify; text-indent: 20; line-height: 150%"><strong><span style="font-family:&quot;Century Gothic&quot;;font-weight:normal">El
				formato para definir una Lista doblemente enlazada es:
				</span></strong></p>

				<p style="text-indent: 20; line-height: 150%" >Id_Listadoble
				= ^ Id_Nodo</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >Id_Nodo
				= <b>REGISTRO</p>
				</b>
				<p style="text-indent: 100; line-height: 100%" >Nombre
				del campo de informacion: Tipo</p>
				<p style="text-indent: 100; line-height: 100%" >Nombre
				del campo de Enlace anterior:;</p>
				<p style="text-indent: 100; line-height: 100%" >Id_Listadoble</p>
				<p style="text-indent: 100; line-height: 100%" >Nombre
				del campo de enlace Siguiente:</p>
				<p style="text-indent: 100; line-height: 100%" >Id_Listadoble</p>
				<b>
				<p style="text-indent: 20; line-height: 100%" >Fin_Id_Nodo</p>
				</b>
				<p  style="text-indent: 20; line-height: 150%"></p>
				<p  style="text-indent: 0; line-height: 150%">Donde:</p>
				<b>
				<p  style="text-indent: 20; line-height: 150%">Id_Listadoble
				:</b><i> </i>Identificador válido
				de la lista doble.</p>
				<b>
				<p  style="text-indent: 20; line-height: 150%">Id_Nodo:</b><i>
				</i>Identificador válido de la<i> </i>estructura de cada nodo de la lista.</p>
				<b>
				<p  style="text-indent: 20; line-height: 150%">Nombre
				del campo de informacion : </b>Campo
				de información del nodo apuntado</p>
				<b>
				<p  style="text-indent: 20; line-height: 150%">Nombre
				del campo de enlace anterior:</b> es
				la dirección de memoria del nodo anterior</p>
				<b>
				<p  style="text-indent: 20; line-height: 150%">Nombre
				del campo de enlace siguiente:</b>
				es la dirección </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%">de memoria del siguiente nodo.</p>

				<p style="line-height: 100%"></p>

				<p style="text-indent: 20; line-height: 150%"><b>Tipo: </b>Describe
				el tipo del <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Campo de información" v-bind:data-content="CampoDeInformación">campo de información</a> del nodo apuntado. Puede ser de cualquier
				tipo.</p>

				<p style="line-height: 100%"></p>
				<p class="MsoNormal" style="text-align: justify; text-indent: 14.2pt; line-height: 200%"><strong><span style="font-family:&quot;Century Gothic&quot;;font-weight:normal">Las
				variables de tipo Listadoble se declaran en la sección Variables.				</span></strong></p>
				<p class="MsoBodyText" style="line-height: 200%" ><b><span style="font-family:&quot;Century Gothic&quot;">
				;</span>Tipo				</b></p>
				<p style="line-height: 100%; text-indent: 20" >Listadoble
				= ^Nodo				</p>
				<p style="line-height: 100%; text-indent: 20" ><span style="mso-spacerun:
				yes"></span>Nodo = Registro				</p>
				<p style="line-height: 100%; text-indent: 80" >Info:
				Entero				</p>
				<p style="line-height: 100%; text-indent: 80" >Ant:
				Listadoble				</p>
				<p style="line-height: 100%; text-indent: 80" >Sig
				: Listadoble				</p>
				<p style="line-height: 100%; text-indent: 20" >Fin_Nodo				</p>
				<p style="text-indent: 0; line-height: 200%" ><b>Variable</b>:				</p>
				<p style="text-indent: 14.2pt; line-height: 100%" >L
				: Listadoble</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.3.1
				Operaciones con Lista Doblemente Enlazada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones que pueden llevarse a cabo con este tipo de estructuras son
				básicamente las mismas que se han tratado para los casos anteriores. Se
				presentarán en esta sección los algoritmos correspondientes para las
				operaciones de:</p>
				<b>				<p  style="line-height: 150%; text-indent: 0">4.4.3.1.1
				Recorrido de una Lista Doblemente Enlazada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Al
				tener doble enlace, la lista puede recorrerse tanto del inicio al final (tomando
				los enlaces anteriores), como en sentido inverso (tomando los enlaces
				siguientes). Cualquiera que sea el sentido del recorrido, el algoritmo es
				similar al que se presenta para listas simples.</p>				
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.15">Algoritmo
				4.15. Imprimir Lista Doble</a></p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.3.1.2
				Inserción de un Elemento en una Lista Doblemente Enlazada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				inserción de un elemento consiste en agregar un nuevo</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%" >nodo a la lista y establecer los
				punteros correspondientes, tomando en consideración que ahora hay
				que manipular dos punteros: anterior y siguiente. La inserción puede llevarse a
				cabo de cuatro formas diferentes:</p>

				<p style="line-height: 150%" ></p>
				<b>
				<p  style="line-height: 150%">4.4.3.1.2.1
				Inserción al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca al principio de la lista y se establecen los enlaces
				correspondientes, asignándole al campo anterior del nuevo nodo, nulo; de este
				modo se convierte en el primero de la misma. El algoritmo 4.16. describe este
				proceso.</p>				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.16">Algoritmo
				4.16. Inserción al Inicio</a></p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"></p>
				<p ALIGN="left" style="line-height: 150%; text-indent: 0"><b>EJEMPLO
				4.8.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.17. se da un ejemplo de inserción al inicio de una lista doblemente
				ligada.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p ALIGN="center" style="line-height: 150%"><b>Figura</b>
				<b>4.17.</b> Inserción al inicio de la lista</p>

				<p ALIGN="center" style="line-height: 100%"></p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/fig4.17.gif" width="310" height="90"></p>
				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>

				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/nota8.gif" width="411" height="28"></p>

				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">4.4.3.1.2.2
				Inserción al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca al final de la lista y se establecen los enlaces
				correspondientes, convirtiéndose en el último de la misma. El algoritmo 4.17.
				describe el proceso.</p>				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.17">Algoritmo
				4.17. Inserción al Final</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%">EJEMPLO
				4.9.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.18. se presenta un ejemplo de inserción al final de una lista
				doblemente ligada.</p>

				<b>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"></p>
				<p ALIGN="center" style="line-height: 100%">Figura
				4.18.</b> Inserción al final de la
				lista</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.18.gif" width="287" height="89"></p>
				<p ALIGN="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>

				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota9.gif" width="411" height="23"></p>

				<p ALIGN="center" style="line-height: 150%"></p>

				<b>
				<p  style="line-height: 150%">4.4.3.1.2.3
				Inserción antes de un Elemento Dado</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca precediendo a otro nodo dado como referencia y
				estableciendo los enlaces correspondientes.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p ALIGN="center" style="line-height: 100%"><a href="a4.18">Algoritmo
				4.18. Inserción antes de un Elemento Dado</a></p>

				<p ALIGN="center" style="line-height: 100%"></p>

				<b>
				<p  style="line-height: 100%">EJEMPLO
				4.10.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.19 se presenta un ejemplo de inserción, aplicando el algoritmo
				4.18.</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<b>
				<p ALIGN="center" style="line-height: 100%">Figura
				4.19.</b> Inserción de un nodo antes de un
				elemento dado</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.19.gif" width="294" height="97"></p>
				<p  style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota7.gif" width="411" height="25"></p>

				<b>
				<p  style="line-height: 100%">4.4.3.1.2.4
				Inserción después de un Elemento Dado.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca sucediendo a otro nodo dado como referencia y estableciendo
				los enlaces correspondientes.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.19">Algoritmo
				4.19. Inserción
				después de un Nodo Dado</a> </p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">EJEMPLO
				4.11.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.20 se presenta un ejemplo de inserción, aplicando el algoritmo
				4.19.</p>				<b>
				<p ALIGN="center" style="line-height: 150%">Figura
				4.20.</b> Inserción de un nodo
				después de un elemento dado</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/fig4.20.gif" width="293" height="131"></p>
				<p  style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota3.gif" width="410" height="25"></p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%">4.4.3.1.3
				Borrado de un Elemento en una Lista Doblemente Enlazada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de borrado de un nodo en una lista doblemente enlazada, al igual que
				en el caso de las listas simples, consiste en eliminar un elemento de la lista y
				restablecer los enlaces que correspondan. Pueden presentarse tres casos:</p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">4.4.3.1.3.1
				Borrado al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en quitar el primer nodo de la lista, cualquiera que sea su información,
				redefiniendo el puntero al inicio de la lista. El algoritmo 4.20. describe este
				proceso.</p>
				<p  style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.20">Algoritmo
				4.20. Borrado al Inicio</a></p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">EJEMPLO
				4.12.</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.21. se presenta un ejemplo de borrado del primer nodo de una lista,
				aplicando el algoritmo 4.20.</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p ALIGN="center" style="line-height: 100%">Figura
				4.21.</b> Eliminación del primer
				nodo de una lista</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.21.gif" width="337" height="92"></p>
				<p  style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota4.gif" width="411" height="24"></p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.3.1.3.2
				Borrado al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en quitar el último nodo de la lista, cualquiera que sea su información,
				redefiniendo el puntero al final de la lista. El algoritmo 4.21. describe este
				proceso.</p>
				<p  style="line-height: 100%"></p>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.21">Algoritmo
				4.21. Borrado al Final</a></p>
				<p ALIGN="center" style="line-height: 100%"></p>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.22. se da un ejemplo de borrado del último nodo de una lista,
				aplicando el algoritmo 4.21.</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p ALIGN="center" style="line-height: 150%">Figura
				4.22.</b> Eliminación del último
				nodo de una lista</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.22.gif" width="287" height="89"></p>
				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="C:\Edl-Book\contenido\capIV\nota5.gif" width="411" height="23"></p>
				<p ALIGN="center" style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.3.1.3.3
				Borrado de un Nodo con Información Dada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en quitar el nodo que contenga una información especifica, y establecer los
				enlaces entre su antecesor y su sucesor. Se deben considerar los casos en los
				cuales el nodo a eliminar sea el primero o el último de la lista.</p>
				<p ALIGN="center" style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.22">Algoritmo
				4.22.
				Eliminar Nodo</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%">EJEMPLO
				4.12.</b></p>
				<p  style="line-height: 150%; text-indent: 20">En
				la figura 4.23. se presenta un ejemplo de borrado de un nodo con información
				dada contemplando todos los casos.</p>

				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><b>Figura</b>
				<b>4.23.</b> Eliminación de un nodo con información Dada.</p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.23a.gif" width="323" height="62"></p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0">a)
				El nodo es el primero,</p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.23b.gif" width="288" height="66"></p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0">b)
				El nodo es el último,</p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.23c.gif" width="313" height="73"></p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0">c)
				El nodo es intermedio</p>

				<p ALIGN="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p style="line-height: 150%" >4.4.3.1.4
				Búsqueda en una Lista Doblemente Enlazada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista doblemente enlazada se
				realiza de modo secuencial, al igual que en una lista simple. Se puede realizar
				el recorrido de la lista en dos sentidos, sin embargo en este caso sólo se
				estudiará el recorrido a través del enlace siguiente.</p>
				<p align="center" style="line-height: 200%; text-indent: 0"><a href="a4.23">Algoritmo
				4.23. Búsqueda en una Lista Doblemente Enlazada</a></p>
				<p  style="line-height: 150%; text-indent: 0"><b>4.4.4
				Lista Doblemente Enlazada Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Según
				el Web Site <b>http://www.itlp.edu.mx/publica/tuto-; riales/ estru1/46.htm</b>, en este tipo de lista doble, el puntero izquierdo
				del primer nodo apunta al último nodo de la lista, y el puntero derecho del
				último nodo apunta al primer nodo de la lista (Ver figura 4.24).</p>
				<p align="center" style="line-height: 150%; text-indent: 0"><b>Figura
				4.24.</b> Lista circular doblemente enlazada</p>
				<p align="center" style="line-height: 150%; text-indent: 0"><img border="0" src="contenido/capIV/fig4.24.gif" width="302" height="56"></p>
				<p align="right" style="line-height: 150%; text-indent: 0"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">La
				principal ventaja de las listas circulares es que permiten la navegación en
				cualquier sentido a través de la misma y además, se puede recorrer toda la
				lista partiendo de cualquier nodo. Sin embargo, se debe hacer notar que hay que
				establecer condiciones adecuadas para detener el recorrido de una lista y así
				evitar caer en ciclos infinitos.</p>				<b>
				<p  style="line-height: 150%; text-indent: 20">Declaración
				de una Lista Doble Enlazada Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				declaración de una lista doblemente enlazada circular es similar a la
				declaración de una lista doble, en caso de duda se sugiere al lector remitirse
				al apartado <a href="c434"> 4.4.3</a> de este capítulo.</p>
				<b>				<p  style="line-height: 150%; text-indent: 0">4.4.4.1
				Operaciones de una Lista Doblemente Enlazada Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones en listas doblemente enlazada circular son similares a las
				operaciones consideras en listas circulares, por tal motivo, se recomienda
				dirigirse al apartado <a href="c430">4.4.2</a><b> </b>para profundizar sobre dichas
				operaciones, ya que a continuación explicamos solo el recorrido de una lista
				doblemente enlazada circular.</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%; text-indent: 0">4.4.4.1.1
				Recorrido de una Lista Doblemente Enlazada Circular</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">En
				el caso de la operación de recorrido de listas doblemente enlazadas circulares,
				es necesario aclarar que se debe considerar algún criterio para detectar
				cuándo se han visitado todos los nodos y así evitar caer en ciclos infinitos.
				Al tener doble enlace, la lista puede recorrerse tanto del inicio al final
				(tomando los enlaces anteriores), como en sentido inverso (tomando los enlaces
				siguientes). Cualquiera que sea el sentido del recorrido, el algoritmo es
				similar al que se presenta para listas circulares.</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><a href="a4.24">Algoritmo
				4.24. Imprimir</a> </p>
				<i>				</i><b>
				<p ALIGN="left" style="line-height: 150%; text-indent: 0">4.4.5
				Lista Múltiple o Multiligada</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Es
				aquella lista enlazada (lista principal) que posee uno o varios campos
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a> a otras listas enlazadas (sublistas). Las listas Multiligadas pueden
				ser simples, dobles, circulares y dobles circulares.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>
				<p  style="line-height: 150%; text-indent: 20">Existen
				2 tipos de listas Multiligadas:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b><i>
				<p  style="line-height: 150%; text-indent: 20">Lista
				Multiligada Homogénea</i></b><i>: </i>También
				llamada Lista Generalizada, y es aquella en la cual todos los nodos poseen la
				misma estructura, por lo tanto se declara sólo un registro para identificar la
				estructura de los nodos de dicha lista.</p>
				<i>
				<p  style="line-height: 150%"></p>
				</i><b>
				<p  style="line-height: 150%">Declaración
				de una lista Multiligada Homogénea:</p>
				</b>
				<p  style="line-height: 200%; text-indent: 20">La
				declaración de una lista multiligada homogénea se realizará dependiendo de la
				estructura de la misma (simple o doble). Para la explicación de este tipo de
				lista se utilizará la estructura de una lista simple, Véase Figura 4.25.</p>
				<b>
				<p  style="line-height: 100%">Tipo</p>
				</b>
				<p  style="line-height: 100%; text-indent: 20">Listamult
				= ^Nodo</p>
				<p  style="line-height: 100%; text-indent: 20">Nodo
				= Registro</p>
				<p  style="line-height: 100%; text-indent: 80">Info:
				Entero</p>
				<p  style="line-height: 100%; text-indent: 80">Sublist
				: Listamult</p>
				<p  style="line-height: 100%; text-indent: 80">Sig:
				Listamult</p>
				<p  style="line-height: 100%; text-indent: 20">Fin_Nodo</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%">Variables</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">L
				: Listamult</p>				<b>
				<p ALIGN="center" style="line-height: 150%">Figura
				4.25. </b>Lista Multiligada
				Homogénea</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="fig4.25.gif" width="303" height="78"></p>
				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p  style="line-height: 150%; text-indent: 20"><b><i>Lista
				Multiligada Heterogénea:</i></b> Es
				aquella en la cual existen nodos con diferentes estructuras, es decir, se
				declarará un registro para identificar la estructura de cada nodo.</p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%; text-indent: 20">Declaración
				de una lista Multiligada Heterogénea:</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				declaración de una lista multiligada heterogénea se realizará dependiendo de
				la estructura de la misma (simple o doble). Para la explicación de este tipo de
				lista se utilizará la estructura de una lista simple (ver Figura 4.26).</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 100%">Tipo</p>
				</b>
				<p  style="line-height: 100%; text-indent: 20">Listamult
				= ^Nodo</p>
				<p  style="line-height: 100%; text-indent: 20">Sub
				= ^Nodo_Sub</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<p  style="text-indent: 20; line-height: 200%">Nodo
				= Registro</p>
				<p  style="line-height: 100%; text-indent: 80">Info:
				Entero</p>
				<p  style="line-height: 100%; text-indent: 80">Sublist
				: Sub</p>
				<p  style="line-height: 100%; text-indent: 80">Sig:
				Listamult</p>
				<p  style="line-height: 100%; text-indent: 20">Fin_Nodo</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<p  style="line-height: 200%; text-indent: 20">Nodo_Sub
				= Registro</p>
				<p  style="line-height: 100%; text-indent: 80">Info2
				: Entero</p>
				<p  style="line-height: 100%; text-indent: 80">Sig2:
				Sub</p>
				<p  style="line-height: 100%; text-indent: 20">Fin_Nodo_Sub</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%; text-indent: 20">Variables</p>
				</b>
				<p  style="line-height: 100%; text-indent: 40">L
				: Listamult</p>

				<b>
				<p ALIGN="center" style="line-height: 100%">Figura
				4.26. </b>Lista</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.26.gif" width="319" height="66"></p>
				<p  style="line-height: 200%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20" align="right"></p>
				<b>
				<p  style="line-height: 150%">4.4.5.1
				Operaciones con Lista Múltiple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones que se muestran a continuación son similares a las utilizadas en
				una lista simple, con la diferencia de que se implementarán algoritmos
				distintos para la manipulación de la lista principal y las sublistas, por tal
				motivo, solo se explicarán los siguientes algoritmos: Recorrido de una lista
				múltiple, inserción al inicio, inserción al final, eliminación al inicio y
				eliminación al final. Recomendamos revisar el apartado de listas simples si
				desea recordar las siguientes operaciones: Inserción<a href="c423"> antes</a> de un elemento
				dado,
				inserción <a href="c424"> después</a> de un elemento dado,; <a href="c428">Borrado de un
				nodo</a> con información dada.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%">4.4.5.1.1
				Recorrido de una Lista Múltiple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				debe recorrer secuencialmente cada nodo de la lista principal con su sublista,
				hasta llegar al final de la lista principal.</p>

				<p ALIGN="center" style="line-height: 150%"><a href="a4.25">Algoritmo
				4.25. Imprimir</a></p>

				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%">4.4.5.1.2
				Inserción de un Elemento en una Lista Múltiple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de inserción consiste en agregar un nuevo nodo a la lista principal
				o a la sublista;<b> </b>siempre supone crear un nuevo nodo y hacer un movimiento
				de punteros. Se pueden presentar cuatro casos en la operación de inserción,
				que se tratarán tanto para la lista principal como para la sublista:</p>
				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.5.1.2.1
				Inserción al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en agregar un elemento nuevo al inicio de la lista principal o sublista. Cabe
				señalar que antes de realizar la inserción en la sublista se debe recorrer la
				lista principal hasta ubicar el nodo en el cual se insertará el nuevo nodo.</p>
			</div>
			<div class="pagina" @click="navNext">
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.26">Algoritmo
				4.26. Inserción al Inicio en la Lista Principal</a></p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				<p ALIGN="center" style="line-height: 150%"><a href="a4.27">Algoritmo
				4.27. Inserción al Inicio en la Sublista</a></p>
				<p ALIGN="center" style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.5.1.2.2
				Inserción al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en agregar un elemento nuevo al final de la lista principal o sublista. Cabe
				señalar que antes de realizar la inserción en la sublista se debe recorrer la
				lista principal hasta ubicar el nodo en el cual se insertará el nuevo nodo.</p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.28">Algoritmo
				4.28. Inserción
				al Final de la Lista Principal</a> </p>				<p ALIGN="center" style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.29">Algoritmo
				4.29. Inserción
				al Final de la Sublista</a> </p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				<p  style="line-height: 150%"><b>4.4.5.1.3
				Borrado de un elemento en una lista múltiple</b></p>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en eliminar un nodo, ya sea en la lista principal o</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 0"> sublista.Se pueden
				presentar tres casos en la operación de eliminación, los cuales se tratarán
				tanto para la lista principal como para la sublista:</p>

				<p  style="line-height: 150%; text-indent: 0"></p>

				<b>
				<p  style="line-height: 150%">4.4.5.1.3.1
				Borrado al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste
				en eliminar un elemento al inicio de la lista principal o sublista. Cabe
				señalar que antes de realizar la eliminación en la sublista se debe recorrer
				la lista principal hasta ubicar el nodo que referencia a la sublista donde se
				eliminará el nodo.</p>
				<p  style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.30">Algoritmo
				4.30. Borrado al Inicio de un Elemento en la Lista Principal</a></p>				<p ALIGN="center" style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.31">Algoritmo
				4.31. Borrado al Inicio de un Elemento en la Sublista</a></p>				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.5.1.3.2
				Borrado al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Consiste;
				en; eliminar; un; elemento ;del; final; de; la;
				lista </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%" >principal o sublista. Cabe señalar que
				antes de realizar la eliminación en la sublista se debe
				recorrer la lista principal hasta ubicar el nodo que referencia a la sublista
				donde se eliminará el nodo, y si por el contrario, el nodo a eliminar se
				encuentra en la lista principal y si éste posee sublista, antes de proceder a
				la eliminación del nodo de la lista principal se deben eliminar todos los nodos
				de la sublista.</p>

				<p style="line-height: 150%" ></p>

				
				<p align="CENTER" style="line-height: 150%"><a href="a4.32">Algoritmo
				4.32. Borrado
				al final de un elemento en la lista principal</a> </p>
				<b>
				<p align="center" style="line-height: 150%"></p>
				</b>
				
				<p align="CENTER" style="line-height: 150%"><a href="a4.33">Algoritmo
				4.33. Borrado
				al Final de un Elemento en la Sublista</a> </p>				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.5.1.4
				Búsqueda en una lista múltiple</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista se realiza de modo
				secuencial. Se deben recorrer los nodos con la finalidad de encontrar el
				elemento buscado ya sea en la lista principal o sublista. Puede decirse que esta
				operación está implícita en algunos de los casos de inserción y borrado.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.34">Algoritmo
				4.34. Búsqueda
				en la Lista Principal</a> </p>				<p ALIGN="center" style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.35">Algoritmo
				4.35. Búsqueda
				en la Sublista</a> </p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.6
				Listas con nodos Cabeceras</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Al
				escribir los algoritmos de insertar y suprimir en listas enlazadas, se vió que
				existían casos especiales cuando se trata del primer nodo. Una forma de
				simplificar los algoritmos de inserción y supresión es asegurarse de que nunca
				se inserta antes del primer nodo y que nunca se suprime el primer nodo.</p>				<p  style="line-height: 150%; text-indent: 20">Un
				nodo cabecera es un nodo que siempre estará al comienzo de una lista usado para
				simplificar el procesamiento de la lista y/o para contener información sobre la
				lista. La cabecera es un nodo normal del mismo tipo que los nodos de datos de la
				lista. Sin embargo, tiene una función diferente; en vez de almacenar datos de
				la lista actúan como limitadores de lugar.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 20">Una
				lista con nodo de cabecera es aquella en la que el primer nodo de la lista
				contendrá en su campo dato algún valor que lo diferencie de los demás nodos.
				Este valor debe ser asignado antes de la manipulación de la lista con se
				muestra en el algoritmo 4.36.</p>
				<p  style="line-height: 150%"></p>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.36">Algoritmo
				4.36. Crear Nodo Cabecera</a></p>
				<p ALIGN="center" style="line-height: 150%"></p>
				<p  style="line-height: 150%; text-indent: 20">Un
				ejemplo de lista con nodo de cabecera es el representado en la figura 4.27.</p>
				<b>
				<p  style="line-height: 150%"></p>
				<p ALIGN="center" style="line-height: 150%">Figura
				4.27. </b>Lista con nodo cabecera.</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="contenido/capIV/fig4.27.gif" width="309" height="56"></p>
				<p  style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" align="right" width="156" height="20"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%; text-indent: 20">Declaración
				de una Lista con Nodo Cabecera</p>
				</b>
				<p  style="line-height: 100%; text-indent: 20">La
				implementación de nodos cabeceras en las listas no altera las declaraciones de
				las mismas sino su manipulación, por lo tanto la declaración de una lista con
				nodo cabecera es similar a la declaración de una lista <a href="c416">simple</a>,
				<a href="c434"> doble</a> o <a href="c450">multiligada</a>, dependiendo del caso que se desee implementar.</p>
				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 100%">4.4.6.1
				Operaciones de las Listas con Cabeceras</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones que se muestran a continuación son similares a las utilizadas en
				una lista simple, con la diferencia de que se implementarán algoritmos tomando
				en cuenta el nodo cabecera, entre los cuales se encuentran: Recorrido de listas
				con cabecera, inserción de un elemento al inicio o al final de la lista,
				eliminación de un elemento al inicio o al final, se recomienda dirigirse a los
				apartados de las listas simples si desea profundizar sobre los algoritmos de
				Inserción; <a href="c423">antes </a> o <a href="c424">después</a> de un nodo
				dado y <a href="c428">Borrado</a> de un nodo con información dada.</p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<b>
				<p  style="line-height: 100%">4.4.6.1.1
				Recorrido de Listas con Cabeceras</p>
				</b>
				<p  style="line-height: 100%; text-indent: 20">Se
				realiza un recorrido secuencial por todos los nodos de la</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%">lista
				a través del campo siguiente.</p>
				<p  style="line-height: 100%"></p>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.37">Algoritmo
				4.37. Imprimir</a> </p>
				<b>
				<p  style="line-height: 100%"></p>
				</b><b>
				<p  style="line-height: 100%">4.4.6.1.2
				Inserción de un Elemento en una Lista con Cabecera</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				pueden presentar cuatro casos para la inserción de un elemento en una lista con
				cabecera:</p>
				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 100%">4.4.6.1.2.1
				Inserción al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de inserción consiste en agregar un nuevo nodo al inicio de la
				lista, siempre supone crear un nuevo nodo y hacer un movimiento de punteros.</p>
				
				<b>
				<p  style="line-height: 100%"></p>
				</b>
				<p ALIGN="center" style="line-height: 100%"><a href="a4.38">Algoritmo
				4.38. Inserción
				al Inicio de una Lista con Nodo Cabecera</a> </p>

				<p ALIGN="center" style="line-height: 100%"></p>

				<b>
				<p  style="line-height: 100%">4.4.6.1.2.2 Inserción
				al Final</p>
				</b>
				<p style="text-indent: 20">El nuevo nodo se
				coloca al final de la lista, convirtiéndose</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%">en
				el último de la misma. El algoritmo 4.50. describe este proceso.</p>
				<p  style="line-height: 100%"></p>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.39">Algoritmo
				4.39. Inserción
				al Final</a> </p>
				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%">4.4.6.1.3
				Borrado de un Elemento en una Lista con Cabecera</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">A
				continuación se tratará sobre la operación de borrado de nodos de una lista
				con cabecera. La operación de borrado supone enlazar el nodo anterior con el
				nodo siguiente al que va a ser borrado y liberar la memoria ocupada por el nodo
				a borrar.</p>
				<b>
				<p  style="line-height: 100%"></p>
				<p  style="line-height: 150%">4.4.6.1.3.1
				Borrado al Inicio</b></p>
				<p  style="line-height: 150%">Se
				quita el primer elemento de la lista, redefiniendo el valor del puntero al nuevo
				inicio de la misma. El proceso aparece descrito en el algoritmo 4.42.</p>
				<b>
				<p  style="line-height: 100%"></p>
				</b>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.40">Algoritmo
				4.40.
				Borrado al Inicio en una Lista con Nodo de Cabecera</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%"><b>4.4.6.1.3.2
				Borrado al Final</b></p>
				<p  style="line-height: 150%; text-indent: 20">Se
				quita el último nodo de la lista, redefiniendo a Nulo el campo Siguiente de su
				predecesor. Para alcanzar el último nodo se deberá recorrer previamente toda
				la lista. A continuación se presenta un algoritmo de solución, considerando
				que solamente se tiene un puntero al inicio de la lista.</p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.41">Algoritmo
				4.41.
				Borrado al Final en una Lista con Nodo de Cabecera</a></p>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%"><b>4.4.6.1.4
				Búsqueda de Listas con Cabeceras</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista se realiza de modo
				secuencial. Se deben recorrer los nodos, tomando el campo SIGUIENTE como puntero
				al siguiente nodo a visitar. Debido a esto puede decirse que esta operación
				está implícita en algunos de los casos de inserción y borrado.</p>				<p ALIGN="center" style="line-height: 150%; text-indent: 20"><a href="a4.42">Algoritmo
				4.42. Búsqueda en una Lista con Cabeceras</a></p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%">4.4.7
				Listas con Nodos Finales</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Al
				escribir los algoritmos en listas enlazadas, se observó que existían casos
				especiales cuando se trata del último nodo. Una forma de simplificar los
				algoritmos de inserción y supresión es asegurarse de que nunca se inserta
				después del último.</p>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%; text-indent: 20">Un
				nodo final es un nodo que siempre estará al final de una lista usado para
				simplificar el procesamiento de la misma. Véase figura 4.28.</p>				<p ALIGN="center" style="line-height: 150%; text-indent: 0"><b>Figura
				4.28. </b>Lista con finales</p>
				<p ALIGN="center" style="line-height: 150%"><img border="0" src="C:\Edl-BooK\contenido\capIV\fig4.28.gif" width="326" height="28"></p>
				<p ALIGN="right" style="line-height: 150%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>
				<p  style="line-height: 150%; text-indent: 20">El
				nodo final es un nodo normal del mismo tipo que los nodos de datos reales de la
				lista. Sin embargo, tiene una función diferente en vez de almacenar datos de la
				lista actúan como limitadores de lugar. El algoritmo 4.56. muestra el proceso
				de creación de un nodo final.</p>
			</div>
			<div class="pagina" @click="navNext">
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.43">Algoritmo
				4.43. Crear Nodo Final</a> </p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%; text-indent: 20">Declaración
				de una Lista con Finales</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				implementación de finales en las listas no altera las declaraciones de las
				mismas sino su manipulación, por lo tanto la declaración de una lista con nodo
				final es similar a la declaración de una lista simple, doble o multiligada,
				dependiendo del caso que se desee implementar.</p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.7.1
				Operaciones de las Listas con Nodos Finales</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Las
				operaciones que se muestran a continuación son similares a las utilizadas en
				una lista simple, con la diferencia de que se implementarán los algoritmos tomando
				en cuenta al nodo final, entre ellas se encuentran: Recorrido de listas con
				finales, inserción de un elemento al inicio o al final de la lista, Borrado de un elemento al inicio o al
				final de la lista,; se recomienda dirigirse a los; apartados de;
				listas; simples si desea profundizar sobre los algoritmos de
				Inserción; <a href="c423">antes</a> o <a href="c424"> después</a> de un nodo dado,;
				y; <a href="c428">Borrado</a> de un nodo con información dada .</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 150%">4.4.7.1.1
				Recorrido de Listas con Nodos Finales</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				realiza un recorrido secuencial por todos los nodos de la de la lista a través
				del campo siguiente hasta encontrar el nodo final.</p>
				<p  style="line-height: 100%"></p>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.44">Algoritmo
				4.44. Imprimir</a> </p>				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.7.1.2
				Inserción de un Elemento en una Lista con Nodo Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				pueden presentar cuatro casos para la inserción de un elemento en una lista con
				nodo final:</p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 150%">4.4.7.1.2.1
				Inserción al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de inserción consiste en agregar un nuevo nodo al inicio de la
				lista, siempre supone crear un nuevo nodo y hacer un movimiento de punteros. Se
				pueden presentar cuatro casos en la operación de inserción:</p>

				<p  style="line-height: 100%; text-indent: 20"></p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><a href="a4.45">Algoritmo
				4.45. Inserción al Inicio</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 150%">4.4.7.1.2.2
				Inserción al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">El
				nuevo nodo se coloca al final de la lista, pero antes del nodo final
				convirtiéndose en el último elemento válido de la misma . El algoritmo 4.48.
				describe este proceso.</p>
				<b>
				<p  style="line-height: 150%"></p>
				</b>
				
				<p ALIGN="center" style="line-height: 150%"><a href="a4.46">Algoritmo
				4.46. Inserción
				al Final</a> </p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">4.4.7.1.3
				Borrado de un Elemento en una Lista con Nodo Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">A
				continuación se tratará sobre la operación de borrado de nodos de una lista
				con finales. La operación de borrado supone enlazar el nodo anterior con el
				nodo siguiente al que va a ser borrado y liberar la memoria ocupada por el nodo
				a borrar. Se pueden presentar Tres casos en esta operación:</p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">4.4.7.1.3.1
				Borrado al Inicio</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				quita el primer elemento de la lista, redefiniendo el valor del puntero al nuevo
				inicio de la misma. El proceso aparece descrito en el algoritmo 4.49</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.47">Algoritmo
				4.47. Borrado
				al Inicio de un Elemento</a> </p>
				<p ALIGN="center" style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.7.1.3.2
				Borrado al Final</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Se
				quita el último nodo válido de la lista, redefiniendo el campo SIGUIENTE de su
				predecesor. Para alcanzar el último nodo se deberá recorrer previamente toda
				la lista hasta encontrar el nodo final.</p>
				<b>
				<p  style="line-height: 100%"></p>
				</b>
				
				<p ALIGN="center" style="line-height: 100%"><a href="a4.48">Algoritmo
				4.48.
				Borrado al Final</a></p>
				<p  style="line-height: 100%"></p>
				<b>
				<p  style="line-height: 150%">4.4.7.1.4
				Búsqueda en Listas con Nodos Finales</b></p>
				<p  style="line-height: 150%; text-indent: 20">La
				operación de búsqueda de un elemento en una lista se realiza de modo
				secuencial. Se deben recorrer los nodos, tomando el campo SIGUIENTE como puntero
				al siguiente nodo a visitar hasta encontrar el nodo final. Debido a esto puede
				decirse que esta operación está implícita en algunos de los casos de
				inserción y borrado.</p>
				<p  style="line-height: 100%"></p>
				
				<p ALIGN="center" style="line-height: 100%"><a href="c449">Algoritmo
				4.49. </a> <a href="a4.49">Búsqueda
				en Listas con Nodos Finales</a></p>

			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="line-height: 100%">4.4.8
				Otros Tipos de Listas</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Además
				de los tipos de listas descritos anteriormente, se pueden encontrar las
				siguientes: Auto-organizadas, con saltos y ortogonales, a continuación se da
				una breve descripción de cada una.</p>
				<p  style="line-height: 150%"></p>
				<b>
				<p  style="line-height: 100%">4.4.8.1.
				Listas Auto-organizadas</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Según
				Heileman (1998), consisten en reordenar los elementos de las listas conforme a
				reglas heurísticas que pretenden colocar los elementos que son más
				frecuentemente solicitados hacia el inicio de la lista. Se emplea una búsqueda
				secuencial, comenzando por la cabeza de la lista, para encontrar un elemento
				específico. El objetivo es organizar los elementos de la lista de tal forma que
				el tiempo total de una secuencia de operaciones de búsqueda secuencial sea tan
				pequeño como sea posible.</p>				<p  style="line-height: 150%; text-indent: 20">Si
				se conoce la probabilidad de petición de cada elemento, y cada petición es
				independiente de las otras peticiones de la secuencia, entonces desde un punto
				de vista estadístico,; el; mejor; planteamiento; es; almacenar;
				los</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 0">elementos;
				de la lista en orden no decreciente de probabilidad de petición, y
				nunca reordenarlos. Esta disposición es referida como<b> <i>ordenación enfática
				óptima. </i></b>Desde luego, en la práctica raramente se conocen estas probabilidades y deben desarrollarse otras técnicas para reordenar dinámicamente los elementos de las
				listas conforme se producen las peticiones. Las técnicas que reordenan los
				elementos de la lista basándose únicamente en la secuencia de peticiones que
				se han producido hasta el momento se conocen como<b> <i>heurísticas de
				auto-organización.</i></b> Tres heurísticas comúnmente utilizadas son:</p>

				<ol>
				  <li>
				    <p  style="line-height: 150%; text-indent: 0; margin-left: 20;font-size:12px"><b>Mover al frente</b>. Después de la búsqueda con éxito de una clave, se mueve
				el elemento correspondiente al frente (cabeza) de la lista, sin cambiar el orden
				relativo de los otros elementos. Si se inserta un elemento, se coloca al frente
				de la lista.
				  </li>
				  <li>
				    <p  style="line-height: 150%; text-indent: 0; margin-left: 20;font-size:12px"><b>Transponer</b>. Después de la búsqueda con éxito de una clave, se intercambia el elemento correspondiente con el elemento que está inmediatamente delante enla lista. Si se inserta un elemento, se coloca al frente de la lista.
				  </li>
				  <li>
				    <p  style="line-height: 150%; text-indent: 0; margin-left: 20;font-size:12px"><b>Recuento
				    de frecuencia.</b> Se debe mantener un recuento de frecuencia para cada
				    elemento.</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%" > El
				    recuento de cada elemento (que se inicializa a cero) se incrementa cuando se
				    inserta un elemento, o cuando el elemento es el
				resultado de una petición de búsqueda. Este recuento se pone a cero cuando el
				elemento es eliminado. La lista se mantiene de tal forma que los elementos
				aparecen en orden no decreciente del recuento de frecuencia.

				    <p style="line-height: 150%" >
				<p  style="line-height: 150%; text-indent: 20">Téngase
				en cuenta que los dos primeros enfoques no requieren memoria adicional, pero la
				tercera técnica requiere un campo recuento adicional para cada elemento de la
				lista.</p>
				<b>
				<p  style="line-height: 150%"></p>
				<p  style="line-height: 150%">4.4.8.2.
				Listas con Saltos</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Según
				Heileman (1998), las listas con saltos mejoran el tiempo de búsqueda añadiendo
				punteros adicionales a ciertos nodos de la lista. Estos punteros “saltan” un
				número fijo de nodos en las listas. Si la lista se mantiene ordenada, estos
				punteros de salto pueden ser usados para realizar un tipo de búsqueda binaria.</p>

				    <p  style="line-height: 150%; text-indent: 20">Considérese,
				    por ejemplo, la lista con saltos mostrada en la</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%" >Figura
				4.29., en la cual todo nodo contiene un puntero al nodo dos lugares más
				adelante en la lista, y los elementos de la lista están almacenados en orden.
				Además, se ha colocado un nodo cabecera en la lista que sólo contiene
				punteros. La línea de puntos en esta figura muestra el camino de búsqueda para
				el elemento con valor de clave 19. La estrategia conlleva seguir el camino de
				punteros que saltan los nodos hasta que se encuentre un nodo que apunte a un
				elemento cuya clave sea mayor que la clave de búsqueda. En ese punto, se
				comienza a recorrer la lista con saltos de nodo en nodo. No es difícil ver que
				utilizando este planteamiento, el número máximo de nodos que deben ser
				examinados durante una búsqueda de una lista con saltos con <i>n </i>nodos
				construida de la manera indicada por la Figura 4.29 es [n/2] + 1.</p>
				<p align="center" style="line-height: 200%"><b>Figura
				4.29.</b> Lista con Saltos</p>
				<p align="center" style="line-height: 150%"><img border="0" src="fig4.29.gif" width="281" height="59"></p>
				<p style="line-height: 150%" ><img border="0" src="fuente%20heileman.gif" width="193" height="18" align="right"></p>

				<p style="line-height: 150%; text-indent: 20" >Aunque
				una lista con saltos completa permite búsquedas rápidas, mantener la
				estructura requerida después de una</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%" > operación, ya sea de
				Eliminación o inserción, es prohibitivamente costoso, necesitando
				reasignaciones de punteros. Si, por el contrario, el nivel de un nodo se escoge
				aleatoriamente durante la inserción, entonces estas operaciones puede ser
				realizadas usando solamente modificaciones locales de punteros. Esto se logra
				utilizando un algoritmo probabilista para escoger el nivel de un nodo de acuerdo
				a su proporción en una lista con saltos completa.</p>
				<p style="line-height: 100%" ></p>
				<p style="line-height: 150%" ><b>4.4.8.3.
				Listas Ortogonales</b></p>
				<p style="line-height: 150%; text-indent: 20" >Según
				el Web Site <b> http:// www.itlp.edu.mx/ publica/;;; tutoriales/estru1/46.htm,</b> este tipo
				de lista se utiliza para representar matrices. Los nodos contienen cuatro
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a>(Figura 4.30). Uno para apuntar al nodo izquierdo (li),otro para
				apuntar al derecho(ld), otro al nodo inferior(lb) y por último un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> al
				nodo superior(la).</p>
				<p style="line-height: 200%" align="center"><b>Figura
				4.30. </b>Nodo de una lista ortogonal</p>
				<p style="line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capIV\fig4.30.gif" width="128" height="27"></p>
				<p style="line-height: 150%" align="right"><img border="0" src="contenido/capIV/fuenteurl.gif" width="355" height="16"></p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<b>
				<p  style="line-height: 100%">4.5
				Implementación de una Lista Enlazada como un Arreglo Sencillo</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Una
				lista enlazada puede almacenarse en un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de registros" v-bind:data-content="ArreglosDeRegistros">arreglo de registros</a>, con cada nodo de la
				lista constando de un de registro (al menos) dos campos, Info y Siguiente, Tal
				como lo muestra la figura 4.31. Estos registros pueden almacenarse en cualquier
				orden físico en el arreglo y ser enlazados juntos mediante sus campos
				Siguiente. El campo Siguiente de un nodo de la lista contiene el índice del
				elemento de la lista que le sigue.</p>
				<p ALIGN="center" style="line-height: 100%; text-indent: 0"><b>Figura
				4.31. </b>Implementación de una
				lista con un arreglo</p>
				<p ALIGN="center" style="line-height: 100%"><img border="0" src="contenido/capIV/fig4.31.gif" width="234" height="200"></p>
				<p ALIGN="right" style="line-height: 100%"><img border="0" src="contenido/capIV/fuente2.gif" width="156" height="20"></p>				
			</div>
			<div class="pagina" @click="navNext">				<p  style="line-height: 150%; text-indent: 20">Todas
				las instrucciones de listas enlazadas se pueden implantar simplemente utilizando
				un arreglo. Por supuesto, en algunos lenguajes tiene que haberse declarado el
				tamaño del arreglo en tiempo de compilación, y en lenguajes que permiten que
				un arreglo sea asignado “al vuelo”, el tamaño necesita conocerse en ese
				momento. Así, se requiere algún cálculo del tamaño máximo de la lista. Por
				lo regular esto exige una sobrevaloración, la cual consume espacio
				considerable, y esto podría ser una limitación seria, en especial si hay
				varias listas de tamaño desconocido.</p>
				<p  style="line-height: 150%; text-indent: 20">Una
				implantación a base de arreglos permite llevar a cabo las operaciones <i>Imprimir
				y búsqueda </i>en forma lineal tal y como se utiliza en las listas, no
				obstante, la inserción y eliminación son costosas. Por ejemplo, la inserción
				al inicio de una lista simple equivale en un arreglo a insertar un elemento en
				la posición 0, lo que requiere empujar primero todo el arreglo una posición
				para hacer espacio, mientras que eliminar el primer elemento requiere desplazar
				una posición hacia adelante todos los elementos de la lista. En promedio, la
				mitad de la lista necesita moverse para cualquier operación, lo que exige gran
				cantidad de tiempo.</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">Debido
				a que el tiempo de ejecución de las inserciones y eliminaciones es tan alto y
				que el tamaño de la lista debe conocerse por anticipado, no suelen usarse
				arreglos sencillos para implantar listas.</p>
				<b>
				<p  style="line-height: 150%; text-indent: 0">Resumen</p>
				</b>
				<p  style="line-height: 150%; text-indent: 20">Una
				<i>lista enlazada</i> es una secuencia de nodos en el que cada nodo está
				enlazado o conectado con el siguiente. <i>Un nodo</i> es una secuencia de
				caracteres en memoria dividida en campos (de cualquier tipo). La lista enlazada
				es una estructura de datos dinámica cuyos nodos suelen ser normalmente
				registros y que no tienen un tamaño fijo. No se puede acceder a los nodos de
				una lista enlazada directamente.</p>				<p  style="line-height: 150%; text-indent: 20">En
				un nodo se puede considerar que hay dos campos, <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Campo de información" v-bind:data-content="CampoDeInformación">campo de información</a> del nodo
				apuntado (info) y campo de enlace (siguiente) que es la dirección de memoria
				del siguiente nodo, si éste existe.</p>				<p  style="line-height: 150%; text-indent: 20">A
				una lista enlazada se accede desde un puntero externo</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 0"> que contiene la
				dirección (referencia) al primer nodo de la lista, la lista vacía, aquella que no tiene
				nodos, tiene el puntero externo de acceso a la lista apuntando a nulo.</p>

				<p style="line-height: 150%; text-indent: 20" ></p>

				<p style="line-height: 150%; text-indent: 20" >Si
				todos los elementos almacenados en una lista son del mismo tipo, entonces se
				dice que la lista es homogénea. Sin embargo, si distintos tipos de elementos
				están almacenados en la lista (un elemento es entero y otro es un número
				real), entonces la lista se dice que es heterogénea. Las operaciones que se
				pueden realizar en las listas enlazadas son: Recorrido, Inserción, Eliminación,
				Búsqueda.</p>
				<p style="line-height: 150%; text-indent: 20" ></p>
				<p style="line-height: 150%; text-indent: 20" >La
				mayor ventaja de las listas enlazadas es la utilización eficaz de la memoria,
				ya que se puede utilizar cualquier espacio vacío por no exigir posiciones
				secuenciales de memoria. Por el contrario, su mayor inconveniente es la lentitud
				del proceso, ya que es necesario ir recorriendo elemento a elemento a través de
				los punteros para poder acceder a uno específico. Las listas enlazadas pueden
				ser: Simples, Dobles, Circulares, Dobles Circulares y Multiligadas. Todas las
				listas nombradas anteriormente pueden tener nodo cabecera o nodo final, el
				primero es un nodo que siempre</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 150%" >estará al comienzo de una lista
				usado para simplificar el procesamiento de la lista y/o para contener información
				sobre la lista, mientras que el segundo es un nodo que siempre estará al final
				de una lista usado para simplificar el procesamiento de la lista.</p>

				<p style="line-height: 150%" ></p>

				<p style="line-height: 150%" ><b>Ejercicios
				propuestos</b></p>
				<ol>
				  <li>
				    <p style="line-height: 150%" >Ud.
				    fue contratado para trabajar en una Farmacia, y su primera obligación es
				    realizar un programa de computadora en pseudocódigo para almacenar todos
				    los productos que se tienen en la misma. La información recopilada por Ud.
				    es que todo producto posee un nombre, una descripción, un tipo, una fecha
				    de elaboración, una fecha de vencimiento, una fecha de ingreso, una
				    cantidad en existencia, un stock mínimo, un precio de compra, un precio de
				    venta y un código de identificación. Ud., elaboró un archivo con esos
				    datos y las rutinas que complementarían el trabajo se realizarían usando
				    estructuras de datos dinámicas (listas enlazadas simples circulares),
				    actualmente se le pide un informe en el cual aparezca lo siguiente:</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 100%; text-indent: 62" >a)
				Declaración en; pseudocódigo de la estructura</p>

				<p style="line-height: 100%; text-indent: 81" >a
				utilizar.</p>

				<p style="line-height: 100%; text-indent: 62" >b)
				Procedimiento en pseudocódigo que permita</p>

				<p style="line-height: 100%; text-indent: 80" >imprimir en
				pantalla
				todos los productos que</p>

				<p style="line-height: 100%; text-indent: 80" >ingresaron antes de una fecha dada y no están</p>

				<p style="line-height: 100%; text-indent: 80" >vencidos
				actualmente.</p>
				<p style="line-height: 150%; text-indent: 62; word-spacing: 0; margin-top: 5; margin-bottom: 5" >c)
				Función en pseudocódigo que permita determinar</p>
				<p style="line-height: 150%; text-indent: 80; word-spacing: 0; margin-top: 5; margin-bottom: 5" >cuál es el
				código
				del producto que excede en;</p>
				<p style="line-height: 150%; text-indent: 80; word-spacing: 0; margin-top: 5; margin-bottom: 5" > 80%
				su stock mínimo.</p>
				<ol start="2">
				  <li>
				    <p style="line-height: 150%" >Realice
				    un algoritmo en pseudocódigo que permita insertar un elemento al final de
				    una lista enlazada doble circular.</li>
				  <li>
				    <p style="line-height: 150%" >Realice
				    un algoritmo en pseudocódigo que permita la eliminación de un nodo
				    después de un elemento dado en una lista enlazada doble circular.</li>
				  <li>
				    <p style="line-height: 150%" >Dadas
				    dos listas enlazadas simples ordenadas ascendentemente, L1 y L2, escriba un
				    algoritmo en pseudocódigo para determinar L1 <span style="mso-char-type: symbol; mso-symbol-font-family: Symbol; font-size: 12.0pt; font-family: Symbol; mso-ascii-font-family: Times New Roman; mso-fareast-font-family: Times New Roman; mso-hansi-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">Ç</span>
				    L2.</li>
				</ol>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ol start="5">
				  <li>
				    <p style="line-height: 150%" >Realice
				    un algoritmo para insertar en una lista enlazada simple ordenada, un
				    elemento antes de otro nodo dado como referencia.</p>
				  </li>
				  <li>
				    <p style="line-height: 150%" >Realice
				    un algoritmo en pseudocódigo que permita eliminar un nodo en una lista
				    enlazada simple ordenada.</p>
				  </li>
				  <li>
				    <p style="line-height: 150%" >Escriba
				    un subprograma en pseudocódigo que dada una lista simple la cual contiene números
				    enteros, la divida en dos listas independientes, una formada por los números
				    positivos y otra con los números negativos.</p>
				  </li>
				  <li>
				    <p style="line-height: 150%" >Escriba
				    un subprograma en pseudocódigo que dadas dos listas enlazadas ordenadas
				    ascendentemente, las mezcle generando una nueva lista enlazada ordenada
				    descendentemente.</p>
				  </li>
				  <li>
				    <p style="line-height: 150%" >Se
				    tiene una lista enlazada simple circular con los datos de todos los
				    ingenieros inscritos en el C.I.V. (cédula, nombre, apellido, especialidad,
				    edad, estado civil, número de hijos y nombre de la universidad en la cual
				    se graduó). Por cada</p>
				  </li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 150%; text-indent: 0" >
				ingeniero se posee una lista enlazada simple de todas
				las empresas en las cuales ha trabajado (nombre, ciudad, cargo, fecha
				inicio, fecha final).Además, por cada empresa se posee una lista enlazada
				doble circular que contiene los datos de todos los empleados actualmente (cédula,
				nombre, apellido,edad,
				cargo, sueldo), se requiere que Ud. realice lo siguiente:</p>

				<p style="line-height: 100%; text-indent: 62" >a)
				Gráfico de la estructura</p>
				<p style="line-height: 100%; text-indent: 62" >b)
				Declaración en pseudocódigo de la estructura</p>

				<p style="line-height: 100%; text-indent: 62" >c)
				Procedimiento en pseudocódigo para;imprimir;</p>

				<p style="line-height: 100%; text-indent: 80" > la cédula de la
				persona
				que;gana más dinero,</p>

				<p style="line-height: 100%; text-indent: 80" >
				además del nombrede la
				empresa
				para la cual</p>

				<p style="line-height: 100%; text-indent: 80" >
				trabaja.</p>

				<p style="line-height: 100%; text-indent: 62" >d)
				Procedimiento para eliminar todas las;empresas;</p>
				<p style="line-height: 100%; text-indent: 80" > cuya suma
				de
				sueldos es menor;de 1 millón de</p>
				<p style="line-height: 100%; text-indent: 80" >
				bolívares.</p>
				<ol start="10">
				  <li>
				    <p style="line-height: 150%" >Realice
				    una rutina que permita verificar si dos listas son semejantes (mismos
				    elementos aunque estén en diferente orden).</p>
				  </li>
				</ol>
				<p style="line-height: 150%; text-indent: 20" ><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 4)">Autoevaluación</a></p>
			</div>
		</div>
		<!--Contenido 5-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>5.1
				Introducción</b></p>
				<p style="text-indent: 20; line-height: 150%" >Cuando
				se trataron los arreglos, en el capítulo II, se dijo que eran estructuras
				lineales. Al analizar las operaciones de inserción y eliminación, se vió que
				los elementos podían insertarse o eliminarse en cualquier posición del
				arreglo. Sin embargo, en ciertos casos es necesario que los elementos deban
				agregarse o quitarse solamente por un extremo, generando la creación de
				estructuras denominadas pilas.</p>				<p style="text-indent: 20; line-height: 150%" >Las
				pilas son otro tipo de estructura de datos lineales, las cuales presentan
				restricciones en cuanto a la posición en la cual pueden realizarse las
				inserciones y las extracciones de elementos. Estas estructuras no están
				incorporadas en la mayoría de los lenguajes de programación, sino que han de
				ser creadas por el programador en cada programa antes de que puedan usarse.</p>

				<p style="text-indent: 20; line-height: 150%" >En
				la vida cotidiana existen muchos ejemplos de pilas, una pila de platos en una
				alacena, una pila de latas en un supermercado, una pila de papeles sobre un
				escritorio, etc.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Este
				capítulo se dedicará al estudio de las pilas, realizando entre otras cosas su
				implementación con estructuras de datos como arreglos y listas enlazadas.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>5.2
				Definición de Pila</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				pila es un grupo ordenado de elementos homogéneos (todos del mismo tipo). El
				quitar elementos existentes y añadir nuevos elementos puede realizarse sólo
				por la cabeza de la pila. En consecuencia, los elementos de una pila serán
				eliminados en orden inverso al que se insertaron, es decir, el último elemento
				que se mete en la pila es el primero que se saca. Debido a esta característica,
				las pilas también reciben el nombre de estructuras LIFO (Last In, First Out).</p>
				<p style="text-indent: 20; line-height: 150%" >La
				pila se considera un grupo ordenado de elementos porque los elementos están
				ordenados de acuerdo al tiempo que llevan en la pila.</p>

				<p style="text-indent: 20; line-height: 150%" >Por
				ejemplo, si su camisa favorita está colocada debajo de una roja en una pila de
				camisas (ver figura 5.1), primero debe</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" > quitar la camisa roja (el elemento
				cabeza) de la pila. Sólo
				entonces puede quitar la camisa azul deseada, la cual es ahora el elemento de
				encima de la pila. La camisa roja puede luego colocarse de nuevo en la cabeza o
				cima de la pila o en cualquier otro sitio.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				5.1.</b> Ejemplos de Pilas</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capV/5.1.gif" width="263" height="114"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="F5.1.gif" align="right" width="169" height="10"></p>
				<p style="text-indent: 0; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:189.75pt;
				 height:74.25pt' fillcolor="window">
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title=""/>
				</v:shape><![endif]-->
				</span><b>5.3
				Aplicaciones de las Pilas</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				pilas son estructuras de datos muy usadas en la solución de diversos tipos de
				problemas. Según Cairó y Guardatí(1993), los casos más representativos de
				aplicación de pilas son los siguientes:</p>

				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Llamadas
				    a subprogramas</li>
				</ul>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Recursión</li>
				</ul>

				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Tratamiento
				    de expresiones aritméticas</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 150%" >Ordenación</li>
				</ul>

				<p style="text-indent: 20; line-height: 150%" ><b>Llamadas
				a subprogramas</b></p>
				<p style="text-indent: 20; line-height: 150%" >Cuando
				se tiene un programa que llama a un subprograma, internamente se usan pilas para
				guardar el estado de las variables del programa en el momento que se hace la
				llamada.</p>				<p style="text-indent: 20; line-height: 150%" > Así, cuando termina la ejecución del subprograma, los valores
				almacenados en la pila pueden recuperarse para continuar con la ejecución del
				programa en el punto en el cual fue interrumpido.</p>				<p style="text-indent: 20; line-height: 150%" > Además de las variables, debe
				guardarse la dirección del programa en la que se hizo la llamada, porque es a
				esa posición a la que regresa el control del proceso.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Por
				ejemplo, se tiene un programa principal (PP) que llama a los subprogramas UNO y
				DOS.A su vez, el subprograma DOS llama al subprograma TRES.</p>

				<p style="text-indent: 20; line-height: 150%" >Cada vez que la
				ejecución de uno de los subprogramas concluye, se regresa el control al nivel
				inmediato superior (figura 5.2).</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				5.2.</b> Llamada a subprogramas</p>
				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capV/5.2.gif" width="101" height="156"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capV/F5.2.gif" align="right" width="201" height="11"></p>

				<p  style="line-height: 150%; text-indent: 20">Cuando
				el programa PP llama a UNO, se guarda en una pila la posición en la que se hizo
				la llamada:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">				<p align="center" style="line-height: 150%; text-indent: 0"><img border="0" src="contenido/capV/O1.gif" width="109" height="115"></p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p  style="line-height: 150%; text-indent: 20">Al
				terminar UNO, el control se regresa a PP recuperando previamente la dirección
				de la pila.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capV/O2.gif" width="61" height="126"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:45pt;
				 height:99pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/04/clip_image001.png"
				  o:title="f2"/>
				</v:shape><![endif]--> 
				</span></p>
				<p  style="line-height: 100%; text-indent: 20"></p>
				<p  style="line-height: 100%; text-indent: 20">Al
				llamar a DOS, nuevamente se guarda la dirección de PP en la pila:</p>

			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capV\O1.gif" width="109" height="115"><!--[if gte vml 1]><v:shape id="_x0000_i1026" type="#_x0000_t75" style='width:75.75pt;
				 height:91.5pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/04/clip_image003.png"
				  o:title="f5"/>
				</v:shape><![endif]-->
				 ;</p>
				<p  style="text-indent: 20; line-height: 150%"></p>

				<p  style="text-indent: 20; line-height: 150%">Al
				terminar DOS se regresa el control a PP, obteniendo previamente la dirección
				guardada en la pila:</p>

				<p  style="text-indent: 20; line-height: 150%"></p>

				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="C:\Edl-Book\contenido\capV\O4.gif" width="61" height="126"></p>


				<p  style="text-indent: 20; line-height: 150%">Se
				puede concluir que las pilas son necesarias en esta área de aplicaciones por lo
				siguiente:</p>				
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p  style="text-indent: 0; line-height: 200%">Permiten
				    guardar la dirección del programa (subprograma) desde donde se hizo la
				    llamada a otros subprogramas, para poder regresar y seguir ejecutándolo a
				    partir de la instrucción inmediata a la llamada.</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Permiten
				    guardar el estado de las variables en el momento que se hace la llamada,
				    para poder seguir ocupándolas al regresar del subprograma.</li>
				</ul>

				    <p style="text-indent: 0; line-height: 100%" >

				<p style="text-indent: 20; line-height: 150%" ><b>Recursión</b></p>
				<p style="text-indent: 20; line-height: 150%" >Otra
				aplicación que se le puede dar a las pilas es la implementación de la
				recursividad, como por ejemplo para el manejo de la pila de ejecución en la
				memoria de la computadora.</p>				<p style="text-indent: 20; line-height: 150%" ><b>Tratamiento
				de expresiones aritméticas</b></p>
				<p style="text-indent: 20; line-height: 150%" >Un
				problema interesante en computación es poder convertir expresiones en notación
				infíja a su equivalente en notación postfija (o prefija).</p>

			</div>
			<div class="pagina" @click="navNext">
				    <ul>
				      <li>
				        <p style="text-indent: 0; line-height: 200%" >Dada
				    la expresión A+B se dice; que; está; en; notación
				        infija,
				    y su nombre se debe
				    a que el operador (+) está entre los operandos (A y B).</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Dada
				    la expresión AB+ se dice que está en notación postfija, y su nombre se
				    debe a que el operador (+) está después de los operandos (A y B).</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Dada
				    la expresión +AB se dice que está en notación prefija, y su nombre se
				    debe a que el operador (+) está antes que los operandos (A y B).</li>
				    </ul>

				<p style="text-indent: 20; line-height: 150%" >La
				ventaja de usar expresiones en notación polaca postfija o prefija radica en que
				no son necesarios los paréntesis para indicar el orden de operación, ya que
				éste queda establecido por la ubicación de los operadores con respecto a los
				operandos.</p>

				<p style="text-indent: 20; line-height: 150%" > Para convertir una expresión dada en notación infija;
				a; una; en; notación; postfija; (o prefija),; deberán;
				establecerse</p>	
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" > previamente ciertas
				condiciones:</p>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Solamente
				    se manejarán los siguientes operadores (están dados ordenadamente de mayor
				    a menor según su prioridad de ejecución):</li>
				</ul>
				<p style="text-indent: 80; line-height: 200%" >^
				(potencia)</p>
				<p style="text-indent: 80; line-height: 200%" >*
				/ (multiplicación y división)</p>
				<p style="text-indent: 80; line-height: 200%" >+
				- (suma y resta)</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Los
				    operadores de más alta prioridad se ejecutan primero.</li>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Si
				    hubiera en una expresión dos o más operadores de igual prioridad, entonces
				    se procesarán de izquierda a derecha.</li>
				</ul>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 200%" >Las
				    subexpresiones parentizadas tendrán más prioridad que cualquier operador.</li>
				</ul>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				5.1.</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				proceso de conversión que se expone a continuación acepta una expresión
				infija como entrada y produce una expresión postfija como salida. La idea
				general es utilizar una pila para almacenar los operandos conforme son
				encontrados, para más tarde <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Desapilar" v-bind:data-content="Desapilar">Desapilar</a> estos operandos de acuerdo a su
				precedencia.</p>				<p style="text-indent: 20; line-height: 150%" >La expresión infija es sondeada de izquierda a
				derecha y procesada de acuerdo a las siguientes reglas:</p>				<p style="text-indent: 20; line-height: 150%" >1.
				Cuando se encuentra un operando se lleva a la salida.</p>
				<p style="text-indent: 20; line-height: 150%" >2.
				Cada vez que se lee un operador la pila es desapilada repetidamente y los
				operandos se llevan a la salida, hasta que se encuentre un operador que tenga
				una precedencia menor que la del operador más recientemente leído. Entonces se
				apila el operador más recientemente leído.</p>

				<p style="text-indent: 20; line-height: 150%" >3.
				Cuando se alcanza el final de la expresión infija todos los símbolos restantes
				de la pila son desapilados y llevados a la salida.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Como
				se pueden usar paréntesis para cambiar el orden de evaluación en las
				expresiones infijas, estos se deben incorporar en el proceso
				de conversión. Esto se consigue tratando los paréntesis como operadores que
				tienen una precedencia superior a la de cualquier otro operador. Además, no se
				permite que los paréntesis derechos sean apilados en la pila, sólo se permite
				que un paréntesis izquierdo sea desapilado después de que haya sido leído un
				paréntesis derecho. Téngase en cuenta, no obstante, que los paréntesis no
				deben ser llevados a la salida cuando son desapilados de la pila dado que no
				aparecen en las expresiones postfijas.</p>

				<p style="text-indent: 20; line-height: 150%" >Como
				demostración de este proceso de conversión, considérese la expresión infija:</p>
				<p style="text-indent: 0; line-height: 100%" align="center">a
				* ( b + c ) + d/e</p>
				<p style="text-indent: 20; line-height: 150%" >El
				primer símbolo leído es a, como es un operando se lleva a la salida. A
				continuación se lee el operador “*”. Debido a que la pila está vacía en
				el momento actual, no se desapila ningún operador y se apila “*”.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 20">De
				este modo se tiene:</p>

				<p align="center" style="line-height: 150%; text-indent: 0"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:121.5pt;
				 height:30.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="f7"/>
				</v:shape><![endif]--><img border="0" src="C:\Edl-Book\contenido\capV\O5.gif" width="134" height="39"></span></p>

				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD">A
				continuación se lee un “ ( ”, y como todos los operadores tiene una
				precedencia inferior a la del paréntesis izquierdo, es inmediatamente apilado
				en la pila. Entonces se lee “b” y se lleva a la salida, esto produce:</span></p>

				<p align="center" style="line-height: 150%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capV\O6.gif" width="144" height="63"></p>

				<p align="center" style="line-height: 150%; text-indent: 0"></p>
				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">El
				siguiente símbolo leído es el operador “+”. Aunque “ ( ” tiene una
				precedencia superior que la de este operador, no puede ser desapilado de la pila
				hasta que haya sido leído un “ ) ”.
				</span></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD"> De este modo, nada se desapila y el
				operador “+” es apilado. Después de esto, se lee “c” y se lleva a la
				salida:
				</span></p>				<p align="center" style="line-height: 150%; text-indent: 0"><img border="0" src="C:\Edl-Book\contenido\capV\O7.gif" width="81" height="80"></p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">A
				continuación se lee “)”. Como “+” tiene una precedencia inferior a la
				de este operador, no se desapila nada. El símbolo “ ) ” es descartado; sin
				embargo, cuando se produzca el momento, se desapilará el paréntesis izquierdo,
				dado que su paréntesis derecho correspondiente ha sido leído.</span></p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">El siguiente
				símbolo que se encuentra es el operador “+”. Al leer este símbolo se
				desapilan todos los símbolos almacenados en ese momento (ninguno de estos
				operadores tiene una precedencia menor que “+”), pero sólo los operadores
				“+” y “*” son llevados a la salida.</span></p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">El operador más recientemente
				leído. “+”, es entonces apilado:</span></p>

				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capV\O8.gif" width="177" height="58"><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:150pt;
				 height:42pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="f10"/>
				</v:shape><![endif]-->
				 
				 </span></p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">A
				continuación se lee “d” (y se lleva a la salida) seguido por “/”. Como
				la suma tiene una precedencia inferior a la de la división, el operador “/”
				es apilado:</span></p>

				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capV\O9.gif" width="86" height="57"></p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD">El
				símbolo Final es “e”, que es llevado a la salida. Entonces los restantes
				operadores almacenados en la pila son desapilados y llevados a la salida como se
				muestra a continuación:</span></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><img border="0" src="C:\Edl-Book\contenido\capV\O10.gif" width="83" height="40"><span lang="ES-TRAD" style="mso-ansi-language:ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:63.75pt;
				 height:31.5pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="f12"/>
				</v:shape><![endif]-->
				 
				 </span></p>

				<p style="text-indent: 20; line-height: 150%" >La
				salida final es la expresión postfija correcta.</p>

				<p style="text-indent: 0; line-height: 150%" ><b>5.4
				Operaciones con Pilas</b></p>

				<p style="text-indent: 20; line-height: 150%" >Según
				Cairó y Guardatí(1993), las operaciones elementales que pueden realizarse en
				una pila son:</p>

				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 200%" >Poner
				    un elemento (Push).</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 20; line-height: 200%" >Quitar
				    un elemento (Pop).</li>
				</ul>

				<p style="text-indent: 20; line-height: 150%" >A
				continuación se presenta un ejemplo para ilustrar el funcionamiento de las
				operaciones de inserción (poner) y eliminación (quitar) en pilas.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>EJEMPLO
				5.2</b></p>
				<p style="text-indent: 20; line-height: 150%" >Si
				se pusieran los elementos enero, febrero, marzo, abril y mayo (en este orden) en
				PILA, la estructura quedaría como lo indica
				la siguiente figura:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				5.3.</b> PILA</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capV/5.3.gif" width="114" height="111"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capV/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >Si se quitara mayo, la cabeza apuntaría a abril:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				5.4.</b> PILA</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capV/5.4.gif" width="115" height="111"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capV/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Si
				se pretendiera eliminar febrero, antes deberían quitarse marzo y abril, para
				que quede febrero en la cima de PILA y pueda
				extraerse.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				5.5.</b> PILA</p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capV/5.5.gif" width="114" height="111"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 0; line-height: 100%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype id="_x0000_t75"
				 coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
				 filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:88.5pt;
				 height:93.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="5"/>
				</v:shape><![endif]-->
				</span></p>
				<p style="text-indent: 0; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				</span><b>5.5
				Implementación de una Pila con un Arreglo</b></p>
				<p style="text-indent: 20; line-height: 150%" >Debido
				a que todos los elementos de una pila son del mismo tipo, un arreglo parece una
				estructura razonable para contener la pila.</p>

				<p style="text-indent: 20; line-height: 150%" >Es posible poner los elementos de
				forma secuencial en el arreglo, colocando el primer elemento en la primera
				posición del arreglo, el segundo en la segunda posición del; arreglo,;
				y</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >así
				sucesivamente. Las
				pilas no son estructuras fundamentales de datos, es decir, no están definidas
				como tales en los lenguajes de programación (como lo están por ejemplo los
				arreglos). Al realizar su representación con arreglos deberá definirse cuál
				será el tamaño máximo de la pila, y además una variable auxiliar a la que se
				denominará TOPE; que; será; un; índice; al último
				elemento insertado en la pila.</p>

				<p style="text-indent: 20; line-height: 150%" > En la figura 5.6 se presentan dos alternativas de
				representación de una pila, utilizando arreglos.</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				5.6.</b> Representación de pilas</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capV/5.6.gif" width="325" height="138"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capV/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Al
				utilizar arreglos para implementar pilas se tienen las limitaciones de espacio
				de memoria reservada, propia de los arreglos. Una vez dado un máximo de
				capacidad a la pila, no es posible insertar un número de elementos mayor al
				máximo fijado. Si la pila estuviera llena y se intentara insertar un nuevo
				valor, se tendría un error conocido con el nombre de desbordamiento (overflow).</p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				</span>En
				la figura 5.7 se presentan ejemplos de pila llena a), pila con algunos elementos
				b) y pila vacía c).</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				5.7.</b> Representación de pilas, a) Pila llena,</p>
				<p style="text-indent: 0; line-height: 100%" align="center">b)
				Pila con algunos elementos, c) Pila vacía</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capV/5.7.gif" width="283" height="111"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capV/elabpropia.gif" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype id="_x0000_t75"
				 coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
				 filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:210.75pt;
				 height:85.5pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/03/clip_image001.png"
				  o:title="5"/>
				</v:shape><![endif]-->
				</span>Por ejemplo, si en la pila presentada en la figura 5.7a), donde CABEZA = MAX, se
				quisiera insertar otro elemento, se tendría un error de desbordamiento. La pila
				está llena y el espacio reservado de memoria es fijo, no puede expandirse o
				contraerse. Una posible solución a este tipo de errores consiste en definir
				pilas de gran tamaño, pero esto resultaría ineficiente y costoso si sólo se
				utilizaran algunos elementos. No siempre es posible saber con exactitud cuál es
				el número de elementos a tratar, por lo tanto siempre existe la posibilidad de
				cometer un error de desbordamiento (si se reserva menos espacio del que
				efectivamente se usará) o bien, de hacer uso ineficiente de la memoria (si se
				reserva más espacio del que se empleará).</p>

				<p style="text-indent: 20; line-height: 150%" > Existe otra alternativa de solución a este problema. Consiste en
				usar espacios compartidos de memoria para la implementación de pilas.
				Supóngase que se necesitan dos pilas, cada una de ellas con un tamaño máximo
				de N elementos.</p>				<p style="text-indent: 20; line-height: 150%" >Se
				definirá un solo arreglo de 2 * N elementos, en lugar de dos arreglos de N
				elementos cada uno.</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				5.8.</b> Representación de pilas en espacios compartidos</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capV/5.8.gif" width="357" height="66"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capV/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Como
				se ilustra en la figura 5.8 la PILA1 ocupará de la posición 1 en adelante (2,
				3,. . .), mientras que la PILA2 ocupará de la posición 2N hacia atrás (2N -
				1, 2N - 2,. . . ). </p>

				<p style="text-indent: 20; line-height: 150%" >Si en algún punto del proceso la PILA1 necesitara más
				de N espacios y en ese momento la PILA2 no tuviera ocupados sus N lugares,
				entonces se podrían seguir agregando elementos a la PILA1 sin caer en un error
				de desbordamiento (véase la figura 5.9a).</p>

				<p style="text-indent: 20; line-height: 150%" > Lo mismo podría suceder para la
				PILA2: que ésta necesitara más de N espacios y que la PILA1 tuviera lugares
				disponibles (véase la figura 5.9b).</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				5.9. </b>Representación de pilas en espacios compartidos, a) Pila1 tiene más
				de N elementos y Pila2 tiene menos de N elementos, b) Pila2 tiene más de N
				elementos y Pila1 tiene menos de N elementos</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capV/5.9.gif" width="331" height="176"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capV/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Otro
				error que puede presentarse al trabajar con pilas es tratar de eliminar un
				elemento de una pila vacía. Este tipo de error se conoce con el nombre de
				subdesbordamiento (underflow). Por ejemplo, si en la pila presentada en la
				figura 5.7c), donde CABEZA &lt; 1, se deseara quitar un elemento, se tendría un
				error de subdesbordamiento.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>Declaración
				de la estructura pila con arreglo</b></p>
				<p style="text-indent: 20; line-height: 150%" >Siguiendo
				el formato utilizado para definir un tipo arreglo en el capítulo II, se
				declarará una pila de 100 elementos como se muestra a continuación:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Constantes</b></p>
				<p style="text-indent: 50; line-height: 100%" >MaxPila
				: 100 {Tamaño de la pila}</p>				<p style="text-indent: 20; line-height: 100%" ><b>Tipos</b></p>
				<p style="text-indent: 50; line-height: 100%" >TipoPila
				: Arreglo [1..MaxPila] de Entero</p>				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 50; line-height: 100%" >Pila
				: TipoPila</p>
				<p style="text-indent: 50; line-height: 100%" >Tope:
				Entero</p>

				<p style="text-indent: 50; line-height: 100%" ></p>

				<p style="text-indent: 20; line-height: 150%" >Considerando
				que se tiene una pila que puede almacenar un máximo número de 100 elementos y
				que el último de ellos está indicado; por; TOPE,; los; algoritmos para; poner;
				y; quitar</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >elementos son los siguientes:</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a5.1">Algoritmo
				5.1. Poner elemento</a></p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a5.2">Algoritmo
				5.2. Quita elemento</a></p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>5.6
				Implementación de una Pila con una Lista Enlazada</b></p>
				<p style="text-indent: 20; line-height: 150%" >Es
				posible implementar una pila utilizando una lista enlazada
				simple.</p>

				<p style="text-indent: 20; line-height: 150%" >Se realiza un
				meter insertando al frente de la lista, se efectúa un sacar eliminando el
				elemento que está al frente de la lista. Se podrían utilizar llamadas a las
				rutinas sobre listas enlazadas del capítulo anterior, pero serán tratadas para
				evitar la pérdida de claridad.</p>

				<p style="text-indent: 20; line-height: 150%" >El
				meter se implanta como inserción al frente de la lista enlazada, donde el
				frente de la lista sirve como cima de la pila. El sacar se implanta eliminando
				del frente de la lista. </p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Debe quedar claro que todas las operaciones
				tarden un tiempo constante, porque en ninguna de las rutinas hay la menor
				referencia al tamaño de la pila (excepto en la comprobación de si está vacía);
				mucho menos se encuentra un ciclo que dependa del tamaño.</p>
				<p style="text-indent: 20; line-height: 150%" > La desventaja de esta
				implantación es que las llamadas a crear y liberar nodos son costosas, en
				especial en comparación con las rutinas de manipulación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a>.</p>
				<p style="text-indent: 20; line-height: 150%" > Algo de esto se puede evitar mediante una segunda pila, que al
				principio está vacía.</p>
				<p style="text-indent: 20; line-height: 150%" > Cuando se libera una celda de la primera pila, sólo se
				coloca en la segunda pila, entonces, cuando se necesitan nuevas celdas para la
				primera pila, primero se buscan en la segunda pila.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>Declaración
				de la estructura pila</b></p>				<p style="text-indent: 20; line-height: 100%" ><b>Tipos</b></p>
				<p style="text-indent: 70; line-height: 100%" >Lista
				= ^Nodo</p>
				<p style="text-indent: 70; line-height: 100%" >Nodo</p>
				<p style="text-indent: 100; line-height: 100%" >Info:
				Entero</p>
				<p style="text-indent: 100; line-height: 100%" >Sig:
				Lista</p>
				<p style="text-indent: 70; line-height: 100%" >Fin
				Nodo</p>
				<p style="text-indent: 70; line-height: 100%" >TipoPila
				: Lista</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 70; line-height: 100%" >Pila
				: TipoPila</p>
				<p style="text-indent: 70; line-height: 100%" >Tope:
				Entero</p>

				<p style="text-indent: 70; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a5.3">Algoritmo
				5.3. Meter un elemento (Apilar)</a></p>

				<p style="text-indent: 0; line-height: 200%" align="center"><a href="a5.4">Algoritmo
				5.4. Sacar un elemento (Desapilar)</a></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>5.7
				Representación en Memoria de una Pila</b></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				se mencionó anteriormente, las pilas no son estructuras de datos fundamentales,
				es decir, no están definidas como tales en los lenguajes de programación, por
				lo tanto pueden representarse mediante el uso de Arreglos o Listas enlazadas,
				trayendo como consecuencia que su representación en memoria dependa de la
				estructura seleccionada para su representación.</p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 150%" ><b>Resumen</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				pila es un grupo ordenado de elementos homogéneos (todos del mismo tipo). </p>

				<p style="text-indent: 20; line-height: 150%" >Las
				operaciones elementales que pueden realizarse en una pila
				son: Poner un elemento (Push) y quitar un elemento (Pop). El quitar elementos
				existentes y añadir nuevos elementos puede realizarse sólo por la cabeza de la
				pila. En consecuencia, los elementos de una pila serán eliminados en orden
				inverso al que se insertaron, es decir, el último elemento que se mete en la
				pila es el primero que se saca, por lo que se denominan estructuras LIFO. </p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" > Si se
				desea insertar un elemento en una pila llena se presenta un error de
				desbordamiento de pila (overflow), otro error que puede presentarse al trabajar con
				pilas es tratar de eliminar un elemento de una pila vacía, este tipo de
				error se conoce con el nombre de subdesbordamiento (underflow). Debido
				a que las pilas no son estructuras de datos fundamentales, se deben implementar con
				arreglos o con listas enlazadas.</p>

				<p style="text-indent: 20; line-height: 150%" >Se
				pueden implementar con arreglos, ya que todos los elementos de una pila son del
				mismo tipo, es posible poner los elementos de forma secuencial en el arreglo,
				colocando el primer elemento en la primera posición del arreglo, el segundo en
				la segunda posición, y así sucesivamente. </p>

				<p style="text-indent: 20; line-height: 150%" > Al realizar la representación con arreglos deberá definirse cuál será el
				tamaño máximo de la pila, y además una variable auxiliar a la que se
				denominará TOPE que será un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> al último elemento insertado en la
				pila, al utilizar arreglos para implementar pilas se tienen las limitaciones de
				espacio de memoria reservada, propia de los arreglos. </p>			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" > Una vez dado un máximo de
				capacidad a la pila, no es posible insertar un número de elementos mayor al
				máximo fijado. </p>


				<p style="text-indent: 20; line-height: 150%" >  Una
				manera de evitar el problema de memoria que se presenta al implementar pilas con
				arreglos es mediante una lista enlazada simple. Se realiza una inserción
				al frente de la lista, se efectúa una eliminación del elemento que está al
				frente de la lista. El único error que se podría presentar es el subdesbordamiento por tratar de eliminar elementos en una pila vacía; en
				ningún momento se presentarán limitaciones en las inserciones por falta de
				espacio. </p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="text-indent: 0; line-height: 150%">Ejercicios
				Propuestos</p>
				</b>
				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Traduzca
				    la siguientes expresiones a expresiones postfija, prefija e infija
				    respectivamente:</li>
				</ol>
				<p  style="text-indent: 50; line-height: 150%">-
				Z/(X + Y * T) ^W</p>
				<p  style="text-indent: 50; line-height: 150%">-
				(Z * (K - W) + X) ^Y - T</p>
				<p  style="text-indent: 50; line-height: 150%">-
				(X - T) ^ Z</p>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="2">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Elabore
				    un pseudocódigo que lea una expresión infija y la traduzca a postfija.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="3">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Elabore
				    un pseudocódigo que lea una expresión postfija y la traduzca a prefija.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="4">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo que inserte un elemento en una pila. Utilice un arreglo
				    para realizar la implementación.</li>
				</ol>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				    <ol style="font-family: Century Gothic; font-size: 12 pt" start="5">
				      <li>
				        <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Escriba
				        un pseudocódigo que elimine un elemento de una pila. Utilice una lista
				    enlazada simple para realizar la implementación.</li>
				    </ol>
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>

				<ol style="font-family: Century Gothic; font-size: 12 pt" start="6">
				  <li>
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Escriba
				    un pseudocódigo que elimine los elementos repetidos de una pila. Los
				    elementos repetidos ocupan posiciones sucesivas. Utilice la estructura que
				    considere más adecuada para realizar la implementación.</li>
				</ol>
				<p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="7">
				  <li>
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Realice
				    un pseudocódigo para eliminar todos los libros que se encuentran
				    almacenados en una pila utilizando un Arreglo.</li>
				</ol>
				<p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="8">
				  <li>
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Realice
				    el ejercicio anterior utilizando una lista enlazada simple.</li>
				</ol>

				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>
				    <ol style="font-family: Century Gothic; font-size: 12 pt" start="9">
				      <li>
				        <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Realice
				        un pseudocódigo para incluir un máximo de 100</li>
				    </ol>

				    <p  style="text-indent: 35; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">libros en una pila
				    utilizando una lista enlazada simple.
				<p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>
				<p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"></p>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="10">
				  <li>
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">Realice
				    el ejercicio anterior utilizando un arreglo.</li>
				</ol>

				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">

				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3">
				    <p  style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 3; margin-bottom: 3"><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 5)">Autoevaluación</a>
			</div>
			<div class="pagina" @click="navNext">			</div>
		</div>
		<!--Contenido 6-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>6.1
				Introducción</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				el Capítulo V se trataron a las pilas, una estructura de datos con la propiedad
				especial de que los elementos siempre se añaden y quitan por un extremo.</p>				<p style="text-indent: 20; line-height: 150%" >Sin
				embargo se sabe que en muchas ocasiones es necesario que elementos funcionen de
				una manera inversa, es decir, se añadan por un extremo y se eliminen por otro.
				Esta estructura, llamada una Cola tiene muchos usos en los programas de
				computadoras.</p>				<p style="text-indent: 20; line-height: 150%" >El
				concepto de cola es uno de los que más abundan en la vida cotidiana:
				espectadores esperando en una taquilla, clientes de un supermercado esperando
				para pagar, etc.</p>
				<p style="text-indent: 20; line-height: 150%" >En
				el capítulo que se presenta a continuación se realizará un estudio de esta
				estructura de datos abarcando su definición, clasificación e implementación
				con arreglos y listas enlazadas.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>6.2
				Definición de cola</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				cola es una lista ordenada de elementos, en la cual las eliminaciones se
				realizan en un solo extremo, llamado frente o principio de la cola, y los nuevos
				elementos son añadidos por el otro extremo, llamado fondo o final de la cola.
				Los elementos se eliminan en el mismo orden en el que se insertaron.</p>

				<p style="text-indent: 20; line-height: 150%" >Por
				lo tanto el primer elemento que entra a la cola será el primero en salir.
				Debido a esta característica, las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> también reciben el nombre de
				estructuras FIFO (First-In, First-Out: primero en entrar, primero en salir).</p>

				<p style="text-indent: 20; line-height: 150%" >Existen
				numerosos casos de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> en la vida real: las personas esperando para usar un
				teléfono público (la primera persona de la cola será la primera que use el
				teléfono), las personas que esperan para ser atendidas en la caja de una
				librería (ver figura 6.1.); se podrían seguir nombrando otros casos como los
				autos que esperan el cambio de luz de un semáforo en rojo, o los niños que
				esperan para subir a un juego mecánico.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.1.</b> Cola de personas que esperan para ser atendidas en la caja de una
				librería</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.1.gif" width="274" height="163"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/F6.1.gif" align="right" width="166" height="10"></p>

				<p  style="text-indent: 0; line-height: 100%"><b>6.3
				Aplicaciones de las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></b></p>
				<p  style="text-indent: 20; line-height: 150%">El
				concepto de cola está ligado a la computación. Una aplicación en la que las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> figuran como una estructura de datos prominente es la simulación por
				computadora de situaciones del mundo real.</p>

				<p  style="text-indent: 20; line-height: 150%">Las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> también se utilizan de muchas maneras en los sistemas operativos para
				planificar el uso de los distintos recursos de la computadora. Uno de estos
				recursos es la propia CPU (Unidad Central de Procesamiento).</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="text-indent: 20; line-height: 150%"> Si se está
				trabajando en un sistema multiusuario, cuando se
				le dice a la computadora que ejecute un programa concreto, el sistema operativo
				añade la petición a su “cola de trabajo”. Cuando la petición llega al
				frente de la cola, el programa solicitado pasa a ejecutarse. Igualmente, las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> se utilizan para asignar tiempo a los distintos usuarios de los
				dispositivos de entrada/salida (E/S), impresoras, discos, cintas y demás. El
				sistema operativo mantiene <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> para peticiones de imprimir, leer o escribir en
				cada uno de estos dispositivos.</p>

				<p  style="text-indent: 20; line-height: 150%"></p>

				<p style="text-indent: 0; line-height: 150%" ><b>6.4
				Operaciones con <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></b></p>
				<p style="text-indent: 20; line-height: 150%" >Según
				Cairó y Guardatí(1993), las operaciones que pueden realizarse en una cola son:</p>

				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Insertar
				    un elemento</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Eliminar
				    un elemento</li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Las
				inserciones se llevarán a cabo por el final de la cola, mientras que las
				eliminaciones se harán por el frente (FIFO - primero en entrar es el primero en
				salir).</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Considerando
				que una cola puede almacenar un máximo número de elementos (MAX) y que además
				la posición del primer elemento está almacenada en FRENTE y la posición del
				último en FINAL, se presentan ahora los algoritmos de inserción y eliminación
				en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> usando arreglos:</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.1">Algoritmo
				6.1. Inserción cola</a></p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.2">Algoritmo
				6.2. Eliminación cola</a></p>				<p style="text-indent: 20; line-height: 150%" >El
				ejemplo que se presenta a continuación ilustra el funcionamiento de las
				operaciones de inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				6.1.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				insertan en COLA los elementos: enero, febrero, marzo, abril y mayo (en ese
				orden), de modo que la estructura queda como se muestra en la figura 6.2.  Para
				este ejemplo el máximo número de elementos (MAX) es igual a 7.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				6.2.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/6.2.gif" width="119" height="112"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				6.3.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>, a) Luego de eliminar enero, b)
				Luego de insertar junio</p>				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/6.3.gif" width="239" height="140"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >El
				elemento enero es el primero que puede eliminarse por ser el primero que se
				insertó. Luego de la eliminación, FRENTE guarda la posición del siguiente
				elemento (figura 6.3.a). Si
				se insertara junio, éste ocuparía la posición siguiente a mayo (figura
				6.3.b).</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.4.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>, a) Luego de eliminar febrero,
				marzo, abril y mayo, b) Luego de insertar julio, c) Luego de eliminar junio</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.4.gif" width="363" height="140"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 100%" > </p>

				<p style="text-indent: 20; line-height: 150%" >Si
				se quisiera insertar un nuevo elemento ya no se podría, puesto que FINAL es
				igual a MAX. Sin embargo, como lo refleja la figura 6.4.c), hay espacio
				disponible. </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" > Esta es una situación conflictiva, a pesar de que hay espacio, no
				se pueden insertar otros elementos, este inconveniente puede superarse
				manejando la cola como una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a> o dejando el frente fijo en la
				posición 1 cuando hay elementos.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>6.5.
				Clasificación de las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> pueden ser clasificadas de la siguiente manera:</p>
				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 150%" ><b>6.5.1
				Cola Circular</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> lineales tienen un grave problema, como las extracciones sólo pueden
				realizarse por un extremo, puede llegar un momento en que el <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> Frente
				sea igual al máximo número de elementos en la cola, siendo que al frente de la
				misma existan lugares vacíos, y al insertar un nuevo elemento arrojará un
				error de overflow (cola llena).</p>				<p style="text-indent: 20; line-height: 150%" >Para
				solucionar el problema de desperdicio de memoria se implementaron las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>
				circulares, en las cuales existe un índice desde el último elemento al primero
				de la cola.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >En
				la figura 6.5. se ilustra cómo se actualizan los punteros FRENTE y FINAL en una
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a>, a medida que se insertan o eliminan elementos. </p>

				<p style="text-indent: 20; line-height: 150%" > </p>

				<p style="text-indent: 20; line-height: 150%" >En la figura
				6.5.a), la cola tiene algunos elementos (FRENTE = 2 y FINAL = 8). </p>

				<p style="text-indent: 20; line-height: 150%" >En la figura
				6.5.b), se han eliminado de la cola dos elementos (primero se quitó 12
				y luego 45), quedando FRENTE = 4. ;</p>


				<p style="text-indent: 20; line-height: 150%" > Por
				último, en la figura 6.5.c) se ha insertado un nuevo elemento (20) en la cola.
				Como FINAL = MAX se llevó el <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> a la primera posición que estaba vacía
				(FINAL = 1).</p>

				<p style="text-indent: 20; line-height: 150%" >De
				esta manera se logra un mejor aprovechamiento del espacio de memoria disponible,
				ya que al eliminar un elemento esa casilla de la cola queda disponible para
				futuras inserciones.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.5.</b> Representación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares, a) Frente &lt; Final, b) Frente &lt;
				Final, c) Frente &gt; Final</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.5.gif" width="293" height="252"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >El
				que sigue es otro ejemplo para ilustrar el funcionamiento de las operaciones de
				inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>EJEMPLO
				6.2.</b></p>
				<p style="text-indent: 20; line-height: 150%" >Dado
				una estructura tipo <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a> de 8 elementos (MAX = 8), en la cual se tienen
				algunos elementos como se muestra en la figura 6.6.a). En la figura 6.6.b) se
				presenta el estado de la estructura luego de insertar el elemento 20.</p>

				<p align="center" style="text-indent: 0; line-height: 100%"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD; mso-bidi-font-weight: bold"><b>Figura
				6.6.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares, a) Estado inicial de la
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a>, b) Luego de insertar 20</span></p>

				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capVI/6.6.gif" width="293" height="163"></p>

				<p align="right" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>
				<p  style="text-indent: 20; line-height: 150%">Si
				se quisiera insertar otro elemento se presentaría un error de desbordamiento,
				ya que FINAL + 1 = FRENTE.Se eliminan los valores 12, 45, 78, 47, 56 y 23 en
				ese orden. La cola queda como se muestra en la figura 6.7.:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="text-indent: 0; line-height: 100%"><b>Figura
				6.7.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares</p>
				<p align="center" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capVI/6.7.gif" width="273" height="70"></p>

				<p align="right" style="text-indent: 0; line-height: 100%"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Ahora
				se elimina el siguiente elemento 15, al ser FRENTE = MAX, se le da el valor de
				1, figura 6.8.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.8.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.8.gif" width="269" height="70"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Al
				eliminar 20, como FRENTE = FINAL (sólo hay un elemento en la cola) se
				actualizan los dos punteros en cero. La cola queda vacía, figura 6.9.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.9.</b> Inserción y eliminación en <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> circulares</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capVI/6.9.gif" width="269" height="70"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 00; margin-bottom: 0" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD; mso-bidi-font-weight: bold"><!--[if gte vml 1]><v:shape
				 id="_x0000_i1026" type="#_x0000_t75" style='width:207pt;height:39.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/05/clip_image003.png"
				  o:title="6"/>
				</v:shape><![endif]-->
				</span>A
				continuación se presentan los algoritmos para realizar la inserción y
				eliminación de elementos en una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a>:</p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.3">Algoritmo
				6.3. Inserta circular</a></p>				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.4">Algoritmo
				6.4. Elimina circular</a></p>

				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 100%" ><b>6.5.2
				Doble Cola</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> es una generalización de una estructura de cola simple.</p>
				<p style="text-indent: 20; line-height: 150%" > En
				una doble cola, los elementos; pueden; ser; insertados; o</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" > eliminados por cualquiera
				de los extremos, es decir, se pueden insertar y eliminar valores tanto por el
				frente como por el final de la cola, es en realidad, una cola bidireccional.</p>

				<p style="text-indent: 20; line-height: 150%" > Una
				doble cola se representa como se muestra en la figura 6.10.</p>
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.10.</b> Representación de doble cola</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capVI/6.10.gif" width="353" height="44"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capVI/F6.10.gif" width="359" height="10"></p>				<p style="text-indent: 20; line-height: 150%" >Las
				dos flechas en cada extremo indican que pueden ejecutarse las operaciones de
				inserción y eliminación.</p>

				<p style="text-indent: 20; line-height: 150%" >Según
				el web site http://www.itlp.edu.mx/publica/tutori ales/estru1/25.htm, existen dos
				variantes de las dobles <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" ><b>Doble
				    cola con entrada restringida</b></li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Permite
				que las eliminaciones puedan hacerse por cualquiera de los dos extremos,
				mientras que las inserciones sólo por el final de la cola (figura 6.11.).</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.11.</b> Doble cola con entrada restringida</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.11.gif" width="342" height="44"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >A
				continuación se presentan los algoritmos de inserción y eliminación en una
				cola con entrada restringida, donde el máximo número de elementos está
				representado por Max, el frente de la cola está representado por F y el final
				por A. Inicialmente estas variables deben ser inicializadas de la siguiente
				manera: F = 1 y A = 0.</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.5">Algoritmo
				6.5. Inserción <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> entrada restringida</a></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.6">Algoritmo
				6.6. Eliminación <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> entrada restringida</a></p>				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" ><b>Doble
				    cola con salida restringida</b></li>
				</ul>
				<p style="text-indent: 20; line-height: 150%" >Permite
				que las inserciones puedan hacerse por cualquiera de los dos extremos, mientras
				que las eliminaciones sólo por el frente de la cola (figura 6.12.).</p>				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.12.</b> <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> con salida restringida</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.12.gif" width="340" height="44"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >A
				continuación se presentan los algoritmos de inserción y eliminación en una
				cola con salida restringida, donde el máximo número de elementos está
				representado por Max, el frente de la cola está representado por F y el final
				por A. Inicialmente estas variables deben ser inicializadas de la siguiente
				manera: F = 0 y A = MAX.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.7">Algoritmo
				6.7. Inserción <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> salida restringida</a></p>
				<p style="text-indent: 0; line-height: 150%" align="center"><a href="a6.8">Algoritmo
				6.8. Eliminación <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> salida restringida</a></p>				<p style="text-indent: 20; line-height: 150%" >Para
				representar una bicola se puede elegir una representación estática, con
				arreglos, o bien una representación dinámica, con listas enlazadas.</p>				<p style="text-indent: 20; line-height: 150%" >En
				la representación dinámica, la mejor opción es mantener la bicola con una
				lista doblemente enlazada: los dos extremos de la lista se representan con las
				variables puntero izquierdo y derecho, respectivamente.</p>
				<p style="text-indent: 20; line-height: 150%" >En
				la representación estática se mantienen los elementos de la bicola con un
				arreglo circular y dos variables índice del extremo izquierdo y derecho,
				respectivamente.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>6.5.3
				Cola de Prioridades</b></p>
				<p style="text-indent: 20; line-height: 150%" >El
				término cola sugiere la forma en que ciertas personas u objetos esperan la
				utilización de un determinado servicio. Por otro lado, el termino prioridad
				sugiere que el servicio no se proporciona únicamente aplicando el concepto de
				cola (FIFO),; sino; que cada; persona tiene asociada; una; prioridad
				basada
				en un criterio objetivo.</p>

				<p style="text-indent: 20; line-height: 150%" >Un
				ejemplo típico de organización formando <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> de prioridades, es el sistema de
				tiempo compartido necesario para mantener un conjunto de procesos que esperan
				servicio para trabajar, donde se asigna cierta prioridad a cada proceso.</p>				<p style="text-indent: 20; line-height: 150%" >Según
				Joyanes y Zahonero (1998), el orden en que los elementos son procesados y por
				tanto eliminados sigue estas reglas:</p>				<p style="text-indent: 20; line-height: 150%" >1.
				Se elige la lista de elementos que tienen la mayor prioridad.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >2.
				En la lista de mayor prioridad, los elementos se procesan según el orden de
				llegada, es decir, según la organización de una cola: primero en llegar,
				primero en ser procesado.</p>

				<p style="text-indent: 20; line-height: 150%" >Las
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> de prioridades pueden implementarse de dos formas: mediante una única
				lista o bien mediante una lista de n <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>.</p>

				<p style="text-indent: 20; line-height: 150%" ><i>Implementación
				mediante una única lista</i></p>
				<p style="text-indent: 20; line-height: 150%" >Cada
				proceso forma un nodo de la lista enlazada. La lista se mantiene ordenada por el
				campo prioridad.</p>

				<p style="text-indent: 20; line-height: 150%" >La
				operación de añadir un nuevo nodo hay que hacerla tomando en cuenta que la
				nueva lista ha de permanecer ordenada.</p>

				<p style="text-indent: 20; line-height: 150%" > De existir más de un nodo con igual
				prioridad, se añade como último en el grupo de nodos de igual prioridad.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" > ;De
				esta manera la lista queda organizada de tal forma que un nodo X precede a un
				nodo Y si:</p>

				<p style="text-indent: 20; line-height: 150%" >1.
				Prioridad (X) &gt; Prioridad (Y).</p>
				<p style="text-indent: 20; line-height: 150%" >2.
				Ambos tienen la misma prioridad, pero X se añadió antes que Y.</p>

				<p style="text-indent: 20; line-height: 150%" ><i>Implementación
				mediante una lista de n <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></i></p>
				<p style="text-indent: 20; line-height: 150%" >Se
				utiliza una cola separada para cada nivel de prioridad. Cada cola puede
				representarse con un arreglo circular, mediante una lista enlazada, o bien
				mediante una lista circular, en cualquier caso con su Frente y Final. Para
				agrupar todas las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>, se utiliza un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de registros" v-bind:data-content="ArreglosDeRegistros">arreglo de registros</a>.</p>

				<p style="text-indent: 20; line-height: 150%" >Cada registro
				representa un nivel de prioridad, y tiene el frente y el final de la cola
				correspondiente.</p>

				<p style="text-indent: 20; line-height: 150%" > La razón para utilizar un arreglo radica en que los niveles de
				prioridad son establecidos de antemano (ver figura 6.13).</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.13.</b> Cola de prioridad con n <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.13.gif" width="161" height="126"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/F6.13.gif" align="right" width="224" height="10"></p>
				<p style="text-indent: 0; line-height: 100%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				</span><b>6.6 Implementación de una
				Cola con un Arreglo</b></p>
				<p style="text-indent: 20; line-height: 150%" >Al
				igual que las pilas, las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> no existen como estructuras de datos estándares
				en los lenguajes de programación.</p>				<p style="text-indent: 20; line-height: 150%" >Al representar una cola a través de un
				arreglo, debe definirse un tamaño máximo para la cola y dos variables
				auxiliares. Una de ellas para que guarde la posición del primer elemento de la
				cola (frente) y otra para que guarde la posición del último elemento de la
				cola (final).</p>
				<p style="text-indent: 20; line-height: 150%" >En
				la figura 6.14. se muestra la representación de una cola</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >en la cual se han
				insertado tres elementos: 111,222 y 333, en ese orden.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.14.</b> Representación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a></p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.14.gif" width="272" height="116"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/F6.14.gif" align="right" width="207" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >El
				elemento 111 está en el frente ya que es el primero que entró en la cola.
				Mientras que el elemento 333, que fue el último en entrar, está en el FINAL de
				la cola.</p>

				<p style="text-indent: 20; line-height: 150%" >En la figura 6.15. se muestran algunos ejemplos de cola:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.15.</b> Representación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>. a) Cola llena, b) Cola con algunos elementos
				y c) Cola vacía.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.15.gif" width="333" height="144"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/F6.14.gif" width="207" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Un
				arreglo es una estructura estática y por tanto tiene dimensión finita; por el
				contrario, una cola puede crecer y crecer sin límite, y en consecuencia se
				puede originar un desbordamiento, por lo tanto no es recomendable la
				implementación de una cola con un arreglo.</p>

				<p style="text-indent: 20; line-height: 150%" ><b>Declaración
				de la estructura cola con arreglo</b></p>
				<p style="text-indent: 20; line-height: 150%" >Siguiendo
				el formato utilizado para definir un tipo; arreglo; en; el; capítulo;
				II, se; declarará;
				una; cola; de; 100; elementos</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 200%" >como
				se muestra a continuación:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Constantes</b></p>
				<p style="text-indent: 50; line-height: 100%" >MaxCola
				: 100 {Tamaño de la cola}</p>				<p style="text-indent: 20; line-height: 100%" ><b>Tipos</b></p>
				<p style="text-indent: 50; line-height: 100%" >TipoCola
				: Arreglo [1..MaxCola] de Entero</p>				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 50; line-height: 100%" >Cola
				: TipoCola</p>
				<p style="text-indent: 20; line-height: 150%" >Los
				algoritmos presentados hasta ahora sobre inserción y eliminación de elementos
				en los distintos tipos de cola se han basado en la implementación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> con
				arreglos.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>
				<p style="text-indent: 0; line-height: 100%" ><b>6.7
				Implementación de una Cola con Arreglos Circulares</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				implementación de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> mediante un arreglo lineal; es; poco eficiente, imagínese;
				un; arreglo; como; una; estructura</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" > circular en la que al último elemento
				le sigue el primero; esta representación implica que aún estando ocupado el
				último elemento del arreglo, pueda añadirse uno nuevo
				detrás de él, ocupando la primera posición del mismo (ver figura 6.16).</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.16.</b> Arreglos circulares.</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capVI/6.16.gif" width="353" height="165"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capVI/F6.16.gif" width="224" height="10"></p>

				<p style="text-indent: 20; line-height: 150%" >Para
				añadir un elemento a la cola, se mueve el Índice final una posición en el
				sentido de las manecillas del reloj, y se asigna el elemento (ver figura 6.17).
				Para suprimir un elemento es suficiente con mover el índice frente una
				posición en el sentido del avance de las manecillas del reloj (ver figura
				6.18).</p>
			</div>
			<div class="pagina" @click="navNext">
				<p align="center" style="text-align:center;line-height:150%"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				<b>Figura 6.17.</b> Inserción de nuevos elementos en un arreglo circular.</span></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/6.17.gif" width="193" height="147"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/F6.17.gif" align="right" width="224" height="10"></p>

				<p align="center" style="text-align:center;line-height:150%"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				<!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:150pt;
				 height:115.5pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/05/clip_image001.png"
				  o:title="6"/>
				</v:shape><![endif]-->
				 </span></p>

				<p align="center" style="text-align:center;line-height:150%"><b>Figura
				6.18.</b> Supresión de un elemento en un arreglo circular.</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/6.18.gif" width="192" height="149"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/F6.17.gif" align="right" width="224" height="10"></p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >De
				esta manera, la cola se mueve en un mismo sentido, tanto si se realizan
				inserciones como supresiones de elementos (ver figura 6.19).</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.19.</b> Evolución de arreglos circulares.</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.19.gif" width="330" height="141"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/F6.17.gif" align="right" width="224" height="10"></p>				<p style="text-indent: 20; line-height: 150%" >Según
				se puede observar en la representación, la condición de cola vacía [frente =
				siguiente (final)] va a coincidir con una cola que ocupa el círculo completo,
				una cola que llene todo el arreglo.</p>
				<p style="text-indent: 20; line-height: 100%" >Para;;
				resolver;; el; problema,; una; primera; tentativa; sería</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >considerar
				que el elemento final referencie a una posición adelantada a la que realmente
				ocupa el elemento, en el sentido del avance del reloj. Teniendo
				presente esta consideración cuando la cola esté llena, el índice siguiente a Final será igual al frente.</p>

				<p style="text-indent: 20; line-height: 150%" >Considérese ahora el caso en que queda
				un solo elemento en la cola. Si en estas condiciones se suprime el elemento, la
				cola queda vacía, el puntero Frente avanza una posición en el sentido de las
				manecillas del reloj y va a referenciar a la misma posición que el siguiente al
				puntero Final, es decir, está exactamente en la misma posición relativa que
				ocuparía si la cola estuviera llena.</p>
				<p style="text-indent: 20; line-height: 150%" >Una
				solución a este problema es sacrificar un elemento del arreglo y dejar que
				Final referencie a la posición realmente ocupada por el último elemento
				añadido.</p>

				<p style="text-indent: 20; line-height: 150%" >Si el arreglo tiene Long posiciones, no se debe dejar que la cola
				crezca más que long-1.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>6.8. Implementación de una Cola con Lista Enlazada</b></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				las pilas, las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> pueden ser implementadas como una estructura enlazada. Una
				cola FIFO es otro tipo de lista de acceso restringido: los elementos se añaden
				siempre por el final de la lista y se quitan siempre por el frente.</p>
				<p style="text-indent: 20; line-height: 150%" >En
				una representación enlazada se pueden usar dos punteros externos, FrenteCola y
				FinalCola, para marcar el frente y final de la cola (véase Figura 6.20).</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.20.</b> Una representación enlazada de cola.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.20.gif" width="292" height="54"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" ><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:225.75pt;
				 height:47.25pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/05/clip_image001.png"
				  o:title="6"/>
				</v:shape><![endif]--></span></p>

				<p style="text-indent: 20; line-height: 150%" >A
				continuación se presentan los algoritmos para la inserción y eliminación de
				elementos en una cola mediante la implementación de una lista enlazada simple:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" ><b>Declaración
				de una cola mediante la implementación de una lista enlazada simple</b></p>				<p style="text-indent: 20; line-height: 100%" ><b>Tipos</b></p>
				<p style="text-indent: 50; line-height: 100%" >Lista
				= ^Nodo</p>
				<p style="text-indent: 50; line-height: 100%" >Nodo</p>

				<p style="text-indent: 70; line-height: 100%" >Info:
				Entero</p>
				<p style="text-indent: 70; line-height: 100%" >Sig:
				Lista</p>
				<p style="text-indent: 50; line-height: 100%" >Fin
				Nodo</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 50; line-height: 100%" >FrenteCola,
				FinalCola : Lista</p>

				<p style="text-indent: 50; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 100%" align="center"><a href="a6.9">Algoritmo
				6.9. Inserción de un elemento en la cola</a></p>				<p style="text-indent: 20; line-height: 150%" >La
				implementación de este algoritmo puede visualizarse en la siguiente figura:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				6.21.</b> Inserción de un elemento en la cola</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.21.gif" width="334" height="79"></p>
				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>				<p style="text-indent: 0; line-height: 100%" align="center"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD">
				</span><a href="a6.10">Algoritmo
				6.10. Borrado de un elemento en la cola</a></p>				<p style="text-indent: 20; line-height: 150%" >La
				implementación de este algoritmo puede visualizarse en la figura que se
				presenta a continuación:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				6.22.</b> Eliminación de un elemento en la cola</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVI/6.22.gif" width="301" height="86"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVI/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%" ><b>6.9
				Implementación de una Cola con Lista Circular</b></p>
				<p style="text-indent: 20; line-height: 150%" >Esta
				implementación es una variante de la realización con listas enlazadas. Al
				realizar una lista circular se estableció, por conveniencia, que el puntero de
				acceso a la lista referenciaba al último nodo, y que el nodo siguiente se
				considera el primero. El criterio de tener el puntero externo a una lista
				enlazada circular apuntando al último nodo facilitaría el acceso a los dos
				extremos de la lista. Esta característica hace posible incrementar las
				operaciones de cola usando un puntero a la cola en vez de punteros separados al
				frente y final. (Véase Figura 6.23.)</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				6.23.</b> Cola con lista circular</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVI/6.23.gif" width="235" height="41"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="elabpropia.gif" align="right" width="150" height="11"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:225.75pt;
				 height:33.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/05/clip_image001.png"
				  o:title="6"/>
				</v:shape><![endif]-->
				</span></p>

				<p  style="line-height: 150%; text-indent: 20">Para
				poder insertar un nuevo elemento a la cola se puede accesar al nodo Final
				mediante el puntero externo Cola. Para eliminar un elemento de la cola, se
				accede al nodo frente mediante Cola^.Sig. Una cola vacía se representaría como
				Cola = Nulo.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">La
				codificación de las operaciones de cola usando una lista enlazada se muestran a
				continuación:</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><a href="a6.11">Algoritmo
				6.11. Inserción de un elemento en la cola</a></p>				<p align="center" style="line-height: 100%; text-indent: 0"><a href="a6.12">Algoritmo
				6.12. Borrado de un elemento en la cola</a></p>
				<p  style="line-height: 100%; text-indent: 0"><b>6.10
				Representación en Memoria de una Cola</b></p>
				<p  style="line-height: 150%; text-indent: 20">Como
				se mencionó anteriormente, las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> no son estructuras de datos fundamentales,
				es decir, no están definidas en los lenguajes de programación, lo que implica
				que deban representarse mediante el uso de Arreglos o Listas enlazadas, trayendo
				como consecuencia que su representación
				en memoria dependa de la estructura seleccionada para su representación.</p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 100%" ><b>Resumen</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				cola es una lista ordenada de elementos, en la cual las eliminaciones se
				realizan en un solo extremo, llamado frente, y</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" >los nuevos elementos son
				añadidos por el otro extremo, llamado fondo o final de la cola. Los elementos
				se eliminan en el mismo orden en el que se insertaron. Por lo tanto el primer
				elemento que entra a la cola será el primero en salir.</p>

				<p style="text-indent: 20; line-height: 150%" >Las
				operaciones que
				pueden realizarse en una cola son: Insertar un elemento y Eliminar un elemento.
				Las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> pueden ser:</p>				<p style="text-indent: 20; line-height: 150%" >Circulares,
				buscan solucionar el grave problema que presentan las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> lineales, es decir,
				debido a que las extracciones sólo se pueden realizar por un extremo, puede
				llegar un momento en que el <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> Frente sea igual al máximo número de
				elementos en la cola, siendo que al frente de la misma existan lugares vacíos,
				y al insertar un nuevo elemento arrojará un error de overflow (cola llena).</p>

				<p style="text-indent: 20; line-height: 150%" >Bicola
				es una generalización de una estructura de cola simple. En una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a>, los
				elementos pueden ser insertados o eliminados por cualquiera de los extremos, es
				en realidad, una cola bidireccional.</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Existen dos variantes de las dobles <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a>:
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a> con entrada restringida en éstas las eliminaciones se pueden hacer
				por cualquiera de los dos extremos mientras que las inserciones sólo por el
				final de la cola; <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola con calida restringida" v-bind:data-content="DobleColaConSalidaRestringida">Doble cola con salida restringida</a> permiten que las inserciones
				se realicen por cualquiera de los dos extremos, mientras que las eliminaciones
				sólo por el frente de la cola. Se puede representar dinámicamente con una
				lista doblemente enlazada en donde los dos extremos de la lista se representan
				con las variables puntero izquierdo y derecho, respectivamente. También se
				puede representar estáticamente con un arreglo circular y dos variables índice
				del extremo izquierdo y derecho, respectivamente.</p>

				<p style="text-indent: 20; line-height: 150%" >Cola
				de Prioridades el término prioridad sugiere que la cola no se basará sólo el
				concepto de cola (FIFO), sino que cada elemento de la cola tendrá asociada una
				prioridad basada en un criterio objetivo.</p>

				<p style="text-indent: 20; line-height: 150%" >Al
				igual que las pilas, las <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola" v-bind:data-content="Cola">colas</a> no existen como estructuras de datos estándares
				en los lenguajes de programación, lo que implica que deban representarse
				mediante el uso de Arreglos o Listas enlazadas.</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="text-indent: 0; line-height: 150%">Ejercicios
				Propuestos</p>
				</b>
				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Sea
				    C una cola de 4 elementos. Inicialmente la cola está vacía. Dibuje el
				    estado de C luego de realizar las siguientes operaciones:</li>
				</ol>
				<p  style="text-indent: 68; line-height: 150%">a)
				Insertar 11</p>
				<p  style="text-indent: 68; line-height: 150%">b)
				Insertar 13</p>
				<p  style="text-indent: 68; line-height: 150%">c)
				Eliminar 11</p>
				<p  style="text-indent: 68; line-height: 150%">d)
				Insertar 20, 30, 25</p>
				<p  style="text-indent: 68; line-height: 150%">e)
				Eliminar 20</p>
				<p  style="text-indent: 68; line-height: 150%">-
				¿Con cuántos elementos quedó C?</p>
				<blockquote>
				  
				</blockquote>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="2">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para incluir un elemento en una cola utilizando un arreglo.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="3">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para incluir un elemento en una cola utilizando una lista
				    enlazada simple.</li>
				</ol>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="4">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para eliminar un elemento en una cola utilizando una Lista
				    enlazada simple.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="5">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para eliminar un elemento en una cola utilizando un
				    arreglo.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="6">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para incluir un elemento en una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a>. Utilice una
				    lista enlazada para realizar la implementación.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="7">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para eliminar un elemento en una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Cola circular" v-bind:data-content="ColaCircular">cola circular</a>. Utilice un
				    arreglo para realizar la implementación.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="8">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para incluir un elemento en una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a>. Utilice un arreglo
				    para realizar la implementación.</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="9">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo para eliminar un elemento en una <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Doble cola o bicola" v-bind:data-content="DobleColaOBicola">Doble cola o bicola</a>. Utilice un arreglo
				    para realizar la implementación.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="10">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Escriba
				    un pseudocódigo que invierta los elementos de una cola. Utilice la
				    estructura que considere más adecuada para realizar la implementación.</li>
				</ol>
				<p  style="text-indent: 20; line-height: 150%"><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 6)">Autoevaluación</a></p>
			</div>
		</div>
		<!--Contenido 7-->
		<div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" ><b>7.1
				Introducción</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				vez estudiadas las diferentes estructuras de datos lineales, es fácil observar
				que en ocasiones ninguna de ellas se adapta a una situación específica, lo que
				implica que sea necesario agrupar a una o más de ellas en una misma estructura,
				dando origen a lo que se denomina una estructura de datos lineal combinada.</p>				<p style="text-indent: 20; line-height: 150%" >Para
				el manejo de este tipo de estructuras hay que tener presente el funcionamiento
				de cada una de las estructuras involucradas, ya que se tratará a cada una
				según sus características básicas pero de forma anidada, lo que implica que
				el uso de este tipo de estructura requiera mayor concentración por parte del
				programador.</p>
				<p style="text-indent: 20; line-height: 150%" >En
				el capítulo que se presenta a continuación se tratarán las principales
				estructuras de datos combinadas, estableciendo para cada una de ellas su
				declaración y manipulación.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" ><b>7.2
				Arreglos de Registros</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				este caso, cada elemento del arreglo será un registro (todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a>
				del arreglo serán del mismo tipo de registro). A continuación se presenta un
				ejemplo:</p>

				<p style="text-indent: 0; line-height: 200%" ><b>EJEMPLO
				7.1</b></p>
				<p style="text-indent: 20; line-height: 150%" >Una
				empresa registra para cada uno de sus clientes los siguientes datos:</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Nombre
				    (cadena de caracteres).</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Teléfono
				    (cadena de caracteres).</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 150%" >Saldo
				    (real).</li>
				</ul>				<p style="text-indent: 20; line-height: 150%" >Suponiendo
				que la empresa tenga N clientes necesitará entonces un arreglo de N elementos,
				en el cual cada componente del mismo es un registro del tipo descrito en el
				ejemplo 7.1. La
				tabla 7.1. muestra la estructura requerida: </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 200%" align="center">A
				= ARREGLO [1..10] DE CLIENTE</p>

				<p align="center" style="line-height: 100%; text-indent: 0"><span lang="ES-TRAD" style="mso-bidi-font-size: 14.0pt; mso-ansi-language: ES-TRAD"><b>Tabla
				7.1.</b> Arreglos de Registros</span></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVII/T7.1.gif" width="406" height="84"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p  style="line-height: 150%; text-indent: 20">Cada
				elemento de A será un dato tipo CLIENTE. Por lo tanto si se quiere, por
				ejemplo, leer el arreglo A debe leerse por cada componente cada uno de los
				campos que forman al registro.</p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p  style="line-height: 100%; text-indent: 20"><b>Para</b>
				I = 1 <b>Hasta</b> N <b>Inc</b> [1]</p>

				<p  style="line-height: 100%; text-indent: 50"><b>Leer</b>
				A[I].nombre</p>

				<p  style="line-height: 100%; text-indent: 50"><b>Leer</b>
				A[I].teléfono</p>

				<p  style="line-height: 100%; text-indent: 50"><b>Leer</b>
				A [I].saldo</p>

				<p  style="line-height: 100%; text-indent: 20"><b>Fin
				Para</b></p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p  style="line-height: 150%; text-indent: 20">Con
				A[I] se hace referencia al elemento I del arreglo A, que es un registro; con
				.id_campo se especifica cuál de los campos del registro es el que se leerá.</p>

				<p  style="line-height: 150%; text-indent: 20"> De
				manera similar se procedería para escritura, asignación o comparación de
				elementos.</p>

				<p  style="line-height: 150%; text-indent: 20">Se
				han presentado los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de registros" v-bind:data-content="ArreglosDeRegistros">arreglos de registros</a> como una solución adecuada para
				aquellos casos en los que se necesita que los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo sean
				estructurados.</p>

				<p  style="line-height: 150%; text-indent: 20"> Otra alternativa de solución consiste en utilizar arreglos
				paralelos.</p>

				<p style="text-indent: 20; line-height: 150%" ><b>Uso
				de <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de registros" v-bind:data-content="ArreglosDeRegistros">arreglos de registros</a></b></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				estudiar el uso de arreglos de registros, véase la figura 7.1:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				7.1.</b> Arreglo de registros</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVII/7.1.gif" width="227" height="115"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVII/F7.1.gif" align="right" width="198" height="11"></p>				<p style="text-indent: 20; line-height: 150%" >En
				este caso, cada componente del arreglo ALUMNO es un registro que tiene dos
				campos: NOMBRE y CALIF.</p>
				<p style="text-indent: 20; line-height: 150%" >Así:
				ALUMNOS[I].NOMBRE hará referencia al nombre del alumno I y; ALUMNOS[I].CALIF
				hará referencia a la calificación obtenida por el alumno I </p>

				<p style="text-indent: 0; line-height: 150%" ><b>7.3
				Registros de Arreglos</b></p>
				<p style="text-indent: 20; line-height: 150%" >Los
				registros con arreglos tienen por lo menos un campo que es un arreglo. Póngase
				atención al siguiente ejemplo.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>EJEMPLO
				7.2.</b></p>
				<p style="text-indent: 20; line-height: 100%" >Una
				empresa registra para cada uno de sus clientes estos datos:</p>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Nombre
				    (cadena de caracteres).</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Teléfono
				    (cadena de caracteres).</li>
				</ul>
				<ul>
				  <li>
				    <p style="text-indent: 0; line-height: 100%" >Saldo
				    mensual del último año (arreglo de reales).</li>
				</ul>				<p style="text-indent: 20; line-height: 100%" >La
				definición del registro correspondiente es la siguiente:</p>				<p style="text-indent: 20; line-height: 100%" ><b>CLIENTE</b></p>
				<p style="text-indent: 50; line-height: 100%" >nombre:
				cadena de caracteres</p>
				<p style="text-indent: 50; line-height: 100%" >teléfono:
				cadena de caracteres</p>
				<p style="text-indent: 50; line-height: 100%" >saldos:
				ARREGLO [1..12] de real</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Cliente</b></p>
				<p style="text-indent: 20; line-height: 150%" >En
				este caso el registro tiene un campo (saldos) que es un arreglo de doce
				elementos reales.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Para
				hacer referencia a ese campo, se procederá de la siguiente manera:</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>variable_registro.id_campo[índice]</b></p>

				<p style="text-indent: 20; line-height: 150%" >La
				tabla 7.2. muestra la estructura requerida:</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Tabla
				7.2.</b> Registros con arreglos</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVII/T7.2.gif" width="340" height="82"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" >Para
				accesar los campos del registro presentado en la tabla 7.2. debe seguirse la
				siguiente secuencia:</p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >CLIENTE.nombre</p>
				<p style="text-indent: 20; line-height: 100%" >CLIENTE.teléfono</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Para</b>
				I = 1 <b>Hasta</b> 12 <b>Inc</b> [1]</p>
				<p style="text-indent: 50; line-height: 100%" >CLIENTE.saldos[I]</p>
				<p style="text-indent: 20; line-height: 100%" ><b>Fin
				Para</b></p>

				<p style="text-indent: 0; line-height: 100%" ></p>

				<p style="text-indent: 0; line-height: 100%" ><b>7.4.
				Registros de Listas</b></p>
				<p style="text-indent: 20; line-height: 150%" >Un
				Registro de Listas es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a>, donde cada uno de sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> se
				denomina campo.</p>				<p style="text-indent: 20; line-height: 150%" > Los campos de un registro pueden ser todos de diferentes tipos,
				y entre ellos se pueden encontrar uno o más <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a> a estructuras
				dinámicas, tal como lo muestra la figura 7.2.</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				7.2.</b> Registro de Lista</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVII/7.2.gif" width="123" height="42"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVII/elabpropia.gif" align="right" width="150" height="11"></p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" ><b>Declaración
				de un Registro de Lista:</b></p>
				<p style="text-indent: 20; line-height: 150%" >La
				declaración del registro representado en la figura 7.2, se realiza de la
				siguiente manera:</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Tipo</b></p>
				<p style="text-indent: 20; line-height: 100%" >Lista
				= ^Nodo</p>

				<p style="text-indent: 20; line-height: 100%" >RegLis</p>

				<p style="text-indent: 50; line-height: 100%" >Info1:
				Entero</p>

				<p style="text-indent: 50; line-height: 100%" >L:
				Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin
				RegLis</p>

				<p style="text-indent: 20; line-height: 100%" >Nodo</p>
				<p style="text-indent: 50; line-height: 100%" >Info2:
				Real</p>
				<p style="text-indent: 50; line-height: 100%" >Sig:
				Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin
				Nodo</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Variable</b></p>
				<p style="text-indent: 20; line-height: 100%" >R:
				RegLis</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>Acceso
				a los Campos de un Registro</b></p>
				<p style="text-indent: 20; line-height: 150%" >Como
				un registro de Lista es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a> no se puede accesar directamente
				como un todo, sino que debe especificarse qué elemento (campo) del registro
				interesa.</p>

				<p style="text-indent: 20; line-height: 150%" >Para ello, en la mayoría de los lenguajes se tiene la siguiente
				sintaxis:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>variable_registro.id_campo</b></p>
				<p style="text-indent: 20; line-height: 100%" >Donde:
				<b>
				variable_registro</b> es una variable de tipo registro</p>

				<p style="text-indent: 85; line-height: 100%" ><b>id_campo</b>
				es el identificador del campo deseado</p>

				<p style="text-indent: 20; line-height: 150%" >Se
				debe seguir la misma regla para accesar al campo que apunta a la lista, es
				decir, se usarán dos nombres para hacer referencia a un elemento: el nombre de
				la variable tipo registro y el nombre del componente, separados entre sí por un
				punto.</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Las
				operaciones de lectura, escritura y asignación se realizan de la siguiente
				manera:</p>

				<p style="text-indent: 20; line-height: 150%" >Para
				Asignar los valores de los campos de una variable R tipo RegLis a otras
				variables del mismo tipo de cada campo, se puede realizar de la siguiente
				manera, suponiendo que C es una variable entera y P es de tipo lista.</p>
				<p style="text-indent: 20; line-height: 150%" >C
				= RegLis.Info1</p>

				<p style="text-indent: 20; line-height: 150%" >P
				= RegLis.L</p>
				<p style="text-indent: 20; line-height: 150%" >En
				el caso contrario, si se desea asignar el valor de una variable a un campo de un
				registro, se haría de la siguiente manera, siempre y cuando C sea una variable
				entera y P sea un nodo de tipo Lista:</p>

				<p style="text-indent: 20; line-height: 150%" >RegLis.Info1=
				C</p>
				<p style="text-indent: 20; line-height: 150%" >RegLis.L
				= P</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Si
				P está enlazado al RegLis y se quisiera asignar el valor de su campo Info a
				otra variable ésta debe ser del mismo tipo del campo Info de la Lista, es
				decir, Real. Si R es una variable Real y la lista (RegLis.L) es distinto de
				Nulo, se puede asignar el valor de su campo Info a la variable R, de la
				siguiente manera:</p>
				<p style="text-indent: 20; line-height: 100%" >R
				= RegLis.L^.Info2</p>
				<p style="text-indent: 20; line-height: 150%" >En
				general, el orden en el cual se manejan los campos de un registro de lista no es
				importante. Sólo
				debe tenerse en cuenta que los datos se correspondan en tipo con los campos.</p>

				<p style="text-indent: 0; line-height: 100%" ><b>7.5.
				Listas de Registros</b></p>
				<p style="text-indent: 20; line-height: 150%" >Es
				una lista enlazada en la que uno o más de sus campos está conformado por un
				registro. Para comprender el funcionamiento de esta estructura, supóngase que
				en una empresa se desea llevar el control de empleados por su cédula de
				identidad y almacenando para cada uno datos personales como nombre, edad,
				teléfono, dirección y sexo. </p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >Al
				resolver este problema aplicando listas de registros, queda de la siguiente
				manera:</p>

				<p style="text-indent: 20; line-height: 200%" ><b>Declaración
				de una lista de registros</b></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Tipo</b></p>
				<p style="text-indent: 20; line-height: 100%" >Lista
				= ^Empleado</p>
				<p style="text-indent: 20; line-height: 100%" >Empleado</p>
				<p style="text-indent: 50; line-height: 100%" >CI:
				Entero</p>
				<p style="text-indent: 50; line-height: 100%" >Datos
				= Registro</p>
				<p style="text-indent: 80; line-height: 100%" >Nombre
				: cadena de caracteres</p>
				<p style="text-indent: 80; line-height: 100%" >Edad
				: entero</p>

				<p style="text-indent: 80; line-height: 100%" >Telefono
				: entero</p>
				<p style="text-indent: 80; line-height: 100%" >Direccion
				: cadena de caracteres</p>
				<p style="text-indent: 80; line-height: 100%" >Sexo
				: caracter</p>
				<p style="text-indent: 50; line-height: 100%" >Fin
				Datos</p>
				<p style="text-indent: 50; line-height: 100%" >Sig:
				Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin
				Empleado</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 20; line-height: 100%" >L
				: Lista</p>

				<p style="text-indent: 20; line-height: 100%" >Su
				representación gráfica sería la siguiente:</p>
				<p style="text-indent: 0; line-height: 100%" align="center"><b>Figura
				7.3.</b> Lista de Registros</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="contenido/capVII/7.3.gif" width="354" height="100"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capVII/elabpropia.gif" align="right" width="150" height="11"></p>
				<p style="text-indent: 20; line-height: 150%" ><span style="mso-bidi-font-size: 12.0pt">Al
				igual que con las listas de arreglos, con esta estructura de datos se realizan
				las mismas operaciones de una lista enlazada simple, por lo que se utilizan las
				rutinas vistas en el capítulo IV sobre listas enlazadas, sólo se modifica la
				forma de acceder al campo Datos del nodo, ya que se debe
				</span> utilizar el formato de acceso a un
				registro.</p>

				<p style="text-indent: 20; line-height: 150%" > A continuación se presenta un ejemplo de llenado de un nodo de este
				tipo de estructura:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 150%" >L^.CI
				= 13541556</p>
				<p style="text-indent: 20; line-height: 150%" >L^.Datos.Nombre
				= “Alfonzo Teresa”</p>
				<p style="text-indent: 20; line-height: 150%" >L^.Datos.Edad
				= 23</p>
				<p style="text-indent: 20; line-height: 150%" >L^.Datos.Telefono
				= 02952633470</p>

				<p style="text-indent: 20; line-height: 150%" >L^.Datos.Dirección
				= “Porlamar – Edo. Nva. Esparta”</p>

				<p style="text-indent: 20; line-height: 150%" >L^.Datos.Sexo
				= “F”</p>
				<p style="text-indent: 20; line-height: 150%" >L^.Sig
				= Nulo</p>				<p style="text-indent: 0; line-height: 150%" ><b>7.6
				Arreglos de Listas</b></p>
				<p style="text-indent: 20; line-height: 150%" >Son
				estructuras de datos estáticas que poseen en su <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Campo de información" v-bind:data-content="CampoDeInformación">campo de información</a> uno o
				más campos <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a> a ;estructuras de datos dinámicas (Listas).</p>

				<p style="text-indent: 20; line-height: 150%" >En la
				Figura 7.4. se representa un arreglo de las letras del abecedario con su lista
				de palabras por letras; si una letra no posee palabras almacenadas el <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a>
				a la lista del Arreglo, es nulo.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p align="center" style="line-height: 100%; text-indent: 0"><b>Figura
				7.4.</b> <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">arreglo de lista</a></p>

				<p align="center" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVII/7.4.gif" width="301" height="146"></p>

				<p align="right" style="line-height: 100%; text-indent: 0"><img border="0" src="contenido/capVII/elabpropia.gif" align="right" width="150" height="11"></p>

				<p  style="line-height: 100%; text-indent: 20"></p>

				<p  style="line-height: 150%; text-indent: 20"><span lang="ES-TRAD" style="mso-bidi-font-size: 12.0pt; mso-ansi-language: ES-TRAD"><!--[if gte vml 1]><v:shapetype
				 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
				 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
				 <v:stroke joinstyle="miter"/>
				 <v:formulas>
				  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
				  <v:f eqn="sum @0 1 0"/>
				  <v:f eqn="sum 0 0 @1"/>
				  <v:f eqn="prod @2 1 2"/>
				  <v:f eqn="prod @3 21600 pixelWidth"/>
				  <v:f eqn="prod @3 21600 pixelHeight"/>
				  <v:f eqn="sum @0 0 1"/>
				  <v:f eqn="prod @6 1 2"/>
				  <v:f eqn="prod @7 21600 pixelWidth"/>
				  <v:f eqn="sum @8 21600 0"/>
				  <v:f eqn="prod @7 21600 pixelHeight"/>
				  <v:f eqn="sum @10 21600 0"/>
				 </v:formulas>
				 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
				 <o:lock v:ext="edit" aspectratio="t"/>
				</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:238.5pt;
				 height:153.75pt'>
				 <v:imagedata src="../../../WINDOWS/TEMP/msoclip1/05/clip_image001.png"
				  o:title="7"/>
				</v:shape><![endif]--></span>Un
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">arreglo de listas</a> se puede declarar de la siguiente manera:</p>

				<p  style="line-height: 150%; text-indent: 20"><b>Declaración
				de un Arreglo de Lista:</b></p>
				<p  style="line-height: 150%; text-indent: 20">Suponga
				que se desea representar a través de un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">arreglo de listas</a> un diccionario de
				palabras, en donde cada posición del arreglo va a contener un registro de lista
				con los siguientes datos: Letra del Alfabeto, Cantidad de Palabras Almacenadas y
				un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> a una Lista. Esta lista almacenará la siguiente información:
				Palabra, Definición y un; <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a>; al; siguiente</p>
			</div>
			<div class="pagina" @click="navNext">
				<p  style="line-height: 150%; text-indent: 0"> nodo. Cada posición del arreglo quedaría de la siguiente manera:</p>

				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				7.5.</b> Diccionario</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><img border="0" src="contenido/capVII/7.5.gif" width="274" height="175"></p>

				<p style="text-indent: 0; line-height: 100%" align="right"><img border="0" src="contenido/capVII/elabpropia.gif" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >El
				arreglo se declararía de 27 posiciones incluyendo la letra Ñ, tal como se
				muestra a continuación:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 200%" ><b>Tipo</b></p>
				<p style="text-indent: 20; line-height: 100%" >Lista
				= ^Nodo</p>				<p style="text-indent: 20; line-height: 100%" >InfoArreglo</p>
				<p style="text-indent: 50; line-height: 100%" >Letra:
				Carácter</p>
				<p style="text-indent: 50; line-height: 100%" >Cantidad:
				Entero</p>
				<p style="text-indent: 50; line-height: 100%" >PL:
				Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin
				InfoArreglo</p>

				<p style="text-indent: 20; line-height: 100%" >Nodo</p>
				<p style="text-indent: 50; line-height: 100%" >Palabra:
				Cadena</p>
				<p style="text-indent: 50; line-height: 100%" >Definición:
				Cadena</p>
				<p style="text-indent: 50; line-height: 100%" >Sig:
				Lista</p>
				<p style="text-indent: 20; line-height: 100%" >Fin
				Nodo</p>

				<p style="text-indent: 20; line-height: 100%" >Arreglo_Palabras
				= Arreglo [27] de InfoArreglo</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" ><b>Variable</b></p>
				<p style="text-indent: 20; line-height: 100%" >Dicc:
				Arreglo_Palabras</p>				<p style="text-indent: 20; line-height: 100%" ><b>Operaciones
				de un Arreglo de Lista:</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				operaciones de un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">arreglo de lista</a> no difieren en gran manera con respecto a las
				operaciones de arreglos que se realizaron en el Capítulo II ya que la
				inserción de algún elemento implica el recorrido del arreglo para verificar la
				disponibilidad de espacio luego se insertar el elemento en la posición
				disponible, lo demás dependerá de la estructura del campo del arreglo. </p>

				<p style="text-indent: 20; line-height: 150%" >En este
				caso es un registro con un campo <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntador</a> a una lista enlazada,
				es decir, si se desea insertar un registro al arreglo Dicc que fue el que se
				declaró anteriormente, se realizan los siguientes pasos una vez verificado la
				disponibilidad de espacios:</p>

				<p style="text-indent: 0; line-height: 100%" align="center"><b>Variable
				Arreglo[posición]. Nombre del campo = Valor</b></p>			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%" >Dicc[1].Letra
				= “A”</p>
				<p style="text-indent: 20; line-height: 100%" >Dicc[1].Cantidad
				= 0</p>
				<p style="text-indent: 20; line-height: 100%" >Dicc[1].PL
				= Nulo</p>

				<p style="text-indent: 20; line-height: 150%" >Si
				por el contrario, se quiere añadir un nodo nuevo a la lista de palabras
				bastaría con ubicar la letra en; la; cual; se; quiere
				 insertar
				la palabra y accesar a la lista a través del puntero PL; si la Lista es nula se Crea el Nodo, se
				asignan los datos al nodo y se enlaza al puntero del arreglo Dicc, si no es nula
				se recorre a través de una variable auxiliar y se puede insertar por final,
				sino se puede insertar por inicio.</p>

				<p style="text-indent: 20; line-height: 150%" >
				 Para manipular la lista de palabras del
				arreglo que se declaró anteriormente se pueden utilizar todas las rutinas que
				se desarrollaron en el Capítulo IV de listas simples.</p>

				<p style="text-indent: 20; line-height: 150%" >Suponiendo
				que “Nodo” es una variable del Tipo lista, al aquel se le han asignado los
				siguientes datos:</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >Nodo^.Palabra
				= “Ala”</p>
				<p style="text-indent: 20; line-height: 100%" >Nodo^.Definición
				= “ aaa”</p>
				<p style="text-indent: 20; line-height: 100%" >Nodo^.Sig
				= Nulo</p>

				<p style="text-indent: 20; line-height: 100%" >Si
				se desea enlazar el Nodo nuevo a de Dicc entonces:</p>
				<p style="text-indent: 50; line-height: 100%" >Dicc[1].PL
				= Nodo</p>
				<p style="text-indent: 50; line-height: 100%" >Dicc[1].Cantidad
				= Dicc[1].Cantidad +1</p>

				<p style="text-indent: 20; line-height: 150%" >Si
				se quiere conocer cuál es la primera palabra con la letra “A”insertada en
				el <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">arreglo de lista</a> Dicc y cuántas palabras hay insertadas con esa letra:
				Suponiendo que Pal y Def son una variables del tipo Cadena y Can es un entero.
				La asignación quedaría de la siguiente manera:</p>

				<p style="text-indent: 20; line-height: 100%" >Pal
				= Dicc[1].PL^.Palabra</p>
				<p style="text-indent: 20; line-height: 100%" >Def
				= Dicc[1].Pl^.Def</p>

				<p style="text-indent: 20; line-height: 100%" >Cant
				= Dicc[1].Cantidad</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Los
				<a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">Arreglos de listas</a> son estructuras fáciles de implementar sólo que se le debe
				prestar mayor atención al campo que apunta a la lista para evitar la perdida de
				información durante la manipulación del mismo.</p>				<p style="text-indent: 0; line-height: 150%" ><b>7.7
				Listas de Arreglos</b></p>
				<p style="text-indent: 20; line-height: 150%" >Es
				una lista enlazada en la que uno o más de sus campos está conformado por un
				arreglo.</p>

				<p style="text-indent: 20; line-height: 150%" > Supóngase que un profesor desea llevar el control de sus estudiantes
				por su cédula de identidad, almacenando para cada uno las notas obtenidas en
				cada una de las evaluaciones.</p>				<p style="text-indent: 20; line-height: 150%" > Al resolver este problema aplicando listas de
				arreglos, queda de la siguiente manera:</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" ><b>Declaración
				de una lista de arreglos</b></p>
				<p style="text-indent: 20; line-height: 100%" ><b>Tipo</b></p>
				<p style="text-indent: 20; line-height: 100%" >Lista
				= ^Alumno</p>

				<p style="text-indent: 20; line-height: 100%" >Alumno</p>
				<p style="text-indent: 50; line-height: 100%" >CI:
				Entero</p>
				<p style="text-indent: 50; line-height: 100%" >Notas
				: Arreglo [1..5] de Enteros</p>
				<p style="text-indent: 50; line-height: 100%" >Sig:
				Lista</p>

				<p style="text-indent: 20; line-height: 100%" >Fin
				Alumno</p>

				<p style="text-indent: 20; line-height: 100%" ><b>Variables</b></p>
				<p style="text-indent: 20; line-height: 100%" >L
				: Lista</p>				<p style="text-indent: 20; line-height: 100%" >Su
				representación gráfica sería la siguiente:</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 0; line-height: 150%" align="center"><b>Figura
				7.6.</b> Lista de Arreglos</p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 0; margin-bottom: 00" align="center"><img border="0" src="contenido/capVII/7.6.gif" width="339" height="67"></p>

				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="right"><img border="0" src="contenido/capVII/elabpropia.gif" width="150" height="11"></p>

				<p style="text-indent: 20; line-height: 150%" >Con
				esta estructura de datos se pueden realizar las mismas operaciones de una lista
				enlazada simple, por lo que se utilizan las mismas rutinas vistas en el capítulo
				de listas enlazadas, sólo se modifica la forma de acceder al campo Notas del
				nodo,; ya que se debe utilizar el formato de acceso a
				un arreglo.</p>

				<p style="text-indent: 20; line-height: 150%" > A
				continuación se presenta un ejemplo de llenado de un nodo de este tipo de
				estructura:</p>

				<p style="text-indent: 20; line-height: 100%" >L^.CI
				= 12356897</p>
				<p style="text-indent: 20; line-height: 100%" >L^.Notas[1]
				= 1</p>
				<p style="text-indent: 20; line-height: 100%" >L^.Notas[2]
				= 5</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%" >L^.Notas[3]
				= 8</p>
				<p style="text-indent: 20; line-height: 100%" >L^.Notas[4]
				= 10</p>
				<p style="text-indent: 20; line-height: 100%" >L^.Notas[5]
				= 8</p>
				<p style="text-indent: 20; line-height: 100%" >L^.Sig
				= Nulo</p>

				<p style="text-indent: 0; line-height: 150%" ></p>

				<p style="text-indent: 0; line-height: 150%" ><b>Resumen</b></p>
				<p style="text-indent: 20; line-height: 150%" >Las
				estructuras de datos combinadas son aquellas que están conformadas por una o
				más estructuras de datos lineales, entre las cuales se pueden encontrar las
				siguientes:</p>

				<p style="text-indent: 20; line-height: 150%" >Arreglos
				de Registros es una estructura donde cada elemento del arreglo será un registro
				(todos los <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> del arreglo serán del mismo tipo de registro).</p>

				<p style="text-indent: 20; line-height: 150%" >Registros
				de Listas es un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Datos estructurados" v-bind:data-content="DatosEstructurados">dato estructurado</a> donde cada uno de sus <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Componentes" v-bind:data-content="Componentes">Componentes</a> se denomina
				campo y éstos pueden ser todos
				de diferentes tipos; entre ellos se debe encontrar uno o más <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a> a
				estructuras dinámicas.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 150%" >Listas
				de Registros es una lista enlazada en la que uno o más de sus campos está
				conformado por un registro.</p>

				<p style="text-indent: 20; line-height: 150%" >Arreglos
				de Listas son aquellas estructuras de datos estáticas que poseen en su campo de
				información uno o más <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Apuntador" v-bind:data-content="Apuntador">apuntadores</a> a una lista enlazada.</p>
				<p style="text-indent: 20; line-height: 150%" >Listas
				de Arreglos es una lista enlazada en la que uno o más de sus campos está
				conformado por un arreglo.</p>

				<p style="text-indent: 20; line-height: 150%" >No
				es posible definir todos los tipos de estructuras de datos que se puedan
				combinar, ya que dependerá del problema a resolver por parte del programador,
				es decir, él es el encargado de seleccionar y manipular los distintos tipos de
				estructuras de datos adaptándolas a su conveniencia.</p>
			</div>
			<div class="pagina" @click="navNext">
				<b>
				<p  style="text-indent: 0; line-height: 150%">Ejercicios
				Propuestos</p>
				</b>
				<ol style="font-family: Century Gothic; font-size: 12 pt">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de la estructura necesaria para guardar la
				    información de N empleados de una Empresa, almacenando para cada uno
				    Nombre, Cédula, Edad, Sexo, Sueldo y Teléfono.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="2">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de la estructura necesaria para guardar la
				    información de N Estudiantes de un curso, almacenando para cada uno Nombre,
				    Cédula y Calificaciones de los 5 exámenes realizados a lo largo del curso.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="3">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    un pseudocódigo para una empresa que desea llevar el registro de sus
				    empleados por su Cédula de Identidad, almacenando para cada uno datos
				    personales como Nombre, Teléfono y Dirección. Utilice Listas Enlazadas de
				    Registros para solucionar el problema.</li>
				</ol>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="4">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de una Lista enlazada de arreglos para
				    almacenar la información de N Profesores, almacenando para cada uno Nombre,
				    Cédula, y Sueldos mensuales del año pasado.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="5">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de un <a class="datos" role="button" data-toggle="popover" data-placement="top" data-trigger="hover" title="Arreglos de listas" v-bind:data-content="ArreglosDeListas">Arreglo de listas</a> enlazadas para
				    guardar los números del 0 al 9 con al menos 5 cantidades por número.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="6">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de un Registro de Listas enlazadas con
				    información cualquiera.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="7">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de una Lista enlazada de registros con
				    información cualquiera.</li>
				</ol>				<ol style="font-family: Century Gothic; font-size: 12 pt" start="8">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    la Declaración en pseudocódigo de una Lista enlazada de arreglos con
				    información cualquiera.</li>
				</ol>
			</div>
			<div class="pagina" @click="navNext">
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="9">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    un pseudocódigo para almacenar los datos de 100 personas que conforman el
				    personal del Departamento de Compras de una empresa. Se desea almacenar para
				    cada una de ellas Nombre, Cédula de identidad, Fecha de Ingreso y Sueldo
				    actual.</li>
				</ol>
				<blockquote>
				  <blockquote>
				    
				  </blockquote>
				</blockquote>
				<ol style="font-family: Century Gothic; font-size: 12 pt" start="10">
				  <li>
				    <p  style="text-indent: 0; line-height: 150%">Realice
				    un pseudocódigo que almacene para N estudiantes que cursan 4 materias su
				    Nombre, Cédula de Identidad, Nombre de materias que cursa y las
				    calificaciones obtenidas en los diversos exámenes de cada una de esas
				    asignaturas.</li>
				</ol>
				<p  style="text-indent: 20; line-height: 150%"><a data-toggle="modal" data-target="#Evaluaciones" v-on:click="showEvaluation( 7)">Autoevaluación</a></p>
			</div>
		</div><div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="imagenes/Mujer.gif" align="left" width="70" height="95"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="left"><img border="0" src="imagenes/tituloreferenciasB.gif" width="320" height="33"></p>
				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >ANUEL,
				P. y Quijada, M. (2001). LEPROD-II:<b> Libro Electrónico </b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> para la asignatura
				Procesamiento de Datos II de la </b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> carrera</b><b>
				de Licenciatura en Informática de la</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>
				 Universidadde Oriente,
				Núcleo de Nueva Esparta</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>(UDO-NE). </b>Trabajode Grado
				Modalidad  Investigación.</p>
				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >BARKER,
				P. (1992).<b> Electronic Books and Libraries of the future. </b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> Electronic Library. </b>Vol
				10, Nº 3, 139-149.</p>
				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >BARKER,
				P. (1996).<b> Electronic Books: A Review and Assessment</b>
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> of Current Trends.</b>
				Educational Technology Review.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >CAIRÓ,
				O. y Guardatí, S. (1993).<b> Estructuras de Datos.</b> </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >McGraw
				Hill, México.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >DALE,
				N. y Lilly, S. (1989)<b>. Pascal y Estructuras de Datos. </b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Segunda Edición.
				McGraw-Hill Interamericana.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >HEILEMAN,
				G. (1998). <b>Estructuras de Datos, Algoritmos</b>  </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" > <b> y Programación Orientada a
				Objetos.</b>  McGraw-Hill</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" > Interamericana.</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >JIMÉNEZ,
				D. (1999).<b> Libro Electrónico Simulación y Modelos</b> </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> para los estudiantes del
				IX semestre de la carrera</b> </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> Licenciatura en Informática de la Universidad de</b> </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> Oriente Núcleo Nueva Esparta.</b> Trabajo de Grado</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" > No publicado. Universidad de
				Oriente.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >JOYANES
				A., L. (1992). <b>Metodología de la Programación</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Diagramas
				de Flujo, Algoritmos y Programación</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Estructurada.
				McGraw Hill,México.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >JOYANES,
				L. y Zahonero, I. (1998).<b> Estructuras de Datos -</b> </p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b> Algoritmos, abstracción y
				objetos.</b> McGraw Hill,</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >México</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >KENDALL,
				K. y Kendall, J. (1991). <b>Análisis y Diseño de Sistemas</b>.</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Prentice Hall
				Hispanoamericana, México.</p>


				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Universidad
				Nacional Abierta (1991). <b>Técnicas de</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>Documentacióne
				Investigación II - Estudios</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>Generales
				II.</b> Caracas-Venezuela.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Universidad
				Nacional Abierta (1995). <b>Técnicas de</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>Documentacióne
				Investigación I - Estudios</b></p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ><b>Generales.</b>
				Caracas-Venezuela.</p>

				<p style="line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >WEISS,
				M. (1995). <b>Estructuras de Datos y Algoritmos.</b> Addison-</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" >Wesley
				Iberoamericana.</p>

			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="center"><img border="0" src="imagenes/Mujer.gif" align="left" width="70" height="95"></p>
				<p style="text-indent: 0; line-height: 100%; word-spacing: 0; margin: 0" align="left"><img border="0" src="imagenes/tituloreferenciasE.gif" width="320" height="33"></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >ALTAMIRANO,
				F. (s.f.). <b>Tutorial de Pascal</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Portal
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/pascal/</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >pascal.html</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Arreglos
				(s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/arreglos.htm</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Arreglos:
				Introducción (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/estru1/</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >11.htm</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Asignatura:
				Estructura de Datos y de la Información (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.ei.uvigo.es/~pcarrion/edi/principal.</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >html#1.Información y datos</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 13/07/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >BENÍTEZ,
				T y Arredondo, J. (s.f.)</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" > <b>Tutor de
				Estructura de Datos I</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://members.tripod.com/fcc98/tutores/ed1/</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >ed1.html#LIDOENCU3</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 13/07/2001</p>				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Búsqueda
				Binaria (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/binaria.htm</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Búsqueda
				Secuencial (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/secuenci.htm</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >CAIRÓ
				B., O. (1999). <b>Estructuras de Datos</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://allman.rhon.itam.mx/Osvaldo/info/data</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >structures.html</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/01</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" ></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >CAMACHO,
				V. (s.f.) <b>Tutorial de Estructuras de Datos</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Portal
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/estru1/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >index.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >CASTELLS,
				M. (s.f.).<b>Artículos.
				 </b>
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" ><b>Libros
				Electrónicos: un nuevo formato para una nueva
				era
				 </b>
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.emprendedoras.com/articles/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >article89.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 20/03/2001</p>
				<p style="text-indent: 20; line-height: 150%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Con
				clase (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://c.conclase.net/edd/</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 13/07/2001</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Conceptos
				Básicos (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.sscc.co.cl/informatica/conceptos.html</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/04/2001</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Cuenta
				atrás para el Libro Electrónico (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.acetraductores.org/10prensa20000507.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 19/03/2001</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Descripción
				de WWW (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.uniovi.es/Vicest/Otros/FAQs/www.</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >spanish.html</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/04/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>El
				libro y el texto electrónico. Cuaderno de materiales (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.filosofia.net/materiales/tuto/ebook.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 04/12/2001</p>
				<p style="line-height: 150%; text-indent: 20; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructura
				de datos: Introducción (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/estru1/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >introd.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				(s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://members.tripod.cl/e_poblete/edson.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				<p style="text-indent: 20; line-height: 150%; word-spacing: 0; margin: 0" >
				</p>
				<p style="text-indent: 20; line-height: 150%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				de Datos (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Documento
				WWW
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://usuarios.tripod.es/pazita00/
				 
				</p>
				<p style="line-height: 100%; tab-stops: 35.4pt; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>

				<p style="line-height: 150%; tab-stops: 35.4pt; text-indent: 20; word-spacing: 0; margin: 0" ></p>
				<p style="line-height: 150%; tab-stops: 35.4pt; text-indent: 20; word-spacing: 0; margin: 0" ></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				de Datos (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://allman.rhon.itam.mx/Osvaldo/info/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >datastructures.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				de Datos Dinámicas Lineales (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.unife.edu.pe/Algoritmos%20Garcia/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >Listas%20Enlazadas.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 13/07/2001</p>

				<p style="text-indent: 20; line-height: 200%; word-spacing: 0; margin-left: 0; margin-right: 0; margmargmargmargin-top: 7px; margin-bottom: 7px" ></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				de Datos. Unidad 1. Análisis de Algoritmos (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://sistemas.ing.ula.ve/sistemas/ed/analisisDe
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >Algoritmos.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 13/07/2001</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Estructuras
				Dinámicas (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web 
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >URL:
				http://www.fortunecity.es/virtual/webones/612/
				</p>
				<p style="text-indent: 55; line-height: 100%; word-spacing: 0; margin: 0" >estructuras_de_datos/tema1- 5.htm
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001
				 
				</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Hipertextos
				en Educación (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.horizonteweb.com/jie99/astiz.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 04/12/2001</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >
				 <b>Libro
				Electrónico (s.f.)</b>
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.uam.es/departamentos/medicina/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >anesnet/forconred/libroelectro.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Libros
				Electrónicos (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.hipertext.com.mx/productos/Libros.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Libros
				Electrónicos, Tutoriales (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.mendillo.ven.org/Software.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" ><b>Listas
				(s.f.)				</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://members.tripod.cl/e_poblete/hanamishi.html
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001
				 
				</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Circulares Simples (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/listcisi.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001
				</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Doblemente Enlazadas (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://decsai.ugr.es/~jfv/ed1/tedi/cdrom/docs/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >ldoble.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 28/05/2001</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Encadenadas: Listas Lineales (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/43.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Enlazadas: Introducción (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/41.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Enlazadas: Listas Circulares (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/45.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001</p>


				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Enlazadas: Listas Dobles (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/44.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001</p>
				<p style="text-indent: 20; line-height: 200%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Enlazadas: Listas Ortogonales (s.f.)</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/46.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Enlazadas: Operaciones en Listas Enlazadas (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/42.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 03/07/2001
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; word-spacing: 0; margin: 0" >				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Lineales Dobles (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/listlido.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 28/05/2001</p>


				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Listas
				Lineales Simples (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/listlisi.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>
				<p style="line-height: 150%; text-indent: 20; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Método
				Burbuja (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/burbuja.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>
				<p style="text-indent: 20; line-height: 200%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Método
				Burbuja Mejorado (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/burbuja2.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Método
				de Inserción Simple (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/insercio.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" ><b>Método
				de Shell (Shell Sort) (s.f.)
				 
				</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/shell.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>
				<p style="line-height: 150%; text-indent: 20; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Monografias.com
				</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Evolución de los
				Chips de Memoria Ram (s.f.)
				 
				</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.monografias.com/trabajos5/chips/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >chips.shtml</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/04/2001
				 
				</p>
				<p style="text-indent: 20; line-height: 150%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Monografias.com
				– Memoria (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.monografias.com/trabajos/memoria/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >memoria.shtml
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/04/2001
				 
				</p>				</p>
				<p style="text-indent: 20; line-height: 200%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Ordenamiento
				por Selección (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/seleccio.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001
				</p>


				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >Orozko
				(s.f.)<b> ,
				Libros Electrónicos</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://members.es.tripod.de/Orozko/libros.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Página
				Productos de Garbosoft (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.geocities.com/garbosoft_2000/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >products.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Pilas:
				Operaciones en Pilas (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >estru1/36.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha de acceso: 03/07/2001</p>
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>PrincipalIDEA
				(s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.itlp.edu.mx/publica/tutoriales/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >algoritmos/index.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>


				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Programación
				II (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/program2.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001</p>


				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Quick
				sort (s.f.)
				 </b>
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.bufoland.f2s.com/tc/quicsort.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/05/2001
				 
				</p>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >RAMOS
				A., J. (2000). <b>Libros Electrónicos: ¿Horror?</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >URL:
				http://www.jorgeramos.com/articulos/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >articulos41.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Fecha
				de acceso: 19/03/2001</p>				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" ><b>Revista
				e.comm digital (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >URL:
				http://www.ecommdigital.com/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >mostrarpag.cfm?ID=49</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Fecha
				de acceso: 20/03/2001</p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >ROMERO,
				S. (s.f.). <b>TAD Lineales</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Página
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >URL:
				http://www.escomposlinux.org/sromero/prog/
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >tadlin.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Fecha
				de acceso: 03/07/2001</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >SÁEZ,
				C. (2.000). <b>El Libro Electrónico</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >URL:
				http://lettere.unipv.it/scrineum/saez.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin-left: 0; margin-right: 0; margin-top: 5; margin-bottom: 5" >Fecha
				de acceso: 05/02/2001</p>

			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" >SAGREDO,
				F. y Espinosa, M. (s.f.).</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Del
				Libro al Libro Electrónico Digital</b>
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web 
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.ucm.es/info/multidoc/multidoc/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >revista/num9/cine/sagredo.htm#2.Los
				problemas
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/02/2001</p>

				<p style="text-indent: 10; line-height: 100%; word-spacing: 0; margin: 0" ><b>Sin
				título (s.f.)
				 </b>
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Página
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >URL:
				http://members.tripod.cl/e_poblete/bartolo.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" ><b>Sin
				título (s.f.)
				 
				</b></p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Página
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >URL:
				http://www.guiafe.com.ar/aedd/teoria.htm
				 
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001
				 
				</p>
				<p style="text-indent: 10; line-height: 200%; word-spacing: 0; margin: 0" >				</p>
				<p style="text-indent: 10; line-height: 100%; word-spacing: 0; margin: 0" ><b>Sin
				título (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >URL:
				http://www.elpais.es/suplementos/educa/
				</p>
				<p style="line-height: 100%; text-indent: 45; word-spacing: 0; margin: 0" >20010521/33angela.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Fecha
				de acceso: 04/12/2001</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" ><b>Sin
				título (s.f.)				</b></p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >Página
				Web				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" >URL:
				http://patrimoniohistorico.unex.es/documentos/15.htm				</p>
				<p style="line-height: 100%; text-indent: 10; word-spacing: 0; margin: 0" ><span style="mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA">Fecha
				de acceso: 18/01/2002</span>
			</div>
			<div class="pagina" @click="navNext">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>TAD
				Lista Ordenada.</b></p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Implementación
				Mediante Arrays (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.infor.uva.es/~cvaca/asigs/TAD1.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 28/05/2001
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Tipos
				Abstractos de Datos (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.geocities.com/SiliconValley/Park/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >4768/pilas.html</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001</p>
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" ><b>Tipos
				Abstractos de Datos (Listas) (s.f.)
				 
				</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.geocities.com/SiliconValley/Park/</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >4768/listas.html</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 05/04/2001
				 
				</p>
				 
				</p>
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Tutorcillo
				de HTML. Enlaces Hipertexto (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://www.zoom.es/usuario/angel/enlace.html
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 29/04/2001</p>

				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >VILET
				E., G. (2000). <b>El Futuro de los Libros Electrónicos</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://edipo.madm.uaslp.mx/El%20Futuro
				</p>
				<p style="line-height: 100%; text-indent: 55; word-spacing: 0; margin: 0" >%20de%20los%20Libros%20
				Electronicos.htm</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 19/03/2001</p>
			</div>
		</div>
		<div class="bb-item">
			<div class="pagina" @click="navPrev">
				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Virtual
				Libro</b></p>

				<p style="text-indent: 20; line-height: 100%; word-spacing: 0; margin: 0" ><b>Editorial
				Digital del Libro Electrónico en Castellano (s.f.)</b></p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Página
				Web</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >URL:
				http://virtualibro.com
				 
				</p>
				<p style="line-height: 100%; text-indent: 20; word-spacing: 0; margin: 0" >Fecha
				de acceso: 04/12/2001</p>
			</div>
			<div class="pagina" @click="navNext">			</div>
		</div>
		<!--div class="bb-item">
			<div class="pagina" @click="navPrev">			</div>
			<div class="pagina" @click="navNext">			</div>
		</div-->
		
	`
})